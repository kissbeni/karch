
/**
 * @file test_emulator_exec_col_standard_4.cc
 * @author Kiss Benedek
 * @brief Automated tests for emulator (standard column 4)
 */

#include "test_emulator.h"

TEST(ExecutionStandard4, TestStandardCol_4_BAD) {
    _EXEC_TEST_BEGIN;
    _EXEC_COL_BAD_INST(4)
}

// :: INC R?
TEST(ExecutionStandard4, TestStandardCol_4b0) {
    _EXEC_TEST_BEGIN;

    _FILL_REGS
    ctx.RegA() = 10;
    _EXEC_COL_BLOCKROW(4, 0, 0);
    EXPECT_EQ(ctx.RegA(), 11);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);
    ctx.RegA() = 0x7F;
    _EXEC_COL_BLOCKROW(4, 0, 0);
    EXPECT_EQ(ctx.RegA(), 0x80);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgOVERFLOW));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
    ctx.RegA() = 0xFF;
    _EXEC_COL_BLOCKROW(4, 0, 0);
    EXPECT_EQ(ctx.RegA(), 0);
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgOVERFLOW));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));

    ctx.Reset();
    _FILL_REGS
    ctx.RegB() = 10;
    _EXEC_COL_BLOCKROW(4, 0, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 11);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegC() = 10;
    _EXEC_COL_BLOCKROW(4, 0, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 11);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegD() = 10;
    _EXEC_COL_BLOCKROW(4, 0, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 11);
}

// :: DEC R?
TEST(ExecutionStandard4, TestStandardCol_4b1) {
    _EXEC_TEST_BEGIN;

    _FILL_REGS
    ctx.RegA() = 20;
    _EXEC_COL_BLOCKROW(4, 1, 0);
    EXPECT_EQ(ctx.RegA(), 19);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);
    ctx.RegA() = 0x80;
    _EXEC_COL_BLOCKROW(4, 1, 0);
    EXPECT_EQ(ctx.RegA(), 0x7F);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgOVERFLOW));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
    ctx.RegA() = 1;
    _EXEC_COL_BLOCKROW(4, 1, 0);
    EXPECT_EQ(ctx.RegA(), 0);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgOVERFLOW));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));

    ctx.Reset();
    _FILL_REGS
    ctx.RegB() = 20;
    _EXEC_COL_BLOCKROW(4, 1, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 19);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegC() = 20;
    _EXEC_COL_BLOCKROW(4, 1, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 19);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegD() = 20;
    _EXEC_COL_BLOCKROW(4, 1, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 19);
}

// :: INC [R?]
TEST(ExecutionStandard4, TestStandardCol_4b2) {
    _EXEC_TEST_BEGIN_MEM;

    ctx.WriteMemory<karch_word_t>(16, 10);
    ctx.RegA() = 16;
    _EXEC_COL_BLOCKROW(4, 2, 0);
    EXPECT_EQ(ctx.ReadOne(16), 11);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);
    ctx.WriteMemory<karch_word_t>(16, 0x7F);
    _EXEC_COL_BLOCKROW(4, 2, 0);
    EXPECT_EQ(ctx.ReadOne(16), 0x80);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgOVERFLOW));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
    ctx.WriteMemory<karch_word_t>(16, 0xFF);
    _EXEC_COL_BLOCKROW(4, 2, 0);
    EXPECT_EQ(ctx.ReadOne(16), 0);
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgOVERFLOW));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));

    ctx.CurrentMemoryBank() = 2;
    ctx.WriteMemory<karch_word_t>(ctx.GetBankOffset(16), 10);
    _EXEC_COL_BLOCKROW(4, 2, 0);
    EXPECT_EQ(ctx.ReadOne(ctx.GetBankOffset(16)), 11);

    ctx.Reset();
    ctx.WriteMemory<karch_word_t>(16, 10);
    ctx.RegB() = 16;
    _EXEC_COL_BLOCKROW(4, 2, 1);
    EXPECT_EQ(ctx.ReadOne(16), 11);

    ctx.Reset();
    ctx.WriteMemory<karch_word_t>(16, 10);
    ctx.RegC() = 16;
    _EXEC_COL_BLOCKROW(4, 2, 2);
    EXPECT_EQ(ctx.ReadOne(16), 11);

    ctx.Reset();
    ctx.WriteMemory<karch_word_t>(16, 10);
    ctx.RegD() = 16;
    _EXEC_COL_BLOCKROW(4, 2, 3);
    EXPECT_EQ(ctx.ReadOne(16), 11);
}

// :: DEC [R?]
TEST(ExecutionStandard4, TestStandardCol_4b3) {
    _EXEC_TEST_BEGIN_MEM;

    ctx.WriteMemory<karch_word_t>(16, 20);
    ctx.RegA() = 16;
    _EXEC_COL_BLOCKROW(4, 3, 0);
    EXPECT_EQ(ctx.ReadOne(16), 19);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);
    ctx.WriteMemory<karch_word_t>(16, 0x80);
    _EXEC_COL_BLOCKROW(4, 3, 0);
    EXPECT_EQ(ctx.ReadOne(16), 0x7F);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgOVERFLOW));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
    ctx.WriteMemory<karch_word_t>(16, 1);
    _EXEC_COL_BLOCKROW(4, 3, 0);
    EXPECT_EQ(ctx.ReadOne(16), 0);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgOVERFLOW));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));

    ctx.CurrentMemoryBank() = 2;
    ctx.WriteMemory<karch_word_t>(ctx.GetBankOffset(16), 20);
    _EXEC_COL_BLOCKROW(4, 3, 0);
    EXPECT_EQ(ctx.ReadOne(ctx.GetBankOffset(16)), 19);

    ctx.Reset();
    ctx.WriteMemory<karch_word_t>(16, 20);
    ctx.RegB() = 16;
    _EXEC_COL_BLOCKROW(4, 3, 1);
    EXPECT_EQ(ctx.ReadOne(16), 19);

    ctx.Reset();
    ctx.WriteMemory<karch_word_t>(16, 20);
    ctx.RegC() = 16;
    _EXEC_COL_BLOCKROW(4, 3, 2);
    EXPECT_EQ(ctx.ReadOne(16), 19);

    ctx.Reset();
    ctx.WriteMemory<karch_word_t>(16, 20);
    ctx.RegD() = 16;
    _EXEC_COL_BLOCKROW(4, 3, 3);
    EXPECT_EQ(ctx.ReadOne(16), 19);
}
