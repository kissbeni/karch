
/**
 * @file test_emulator_exec_col_extended_6.cc
 * @author Kiss Benedek
 * @brief Automated tests for emulator (extended column 6)
 */

#include "test_emulator.h"

TEST(ExecutionExtended6, TestExtendedCol_6_BAD) {
    _EXEC_TEST_BEGIN;
    _EXEC_COL_BAD_INST(ex6)
}

// :: SUB RA, R?
TEST(ExecutionExtended6, TestExtendedCol_6b0) {
    _EXEC_TEST_BEGIN;

    _FILL_REGS
    ctx.RegA() = 20;
    _EXEC_COL_BLOCKROW(ex6, 0, 0);
    EXPECT_EQ(ctx.RegA(), 0);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgZERO);

    ctx.Reset();
    _FILL_REGS
    ctx.RegB() = 8;
    ctx.RegA() = 20;
    _EXEC_COL_BLOCKROW(ex6, 0, 1);
    EXPECT_EQ(ctx.RegA(), 12);
    EXPECT_EQ(ctx.RegB(), 8);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);
    ctx.RegA() = 0x80;
    _EXEC_COL_BLOCKROW(ex6, 0, 1);
    EXPECT_EQ(ctx.RegA(), 0x78);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgOVERFLOW));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));

    ctx.Reset();
    _FILL_REGS
    ctx.RegC() = 8;
    ctx.RegA() = 20;
    _EXEC_COL_BLOCKROW(ex6, 0, 2);
    EXPECT_EQ(ctx.RegA(), 12);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 8);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegD() = 8;
    ctx.RegA() = 20;
    _EXEC_COL_BLOCKROW(ex6, 0, 3);
    EXPECT_EQ(ctx.RegA(), 12);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 8);
}

// :: SUB RB, R?
TEST(ExecutionExtended6, TestExtendedCol_6b1) {
    _EXEC_TEST_BEGIN;

    _FILL_REGS
    ctx.RegA() = 8;
    ctx.RegB() = 20;
    _EXEC_COL_BLOCKROW(ex6, 1, 0);
    EXPECT_EQ(ctx.RegA(), 8);
    EXPECT_EQ(ctx.RegB(), 12);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegB() = 20;
    _EXEC_COL_BLOCKROW(ex6, 1, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 0);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegC() = 8;
    ctx.RegB() = 20;
    _EXEC_COL_BLOCKROW(ex6, 1, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 12);
    EXPECT_EQ(ctx.RegC(), 8);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegD() = 8;
    ctx.RegB() = 20;
    _EXEC_COL_BLOCKROW(ex6, 1, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 12);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 8);
}

// :: SUB RC, R?
TEST(ExecutionExtended6, TestExtendedCol_6b2) {
    _EXEC_TEST_BEGIN;

    _FILL_REGS
    ctx.RegA() = 8;
    ctx.RegC() = 20;
    _EXEC_COL_BLOCKROW(ex6, 2, 0);
    EXPECT_EQ(ctx.RegA(), 8);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 12);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegB() = 8;
    ctx.RegC() = 20;
    _EXEC_COL_BLOCKROW(ex6, 2, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 8);
    EXPECT_EQ(ctx.RegC(), 12);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegC() = 20;
    _EXEC_COL_BLOCKROW(ex6, 2, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 0);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegD() = 8;
    ctx.RegC() = 20;
    _EXEC_COL_BLOCKROW(ex6, 2, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 12);
    EXPECT_EQ(ctx.RegD(), 8);
}

// :: SUB RD, R?
TEST(ExecutionExtended6, TestExtendedCol_6b3) {
    _EXEC_TEST_BEGIN;

    _FILL_REGS
    ctx.RegA() = 8;
    ctx.RegD() = 20;
    _EXEC_COL_BLOCKROW(ex6, 3, 0);
    EXPECT_EQ(ctx.RegA(), 8);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 12);

    ctx.Reset();
    _FILL_REGS
    ctx.RegB() = 8;
    ctx.RegD() = 20;
    _EXEC_COL_BLOCKROW(ex6, 3, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 8);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 12);

    ctx.Reset();
    _FILL_REGS
    ctx.RegC() = 8;
    ctx.RegD() = 20;
    _EXEC_COL_BLOCKROW(ex6, 3, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 8);
    EXPECT_EQ(ctx.RegD(), 12);

    ctx.Reset();
    _FILL_REGS
    ctx.RegD() = 20;
    _EXEC_COL_BLOCKROW(ex6, 3, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 0);
}
