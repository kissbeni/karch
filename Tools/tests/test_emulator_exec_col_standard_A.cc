
/**
 * @file test_emulator_exec_col_standard_A.cc
 * @author Kiss Benedek
 * @brief Automated tests for emulator (standard column A)
 */

#include "test_emulator.h"

TEST(ExecutionStandardA, TestStandardCol_A_BAD) {
    _EXEC_TEST_BEGIN;
    _EXEC_COL_BAD_INST(A)
}

// :: OR RA, R?
TEST(ExecutionStandardA, TestStandardCol_Ab0) {
    _EXEC_TEST_BEGIN;

    _FILL_REGS
    ctx.RegA() = 0x2C;
    _EXEC_COL_BLOCKROW(A, 0, 0);
    EXPECT_EQ(ctx.RegA(), 0x2C);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.RegA() = 0x2C;
    ctx.RegB() = 0x33;
    ctx.SetFlag(ktools::emulator::MachineFlags::fgALL);
    _EXEC_COL_BLOCKROW(A, 0, 1);
    EXPECT_EQ(ctx.RegA(), 0x3F);
    EXPECT_EQ(ctx.RegB(), 0x33);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
    ctx.SetFlag(ktools::emulator::MachineFlags::fgZERO);
    ctx.SetFlag(ktools::emulator::MachineFlags::fgSIGNED);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgALL));
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgALL));
    ctx.SetFlag(ktools::emulator::MachineFlags::fgOVERFLOW);
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgALL));

    _FILL_REGS
    ctx.RegA() = 0x2C;
    ctx.RegC() = 0x33;
    _EXEC_COL_BLOCKROW(A, 0, 2);
    EXPECT_EQ(ctx.RegA(), 0x3F);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 0x33);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.RegA() = 0x2C;
    ctx.RegD() = 0x33;
    _EXEC_COL_BLOCKROW(A, 0, 3);
    EXPECT_EQ(ctx.RegA(), 0x3F);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 0x33);

    ctx.RegA() = 0xAC;
    ctx.RegB() = 0x33;
    _EXEC_COL_BLOCKROW(A, 0, 1);
    EXPECT_EQ(ctx.RegA(), 0xBF);
    EXPECT_EQ(ctx.RegB(), 0x33);
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));

    ctx.RegA() = 0;
    _EXEC_COL_BLOCKROW(A, 0, 0);
    EXPECT_EQ(ctx.RegA(), 0);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
}

// :: OR RB, R?
TEST(ExecutionStandardA, TestStandardCol_Ab1) {
    _EXEC_TEST_BEGIN;

    _FILL_REGS
    ctx.RegB() = 0x2C;
    ctx.RegA() = 0x33;
    _EXEC_COL_BLOCKROW(A, 1, 0);
    EXPECT_EQ(ctx.RegA(), 0x33);
    EXPECT_EQ(ctx.RegB(), 0x3F);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.RegB() = 0x33;
    _EXEC_COL_BLOCKROW(A, 1, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 0x33);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.RegB() = 0x2C;
    ctx.RegC() = 0x33;
    _EXEC_COL_BLOCKROW(A, 1, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 0x3F);
    EXPECT_EQ(ctx.RegC(), 0x33);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.RegB() = 0x2C;
    ctx.RegD() = 0x33;
    _EXEC_COL_BLOCKROW(A, 1, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 0x3F);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 0x33);
}

// :: OR RC, R?
TEST(ExecutionStandardA, TestStandardCol_Ab2) {
    _EXEC_TEST_BEGIN;

    _FILL_REGS
    ctx.RegC() = 0x2C;
    ctx.RegA() = 0x33;
    _EXEC_COL_BLOCKROW(A, 2, 0);
    EXPECT_EQ(ctx.RegA(), 0x33);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 0x3F);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.RegC() = 0x2C;
    ctx.RegB() = 0x33;
    _EXEC_COL_BLOCKROW(A, 2, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 0x33);
    EXPECT_EQ(ctx.RegC(), 0x3F);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.RegC() = 0x33;
    _EXEC_COL_BLOCKROW(A, 2, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 0x33);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.RegC() = 0x2C;
    ctx.RegD() = 0x33;
    _EXEC_COL_BLOCKROW(A, 2, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 0x3F);
    EXPECT_EQ(ctx.RegD(), 0x33);
}

// :: OR RD, R?
TEST(ExecutionStandardA, TestStandardCol_Ab3) {
    _EXEC_TEST_BEGIN;

    _FILL_REGS
    ctx.RegD() = 0x2C;
    ctx.RegA() = 0x33;
    _EXEC_COL_BLOCKROW(A, 3, 0);
    EXPECT_EQ(ctx.RegA(), 0x33);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 0x3F);

    _FILL_REGS
    ctx.RegD() = 0x2C;
    ctx.RegB() = 0x33;
    _EXEC_COL_BLOCKROW(A, 3, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 0x33);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 0x3F);

    _FILL_REGS
    ctx.RegD() = 0x2C;
    ctx.RegC() = 0x33;
    _EXEC_COL_BLOCKROW(A, 3, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 0x33);
    EXPECT_EQ(ctx.RegD(), 0x3F);

    _FILL_REGS
    ctx.RegD() = 0x33;
    _EXEC_COL_BLOCKROW(A, 3, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 0x33);
}
