
/**
 * @file test_emulator_exec_col_standard_2.cc
 * @author Kiss Benedek
 * @brief Automated tests for emulator (standard column 2)
 */

#include "test_emulator.h"

TEST(ExecutionStandard2, TestStandardCol_2_BAD) {
    _EXEC_TEST_BEGIN;
    _EXEC_COL_BAD_INST(2)
}

// :: MOV RA, R?
TEST(ExecutionStandard2, TestStandardCol_2b0) {
    _EXEC_TEST_BEGIN;

    // :: MOV RA, RA
    _FILL_REGS
    ctx.RegA() = 20;
    _EXEC_COL_BLOCKROW(2, 0, 0);
    EXPECT_EQ(ctx.RegA(), 20);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    // :: MOV RA, RB
    ctx.Reset();
    _FILL_REGS
    ctx.RegB() = 20;
    _EXEC_COL_BLOCKROW(2, 0, 1);
    EXPECT_EQ(ctx.RegA(), 20);
    EXPECT_EQ(ctx.RegB(), 20);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    // :: MOV RA, RC
    ctx.Reset();
    _FILL_REGS
    ctx.RegC() = 20;
    _EXEC_COL_BLOCKROW(2, 0, 2);
    EXPECT_EQ(ctx.RegA(), 20);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 20);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    // :: MOV RA, RD
    ctx.Reset();
    _FILL_REGS
    ctx.RegD() = 20;
    _EXEC_COL_BLOCKROW(2, 0, 3);
    EXPECT_EQ(ctx.RegA(), 20);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 20);
}

// :: MOV RB, R?
TEST(ExecutionStandard2, TestStandardCol_2b1) {
    _EXEC_TEST_BEGIN;

    // :: MOV RB, RA
    _FILL_REGS
    ctx.RegA() = 20;
    _EXEC_COL_BLOCKROW(2, 1, 0);
    EXPECT_EQ(ctx.RegA(), 20);
    EXPECT_EQ(ctx.RegB(), 20);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    // :: MOV RB, RB
    ctx.Reset();
    _FILL_REGS
    ctx.RegB() = 20;
    _EXEC_COL_BLOCKROW(2, 1, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 20);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    // :: MOV RB, RC
    ctx.Reset();
    _FILL_REGS
    ctx.RegC() = 20;
    _EXEC_COL_BLOCKROW(2, 1, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 20);
    EXPECT_EQ(ctx.RegC(), 20);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    // :: MOV RB, RD
    ctx.Reset();
    _FILL_REGS
    ctx.RegD() = 20;
    _EXEC_COL_BLOCKROW(2, 1, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 20);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 20);
}

// :: MOV RC, R?
TEST(ExecutionStandard2, TestStandardCol_2b2) {
    _EXEC_TEST_BEGIN;

    // :: MOV RC, RA
    _FILL_REGS
    ctx.RegA() = 20;
    _EXEC_COL_BLOCKROW(2, 2, 0);
    EXPECT_EQ(ctx.RegA(), 20);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 20);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    // :: MOV RC, RB
    ctx.Reset();
    _FILL_REGS
    ctx.RegB() = 20;
    _EXEC_COL_BLOCKROW(2, 2, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 20);
    EXPECT_EQ(ctx.RegC(), 20);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    // :: MOV RC, RC
    ctx.Reset();
    _FILL_REGS
    ctx.RegC() = 20;
    _EXEC_COL_BLOCKROW(2, 2, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 20);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    // :: MOV RC, RD
    ctx.Reset();
    _FILL_REGS
    ctx.RegD() = 20;
    _EXEC_COL_BLOCKROW(2, 2, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 20);
    EXPECT_EQ(ctx.RegD(), 20);
}

// :: MOV RD, R?
TEST(ExecutionStandard2, TestStandardCol_2b3) {
    _EXEC_TEST_BEGIN;

    // :: MOV RD, RA
    _FILL_REGS
    ctx.RegA() = 20;
    _EXEC_COL_BLOCKROW(2, 3, 0);
    EXPECT_EQ(ctx.RegA(), 20);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 20);

    // :: MOV RD, RB
    ctx.Reset();
    _FILL_REGS
    ctx.RegB() = 20;
    _EXEC_COL_BLOCKROW(2, 3, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 20);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 20);

    // :: MOV RD, RC
    ctx.Reset();
    _FILL_REGS
    ctx.RegC() = 20;
    _EXEC_COL_BLOCKROW(2, 3, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 20);
    EXPECT_EQ(ctx.RegD(), 20);

    // :: MOV RD, RD
    ctx.Reset();
    _FILL_REGS
    ctx.RegD() = 20;
    _EXEC_COL_BLOCKROW(2, 3, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 20);
}
