
/**
 * @file test_emulator_TextGraphics_Wrapper.h
 * @author Kiss Benedek
 * @brief Helper class for testing VGACharacterDevice
 */

#ifndef EMULATOR_TEST_TEXT_GRAPHICS_WRAPPER_H
#define EMULATOR_TEST_TEXT_GRAPHICS_WRAPPER_H

#include <emulator/devices/VGACharacterDevice.hpp>

class TextGraphics_Wrapper : virtual public ktools::emulator::gui::TextGraphics
{
    public:
        TextGraphics_Wrapper() : ktools::emulator::gui::TextGraphics("/dev/null", 640, 480) {
            _reset();
        }

        void Clear() {
            is_cleared = true;
            last_call = CALL_CLEAR;
        }

        void DrawCharacter(const int16_t x, const int16_t y, const uint8_t charCode) {
            last_x = x;
            last_y = y;
            last_char = charCode;
            last_call = CALL_DRAW;
        }

        bool CanFitCharacter(const int16_t x, const int16_t y) {
            last_x = x;
            last_y = y;
            last_call = CALL_CHECK;
            return can_fit;
        }

        void Scroll() {
            scroll = true;
            last_call = CALL_SCROLL;
        }

        void _reset() {
            is_cleared = false;
            can_fit = true;
            scroll = false;
            last_x = -1;
            last_y = -1;
            last_char = 0xff;
            last_call = CALL_RESET;
        }

        bool is_cleared, can_fit, scroll;
        int16_t last_x, last_y;
        uint8_t last_char;
        uint8_t last_call;

        const uint8_t CALL_RESET  = 0;
        const uint8_t CALL_CLEAR  = 1;
        const uint8_t CALL_DRAW   = 2;
        const uint8_t CALL_CHECK  = 3;
        const uint8_t CALL_SCROLL = 4;
};

#endif
