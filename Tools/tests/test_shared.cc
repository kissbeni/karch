
/**
 * @file test_shared.cc
 * @author Kiss Benedek
 * @brief Automatic tests for shared functions
 */

#include <shared/fnv1a.hpp>
#include <shared/stringutil.hpp>
#include <shared/miscfunc.hpp>
#include <shared/types.h>

#include <string>

#include <gtest/gtest.h>

// ---------------------------------------------------------------------------------- //
// ---- FNV1A Tests ----------------------------------------------------------------- //
// ---------------------------------------------------------------------------------- //

TEST(fnv1a, TestImpl32_1) {
    EXPECT_EQ(fnv1a32_const("banana"), hash_32_fnv1a("banana", 6));
    EXPECT_FALSE(fnv1a32_const("banana") == hash_32_fnv1a("BANANA", 6));
    EXPECT_FALSE(fnv1a32_const("banana") == hash_32_fnv1a("banana", 5));
}

TEST(fnv1a, TestImpl32_2) {
    EXPECT_EQ(fnv1a32_const("banana"), hash_32_fnv1a("banana"));
    EXPECT_FALSE(fnv1a32_const("banana") == hash_32_fnv1a("BANANA"));
}

TEST(fnv1a, TestImpl64_1) {
    EXPECT_EQ(fnv1a64_const("banana"), hash_64_fnv1a("banana", 6));
    EXPECT_FALSE(fnv1a64_const("banana") == hash_64_fnv1a("BANANA", 6));
    EXPECT_FALSE(fnv1a64_const("banana") == hash_64_fnv1a("banana", 5));
}

TEST(fnv1a, TestImpl64_2) {
    EXPECT_EQ(fnv1a64_const("banana"), hash_64_fnv1a("banana"));
    EXPECT_FALSE(fnv1a64_const("banana") == hash_64_fnv1a("BANANA"));
}

TEST(fnv1a, TestImpl3) {
    EXPECT_FALSE(hash_64_fnv1a("banana") == hash_32_fnv1a("banana"));
}

// ---------------------------------------------------------------------------------- //
// ---- String util tests ----------------------------------------------------------- //
// ---------------------------------------------------------------------------------- //

TEST(stringutil, ltrim) {
    std::string test = "   \thello world! \t  ";
    ksu::ltrim(test);
    EXPECT_STREQ(test.c_str(), "hello world! \t  ");

    test = "  \t \t   \n \t";
    ksu::ltrim(test);
    EXPECT_STREQ(test.c_str(), "");
}

TEST(stringutil, rtrim) {
    std::string test = "   \thello world! \t  ";
    ksu::rtrim(test);
    EXPECT_STREQ(test.c_str(), "   \thello world!");

    test = "  \t \t   \n \t";
    ksu::rtrim(test);
    EXPECT_STREQ(test.c_str(), "");
}

TEST(stringutil, trim) {
    std::string test = "   \thello world! \t  ";
    ksu::trim(test);
    EXPECT_STREQ(test.c_str(), "hello world!");

    test = "  \t \t   \n \t";
    ksu::trim(test);
    EXPECT_STREQ(test.c_str(), "");
}

TEST(stringutil, begins_with) {
    std::string test = "hello world";
    EXPECT_TRUE(ksu::begins_with(test, std::string("hello")));
    EXPECT_TRUE(ksu::begins_with(test, std::string("he")));
    EXPECT_TRUE(ksu::begins_with(test, std::string("")));
    EXPECT_FALSE(ksu::begins_with(test, std::string("world")));
    EXPECT_FALSE(ksu::begins_with(test, std::string("ello")));
}

TEST(stringutil, tolower) {
    EXPECT_STREQ(ksu::tolower("HeLlO wOrLd").c_str(), "hello world");
    EXPECT_STREQ(ksu::tolower("hello+\"/!%=").c_str(), "hello+\"/!%=");
}

TEST(stringutil, toupper) {
    EXPECT_STREQ(ksu::toupper("HeLlO wOrLd").c_str(), "HELLO WORLD");
    EXPECT_STREQ(ksu::toupper("HELLO+\"/!%=").c_str(), "HELLO+\"/!%=");
}

// ---------------------------------------------------------------------------------- //
// ---- Miscfunc tests -------------------------------------------------------------- //
// ---------------------------------------------------------------------------------- //

TEST(miscfunc, rol8) {
    uint8_t a = 0b11110000;
    EXPECT_EQ(rol8(a, 4), 0b00001111);
    EXPECT_EQ(rol8(a, 0), a);
    EXPECT_EQ(rol8(a, 2), 0b11000011);
}

TEST(miscfunc, ror8) {
    uint8_t a = 0b11110000;
    EXPECT_EQ(ror8(a, 4), 0b00001111);
    EXPECT_EQ(ror8(a, 0), a);
    EXPECT_EQ(ror8(a, 2), 0b00111100);
}

int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
