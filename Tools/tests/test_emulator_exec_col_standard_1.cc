
/**
 * @file test_emulator_exec_col_standard_1.cc
 * @author Kiss Benedek
 * @brief Automated tests for emulator (standard column 1)
 */

#include "test_emulator.h"

TEST(ExecutionStandard1, TestStandardCol_1_BAD) {
    _EXEC_TEST_BEGIN;
    _EXEC_COL_BAD_INST(1)
}

// :: MOV R?, X
TEST(ExecutionStandard1, TestStandardCol_1b0) {
    _EXEC_TEST_BEGIN;

    // :: MOV RA, 20
    inst.arg = 20;
    _EXEC_COL_BLOCKROW(1, 0, 0);
    EXPECT_EQ(ctx.RegA(), 20);
    EXPECT_EQ(ctx.RegB(), 0);
    EXPECT_EQ(ctx.RegC(), 0);
    EXPECT_EQ(ctx.RegD(), 0);

    // :: MOV RB, 20
    ctx.Reset();
    inst.arg = 20;
    _EXEC_COL_BLOCKROW(1, 0, 1);
    EXPECT_EQ(ctx.RegA(), 0);
    EXPECT_EQ(ctx.RegB(), 20);
    EXPECT_EQ(ctx.RegC(), 0);
    EXPECT_EQ(ctx.RegD(), 0);

    // :: MOV RC, 20
    ctx.Reset();
    inst.arg = 20;
    _EXEC_COL_BLOCKROW(1, 0, 2);
    EXPECT_EQ(ctx.RegA(), 0);
    EXPECT_EQ(ctx.RegB(), 0);
    EXPECT_EQ(ctx.RegC(), 20);
    EXPECT_EQ(ctx.RegD(), 0);

    // :: MOV RD, 20
    ctx.Reset();
    inst.arg = 20;
    _EXEC_COL_BLOCKROW(1, 0, 3);
    EXPECT_EQ(ctx.RegA(), 0);
    EXPECT_EQ(ctx.RegB(), 0);
    EXPECT_EQ(ctx.RegC(), 0);
    EXPECT_EQ(ctx.RegD(), 20);
}

// :: MOV R?, [X]
TEST(ExecutionStandard1, TestStandardCol_1b1) {
    _EXEC_TEST_BEGIN;
    ctx.WriteMemory(16, 20);
    inst.arg = 16;

    // :: MOV RA, [16]
    _EXEC_COL_BLOCKROW(1, 1, 0);
    EXPECT_EQ(ctx.RegA(), 20);
    EXPECT_EQ(ctx.RegB(), 0);
    EXPECT_EQ(ctx.RegC(), 0);
    EXPECT_EQ(ctx.RegD(), 0);

    // :: MOV RB, [16]
    ctx.Reset();
    _EXEC_COL_BLOCKROW(1, 1, 1);
    EXPECT_EQ(ctx.RegA(), 0);
    EXPECT_EQ(ctx.RegB(), 20);
    EXPECT_EQ(ctx.RegC(), 0);
    EXPECT_EQ(ctx.RegD(), 0);

    // :: MOV RC, [16]
    ctx.Reset();
    _EXEC_COL_BLOCKROW(1, 1, 2);
    EXPECT_EQ(ctx.RegA(), 0);
    EXPECT_EQ(ctx.RegB(), 0);
    EXPECT_EQ(ctx.RegC(), 20);
    EXPECT_EQ(ctx.RegD(), 0);

    // :: MOV RD, [16]
    ctx.Reset();
    _EXEC_COL_BLOCKROW(1, 1, 3);
    EXPECT_EQ(ctx.RegA(), 0);
    EXPECT_EQ(ctx.RegB(), 0);
    EXPECT_EQ(ctx.RegC(), 0);
    EXPECT_EQ(ctx.RegD(), 20);
}

// :: MOV R?, [X]
TEST(ExecutionStandard1, TestStandardCol_1b2) {
    _EXEC_TEST_BEGIN;

    // :: MOV [RA], 20
    inst.arg = 20;
    ctx.RegA() = 16;
    _EXEC_COL_BLOCKROW(1, 2, 0);
    EXPECT_EQ(ctx.ReadOne(16), inst.arg);

    // :: MOV [RB], 25
    ctx.Reset();
    inst.arg = 25;
    ctx.RegB() = 16;
    _EXEC_COL_BLOCKROW(1, 2, 1);
    EXPECT_EQ(ctx.ReadOne(16), inst.arg);

    // :: MOV [RC], 30
    ctx.Reset();
    inst.arg = 30;
    ctx.RegC() = 16;
    _EXEC_COL_BLOCKROW(1, 2, 2);
    EXPECT_EQ(ctx.ReadOne(16), inst.arg);

    // :: MOV [RD], 35
    ctx.Reset();
    inst.arg = 35;
    ctx.RegD() = 16;
    _EXEC_COL_BLOCKROW(1, 2, 3);
    EXPECT_EQ(ctx.ReadOne(16), inst.arg);
}

// :: MOV [X], R?
TEST(ExecutionStandard1, TestStandardCol_1b3) {
    _EXEC_TEST_BEGIN;
    inst.arg = 16;

    // :: MOV [16], RA
    ctx.RegA() = gTestRegValues[0];
    _EXEC_COL_BLOCKROW(1, 3, 0);
    EXPECT_EQ(ctx.ReadOne(16), ctx.RegA());

    // :: MOV [16], RB
    ctx.Reset();
    ctx.RegB() = gTestRegValues[1];
    _EXEC_COL_BLOCKROW(1, 3, 1);
    EXPECT_EQ(ctx.ReadOne(16), ctx.RegB());

    // :: MOV [16], RC
    ctx.Reset();
    ctx.RegC() = gTestRegValues[2];
    _EXEC_COL_BLOCKROW(1, 3, 2);
    EXPECT_EQ(ctx.ReadOne(16), ctx.RegC());

    // :: MOV [16], RD
    ctx.Reset();
    ctx.RegD() = gTestRegValues[3];
    _EXEC_COL_BLOCKROW(1, 3, 3);
    EXPECT_EQ(ctx.ReadOne(16), ctx.RegD());
}
