
/**
 * @file test_emulator.h
 * @author Kiss Benedek
 * @brief Shared header for emulator testing
 */

#ifndef TEST_EMULATOR_H
#define TEST_EMULATOR_H

#include <gtest/gtest.h>

#include <emulator/MachineContext.hpp>
#include <emulator/VirtualMachine.h>
#include <emulator/execute/exec_col_base.hpp>
#include <string>
#include <sstream>

#define _NEW_CONTEXT(n, s) ktools::emulator::MachineContext n(s);
#define _NEW_VM(n) ktools::emulator::VirtualMachine n;
#define _EXEC_TEST_BEGIN \
    _NEW_CONTEXT(ctx, 64); \
    ktools::emulator::VirtualMachine::RawInstruction inst; \
    bool halt;
#define _EXEC_TEST_BEGIN_MEM \
    _NEW_CONTEXT(ctx, 2048); \
    ktools::emulator::VirtualMachine::RawInstruction inst; \
    bool halt;
#define _EXEC_COL(n) ktools::emulator::execute::_kemu_vm_handle_column_##n(inst, ctx, halt)
#define _EXEC_SETVARS_ROW(r) \
    inst.row = static_cast<uint8_t>(r); \
    inst.block = static_cast<uint8_t>((r) / 4); \
    inst.block_row = static_cast<uint8_t>((r) % 4);
#define _EXEC_COL_ROW(n,r) \
    _EXEC_SETVARS_ROW(r) \
    _EXEC_COL(n)
#define _EXEC_COL_BLOCKROW(n,b,r) \
    inst.row = (b)*4+(r); \
    inst.block = b; \
    inst.block_row = r; \
    _EXEC_COL(n)
#define _FILL_REGS \
    for (int i = 0; i <= ctx.MAX_REGISTER_INDEX; i++) \
        ctx.Reg(i) = gTestRegValues[i];
#define _EXEC_COL_BAD_INST(n) \
    _EXEC_SETVARS_ROW(300) \
    EXPECT_THROW(_EXEC_COL(n),const char*); \
    ctx.Reset();
#define TEST_BAD_INSTRUCTION(type,name,col) \
    TEST(Execution##type##name, Test##type##Col_##name##_BAD) { \
        _EXEC_TEST_BEGIN; \
        _EXEC_COL_BAD_INST(col) \
    }


#define EXEC_TEST(n) int test_emulator_exec_col_##n()
#define EXEC_TEST_N(n) test_emulator_exec_col_##n

static const karch_word_t gTestRegValues[] = { 220, 237, 51, 150 };

class DummyTestDevice : public ktools::emulator::devices::DeviceInterface<1>
{
    public:
        DummyTestDevice() : mValue(0) {}

        void OnWrite(const karch_word_t data) override {
            mValue = data;
        }

        const karch_word_t OnRead() override {
            return mValue;
        }

        karch_word_t mValue;
};

#endif

