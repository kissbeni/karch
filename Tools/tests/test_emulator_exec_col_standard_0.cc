
/**
 * @file test_emulator_exec_col_standard_0.cc
 * @author Kiss Benedek
 * @brief Automated tests for emulator (standard column 0)
 */

#include "test_emulator.h"

TEST(ExecutionStandard0, TestStandardCol_0_BAD) {
    _EXEC_TEST_BEGIN;
    _EXEC_COL_BAD_INST(0)
}

// :: RET
TEST(ExecutionStandard0, TestStandardCol_0r1) {
    _EXEC_TEST_BEGIN;

    ctx.Push((karch_addr_t)0xB00B);
    _EXEC_COL_ROW(0, 1);
    EXPECT_EQ(ctx.IP(), 0xB00B);
}

// :: PUSHA
TEST(ExecutionStandard0, TestStandardCol_0r2) {
    _EXEC_TEST_BEGIN;
    _FILL_REGS

    _EXEC_COL_ROW(0, 2);
    EXPECT_EQ(ctx.SP(), ctx.MAX_REGISTER_INDEX + 1);
    for (int i = ctx.MAX_REGISTER_INDEX; i >= 0; i--)
        EXPECT_EQ(ctx.Pop<karch_word_t>(), ctx.Reg(i));
    EXPECT_EQ(ctx.SP(), 0);
}

// :: POPA
TEST(ExecutionStandard0, TestStandardCol_0r3) {
    _EXEC_TEST_BEGIN;

    for (int i = 0; i <= ctx.MAX_REGISTER_INDEX; i++)
        ctx.Push(gTestRegValues[i]);

    _EXEC_COL_ROW(0, 3);
    for (int i = 0; i <= ctx.MAX_REGISTER_INDEX; i++)
        EXPECT_EQ(ctx.Reg(i), gTestRegValues[i]);
    EXPECT_EQ(ctx.SP(), 0);
}

// :: SETC
TEST(ExecutionStandard0, TestStandardCol_0r5) {
    _EXEC_TEST_BEGIN;
    _EXEC_COL_ROW(0, 5);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgCARRY);
}

// :: SETZ
TEST(ExecutionStandard0, TestStandardCol_0r6) {
    _EXEC_TEST_BEGIN;
    _EXEC_COL_ROW(0, 6);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgZERO);
}

// :: SETS
TEST(ExecutionStandard0, TestStandardCol_0r7) {
    _EXEC_TEST_BEGIN;
    _EXEC_COL_ROW(0, 7);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgSIGNED);
}

// :: SETO
TEST(ExecutionStandard0, TestStandardCol_0r8) {
    _EXEC_TEST_BEGIN;
    _EXEC_COL_ROW(0, 8);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgOVERFLOW);
}

// :: CFC
TEST(ExecutionStandard0, TestStandardCol_0r9) {
    _EXEC_TEST_BEGIN;
    ctx.SetFlag(ktools::emulator::MachineFlags::fgALL);
    _EXEC_COL_ROW(0, 9);
    EXPECT_NE(ctx.FlagSet(), ktools::emulator::MachineFlags::fgALL);
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgALL);
}

// :: CFZ
TEST(ExecutionStandard0, TestStandardCol_0rA) {
    _EXEC_TEST_BEGIN;
    ctx.SetFlag(ktools::emulator::MachineFlags::fgALL);
    _EXEC_COL_ROW(0, 0xA);
    EXPECT_NE(ctx.FlagSet(), ktools::emulator::MachineFlags::fgALL);
    ctx.SetFlag(ktools::emulator::MachineFlags::fgZERO);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgALL);
}

// :: CFS
TEST(ExecutionStandard0, TestStandardCol_0rB) {
    _EXEC_TEST_BEGIN;
    ctx.SetFlag(ktools::emulator::MachineFlags::fgALL);
    _EXEC_COL_ROW(0, 0xB);
    EXPECT_NE(ctx.FlagSet(), ktools::emulator::MachineFlags::fgALL);
    ctx.SetFlag(ktools::emulator::MachineFlags::fgSIGNED);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgALL);
}

// :: CFO
TEST(ExecutionStandard0, TestStandardCol_0rC) {
    _EXEC_TEST_BEGIN;
    ctx.SetFlag(ktools::emulator::MachineFlags::fgALL);
    _EXEC_COL_ROW(0, 0xC);
    EXPECT_NE(ctx.FlagSet(), ktools::emulator::MachineFlags::fgALL);
    ctx.SetFlag(ktools::emulator::MachineFlags::fgOVERFLOW);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgALL);
}

// :: RETI
TEST(ExecutionStandard0, TestStandardCol_0rE) {
    _EXEC_TEST_BEGIN;
    ctx.Push((karch_addr_t)0xDEAD);

    for (int i = 0; i <= ctx.MAX_REGISTER_INDEX; i++)
        ctx.Push(gTestRegValues[i]);

    ktools::emulator::MachineFlags test_flags = ktools::emulator::MachineFlags::fgZERO | ktools::emulator::MachineFlags::fgCARRY;
    ctx.Push(test_flags);
    ctx.SetFlag(ktools::emulator::MachineFlags::fgALL);
    _EXEC_COL_ROW(0, 0xE);
    EXPECT_EQ(ctx.SP(), 0);

    for (int i = 0; i <= ctx.MAX_REGISTER_INDEX; i++)
        EXPECT_EQ(ctx.Reg(i), gTestRegValues[i]);

    EXPECT_EQ(ctx.IP(), 0xDEAD);
    EXPECT_EQ(ctx.FlagSet(), test_flags);
}

// :: HLT
TEST(ExecutionStandard0, TestStandardCol_0rF) {
    _EXEC_TEST_BEGIN;
    _EXEC_COL_ROW(0, 0xF);
    EXPECT_TRUE(halt);
}
