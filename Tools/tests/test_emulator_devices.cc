
/**
 * @file test_emulator_devices.cc
 * @author Kiss Benedek
 * @brief Automated tests for devices
 */

#include "test_emulator.h"

#include <emulator/devices/DeviceInterface.h>
#include <emulator/devices/KeyboardDevice.h>
#include <emulator/devices/LEDDevice.hpp>
#include <emulator/devices/SevenSegmentDevice.hpp>

#include <tests/test_emulator_TextGraphics_Wrapper.h>

TEST(EmulatorDevices, TestAddDevice) {
    _NEW_VM(vm1);

    DummyTestDevice dev;

    EXPECT_NO_THROW(vm1.RegisterDevice(&dev));
    EXPECT_THROW(vm1.RegisterDevice(&dev), std::runtime_error);
}

TEST(EmulatorDevices, TestAccessDevice) {
    _NEW_VM(vm1);

    DummyTestDevice dev;

    std::istringstream ss(std::string("\x01\x23\x05\x01\x45\x01", 6));
    /*
     * 0000: 01 23        : MOV RA, 35
     * 0002: 05 01        : OUT 1, RA
     * 0004: 45 01        : IN RA, 1
     */
    vm1.RegisterDevice(&dev);
    vm1.Load(ss, vm1.GetCurrentContext()->IP());

    vm1.Step();
    EXPECT_EQ(vm1.GetCurrentContext()->RegA(), 35);
    EXPECT_EQ(dev.mValue, 0);

    vm1.Step();
    EXPECT_EQ(dev.mValue, 35);
    dev.mValue = 0x57;

    vm1.Step();
    EXPECT_EQ(vm1.GetCurrentContext()->RegA(), 0x57);
}

TEST(EmulatorDevices, KeyboardDevice) {
    ktools::emulator::gui::DisplayWindow::SetGraphicsEnabled(true);
    ktools::emulator::devices::KeyboardDevice kbd(4);

    //EXPECT_EQ(kbd.IOPort, IOPORT_PS2_DATA);

    EXPECT_TRUE(kbd.IsKeyQueueEmpty());
    EXPECT_EQ(kbd.OnRead(), 0);
    EXPECT_TRUE(kbd.IsKeyQueueEmpty());

    kbd.SendRawKeycode(69);
    EXPECT_FALSE(kbd.IsKeyQueueEmpty());
    EXPECT_EQ(kbd.OnRead(), 69);
    EXPECT_TRUE(kbd.IsKeyQueueEmpty());

    const uint8_t keycodes[] = { 1, 2, 3, 4, 5 };
    kbd.SendRawKeycode(keycodes, 5);
    EXPECT_EQ(kbd.OnRead(), 2);
    EXPECT_EQ(kbd.OnRead(), 3);
    EXPECT_EQ(kbd.OnRead(), 4);
    EXPECT_EQ(kbd.OnRead(), 5);
    EXPECT_TRUE(kbd.IsKeyQueueEmpty());

    kbd.SendASCII('2');
    EXPECT_EQ(kbd.OnRead(), 0x1E);
    EXPECT_TRUE(kbd.IsKeyQueueEmpty());

    EXPECT_THROW(kbd.OnWrite(0), std::runtime_error);
}

TEST(EmulatorDevices, LEDDevice) {
    ktools::emulator::gui::DisplayWindow::SetGraphicsEnabled(true);
    ktools::emulator::gui::LEDDisplay led(5, 5);
    ktools::emulator::devices::LEDDevice ld(&led);

    //EXPECT_EQ(ld.IOPort, IOPORT_LEDS);
    EXPECT_EQ(led.GetValue(), 0);

    ld.OnWrite(0x44);
    EXPECT_EQ(led.GetValue(), 0x04);
}

TEST(EmulatorDevices, SevenSegmentDevice) {
    ktools::emulator::gui::DisplayWindow::SetGraphicsEnabled(true);
    ktools::emulator::gui::SevenSegmentDisplay d1(5, 5);
    ktools::emulator::gui::SevenSegmentDisplay d2(5, 5);
    ktools::emulator::gui::SevenSegmentDisplay d3(5, 5);
    ktools::emulator::gui::SevenSegmentDisplay d4(5, 5);

    ktools::emulator::gui::SevenSegmentDisplay* dps[] = { &d1, &d2, &d3, &d4 };
    ktools::emulator::devices::SevenSegmentDevice sd(dps);

    ktools::emulator::devices::__SevenSegmentDevice::FLAGS* subdev_flags;
    ktools::emulator::devices::__SevenSegmentDevice::SEG_L* subdev_seg_l;
    ktools::emulator::devices::__SevenSegmentDevice::SEG_R* subdev_seg_r;

    EXPECT_NE(subdev_flags = dynamic_cast<ktools::emulator::devices::__SevenSegmentDevice::FLAGS*>(&sd), nullptr);
    EXPECT_NE(subdev_seg_l = dynamic_cast<ktools::emulator::devices::__SevenSegmentDevice::SEG_L*>(&sd), nullptr);
    EXPECT_NE(subdev_seg_r = dynamic_cast<ktools::emulator::devices::__SevenSegmentDevice::SEG_R*>(&sd), nullptr);

    EXPECT_EQ(subdev_flags->OnRead(), 0);

    subdev_flags->OnWrite(0x36);
    EXPECT_EQ(subdev_flags->OnRead(), 0x36);

    EXPECT_THROW(subdev_seg_l->OnRead(), std::runtime_error);
    EXPECT_THROW(subdev_seg_r->OnRead(), std::runtime_error);
}

TEST(EmulatorDevices, VGACharacterDevice) {
    ktools::emulator::gui::DisplayWindow::SetGraphicsEnabled(true);
    EXPECT_THROW(ktools::emulator::gui::TextGraphics f("", 640, 480), std::runtime_error);
    TextGraphics_Wrapper w;

    ktools::emulator::devices::VGACharacterDevice dev(&w);
    EXPECT_TRUE(w.is_cleared);
    EXPECT_EQ(w.last_call, w.CALL_CLEAR);
    w._reset();

    dev.OnWrite(0);
    EXPECT_FALSE(w.is_cleared);
    EXPECT_FALSE(w.scroll);
    EXPECT_EQ(w.last_call, w.CALL_DRAW);
    EXPECT_EQ(w.last_char, 219);
}

