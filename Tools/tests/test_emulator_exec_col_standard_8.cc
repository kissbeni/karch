
/**
 * @file test_emulator_exec_col_standard_8.cc
 * @author Kiss Benedek
 * @brief Automated tests for emulator (standard column 8)
 */

#include "test_emulator.h"

TEST(ExecutionStandard8, TestStandardCol_8_BAD) {
    _EXEC_TEST_BEGIN;
    _EXEC_COL_BAD_INST(8)
}

// :: AND RA, R?
TEST(ExecutionStandard8, TestStandardCol_8b0) {
    _EXEC_TEST_BEGIN;

    _FILL_REGS
    ctx.RegA() = 0xFA;
    _EXEC_COL_BLOCKROW(8, 0, 0);
    EXPECT_EQ(ctx.RegA(), 0xFA);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.RegB() = 0x33;
    ctx.RegA() = 0xFA;
    ctx.SetFlag(ktools::emulator::MachineFlags::fgALL);
    _EXEC_COL_BLOCKROW(8, 0, 1);
    EXPECT_EQ(ctx.RegA(), 0x32);
    EXPECT_EQ(ctx.RegB(), 0x33);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
    ctx.SetFlag(ktools::emulator::MachineFlags::fgZERO);
    ctx.SetFlag(ktools::emulator::MachineFlags::fgSIGNED);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgALL));
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgALL));
    ctx.SetFlag(ktools::emulator::MachineFlags::fgOVERFLOW);
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgALL));

    _FILL_REGS
    ctx.RegC() = 0x33;
    ctx.RegA() = 0xFA;
    _EXEC_COL_BLOCKROW(8, 0, 2);
    EXPECT_EQ(ctx.RegA(), 0x32);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 0x33);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.RegD() = 0x33;
    ctx.RegA() = 0xFA;
    _EXEC_COL_BLOCKROW(8, 0, 3);
    EXPECT_EQ(ctx.RegA(), 0x32);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 0x33);

    ctx.RegB() = 0x80;
    ctx.RegA() = 0xFA;
    _EXEC_COL_BLOCKROW(8, 0, 1);
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));

    ctx.RegB() = 0x00;
    ctx.RegA() = 0xFA;
    _EXEC_COL_BLOCKROW(8, 0, 1);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
}

// :: AND RB, R?
TEST(ExecutionStandard8, TestStandardCol_8b1) {
    _EXEC_TEST_BEGIN;

    _FILL_REGS
    ctx.RegA() = 0x33;
    ctx.RegB() = 0xFA;
    _EXEC_COL_BLOCKROW(8, 1, 0);
    EXPECT_EQ(ctx.RegA(), 0x33);
    EXPECT_EQ(ctx.RegB(), 0x32);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.RegB() = 0xFA;
    _EXEC_COL_BLOCKROW(8, 1, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 0xFA);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.RegC() = 0x33;
    ctx.RegB() = 0xFA;
    _EXEC_COL_BLOCKROW(8, 1, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 0x32);
    EXPECT_EQ(ctx.RegC(), 0x33);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.RegD() = 0x33;
    ctx.RegB() = 0xFA;
    _EXEC_COL_BLOCKROW(8, 1, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 0x32);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 0x33);
}

// :: AND RC, R?
TEST(ExecutionStandard8, TestStandardCol_8b2) {
    _EXEC_TEST_BEGIN;

    _FILL_REGS
    ctx.RegA() = 0x33;
    ctx.RegC() = 0xFA;
    _EXEC_COL_BLOCKROW(8, 2, 0);
    EXPECT_EQ(ctx.RegA(), 0x33);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 0x32);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.RegB() = 0x33;
    ctx.RegC() = 0xFA;
    _EXEC_COL_BLOCKROW(8, 2, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 0x33);
    EXPECT_EQ(ctx.RegC(), 0x32);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.RegC() = 0xFA;
    _EXEC_COL_BLOCKROW(8, 2, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 0xFA);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.RegD() = 0x33;
    ctx.RegC() = 0xFA;
    _EXEC_COL_BLOCKROW(8, 2, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 0x32);
    EXPECT_EQ(ctx.RegD(), 0x33);
}

// :: AND RD, R?
TEST(ExecutionStandard8, TestStandardCol_8b3) {
    _EXEC_TEST_BEGIN;

    _FILL_REGS
    ctx.RegA() = 0x33;
    ctx.RegD() = 0xFA;
    _EXEC_COL_BLOCKROW(8, 3, 0);
    EXPECT_EQ(ctx.RegA(), 0x33);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 0x32);

    _FILL_REGS
    ctx.RegB() = 0x33;
    ctx.RegD() = 0xFA;
    _EXEC_COL_BLOCKROW(8, 3, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 0x33);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 0x32);

    _FILL_REGS
    ctx.RegC() = 0x33;
    ctx.RegD() = 0xFA;
    _EXEC_COL_BLOCKROW(8, 3, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 0x33);
    EXPECT_EQ(ctx.RegD(), 0x32);

    _FILL_REGS
    ctx.RegD() = 0xFA;
    _EXEC_COL_BLOCKROW(8, 3, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 0xFA);
}
