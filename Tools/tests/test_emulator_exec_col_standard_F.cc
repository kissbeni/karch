
/**
 * @file test_emulator_exec_col_standard_F.cc
 * @author Kiss Benedek
 * @brief Automated tests for emulator (standard column F)
 */

#include "test_emulator.h"

TEST(ExecutionStandardF, TestStandardCol_F_BAD) {
    _EXEC_TEST_BEGIN;
    _EXEC_COL_BAD_INST(F)
}

// :: DIV RA, R?
TEST(ExecutionStandardF, TestStandardCol_Fb0) {
    _EXEC_TEST_BEGIN;

    _FILL_REGS
    ctx.RegA() = 3;
    _EXEC_COL_BLOCKROW(F, 0, 0);
    EXPECT_EQ(ctx.RegA(), 1);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegB() = 3;
    ctx.RegA() = 21;
    _EXEC_COL_BLOCKROW(F, 0, 1);
    EXPECT_EQ(ctx.RegA(), 7);
    EXPECT_EQ(ctx.RegB(), 3);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);
    ctx.RegB() = 1;
    ctx.RegA() = 0x80;
    _EXEC_COL_BLOCKROW(F, 0, 1);
    EXPECT_EQ(ctx.RegA(), 0x80);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgOVERFLOW));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
    ctx.RegB() = 3;
    ctx.RegA() = 0;
    _EXEC_COL_BLOCKROW(F, 0, 1);
    EXPECT_EQ(ctx.RegA(), 0);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgOVERFLOW));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));

    ctx.Reset();
    _FILL_REGS
    ctx.RegC() = 3;
    ctx.RegA() = 21;
    _EXEC_COL_BLOCKROW(F, 0, 2);
    EXPECT_EQ(ctx.RegA(), 7);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 3);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegD() = 3;
    ctx.RegA() = 21;
    _EXEC_COL_BLOCKROW(F, 0, 3);
    EXPECT_EQ(ctx.RegA(), 7);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 3);
}

// :: DIV RB, R?
TEST(ExecutionStandardF, TestStandardCol_Fb1) {
    _EXEC_TEST_BEGIN;

    _FILL_REGS
    ctx.RegA() = 3;
    ctx.RegB() = 21;
    _EXEC_COL_BLOCKROW(F, 1, 0);
    EXPECT_EQ(ctx.RegA(), 3);
    EXPECT_EQ(ctx.RegB(), 7);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegB() = 3;
    _EXEC_COL_BLOCKROW(F, 1, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 1);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegC() = 3;
    ctx.RegB() = 21;
    _EXEC_COL_BLOCKROW(F, 1, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 7);
    EXPECT_EQ(ctx.RegC(), 3);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegD() = 3;
    ctx.RegB() = 21;
    _EXEC_COL_BLOCKROW(F, 1, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 7);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 3);
}

// :: DIV RC, R?
TEST(ExecutionStandardF, TestStandardCol_Fb2) {
    _EXEC_TEST_BEGIN;

    _FILL_REGS
    ctx.RegA() = 3;
    ctx.RegC() = 21;
    _EXEC_COL_BLOCKROW(F, 2, 0);
    EXPECT_EQ(ctx.RegA(), 3);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 7);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegB() = 3;
    ctx.RegC() = 21;
    _EXEC_COL_BLOCKROW(F, 2, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 3);
    EXPECT_EQ(ctx.RegC(), 7);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegC() = 3;
    _EXEC_COL_BLOCKROW(F, 2, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 1);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegD() = 3;
    ctx.RegC() = 21;
    _EXEC_COL_BLOCKROW(F, 2, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 7);
    EXPECT_EQ(ctx.RegD(), 3);
}

// :: DIV RD, R?
TEST(ExecutionStandardF, TestStandardCol_Fb3) {
    _EXEC_TEST_BEGIN;

    _FILL_REGS
    ctx.RegA() = 3;
    ctx.RegD() = 21;
    _EXEC_COL_BLOCKROW(F, 3, 0);
    EXPECT_EQ(ctx.RegA(), 3);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 7);

    ctx.Reset();
    _FILL_REGS
    ctx.RegB() = 3;
    ctx.RegD() = 21;
    _EXEC_COL_BLOCKROW(F, 3, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 3);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 7);

    ctx.Reset();
    _FILL_REGS
    ctx.RegC() = 3;
    ctx.RegD() = 21;
    _EXEC_COL_BLOCKROW(F, 3, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 3);
    EXPECT_EQ(ctx.RegD(), 7);

    ctx.Reset();
    _FILL_REGS
    ctx.RegD() = 3;
    _EXEC_COL_BLOCKROW(F, 3, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 1);
}
