
/**
 * @file test_emulator_exec_col_standard_D.cc
 * @author Kiss Benedek
 * @brief Automated tests for emulator (standard column D)
 */

#include "test_emulator.h"

TEST(ExecutionStandardD, TestStandardCol_D_BAD) {
    _EXEC_TEST_BEGIN;
    _EXEC_COL_BAD_INST(D)
}

// :: MUL R?, X
TEST(ExecutionStandardD, TestStandardCol_Db0) {
    _EXEC_TEST_BEGIN;
    inst.arg = 3;

    _FILL_REGS
    ctx.RegA() = 10;
    _EXEC_COL_BLOCKROW(D, 0, 0);
    EXPECT_EQ(ctx.RegA(), 30);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);
    ctx.RegA() = 43;
    _EXEC_COL_BLOCKROW(D, 0, 0);
    EXPECT_EQ(ctx.RegA(), 129);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgOVERFLOW));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
    ctx.RegA() = 0;
    _EXEC_COL_BLOCKROW(D, 0, 0);
    EXPECT_EQ(ctx.RegA(), 0);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgOVERFLOW));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));

    ctx.Reset();
    _FILL_REGS
    ctx.RegB() = 10;
    _EXEC_COL_BLOCKROW(D, 0, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 30);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegC() = 10;
    _EXEC_COL_BLOCKROW(D, 0, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 30);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegD() = 10;
    _EXEC_COL_BLOCKROW(D, 0, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 30);
}

// :: DIV R?, X
TEST(ExecutionStandardD, TestStandardCol_Db1) {
    _EXEC_TEST_BEGIN;
    inst.arg = 3;

    _FILL_REGS
    ctx.RegA() = 21;
    _EXEC_COL_BLOCKROW(D, 1, 0);
    EXPECT_EQ(ctx.RegA(), 7);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);
    inst.arg = 1;
    ctx.RegA() = 0x80;
    _EXEC_COL_BLOCKROW(D, 1, 0);
    EXPECT_EQ(ctx.RegA(), 0x80);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgOVERFLOW));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
    inst.arg = 3;
    ctx.RegA() = 0;
    _EXEC_COL_BLOCKROW(D, 1, 0);
    EXPECT_EQ(ctx.RegA(), 0);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgOVERFLOW));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));

    ctx.Reset();
    _FILL_REGS
    ctx.RegB() = 21;
    _EXEC_COL_BLOCKROW(D, 1, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 7);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegC() = 21;
    _EXEC_COL_BLOCKROW(D, 1, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 7);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegD() = 21;
    _EXEC_COL_BLOCKROW(D, 1, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 7);
}

// :: MUL [R?], X
TEST(ExecutionStandardD, TestStandardCol_Db2) {
    _EXEC_TEST_BEGIN_MEM;
    inst.arg = 3;

    ctx.WriteMemory<karch_word_t>(16, 10);
    ctx.RegA() = 16;
    _EXEC_COL_BLOCKROW(D, 2, 0);
    EXPECT_EQ(ctx.ReadOne(16), 30);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);
    ctx.WriteMemory<karch_word_t>(16, 43);
    _EXEC_COL_BLOCKROW(D, 2, 0);
    EXPECT_EQ(ctx.ReadOne(16), 129);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgOVERFLOW));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
    ctx.WriteMemory<karch_word_t>(16, 0);
    _EXEC_COL_BLOCKROW(D, 2, 0);
    EXPECT_EQ(ctx.ReadOne(16), 0);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgOVERFLOW));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));

    ctx.CurrentMemoryBank() = 2;
    ctx.WriteMemory<karch_word_t>(ctx.GetBankOffset(16), 10);
    _EXEC_COL_BLOCKROW(D, 2, 0);
    EXPECT_EQ(ctx.ReadOne(ctx.GetBankOffset(16)), 30);

    ctx.Reset();
    ctx.WriteMemory<karch_word_t>(16, 10);
    ctx.RegB() = 16;
    _EXEC_COL_BLOCKROW(D, 2, 1);
    EXPECT_EQ(ctx.ReadOne(16), 30);

    ctx.Reset();
    ctx.WriteMemory<karch_word_t>(16, 10);
    ctx.RegC() = 16;
    _EXEC_COL_BLOCKROW(D, 2, 2);
    EXPECT_EQ(ctx.ReadOne(16), 30);

    ctx.Reset();
    ctx.WriteMemory<karch_word_t>(16, 10);
    ctx.RegD() = 16;
    _EXEC_COL_BLOCKROW(D, 2, 3);
    EXPECT_EQ(ctx.ReadOne(16), 30);
}

// :: DIV [R?], X
TEST(ExecutionStandardD, TestStandardCol_Db3) {
    _EXEC_TEST_BEGIN_MEM;
    inst.arg = 3;

    ctx.WriteMemory<karch_word_t>(16, 21);
    ctx.RegA() = 16;
    _EXEC_COL_BLOCKROW(D, 3, 0);
    EXPECT_EQ(ctx.ReadOne(16), 7);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);
    inst.arg = 1;
    ctx.WriteMemory<karch_word_t>(16, 0x80);
    _EXEC_COL_BLOCKROW(D, 3, 0);
    EXPECT_EQ(ctx.ReadOne(16), 0x80);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgOVERFLOW));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
    inst.arg = 3;
    ctx.WriteMemory<karch_word_t>(16, 0);
    _EXEC_COL_BLOCKROW(D, 3, 0);
    EXPECT_EQ(ctx.ReadOne(16), 0);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgOVERFLOW));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));

    ctx.CurrentMemoryBank() = 2;
    ctx.WriteMemory<karch_word_t>(ctx.GetBankOffset(16), 21);
    _EXEC_COL_BLOCKROW(D, 3, 0);
    EXPECT_EQ(ctx.ReadOne(ctx.GetBankOffset(16)), 7);

    ctx.Reset();
    ctx.WriteMemory<karch_word_t>(16, 21);
    ctx.RegB() = 16;
    _EXEC_COL_BLOCKROW(D, 3, 1);
    EXPECT_EQ(ctx.ReadOne(16), 7);

    ctx.Reset();
    ctx.WriteMemory<karch_word_t>(16, 21);
    ctx.RegC() = 16;
    _EXEC_COL_BLOCKROW(D, 3, 2);
    EXPECT_EQ(ctx.ReadOne(16), 7);

    ctx.Reset();
    ctx.WriteMemory<karch_word_t>(16, 21);
    ctx.RegD() = 16;
    _EXEC_COL_BLOCKROW(D, 3, 3);
    EXPECT_EQ(ctx.ReadOne(16), 7);
}
