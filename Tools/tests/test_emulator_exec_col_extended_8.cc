
/**
 * @file test_emulator_exec_col_extended_8.cc
 * @author Kiss Benedek
 * @brief Automated tests for emulator (extended column 8)
 */

#include "test_emulator.h"

TEST(ExecutionExtended8, TestExtendedCol_8_BAD) {
    _EXEC_TEST_BEGIN;
    _EXEC_COL_BAD_INST(ex8)
}

// :: CMP RA, R?
TEST(ExecutionExtended8, TestExtendedCol_8b0) {
    _EXEC_TEST_BEGIN;

    ctx.Reset();
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegA() = 5;
    _EXEC_COL_BLOCKROW(ex8, 0, 0);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgZERO);


    ctx.SetFlag(ktools::emulator::MachineFlags::fgALL);
    ctx.RegA() = 10;
    ctx.RegB() = 5;
    _EXEC_COL_BLOCKROW(ex8, 0, 1);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    ctx.SetFlag(ktools::emulator::MachineFlags::fgZERO);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgALL);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgALL);
    ctx.RegA() = 5;
    ctx.RegB() = 5;
    _EXEC_COL_BLOCKROW(ex8, 0, 1);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgALL);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgALL);
    ctx.RegA() = 4;
    ctx.RegB() = 5;
    _EXEC_COL_BLOCKROW(ex8, 0, 1);
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
    ctx.SetFlag(ktools::emulator::MachineFlags::fgZERO);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgALL);


    ctx.Reset();
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegA() = 10;
    ctx.RegC() = 5;
    _EXEC_COL_BLOCKROW(ex8, 0, 2);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegA() = 5;
    ctx.RegC() = 5;
    _EXEC_COL_BLOCKROW(ex8, 0, 2);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgZERO);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegA() = 4;
    ctx.RegC() = 5;
    _EXEC_COL_BLOCKROW(ex8, 0, 2);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgCARRY);


    ctx.Reset();
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegA() = 10;
    ctx.RegD() = 5;
    _EXEC_COL_BLOCKROW(ex8, 0, 3);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegA() = 5;
    ctx.RegD() = 5;
    _EXEC_COL_BLOCKROW(ex8, 0, 3);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgZERO);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegA() = 4;
    ctx.RegD() = 5;
    _EXEC_COL_BLOCKROW(ex8, 0, 3);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgCARRY);
}

// :: CMP RB, R?
TEST(ExecutionExtended8, TestExtendedCol_8b1) {
    _EXEC_TEST_BEGIN;

    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegB() = 10;
    ctx.RegA() = 5;
    _EXEC_COL_BLOCKROW(ex8, 1, 0);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegB() = 5;
    ctx.RegA() = 5;
    _EXEC_COL_BLOCKROW(ex8, 1, 0);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgZERO);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegB() = 4;
    ctx.RegA() = 5;
    _EXEC_COL_BLOCKROW(ex8, 1, 0);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgCARRY);


    ctx.Reset();
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegB() = 5;
    _EXEC_COL_BLOCKROW(ex8, 1, 1);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgZERO);


    ctx.Reset();
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegB() = 10;
    ctx.RegC() = 5;
    _EXEC_COL_BLOCKROW(ex8, 1, 2);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegB() = 5;
    ctx.RegC() = 5;
    _EXEC_COL_BLOCKROW(ex8, 1, 2);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgZERO);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegB() = 4;
    ctx.RegC() = 5;
    _EXEC_COL_BLOCKROW(ex8, 1, 2);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgCARRY);


    ctx.Reset();
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegB() = 10;
    ctx.RegD() = 5;
    _EXEC_COL_BLOCKROW(ex8, 1, 3);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegB() = 5;
    ctx.RegD() = 5;
    _EXEC_COL_BLOCKROW(ex8, 1, 3);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgZERO);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegB() = 4;
    ctx.RegD() = 5;
    _EXEC_COL_BLOCKROW(ex8, 1, 3);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgCARRY);
}

// :: CMP RC, R?
TEST(ExecutionExtended8, TestExtendedCol_8b2) {
    _EXEC_TEST_BEGIN;

    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegC() = 10;
    ctx.RegA() = 5;
    _EXEC_COL_BLOCKROW(ex8, 2, 0);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegC() = 5;
    ctx.RegA() = 5;
    _EXEC_COL_BLOCKROW(ex8, 2, 0);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgZERO);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegC() = 4;
    ctx.RegA() = 5;
    _EXEC_COL_BLOCKROW(ex8, 2, 0);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgCARRY);


    ctx.Reset();
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegC() = 10;
    ctx.RegB() = 5;
    _EXEC_COL_BLOCKROW(ex8, 2, 1);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegC() = 5;
    ctx.RegB() = 5;
    _EXEC_COL_BLOCKROW(ex8, 2, 1);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgZERO);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegC() = 4;
    ctx.RegB() = 5;
    _EXEC_COL_BLOCKROW(ex8, 2, 1);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgCARRY);


    ctx.Reset();
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegC() = 5;
    _EXEC_COL_BLOCKROW(ex8, 2, 2);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgZERO);


    ctx.Reset();
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegC() = 10;
    ctx.RegD() = 5;
    _EXEC_COL_BLOCKROW(ex8, 2, 3);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegC() = 5;
    ctx.RegD() = 5;
    _EXEC_COL_BLOCKROW(ex8, 2, 3);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgZERO);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegC() = 4;
    ctx.RegD() = 5;
    _EXEC_COL_BLOCKROW(ex8, 2, 3);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgCARRY);
}

// :: CMP RD, R?
TEST(ExecutionExtended8, TestExtendedCol_8b3) {
    _EXEC_TEST_BEGIN;

    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegD() = 10;
    ctx.RegA() = 5;
    _EXEC_COL_BLOCKROW(ex8, 3, 0);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegD() = 5;
    ctx.RegA() = 5;
    _EXEC_COL_BLOCKROW(ex8, 3, 0);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgZERO);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegD() = 4;
    ctx.RegA() = 5;
    _EXEC_COL_BLOCKROW(ex8, 3, 0);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgCARRY);


    ctx.Reset();
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegD() = 10;
    ctx.RegB() = 5;
    _EXEC_COL_BLOCKROW(ex8, 3, 1);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegD() = 5;
    ctx.RegB() = 5;
    _EXEC_COL_BLOCKROW(ex8, 3, 1);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgZERO);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegD() = 4;
    ctx.RegB() = 5;
    _EXEC_COL_BLOCKROW(ex8, 3, 1);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgCARRY);


    ctx.Reset();
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegD() = 10;
    ctx.RegC() = 5;
    _EXEC_COL_BLOCKROW(ex8, 3, 2);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegD() = 5;
    ctx.RegC() = 5;
    _EXEC_COL_BLOCKROW(ex8, 3, 2);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgZERO);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegD() = 4;
    ctx.RegC() = 5;
    _EXEC_COL_BLOCKROW(ex8, 3, 2);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgCARRY);


    ctx.Reset();
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegD() = 5;
    _EXEC_COL_BLOCKROW(ex8, 3, 3);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgZERO);
}
