
/**
 * @file test_emulator_exec_col_standard_3.cc
 * @author Kiss Benedek
 * @brief Automated tests for emulator (standard column 3)
 */

#include "test_emulator.h"

TEST(ExecutionStandard3, TestStandardCol_3_BAD) {
    _EXEC_TEST_BEGIN;
    _EXEC_COL_BAD_INST(3)
}

// :: ADD R?, X
TEST(ExecutionStandard3, TestStandardCol_3b0) {
    _EXEC_TEST_BEGIN;
    inst.arg = 20;

    _FILL_REGS
    ctx.RegA() = 10;
    _EXEC_COL_BLOCKROW(3, 0, 0);
    EXPECT_EQ(ctx.RegA(), 30);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);
    ctx.RegA() = 0x70;
    _EXEC_COL_BLOCKROW(3, 0, 0);
    EXPECT_EQ(ctx.RegA(), 0x84);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgOVERFLOW));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
    ctx.RegA() = 0xEC;
    _EXEC_COL_BLOCKROW(3, 0, 0);
    EXPECT_EQ(ctx.RegA(), 0);
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgOVERFLOW));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));

    ctx.Reset();
    _FILL_REGS
    ctx.RegB() = 10;
    _EXEC_COL_BLOCKROW(3, 0, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 30);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegC() = 10;
    _EXEC_COL_BLOCKROW(3, 0, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 30);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegD() = 10;
    _EXEC_COL_BLOCKROW(3, 0, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 30);
}

// :: SUB R?, X
TEST(ExecutionStandard3, TestStandardCol_3b1) {
    _EXEC_TEST_BEGIN;
    inst.arg = 10;

    _FILL_REGS
    ctx.RegA() = 20;
    _EXEC_COL_BLOCKROW(3, 1, 0);
    EXPECT_EQ(ctx.RegA(), 10);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);
    ctx.RegA() = 0x80;
    _EXEC_COL_BLOCKROW(3, 1, 0);
    EXPECT_EQ(ctx.RegA(), 0x76);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgOVERFLOW));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
    ctx.RegA() = 10;
    _EXEC_COL_BLOCKROW(3, 1, 0);
    EXPECT_EQ(ctx.RegA(), 0);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgZERO);

    ctx.Reset();
    _FILL_REGS
    ctx.RegB() = 20;
    _EXEC_COL_BLOCKROW(3, 1, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 10);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegC() = 20;
    _EXEC_COL_BLOCKROW(3, 1, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 10);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegD() = 20;
    _EXEC_COL_BLOCKROW(3, 1, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 10);
}

// :: ADD [R?], X
TEST(ExecutionStandard3, TestStandardCol_3b2) {
    _EXEC_TEST_BEGIN_MEM;
    inst.arg = 20;

    ctx.WriteMemory<karch_word_t>(16, 10);
    ctx.RegA() = 16;
    _EXEC_COL_BLOCKROW(3, 2, 0);
    EXPECT_EQ(ctx.ReadOne(16), 30);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);
    ctx.WriteMemory<karch_word_t>(16, 0x70);
    _EXEC_COL_BLOCKROW(3, 2, 0);
    EXPECT_EQ(ctx.ReadOne(16), 0x84);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgOVERFLOW));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
    ctx.WriteMemory<karch_word_t>(16, 0xEC);
    _EXEC_COL_BLOCKROW(3, 2, 0);
    EXPECT_EQ(ctx.ReadOne(16), 0);
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgOVERFLOW));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));

    ctx.CurrentMemoryBank() = 2;
    ctx.WriteMemory<karch_word_t>(ctx.GetBankOffset(16), 10);
    _EXEC_COL_BLOCKROW(3, 2, 0);
    EXPECT_EQ(ctx.ReadOne(ctx.GetBankOffset(16)), 30);

    ctx.Reset();
    ctx.WriteMemory<karch_word_t>(16, 10);
    ctx.RegB() = 16;
    _EXEC_COL_BLOCKROW(3, 2, 1);
    EXPECT_EQ(ctx.ReadOne(16), 30);

    ctx.Reset();
    ctx.WriteMemory<karch_word_t>(16, 10);
    ctx.RegC() = 16;
    _EXEC_COL_BLOCKROW(3, 2, 2);
    EXPECT_EQ(ctx.ReadOne(16), 30);

    ctx.Reset();
    ctx.WriteMemory<karch_word_t>(16, 10);
    ctx.RegD() = 16;
    _EXEC_COL_BLOCKROW(3, 2, 3);
    EXPECT_EQ(ctx.ReadOne(16), 30);
}

// :: SUB [R?], X
TEST(ExecutionStandard3, TestStandardCol_3b3) {
    _EXEC_TEST_BEGIN_MEM;
    inst.arg = 10;

    ctx.WriteMemory<karch_word_t>(16, 20);
    ctx.RegA() = 16;
    _EXEC_COL_BLOCKROW(3, 3, 0);
    EXPECT_EQ(ctx.ReadOne(16), 10);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);
    ctx.WriteMemory<karch_word_t>(16, 0x80);
    _EXEC_COL_BLOCKROW(3, 3, 0);
    EXPECT_EQ(ctx.ReadOne(16), 0x76);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgOVERFLOW));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
    ctx.WriteMemory<karch_word_t>(16, 10);
    _EXEC_COL_BLOCKROW(3, 3, 0);
    EXPECT_EQ(ctx.ReadOne(16), 0);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgOVERFLOW));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));

    ctx.CurrentMemoryBank() = 2;
    ctx.WriteMemory<karch_word_t>(ctx.GetBankOffset(16), 20);
    _EXEC_COL_BLOCKROW(3, 3, 0);
    EXPECT_EQ(ctx.ReadOne(ctx.GetBankOffset(16)), 10);

    ctx.Reset();
    ctx.WriteMemory<karch_word_t>(16, 20);
    ctx.RegB() = 16;
    _EXEC_COL_BLOCKROW(3, 3, 1);
    EXPECT_EQ(ctx.ReadOne(16), 10);

    ctx.Reset();
    ctx.WriteMemory<karch_word_t>(16, 20);
    ctx.RegC() = 16;
    _EXEC_COL_BLOCKROW(3, 3, 2);
    EXPECT_EQ(ctx.ReadOne(16), 10);

    ctx.Reset();
    ctx.WriteMemory<karch_word_t>(16, 20);
    ctx.RegD() = 16;
    _EXEC_COL_BLOCKROW(3, 3, 3);
    EXPECT_EQ(ctx.ReadOne(16), 10);
}
