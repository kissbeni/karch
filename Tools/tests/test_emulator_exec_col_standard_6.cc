
/**
 * @file test_emulator_exec_col_standard_6.cc
 * @author Kiss Benedek
 * @brief Automated tests for emulator (standard column 6)
 */

#include "test_emulator.h"

TEST(ExecutionStandard6, TestStandardCol_6_BAD) {
    _EXEC_TEST_BEGIN;
    _EXEC_COL_BAD_INST(6)
}

// :: PUSH R?
TEST(ExecutionStandard6, TestStandardCol_6b0) {
    _EXEC_TEST_BEGIN;

    _FILL_REGS
    _EXEC_COL_BLOCKROW(6, 0, 0);
    EXPECT_EQ(ctx.SP(), 1);
    EXPECT_EQ(ctx.Pop<karch_word_t>(), gTestRegValues[0]);

    _EXEC_COL_BLOCKROW(6, 0, 1);
    EXPECT_EQ(ctx.SP(), 1);
    EXPECT_EQ(ctx.Pop<karch_word_t>(), gTestRegValues[1]);

    _EXEC_COL_BLOCKROW(6, 0, 2);
    EXPECT_EQ(ctx.SP(), 1);
    EXPECT_EQ(ctx.Pop<karch_word_t>(), gTestRegValues[2]);

    _EXEC_COL_BLOCKROW(6, 0, 3);
    EXPECT_EQ(ctx.SP(), 1);
    EXPECT_EQ(ctx.Pop<karch_word_t>(), gTestRegValues[3]);
}

// :: POP R?
TEST(ExecutionStandard6, TestStandardCol_6b1) {
    _EXEC_TEST_BEGIN;

    _FILL_REGS
    ctx.Push<karch_word_t>(40);
    _EXEC_COL_BLOCKROW(6, 1, 0);
    EXPECT_EQ(ctx.SP(), 0);
    EXPECT_EQ(ctx.RegA(), 40);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.Push<karch_word_t>(40);
    _EXEC_COL_BLOCKROW(6, 1, 1);
    EXPECT_EQ(ctx.SP(), 0);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 40);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.Push<karch_word_t>(40);
    _EXEC_COL_BLOCKROW(6, 1, 2);
    EXPECT_EQ(ctx.SP(), 0);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 40);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.Push<karch_word_t>(40);
    _EXEC_COL_BLOCKROW(6, 1, 3);
    EXPECT_EQ(ctx.SP(), 0);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 40);
}

// :: PUSH [R?]
TEST(ExecutionStandard6, TestStandardCol_6b2) {
    _EXEC_TEST_BEGIN_MEM;

    ctx.RegA() = 16;
    ctx.WriteMemory<karch_word_t>(16, 40);
    _EXEC_COL_BLOCKROW(6, 2, 0);
    EXPECT_EQ(ctx.SP(), 1);
    EXPECT_EQ(ctx.Pop<karch_word_t>(), 40);

    ctx.RegB() = 16;
    ctx.WriteMemory<karch_word_t>(16, 40);
    _EXEC_COL_BLOCKROW(6, 2, 1);
    EXPECT_EQ(ctx.SP(), 1);
    EXPECT_EQ(ctx.Pop<karch_word_t>(), 40);

    ctx.RegC() = 16;
    ctx.WriteMemory<karch_word_t>(16, 40);
    _EXEC_COL_BLOCKROW(6, 2, 2);
    EXPECT_EQ(ctx.SP(), 1);
    EXPECT_EQ(ctx.Pop<karch_word_t>(), 40);

    ctx.RegD() = 16;
    ctx.WriteMemory<karch_word_t>(16, 40);
    _EXEC_COL_BLOCKROW(6, 2, 3);
    EXPECT_EQ(ctx.SP(), 1);
    EXPECT_EQ(ctx.Pop<karch_word_t>(), 40);

    ctx.CurrentMemoryBank() = 2;
    ctx.RegA() = 16;
    ctx.WriteMemory<karch_word_t>(ctx.GetBankOffset(16), 40);
    _EXEC_COL_BLOCKROW(6, 2, 0);
    EXPECT_EQ(ctx.SP(), 1);
    EXPECT_EQ(ctx.Pop<karch_word_t>(), 40);
}

// :: NOT R?
TEST(ExecutionStandard6, TestStandardCol_6b3) {
    _EXEC_TEST_BEGIN_MEM;

    _FILL_REGS
    _EXEC_COL_BLOCKROW(6, 3, 0);
    EXPECT_EQ(ctx.RegA(), static_cast<karch_word_t>(~gTestRegValues[0]));
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    // In the current architecture specification NOT does not affect any flags
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);

    _FILL_REGS
    _EXEC_COL_BLOCKROW(6, 3, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), static_cast<karch_word_t>(~gTestRegValues[1]));
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    _EXEC_COL_BLOCKROW(6, 3, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), static_cast<karch_word_t>(~gTestRegValues[2]));
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    _EXEC_COL_BLOCKROW(6, 3, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), static_cast<karch_word_t>(~gTestRegValues[3]));
}
