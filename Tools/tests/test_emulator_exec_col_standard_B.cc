
/**
 * @file test_emulator_exec_col_standard_B.cc
 * @author Kiss Benedek
 * @brief Automated tests for emulator (standard column B)
 */

#include "test_emulator.h"

TEST(ExecutionStandardB, TestStandardCol_B_BAD) {
    _EXEC_TEST_BEGIN;
    _EXEC_COL_BAD_INST(B)
}

// :: PUSH X
TEST(ExecutionStandardB, TestStandardCol_Br0) {
    _EXEC_TEST_BEGIN;

    inst.arg16 = 123;
    _EXEC_COL_ROW(B, 0);
    EXPECT_EQ(ctx.SP(), 1);
    EXPECT_EQ(ctx.Pop<karch_word_t>(), 123);
}

// :: JMP $+X
TEST(ExecutionStandardB, TestStandardCol_Br1) {
    _EXEC_TEST_BEGIN;

    ctx.IP() = 10;
    inst.arg16 = 20;
    _EXEC_COL_ROW(B, 1);
    EXPECT_EQ(ctx.IP(), 28);
}

// :: JE $+X
TEST(ExecutionStandardB, TestStandardCol_Br2) {
    _EXEC_TEST_BEGIN;

    ctx.IP() = 10;
    inst.arg16 = 20;
    _EXEC_COL_ROW(B, 2);
    EXPECT_EQ(ctx.IP(), 10);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgALL);
    ctx.ClearFlag(ktools::emulator::MachineFlags::fgZERO);
    _EXEC_COL_ROW(B, 2);
    EXPECT_EQ(ctx.IP(), 10);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgZERO);
    _EXEC_COL_ROW(B, 2);
    EXPECT_EQ(ctx.IP(), 28);
}

// :: JNE $+X
TEST(ExecutionStandardB, TestStandardCol_Br3) {
    _EXEC_TEST_BEGIN;

    ctx.IP() = 10;
    inst.arg16 = 20;
    ctx.SetFlag(ktools::emulator::MachineFlags::fgZERO);
    _EXEC_COL_ROW(B, 3);
    EXPECT_EQ(ctx.IP(), 10);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgALL);
    _EXEC_COL_ROW(B, 3);
    EXPECT_EQ(ctx.IP(), 10);

    ctx.ClearFlag(ktools::emulator::MachineFlags::fgZERO);
    _EXEC_COL_ROW(B, 3);
    EXPECT_EQ(ctx.IP(), 28);
}

// :: JC $+X
TEST(ExecutionStandardB, TestStandardCol_Br4) {
    _EXEC_TEST_BEGIN;

    ctx.IP() = 10;
    inst.arg16 = 20;
    _EXEC_COL_ROW(B, 4);
    EXPECT_EQ(ctx.IP(), 10);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgALL);
    ctx.ClearFlag(ktools::emulator::MachineFlags::fgCARRY);
    _EXEC_COL_ROW(B, 4);
    EXPECT_EQ(ctx.IP(), 10);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    _EXEC_COL_ROW(B, 4);
    EXPECT_EQ(ctx.IP(), 28);
}

// :: JNC $+X
TEST(ExecutionStandardB, TestStandardCol_Br5) {
    _EXEC_TEST_BEGIN;

    ctx.IP() = 10;
    inst.arg16 = 20;
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    _EXEC_COL_ROW(B, 5);
    EXPECT_EQ(ctx.IP(), 10);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgALL);
    _EXEC_COL_ROW(B, 5);
    EXPECT_EQ(ctx.IP(), 10);

    ctx.ClearFlag(ktools::emulator::MachineFlags::fgCARRY);
    _EXEC_COL_ROW(B, 5);
    EXPECT_EQ(ctx.IP(), 28);
}

// :: JBE $+X
TEST(ExecutionStandardB, TestStandardCol_Br6) {
    _EXEC_TEST_BEGIN;

    ctx.IP() = 10;
    inst.arg16 = 20;
    _EXEC_COL_ROW(B, 6);
    EXPECT_EQ(ctx.IP(), 10);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgALL);
    ctx.ClearFlag(ktools::emulator::MachineFlags::fgCARRY);
    _EXEC_COL_ROW(B, 6);
    EXPECT_EQ(ctx.IP(), 10);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgALL);
    ctx.ClearFlag(ktools::emulator::MachineFlags::fgZERO);
    _EXEC_COL_ROW(B, 6);
    EXPECT_EQ(ctx.IP(), 10);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgALL);
    _EXEC_COL_ROW(B, 6);
    EXPECT_EQ(ctx.IP(), 28);
}

// :: JA $+X
TEST(ExecutionStandardB, TestStandardCol_Br7) {
    _EXEC_TEST_BEGIN;

    ctx.IP() = 10;
    inst.arg16 = 20;
    ctx.SetFlag(ktools::emulator::MachineFlags::fgALL);
    _EXEC_COL_ROW(B, 7);
    EXPECT_EQ(ctx.IP(), 10);

    ctx.ClearFlag(ktools::emulator::MachineFlags::fgOVERFLOW);
    _EXEC_COL_ROW(B, 7);
    EXPECT_EQ(ctx.IP(), 10);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgSIGNED);
    _EXEC_COL_ROW(B, 7);
    EXPECT_EQ(ctx.IP(), 10);

    ctx.ClearFlag(ktools::emulator::MachineFlags::fgALL);
    _EXEC_COL_ROW(B, 7);
    EXPECT_EQ(ctx.IP(), 28);
}

// :: JS $+X
TEST(ExecutionStandardB, TestStandardCol_Br8) {
    _EXEC_TEST_BEGIN;

    ctx.IP() = 10;
    inst.arg16 = 20;
    _EXEC_COL_ROW(B, 8);
    EXPECT_EQ(ctx.IP(), 10);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgALL);
    ctx.ClearFlag(ktools::emulator::MachineFlags::fgSIGNED);
    _EXEC_COL_ROW(B, 8);
    EXPECT_EQ(ctx.IP(), 10);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgSIGNED);
    _EXEC_COL_ROW(B, 8);
    EXPECT_EQ(ctx.IP(), 28);
}

// :: JNS $+X
TEST(ExecutionStandardB, TestStandardCol_Br9) {
    _EXEC_TEST_BEGIN;

    ctx.IP() = 10;
    inst.arg16 = 20;
    ctx.SetFlag(ktools::emulator::MachineFlags::fgSIGNED);
    _EXEC_COL_ROW(B, 9);
    EXPECT_EQ(ctx.IP(), 10);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgALL);
    _EXEC_COL_ROW(B, 9);
    EXPECT_EQ(ctx.IP(), 10);

    ctx.ClearFlag(ktools::emulator::MachineFlags::fgSIGNED);
    _EXEC_COL_ROW(B, 9);
    EXPECT_EQ(ctx.IP(), 28);
}

// :: BNK X
TEST(ExecutionStandardB, TestStandardCol_BrA) {
    _EXEC_TEST_BEGIN;

    ctx.CurrentMemoryBank() = 4;
    inst.arg16 = 1;
    _EXEC_COL_ROW(B, 10);
    EXPECT_EQ(ctx.CurrentMemoryBank(), 1);
}

// :: JO $+X
TEST(ExecutionStandardB, TestStandardCol_BrC) {
    _EXEC_TEST_BEGIN;

    ctx.IP() = 10;
    inst.arg16 = 20;
    _EXEC_COL_ROW(B, 12);
    EXPECT_EQ(ctx.IP(), 10);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgALL);
    ctx.ClearFlag(ktools::emulator::MachineFlags::fgOVERFLOW);
    _EXEC_COL_ROW(B, 12);
    EXPECT_EQ(ctx.IP(), 10);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgOVERFLOW);
    _EXEC_COL_ROW(B, 12);
    EXPECT_EQ(ctx.IP(), 28);
}

// :: JNO $+X
TEST(ExecutionStandardB, TestStandardCol_BrD) {
    _EXEC_TEST_BEGIN;

    ctx.IP() = 10;
    inst.arg16 = 20;
    ctx.SetFlag(ktools::emulator::MachineFlags::fgOVERFLOW);
    _EXEC_COL_ROW(B, 13);
    EXPECT_EQ(ctx.IP(), 10);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgALL);
    _EXEC_COL_ROW(B, 13);
    EXPECT_EQ(ctx.IP(), 10);

    ctx.ClearFlag(ktools::emulator::MachineFlags::fgOVERFLOW);
    _EXEC_COL_ROW(B, 13);
    EXPECT_EQ(ctx.IP(), 28);
}

// :: CALL $+X
TEST(ExecutionStandardB, TestStandardCol_BrE) {
    _EXEC_TEST_BEGIN;

    ctx.IP() = 10;
    inst.arg16 = 20;
    _EXEC_COL_ROW(B, 14);
    EXPECT_EQ(ctx.SP(), 2);
    EXPECT_EQ(ctx.Pop<karch_addr_t>(), 10);
    EXPECT_EQ(ctx.IP(), 28);
}

// :: PUSH [X]
TEST(ExecutionStandardB, TestStandardCol_BrF) {
    _EXEC_TEST_BEGIN_MEM;

    inst.arg16 = 16;
    ctx.WriteMemory<karch_word_t>(16, 123);
    _EXEC_COL_ROW(B, 15);
    EXPECT_EQ(ctx.SP(), 1);
    EXPECT_EQ(ctx.Pop<karch_word_t>(), 123);

    ctx.CurrentMemoryBank() = 2;
    ctx.WriteMemory<karch_word_t>(ctx.GetBankOffset(16), 123);
    _EXEC_COL_ROW(B, 15);
    EXPECT_EQ(ctx.SP(), 1);
    EXPECT_EQ(ctx.Pop<karch_word_t>(), 123);
}
