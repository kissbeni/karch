
/**
 * @file test_emulator_exec_col_standard_E.cc
 * @author Kiss Benedek
 * @brief Automated tests for emulator (standard column E)
 */

#include "test_emulator.h"

TEST(ExecutionStandardE, TestStandardCol_E_BAD) {
    _EXEC_TEST_BEGIN;
    _EXEC_COL_BAD_INST(E)
}

// :: MUL RA, R?
TEST(ExecutionStandardE, TestStandardCol_Eb0) {
    _EXEC_TEST_BEGIN;

    _FILL_REGS
    ctx.RegA() = 3;
    _EXEC_COL_BLOCKROW(E, 0, 0);
    EXPECT_EQ(ctx.RegA(), 9);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegB() = 3;
    ctx.RegA() = 10;
    _EXEC_COL_BLOCKROW(E, 0, 1);
    EXPECT_EQ(ctx.RegA(), 30);
    EXPECT_EQ(ctx.RegB(), 3);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);
    ctx.RegA() = 43;
    _EXEC_COL_BLOCKROW(E, 0, 1);
    EXPECT_EQ(ctx.RegA(), 129);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgOVERFLOW));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
    ctx.RegA() = 0;
    _EXEC_COL_BLOCKROW(E, 0, 1);
    EXPECT_EQ(ctx.RegA(), 0);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgOVERFLOW));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));

    ctx.Reset();
    _FILL_REGS
    ctx.RegC() = 3;
    ctx.RegA() = 10;
    _EXEC_COL_BLOCKROW(E, 0, 2);
    EXPECT_EQ(ctx.RegA(), 30);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 3);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegD() = 3;
    ctx.RegA() = 10;
    _EXEC_COL_BLOCKROW(E, 0, 3);
    EXPECT_EQ(ctx.RegA(), 30);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 3);
}

// :: MUL RB, R?
TEST(ExecutionStandardE, TestStandardCol_Eb1) {
    _EXEC_TEST_BEGIN;

    _FILL_REGS
    ctx.RegA() = 3;
    ctx.RegB() = 10;
    _EXEC_COL_BLOCKROW(E, 1, 0);
    EXPECT_EQ(ctx.RegA(), 3);
    EXPECT_EQ(ctx.RegB(), 30);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegB() = 3;
    _EXEC_COL_BLOCKROW(E, 1, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 9);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegC() = 3;
    ctx.RegB() = 10;
    _EXEC_COL_BLOCKROW(E, 1, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 30);
    EXPECT_EQ(ctx.RegC(), 3);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegD() = 3;
    ctx.RegB() = 10;
    _EXEC_COL_BLOCKROW(E, 1, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 30);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 3);
}

// :: MUL RC, R?
TEST(ExecutionStandardE, TestStandardCol_Eb2) {
    _EXEC_TEST_BEGIN;

    _FILL_REGS
    ctx.RegA() = 3;
    ctx.RegC() = 10;
    _EXEC_COL_BLOCKROW(E, 2, 0);
    EXPECT_EQ(ctx.RegA(), 3);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 30);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegB() = 3;
    ctx.RegC() = 10;
    _EXEC_COL_BLOCKROW(E, 2, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 3);
    EXPECT_EQ(ctx.RegC(), 30);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegC() = 3;
    _EXEC_COL_BLOCKROW(E, 2, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 9);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegD() = 3;
    ctx.RegC() = 10;
    _EXEC_COL_BLOCKROW(E, 2, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 30);
    EXPECT_EQ(ctx.RegD(), 3);
}

// :: MUL RD, R?
TEST(ExecutionStandardE, TestStandardCol_Eb3) {
    _EXEC_TEST_BEGIN;

    _FILL_REGS
    ctx.RegA() = 3;
    ctx.RegD() = 10;
    _EXEC_COL_BLOCKROW(E, 3, 0);
    EXPECT_EQ(ctx.RegA(), 3);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 30);

    ctx.Reset();
    _FILL_REGS
    ctx.RegB() = 3;
    ctx.RegD() = 10;
    _EXEC_COL_BLOCKROW(E, 3, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 3);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 30);

    ctx.Reset();
    _FILL_REGS
    ctx.RegC() = 3;
    ctx.RegD() = 10;
    _EXEC_COL_BLOCKROW(E, 3, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 3);
    EXPECT_EQ(ctx.RegD(), 30);

    ctx.Reset();
    _FILL_REGS
    ctx.RegD() = 3;
    _EXEC_COL_BLOCKROW(E, 3, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 9);
}
