
/**
 * @file test_emulator_exec_col_extended_0.cc
 * @author Kiss Benedek
 * @brief Automated tests for emulator (extended column 0)
 */

#include "test_emulator.h"

TEST(ExecutionExtended0, TestExtendedCol_0_BAD) {
    _EXEC_TEST_BEGIN;
    _EXEC_COL_BAD_INST(ex0)
}

// :: IIC R?
TEST(ExecutionExtended0, TestExtendedCol_0b0) {
    _EXEC_TEST_BEGIN;

    _FILL_REGS
    ctx.RegA() = 10;
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    _EXEC_COL_BLOCKROW(ex0, 0, 0);
    EXPECT_EQ(ctx.RegA(), 11);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);

    ctx.RegA() = 0x7F;
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    _EXEC_COL_BLOCKROW(ex0, 0, 0);
    EXPECT_EQ(ctx.RegA(), 0x80);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgOVERFLOW));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));

    ctx.RegA() = 0xFF;
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    _EXEC_COL_BLOCKROW(ex0, 0, 0);
    EXPECT_EQ(ctx.RegA(), 0);
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgOVERFLOW));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));

    ctx.RegA() = 10;
    ctx.ClearFlag(ktools::emulator::MachineFlags::fgCARRY);
    _EXEC_COL_BLOCKROW(ex0, 0, 0);
    EXPECT_EQ(ctx.RegA(), 10);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);

    ctx.RegA() = 0x7F;
    _EXEC_COL_BLOCKROW(ex0, 0, 0);
    EXPECT_EQ(ctx.RegA(), 0x7F);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);

    ctx.RegA() = 0;
    _EXEC_COL_BLOCKROW(ex0, 0, 0);
    EXPECT_EQ(ctx.RegA(), 0);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgZERO);

    ctx.Reset();
    _FILL_REGS
    ctx.RegB() = 10;
    _EXEC_COL_BLOCKROW(ex0, 0, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 10);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    _EXEC_COL_BLOCKROW(ex0, 0, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 11);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegC() = 10;
    _EXEC_COL_BLOCKROW(ex0, 0, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 10);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    _EXEC_COL_BLOCKROW(ex0, 0, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 11);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegD() = 10;
    _EXEC_COL_BLOCKROW(ex0, 0, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 10);
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    _EXEC_COL_BLOCKROW(ex0, 0, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 11);
}

// :: DIC R?
TEST(ExecutionExtended0, TestExtendedCol_0b1) {
    _EXEC_TEST_BEGIN;

    _FILL_REGS
    ctx.RegA() = 20;
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    _EXEC_COL_BLOCKROW(ex0, 1, 0);
    EXPECT_EQ(ctx.RegA(), 19);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);

    ctx.RegA() = 0x80;
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    _EXEC_COL_BLOCKROW(ex0, 1, 0);
    EXPECT_EQ(ctx.RegA(), 0x7F);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgOVERFLOW));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));

    ctx.RegA() = 1;
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    _EXEC_COL_BLOCKROW(ex0, 1, 0);
    EXPECT_EQ(ctx.RegA(), 0);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgOVERFLOW));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));

    ctx.RegA() = 20;
    _EXEC_COL_BLOCKROW(ex0, 1, 0);
    EXPECT_EQ(ctx.RegA(), 20);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);

    ctx.RegA() = 0x80;
    _EXEC_COL_BLOCKROW(ex0, 1, 0);
    EXPECT_EQ(ctx.RegA(), 0x80);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgSIGNED);

    ctx.RegA() = 1;
    _EXEC_COL_BLOCKROW(ex0, 1, 0);
    EXPECT_EQ(ctx.RegA(), 1);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);

    ctx.Reset();
    _FILL_REGS
    ctx.RegB() = 20;
    _EXEC_COL_BLOCKROW(ex0, 1, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 20);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    _EXEC_COL_BLOCKROW(ex0, 1, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 19);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegC() = 20;
    _EXEC_COL_BLOCKROW(ex0, 1, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 20);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    _EXEC_COL_BLOCKROW(ex0, 1, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 19);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    ctx.Reset();
    _FILL_REGS
    ctx.RegD() = 20;
    _EXEC_COL_BLOCKROW(ex0, 1, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 20);
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    _EXEC_COL_BLOCKROW(ex0, 1, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 19);
}

// :: IIC [R?]
TEST(ExecutionExtended0, TestExtendedCol_0b2) {
    _EXEC_TEST_BEGIN_MEM;

    ctx.RegA() = 16;
    ctx.WriteMemory<karch_word_t>(16, 10);
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    _EXEC_COL_BLOCKROW(ex0, 2, 0);
    EXPECT_EQ(ctx.ReadOne(16), 11);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);
    ctx.WriteMemory<karch_word_t>(16, 0x7F);
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    _EXEC_COL_BLOCKROW(ex0, 2, 0);
    EXPECT_EQ(ctx.ReadOne(16), 0x80);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgOVERFLOW));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
    ctx.WriteMemory<karch_word_t>(16, 0xFF);
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    _EXEC_COL_BLOCKROW(ex0, 2, 0);
    EXPECT_EQ(ctx.ReadOne(16), 0);
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgOVERFLOW));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));

    ctx.WriteMemory<karch_word_t>(16, 10);
    ctx.ClearFlag(ktools::emulator::MachineFlags::fgCARRY);
    _EXEC_COL_BLOCKROW(ex0, 2, 0);
    EXPECT_EQ(ctx.ReadOne(16), 10);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);
    ctx.WriteMemory<karch_word_t>(16, 0x7F);
    _EXEC_COL_BLOCKROW(ex0, 2, 0);
    EXPECT_EQ(ctx.ReadOne(16), 0x7F);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);
    ctx.WriteMemory<karch_word_t>(16, 0);
    _EXEC_COL_BLOCKROW(ex0, 2, 0);
    EXPECT_EQ(ctx.ReadOne(16), 0);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgZERO);

    ctx.CurrentMemoryBank() = 2;
    ctx.WriteMemory<karch_word_t>(ctx.GetBankOffset(16), 10);
    _EXEC_COL_BLOCKROW(ex0, 2, 0);
    EXPECT_EQ(ctx.ReadOne(ctx.GetBankOffset(16)), 10);
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    _EXEC_COL_BLOCKROW(ex0, 2, 0);
    EXPECT_EQ(ctx.ReadOne(ctx.GetBankOffset(16)), 11);

    ctx.Reset();
    ctx.WriteMemory<karch_word_t>(16, 10);
    ctx.RegB() = 16;
    _EXEC_COL_BLOCKROW(ex0, 2, 0);
    EXPECT_EQ(ctx.ReadOne(ctx.GetBankOffset(16)), 10);
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    _EXEC_COL_BLOCKROW(ex0, 2, 1);
    EXPECT_EQ(ctx.ReadOne(16), 11);

    ctx.Reset();
    ctx.WriteMemory<karch_word_t>(16, 10);
    ctx.RegC() = 16;
    _EXEC_COL_BLOCKROW(ex0, 2, 0);
    EXPECT_EQ(ctx.ReadOne(ctx.GetBankOffset(16)), 10);
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    _EXEC_COL_BLOCKROW(ex0, 2, 2);
    EXPECT_EQ(ctx.ReadOne(16), 11);

    ctx.Reset();
    ctx.WriteMemory<karch_word_t>(16, 10);
    ctx.RegD() = 16;
    _EXEC_COL_BLOCKROW(ex0, 2, 0);
    EXPECT_EQ(ctx.ReadOne(ctx.GetBankOffset(16)), 10);
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    _EXEC_COL_BLOCKROW(ex0, 2, 3);
    EXPECT_EQ(ctx.ReadOne(16), 11);
}

// :: DIC [R?]
TEST(ExecutionExtended0, TestExtendedCol_0b3) {
    _EXEC_TEST_BEGIN_MEM;

    ctx.RegA() = 16;
    ctx.WriteMemory<karch_word_t>(16, 20);
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    _EXEC_COL_BLOCKROW(ex0, 3, 0);
    EXPECT_EQ(ctx.ReadOne(16), 19);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);
    ctx.WriteMemory<karch_word_t>(16, 0x80);
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    _EXEC_COL_BLOCKROW(ex0, 3, 0);
    EXPECT_EQ(ctx.ReadOne(16), 0x7F);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgOVERFLOW));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
    ctx.WriteMemory<karch_word_t>(16, 1);
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    _EXEC_COL_BLOCKROW(ex0, 3, 0);
    EXPECT_EQ(ctx.ReadOne(16), 0);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgOVERFLOW));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));

    ctx.WriteMemory<karch_word_t>(16, 20);
    _EXEC_COL_BLOCKROW(ex0, 3, 0);
    EXPECT_EQ(ctx.ReadOne(16), 20);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);
    ctx.WriteMemory<karch_word_t>(16, 0x80);
    _EXEC_COL_BLOCKROW(ex0, 3, 0);
    EXPECT_EQ(ctx.ReadOne(16), 0x80);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgSIGNED);
    ctx.WriteMemory<karch_word_t>(16, 0);
    _EXEC_COL_BLOCKROW(ex0, 3, 0);
    EXPECT_EQ(ctx.ReadOne(16), 0);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgZERO);

    ctx.CurrentMemoryBank() = 2;
    ctx.WriteMemory<karch_word_t>(ctx.GetBankOffset(16), 20);
    _EXEC_COL_BLOCKROW(ex0, 2, 0);
    EXPECT_EQ(ctx.ReadOne(ctx.GetBankOffset(16)), 20);
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    _EXEC_COL_BLOCKROW(ex0, 3, 0);
    EXPECT_EQ(ctx.ReadOne(ctx.GetBankOffset(16)), 19);

    ctx.Reset();
    ctx.WriteMemory<karch_word_t>(16, 20);
    ctx.RegB() = 16;
    _EXEC_COL_BLOCKROW(ex0, 2, 0);
    EXPECT_EQ(ctx.ReadOne(16), 20);
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    _EXEC_COL_BLOCKROW(ex0, 3, 1);
    EXPECT_EQ(ctx.ReadOne(16), 19);

    ctx.Reset();
    ctx.WriteMemory<karch_word_t>(16, 20);
    ctx.RegC() = 16;
    _EXEC_COL_BLOCKROW(ex0, 2, 0);
    EXPECT_EQ(ctx.ReadOne(16), 20);
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    _EXEC_COL_BLOCKROW(ex0, 3, 2);
    EXPECT_EQ(ctx.ReadOne(16), 19);

    ctx.Reset();
    ctx.WriteMemory<karch_word_t>(16, 20);
    ctx.RegD() = 16;
    _EXEC_COL_BLOCKROW(ex0, 2, 0);
    EXPECT_EQ(ctx.ReadOne(16), 20);
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    _EXEC_COL_BLOCKROW(ex0, 3, 3);
    EXPECT_EQ(ctx.ReadOne(16), 19);
}
