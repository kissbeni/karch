
/**
 * @file test_emulator.cc
 * @author Kiss Benedek
 * @brief Automatic tests for the emulator
 */

#include "test_emulator.h"

#include <emulator/gui/DisplayWindow.h>

// ---------------------------------------------------------------------------------- //
// ---- Context Tests --------------------------------------------------------------- //
// ---------------------------------------------------------------------------------- //

TEST(MachineContext, TestProperties) {
    _NEW_CONTEXT(c1, 1);
    c1.CurrentMemoryBank() = 40;
    EXPECT_EQ(c1.CurrentMemoryBank(), 40);
    c1.RegA() = 14;
    EXPECT_EQ(c1.RegA(), 14);
    c1.RegB() = 34;
    EXPECT_EQ(c1.RegB(), 34);
    c1.RegC() = 211;
    EXPECT_EQ(c1.RegC(), 211);
    c1.RegD() = 55;
    EXPECT_EQ(c1.RegD(), 55);
    c1.IP() = 1234;
    EXPECT_EQ(c1.IP(), 1234);
    c1.SP() = 12234;
    EXPECT_EQ(c1.SP(), 12234);
    c1.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    EXPECT_EQ(c1.FlagSet(), ktools::emulator::MachineFlags::fgCARRY);
}

TEST(MachineContext, TestAssign) {
    _NEW_CONTEXT(c1, 64);
    c1.RegA() = 1;
    c1.RegB() = 2;
    c1.RegC() = 3;
    c1.RegD() = 4;
    c1.IP() = 10;
    c1.SP() = 40;
    c1.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    c1.Push((karch_word_t)0x20);

    _NEW_CONTEXT(c2, 64);
    c2.RegA() = 5;
    c2.RegB() = 6;
    c2.RegC() = 7;
    c2.RegD() = 8;
    c2.IP() = 20;
    c2.SP() = 40;
    c2.SetFlag(ktools::emulator::MachineFlags::fgZERO);
    c2.Push((karch_word_t)0x44);
    c2.SP() = 50;

    c2 = c1;
    EXPECT_EQ(c2.RegA(), c1.RegA());
    EXPECT_EQ(c2.RegB(), c1.RegB());
    EXPECT_EQ(c2.RegC(), c1.RegC());
    EXPECT_EQ(c2.RegD(), c1.RegD());
    EXPECT_EQ(c2.IP(), c1.IP());
    EXPECT_EQ(c2.SP(), c1.SP());
    EXPECT_EQ(c2.FlagSet(), ktools::emulator::MachineFlags::fgCARRY);
    karch_word_t s1 = c1.Pop<karch_word_t>();
    karch_word_t s2 = c2.Pop<karch_word_t>();
    EXPECT_NE(s1, s2);
    EXPECT_EQ(s1, 0x20);
    EXPECT_EQ(s2, 0x44);
}

TEST(MachineContext, TestMemory) {
    _NEW_CONTEXT(c1, 100);
    uint32_t val = 0xDEADBEEFu, readval;
    karch_word_t tmp;
    c1.WriteMemory(0, val);
    EXPECT_THROW(c1.WriteMemory(100, val), std::out_of_range);
    EXPECT_NO_THROW(c1.ReadMemory<uint32_t>(100 - sizeof(val)));
    EXPECT_THROW(c1.ReadMemory<uint32_t>(100), std::out_of_range);
    EXPECT_EQ(c1.ReadMemory<uint32_t>(0), val);
    c1.WriteMemoryBulk(5, (karch_word_t*)&val, 0, sizeof(val));
    c1.ReadMemoryBulk(5, (karch_word_t*)&readval, 0, sizeof(val));
    EXPECT_EQ(val, readval);
    EXPECT_EQ(c1.ReadOne(0), *reinterpret_cast<karch_word_t*>(&val));
    EXPECT_THROW(c1.ReadMemoryBulk(101, &tmp, 0, 1), std::out_of_range);
    EXPECT_THROW(c1.WriteMemoryBulk(101, &tmp, 0, 1), std::out_of_range);
    c1.DumpMem();
}

TEST(MachineContext, TestMemoryBanks) {
    _NEW_CONTEXT(c1, 512);
    c1.CurrentMemoryBank() = 1;
    EXPECT_EQ(c1.GetBankOffset(0), 256);
    EXPECT_EQ(c1.GetBankOffset(100), 356);
    c1.CurrentMemoryBank() = 0;
    c1.WriteMemory(c1.GetBankOffset(0), 12345678);
    c1.CurrentMemoryBank() = 1;
    EXPECT_NE(c1.ReadMemory<int>(c1.GetBankOffset(0)), 12345678);
    c1.WriteMemory(c1.GetBankOffset(0), 87654321);
    c1.CurrentMemoryBank() = 0;
    EXPECT_EQ(c1.ReadMemory<int>(c1.GetBankOffset(0)), 12345678);
    c1.CurrentMemoryBank() = 1;
    EXPECT_EQ(c1.ReadMemory<int>(c1.GetBankOffset(0)), 87654321);
}

TEST(MachineContext, TestStack) {
    _NEW_CONTEXT(c1, 2048);
    c1.SP() = 1024;
    c1.Push((karch_word_t)0x11);
    EXPECT_EQ(c1.ReadMemory<karch_word_t>(c1.SP()), 0x11);
    EXPECT_EQ(c1.Pop<karch_word_t>(), 0x11);
    EXPECT_EQ(c1.SP(), 1024);
}

TEST(MachineContext, TestMisc) {
    _NEW_CONTEXT(c1, 1);
    c1.IP() = 10;
    c1.RelativeJump((karch_saddr_t)-5);
    EXPECT_EQ(c1.IP(), 3);
    c1.RelativeJump((karch_word_t)15);
    EXPECT_EQ(c1.IP(), 16);

    for (int i = 0; i <= c1.MAX_REGISTER_INDEX; i++)
        c1.Reg(i) = gTestRegValues[i];

    EXPECT_EQ(c1.Reg(0), c1.RegA());
    EXPECT_EQ(c1.Reg(1), c1.RegB());
    EXPECT_EQ(c1.Reg(2), c1.RegC());
    EXPECT_EQ(c1.Reg(3), c1.RegD());
    EXPECT_THROW(c1.Reg(99), std::out_of_range);

    c1.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    c1.SetFlag(ktools::emulator::MachineFlags::fgZERO);

    EXPECT_TRUE(c1.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_TRUE(c1.FlagSet(ktools::emulator::MachineFlags::fgZERO));
    EXPECT_FALSE(c1.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_FALSE(c1.FlagSet(ktools::emulator::MachineFlags::fgALL));
}

// ---------------------------------------------------------------------------------- //
// ---- VirtualMachine tests -------------------------------------------------------- //
// ---------------------------------------------------------------------------------- //

TEST(VirtualMachine, TestInitialState) {
    _NEW_VM(vm1);
    EXPECT_LT(vm1.GetCurrentContext()->IP(), vm1.GetCurrentContext()->SP());
    EXPECT_EQ(vm1.GetCurrentContext()->RegA(), 0);
    EXPECT_EQ(vm1.GetCurrentContext()->RegB(), 0);
    EXPECT_EQ(vm1.GetCurrentContext()->RegC(), 0);
    EXPECT_EQ(vm1.GetCurrentContext()->RegD(), 0);
}

TEST(VirtualMachine, TestLoad) {
    _NEW_VM(vm1);
    std::istringstream ss(std::string("\x00\x00\x00\x00\xF0", 5));
    /*
     * 0000: 00           : NOP
     * 0001: 00           : NOP
     * 0002: 00           : NOP
     * 0003: 00           : NOP
     * 0004: f0           : HLT
     */
    vm1.Load(ss, vm1.GetCurrentContext()->IP());
    karch_addr_t initialIP = vm1.GetCurrentContext()->IP();

    for (int i = 0; i < 4; i++)
        vm1.Step();

    EXPECT_EQ(vm1.GetCurrentContext()->IP(), initialIP + 4);
    EXPECT_FALSE(vm1.IsHalted());

    vm1.Step();
    EXPECT_EQ(vm1.GetCurrentContext()->IP(), initialIP + 5);
    EXPECT_TRUE(vm1.IsHalted());
}

TEST(VirtualMachine, TestStepAfterHalt) {
    _NEW_VM(vm1);
    std::istringstream ss(std::string("\x00\x00\x00\x00\xF0", 5));
    /*
     * 0000: 00           : NOP
     * 0001: 00           : NOP
     * 0002: 00           : NOP
     * 0003: 00           : NOP
     * 0004: f0           : HLT
     */
    vm1.Load(ss, vm1.GetCurrentContext()->IP());
    karch_addr_t initialIP = vm1.GetCurrentContext()->IP();

    for (int i = 0; i < 100; i++)
        vm1.Step();

    EXPECT_EQ(vm1.GetCurrentContext()->IP(), initialIP + 5);
    EXPECT_TRUE(vm1.IsHalted());
}

TEST(VirtualMachine, TestExecSimple) {
    _NEW_VM(vm1);
    std::istringstream ss(std::string("\x01\x05\x11\x02\x82\x0C\x26\x23\x0C\xF0", 10));
    /*
     * 0000: 01 05        : MOV RA, 5
     * 0002: 11 02        : MOV RB, 2
     * 0004: 82           : MOV RC, RA
     * 0005: 0c           : XOR RA, RA
     * 0006: 26           : PUSH RC
     * 0007: 23 0c        : ADD RC, 12
     * 0009: f0           : HLT
     */
    vm1.Load(ss, vm1.GetCurrentContext()->IP());
    karch_addr_t initialIP = vm1.GetCurrentContext()->IP();

    for (int i = 0; i < 100; i++)
        vm1.Step();

    EXPECT_EQ(vm1.GetCurrentContext()->IP(), initialIP + 10);
    EXPECT_EQ(vm1.GetCurrentContext()->RegA(), 0);
    EXPECT_EQ(vm1.GetCurrentContext()->RegB(), 2);
    EXPECT_EQ(vm1.GetCurrentContext()->RegC(), 17);
    EXPECT_EQ(vm1.GetCurrentContext()->RegD(), 0);
    EXPECT_EQ(vm1.GetCurrentContext()->Pop<karch_word_t>(), 5);
    EXPECT_TRUE(vm1.IsHalted());
}

TEST(VirtualMachine, TestExecComplex) {
    _NEW_VM(vm1);
    std::istringstream ss(std::string("\x01\x07\x11\x2a\x82\x81\x0c\xab\x01\x81\x14\xab\x00\xd0\x02\xd0\x14\x0d\x06\xd0\x00\xc2\xab\x01\x01\x07\xd0\x02\xd0\xc6\xf0", 31));
    /*
     * 0000: 01 07        : MOV RA, 7
     * 0002: 11 2a        : MOV RB, 42
     * 0004: 82           : MOV RC, RA
     * 0005: 81 0c        : MOV [RA], 12
     * 0007: ab 01        : BNK 1
     * 0009: 81 14        : MOV [RA], 20
     * 000b: ab 00        : BNK 0
     * 000d: d0 02        : MOV RA, [RA]
     * 000f: d0 14        : ADD RA, RB
     * 0011: 0d 06        : MUL RA, 6
     * 0013: d0 00        : IIC RA
     * 0015: c2           : MOV RD, RA
     * 0016: ab 01        : BNK 1
     * 0018: 01 07        : MOV RA, 7
     * 001a: d0 02        : MOV RA, [RA]
     * 001c: d0 c6        : SUB RD, RA
     * 001e: f0           : HLT
     */
    vm1.Load(ss, vm1.GetCurrentContext()->IP());
    karch_addr_t initialIP = vm1.GetCurrentContext()->IP();

    for (int i = 0; i < 100; i++)
        vm1.Step();

    EXPECT_EQ(vm1.GetCurrentContext()->IP(), initialIP + 31);
    EXPECT_EQ(vm1.GetCurrentContext()->RegA(), 20);
    EXPECT_EQ(vm1.GetCurrentContext()->RegB(), 42);
    EXPECT_EQ(vm1.GetCurrentContext()->RegC(), 7);
    EXPECT_EQ(vm1.GetCurrentContext()->RegD(), 48);
    EXPECT_TRUE(vm1.IsHalted());
}

TEST(VirtualMachine, TestExecJumpy) {
    _NEW_VM(vm1);
    std::istringstream ss(std::string("\x01\x01\x11\x02\x21\x00\x1b\x05\x24\x1b\x06\xd0\x14\x3b\xfb\xd0\x64\x9b\xf7\xf0", 20));
    /*
     * 0000: 01 01        : MOV RA, 1
     * 0002: 11 02        : MOV RB, 2
     * 0004: 21 00        : MOV RC, 0
     * 0006: 1b 05        : JMP $+5 <label2> [EA:0x000b]
     * label1: <0x8>
     * 0008: 24           : INC RC
     * 0009: 1b 06        : JMP $+6 <label3> [EA:0x000f]
     * label2: <0xb>
     * 000b: d0 14        : ADD RA, RB
     * 000d: 3b fb        : JNE $-5 <label1> [EA:0x0008]
     * label3: <0xf>
     * 000f: d0 64        : ADD RB, RC
     * 0011: 9b f7        : JNS $-9 <label1> [EA:0x0008]
     * 0013: f0           : HLT
     */
    vm1.Load(ss, vm1.GetCurrentContext()->IP());
    karch_addr_t initialIP = vm1.GetCurrentContext()->IP();

    for (int i = 0; i < 500; i++)
        vm1.Step();

    EXPECT_EQ(vm1.GetCurrentContext()->IP(), initialIP + 20);
    EXPECT_EQ(vm1.GetCurrentContext()->RegA(), 3);
    EXPECT_EQ(vm1.GetCurrentContext()->RegB(), 138);
    EXPECT_EQ(vm1.GetCurrentContext()->RegC(), 16);
    EXPECT_EQ(vm1.GetCurrentContext()->RegD(), 0);
    EXPECT_TRUE(vm1.IsHalted());
}

int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
