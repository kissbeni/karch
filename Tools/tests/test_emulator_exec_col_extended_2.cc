
/**
 * @file test_emulator_exec_col_extended_2.cc
 * @author Kiss Benedek
 * @brief Automated tests for emulator (extended column 2)
 */

#include "test_emulator.h"

TEST(ExecutionExtended2, TestExtendedCol_2_BAD) {
    _EXEC_TEST_BEGIN;
    _EXEC_COL_BAD_INST(ex2)
}

// :: MOV RA, [R?]
TEST(ExecutionExtended2, TestExtendedCol_2b0) {
    _EXEC_TEST_BEGIN_MEM;
    ctx.CurrentMemoryBank() = 2;
    ctx.WriteMemory(ctx.GetBankOffset(20), 35);
    ctx.WriteMemory(ctx.GetBankOffset(40), 62);
    ctx.CurrentMemoryBank() = 0;
    ctx.WriteMemory(ctx.GetBankOffset(20), 40);

    // :: MOV RA, [RA]
    _FILL_REGS
    ctx.RegA() = 20;
    _EXEC_COL_BLOCKROW(ex2, 0, 0);
    EXPECT_EQ(ctx.RegA(), 40);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    ctx.CurrentMemoryBank() = 2;
    _EXEC_COL_BLOCKROW(ex2, 0, 0);
    EXPECT_EQ(ctx.RegA(), 62);

    // :: MOV RA, [RB]
    ctx.Reset();
    _FILL_REGS
    ctx.RegB() = 20;
    _EXEC_COL_BLOCKROW(ex2, 0, 1);
    EXPECT_EQ(ctx.RegA(), 40);
    EXPECT_EQ(ctx.RegB(), 20);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    ctx.CurrentMemoryBank() = 2;
    _EXEC_COL_BLOCKROW(ex2, 0, 1);
    EXPECT_EQ(ctx.RegA(), 35);

    // :: MOV RA, [RC]
    ctx.Reset();
    _FILL_REGS
    ctx.RegC() = 20;
    _EXEC_COL_BLOCKROW(ex2, 0, 2);
    EXPECT_EQ(ctx.RegA(), 40);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 20);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    ctx.CurrentMemoryBank() = 2;
    _EXEC_COL_BLOCKROW(ex2, 0, 2);
    EXPECT_EQ(ctx.RegA(), 35);

    // :: MOV RA, [RD]
    ctx.Reset();
    _FILL_REGS
    ctx.RegD() = 20;
    _EXEC_COL_BLOCKROW(ex2, 0, 3);
    EXPECT_EQ(ctx.RegA(), 40);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 20);
    ctx.CurrentMemoryBank() = 2;
    _EXEC_COL_BLOCKROW(ex2, 0, 3);
    EXPECT_EQ(ctx.RegA(), 35);
}

// :: MOV RB, [R?]
TEST(ExecutionExtended2, TestExtendedCol_2b1) {
    _EXEC_TEST_BEGIN_MEM;
    ctx.CurrentMemoryBank() = 2;
    ctx.WriteMemory(ctx.GetBankOffset(20), 35);
    ctx.WriteMemory(ctx.GetBankOffset(40), 62);
    ctx.CurrentMemoryBank() = 0;
    ctx.WriteMemory(ctx.GetBankOffset(20), 40);

    // :: MOV RB, [RA]
    _FILL_REGS
    ctx.RegA() = 20;
    _EXEC_COL_BLOCKROW(ex2, 1, 0);
    EXPECT_EQ(ctx.RegA(), 20);
    EXPECT_EQ(ctx.RegB(), 40);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    ctx.CurrentMemoryBank() = 2;
    _EXEC_COL_BLOCKROW(ex2, 1, 0);
    EXPECT_EQ(ctx.RegB(), 35);

    // :: MOV RB, [RB]
    ctx.Reset();
    _FILL_REGS
    ctx.RegB() = 20;
    _EXEC_COL_BLOCKROW(ex2, 1, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 40);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    ctx.CurrentMemoryBank() = 2;
    _EXEC_COL_BLOCKROW(ex2, 1, 1);
    EXPECT_EQ(ctx.RegB(), 62);

    // :: MOV RB, [RC]
    ctx.Reset();
    _FILL_REGS
    ctx.RegC() = 20;
    _EXEC_COL_BLOCKROW(ex2, 1, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 40);
    EXPECT_EQ(ctx.RegC(), 20);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    ctx.CurrentMemoryBank() = 2;
    _EXEC_COL_BLOCKROW(ex2, 1, 2);
    EXPECT_EQ(ctx.RegB(), 35);

    // :: MOV RB, [RD]
    ctx.Reset();
    _FILL_REGS
    ctx.RegD() = 20;
    _EXEC_COL_BLOCKROW(ex2, 1, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 40);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 20);
    ctx.CurrentMemoryBank() = 2;
    _EXEC_COL_BLOCKROW(ex2, 1, 3);
    EXPECT_EQ(ctx.RegB(), 35);
}

// :: MOV RC, [R?]
TEST(ExecutionExtended2, TestExtendedCol_2b2) {
    _EXEC_TEST_BEGIN_MEM;
    ctx.CurrentMemoryBank() = 2;
    ctx.WriteMemory(ctx.GetBankOffset(20), 35);
    ctx.WriteMemory(ctx.GetBankOffset(40), 62);
    ctx.CurrentMemoryBank() = 0;
    ctx.WriteMemory(ctx.GetBankOffset(20), 40);

    // :: MOV RC, [RA]
    _FILL_REGS
    ctx.RegA() = 20;
    _EXEC_COL_BLOCKROW(ex2, 2, 0);
    EXPECT_EQ(ctx.RegA(), 20);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 40);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    ctx.CurrentMemoryBank() = 2;
    _EXEC_COL_BLOCKROW(ex2, 2, 0);
    EXPECT_EQ(ctx.RegC(), 35);

    // :: MOV RC, [RB]
    ctx.Reset();
    _FILL_REGS
    ctx.RegB() = 20;
    _EXEC_COL_BLOCKROW(ex2, 2, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 20);
    EXPECT_EQ(ctx.RegC(), 40);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    ctx.CurrentMemoryBank() = 2;
    _EXEC_COL_BLOCKROW(ex2, 2, 1);
    EXPECT_EQ(ctx.RegC(), 35);

    // :: MOV RC, [RC]
    ctx.Reset();
    _FILL_REGS
    ctx.RegC() = 20;
    _EXEC_COL_BLOCKROW(ex2, 2, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 40);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    ctx.CurrentMemoryBank() = 2;
    _EXEC_COL_BLOCKROW(ex2, 2, 2);
    EXPECT_EQ(ctx.RegC(), 62);

    // :: MOV RC, [RD]
    ctx.Reset();
    _FILL_REGS
    ctx.RegD() = 20;
    _EXEC_COL_BLOCKROW(ex2, 2, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 40);
    EXPECT_EQ(ctx.RegD(), 20);
    ctx.CurrentMemoryBank() = 2;
    _EXEC_COL_BLOCKROW(ex2, 2, 3);
    EXPECT_EQ(ctx.RegC(), 35);
}

// :: MOV RD, [R?]
TEST(ExecutionExtended2, TestExtendedCol_2b3) {
    _EXEC_TEST_BEGIN_MEM;
    ctx.CurrentMemoryBank() = 2;
    ctx.WriteMemory(ctx.GetBankOffset(20), 35);
    ctx.WriteMemory(ctx.GetBankOffset(40), 62);
    ctx.CurrentMemoryBank() = 0;
    ctx.WriteMemory(ctx.GetBankOffset(20), 40);

    // :: MOV RD, [RA]
    _FILL_REGS
    ctx.RegA() = 20;
    _EXEC_COL_BLOCKROW(ex2, 3, 0);
    EXPECT_EQ(ctx.RegA(), 20);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 40);
    ctx.CurrentMemoryBank() = 2;
    _EXEC_COL_BLOCKROW(ex2, 3, 0);
    EXPECT_EQ(ctx.RegD(), 35);

    // :: MOV RD, [RB]
    ctx.Reset();
    _FILL_REGS
    ctx.RegB() = 20;
    _EXEC_COL_BLOCKROW(ex2, 3, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 20);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 40);
    ctx.CurrentMemoryBank() = 2;
    _EXEC_COL_BLOCKROW(ex2, 3, 1);
    EXPECT_EQ(ctx.RegD(), 35);

    // :: MOV RD, [RC]
    ctx.Reset();
    _FILL_REGS
    ctx.RegC() = 20;
    _EXEC_COL_BLOCKROW(ex2, 3, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 20);
    EXPECT_EQ(ctx.RegD(), 40);
    ctx.CurrentMemoryBank() = 2;
    _EXEC_COL_BLOCKROW(ex2, 3, 2);
    EXPECT_EQ(ctx.RegD(), 35);

    // :: MOV RD, [RD]
    ctx.Reset();
    _FILL_REGS
    ctx.RegD() = 20;
    _EXEC_COL_BLOCKROW(ex2, 3, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 40);
    ctx.CurrentMemoryBank() = 2;
    _EXEC_COL_BLOCKROW(ex2, 3, 3);
    EXPECT_EQ(ctx.RegD(), 62);
}
