
/**
 * @file test_emulator_exec_col_standard_C.cc
 * @author Kiss Benedek
 * @brief Automated tests for emulator (standard column C)
 */

#include "test_emulator.h"

TEST(ExecutionStandardC, TestStandardCol_C_BAD) {
    _EXEC_TEST_BEGIN;
    _EXEC_COL_BAD_INST(C)
}

// :: XOR RA, R?
TEST(ExecutionStandardC, TestStandardCol_Cb0) {
    _EXEC_TEST_BEGIN;

    _FILL_REGS
    ctx.RegA() = 0x2C;
    _EXEC_COL_BLOCKROW(C, 0, 0);
    EXPECT_EQ(ctx.RegA(), 0);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));

    _FILL_REGS
    ctx.RegB() = 0x33;
    ctx.RegA() = 0x2C;
    ctx.SetFlag(ktools::emulator::MachineFlags::fgALL);
    _EXEC_COL_BLOCKROW(C, 0, 1);
    EXPECT_EQ(ctx.RegA(), 0x1F);
    EXPECT_EQ(ctx.RegB(), 0x33);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
    ctx.SetFlag(ktools::emulator::MachineFlags::fgZERO);
    ctx.SetFlag(ktools::emulator::MachineFlags::fgSIGNED);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgALL));
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgALL));
    ctx.SetFlag(ktools::emulator::MachineFlags::fgOVERFLOW);
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgALL));

    _FILL_REGS
    ctx.RegC() = 0x33;
    ctx.RegA() = 0x2C;
    _EXEC_COL_BLOCKROW(C, 0, 2);
    EXPECT_EQ(ctx.RegA(), 0x1F);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 0x33);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.RegD() = 0x33;
    ctx.RegA() = 0x2C;
    _EXEC_COL_BLOCKROW(C, 0, 3);
    EXPECT_EQ(ctx.RegA(), 0x1F);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 0x33);

    ctx.RegA() = 0xAC;
    ctx.RegB() = 0x33;
    _EXEC_COL_BLOCKROW(C, 0, 1);
    EXPECT_EQ(ctx.RegA(), 0x9F);
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
}

// :: XOR RB, R?
TEST(ExecutionStandardC, TestStandardCol_Cb1) {
    _EXEC_TEST_BEGIN;

    _FILL_REGS
    ctx.RegA() = 0x33;
    ctx.RegB() = 0x2C;
    _EXEC_COL_BLOCKROW(C, 1, 0);
    EXPECT_EQ(ctx.RegA(), 0x33);
    EXPECT_EQ(ctx.RegB(), 0x1F);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.RegB() = 0x2C;
    _EXEC_COL_BLOCKROW(C, 1, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 0);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.RegC() = 0x33;
    ctx.RegB() = 0x2C;
    _EXEC_COL_BLOCKROW(C, 1, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 0x1F);
    EXPECT_EQ(ctx.RegC(), 0x33);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.RegD() = 0x33;
    ctx.RegB() = 0x2C;
    _EXEC_COL_BLOCKROW(C, 1, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 0x1F);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 0x33);
}

// :: XOR RC, R?
TEST(ExecutionStandardC, TestStandardCol_Cb2) {
    _EXEC_TEST_BEGIN;

    _FILL_REGS
    ctx.RegA() = 0x33;
    ctx.RegC() = 0x2C;
    _EXEC_COL_BLOCKROW(C, 2, 0);
    EXPECT_EQ(ctx.RegA(), 0x33);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 0x1F);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.RegB() = 0x33;
    ctx.RegC() = 0x2C;
    _EXEC_COL_BLOCKROW(C, 2, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 0x33);
    EXPECT_EQ(ctx.RegC(), 0x1F);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.RegC() = 0x2C;
    _EXEC_COL_BLOCKROW(C, 2, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 0);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.RegD() = 0x33;
    ctx.RegC() = 0x2C;
    _EXEC_COL_BLOCKROW(C, 2, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 0x1F);
    EXPECT_EQ(ctx.RegD(), 0x33);
}

// :: XOR RD, R?
TEST(ExecutionStandardC, TestStandardCol_Cb3) {
    _EXEC_TEST_BEGIN;

    _FILL_REGS
    ctx.RegA() = 0x33;
    ctx.RegD() = 0x2C;
    _EXEC_COL_BLOCKROW(C, 3, 0);
    EXPECT_EQ(ctx.RegA(), 0x33);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 0x1F);

    _FILL_REGS
    ctx.RegB() = 0x33;
    ctx.RegD() = 0x2C;
    _EXEC_COL_BLOCKROW(C, 3, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 0x33);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 0x1F);

    _FILL_REGS
    ctx.RegC() = 0x33;
    ctx.RegD() = 0x2C;
    _EXEC_COL_BLOCKROW(C, 3, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 0x33);
    EXPECT_EQ(ctx.RegD(), 0x1F);

    _FILL_REGS
    ctx.RegD() = 0x2C;
    _EXEC_COL_BLOCKROW(C, 3, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 0);
}
