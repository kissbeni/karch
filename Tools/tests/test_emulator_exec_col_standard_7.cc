
/**
 * @file test_emulator_exec_col_standard_7.cc
 * @author Kiss Benedek
 * @brief Automated tests for emulator (standard column 7)
 */

#include "test_emulator.h"

TEST(ExecutionStandard7, TestStandardCol_7_BAD) {
    _EXEC_TEST_BEGIN;
    _EXEC_COL_BAD_INST(7)
}

// :: AND R?, X
TEST(ExecutionStandard7, TestStandardCol_7b0) {
    _EXEC_TEST_BEGIN;
    inst.arg = 0x33;

    _FILL_REGS
    ctx.RegA() = 0xFA;
    ctx.SetFlag(ktools::emulator::MachineFlags::fgALL);
    _EXEC_COL_BLOCKROW(7, 0, 0);
    EXPECT_EQ(ctx.RegA(), 0x32);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
    ctx.SetFlag(ktools::emulator::MachineFlags::fgZERO);
    ctx.SetFlag(ktools::emulator::MachineFlags::fgSIGNED);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgALL));
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgALL));
    ctx.SetFlag(ktools::emulator::MachineFlags::fgOVERFLOW);
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgALL));

    _FILL_REGS
    ctx.RegB() = 0xFA;
    _EXEC_COL_BLOCKROW(7, 0, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 0x32);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.RegC() = 0xFA;
    _EXEC_COL_BLOCKROW(7, 0, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 0x32);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.RegD() = 0xFA;
    _EXEC_COL_BLOCKROW(7, 0, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 0x32);

    ctx.RegA() = 0x44;
    _EXEC_COL_BLOCKROW(7, 0, 0);
    EXPECT_EQ(ctx.RegA(), 0);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));

    inst.arg = 0x80;
    ctx.RegA() = 0xF4;
    _EXEC_COL_BLOCKROW(7, 0, 0);
    EXPECT_EQ(ctx.RegA(), 0x80);
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
}

// :: OR R?, X
TEST(ExecutionStandard7, TestStandardCol_7b1) {
    _EXEC_TEST_BEGIN;
    inst.arg = 0x33;

    _FILL_REGS
    ctx.RegA() = 0x2C;
    ctx.SetFlag(ktools::emulator::MachineFlags::fgALL);
    _EXEC_COL_BLOCKROW(7, 1, 0);
    EXPECT_EQ(ctx.RegA(), 0x3F);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
    ctx.SetFlag(ktools::emulator::MachineFlags::fgZERO);
    ctx.SetFlag(ktools::emulator::MachineFlags::fgSIGNED);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgALL));
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgALL));
    ctx.SetFlag(ktools::emulator::MachineFlags::fgOVERFLOW);
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgALL));

    _FILL_REGS
    ctx.RegB() = 0x2C;
    _EXEC_COL_BLOCKROW(7, 1, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 0x3F);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.RegC() = 0x2C;
    _EXEC_COL_BLOCKROW(7, 1, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 0x3F);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.RegD() = 0x2C;
    _EXEC_COL_BLOCKROW(7, 1, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 0x3F);

    ctx.RegA() = 0xAC;
    _EXEC_COL_BLOCKROW(7, 1, 0);
    EXPECT_EQ(ctx.RegA(), 0xBF);
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));

    inst.arg = 0;
    ctx.RegA() = 0;
    _EXEC_COL_BLOCKROW(7, 1, 0);
    EXPECT_EQ(ctx.RegA(), 0);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
}

// :: XOR R?, X
TEST(ExecutionStandard7, TestStandardCol_7b2) {
    _EXEC_TEST_BEGIN;
    inst.arg = 0x33;

    _FILL_REGS
    ctx.RegA() = 0x2C;
    ctx.SetFlag(ktools::emulator::MachineFlags::fgALL);
    _EXEC_COL_BLOCKROW(7, 2, 0);
    EXPECT_EQ(ctx.RegA(), 0x1F);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
    ctx.SetFlag(ktools::emulator::MachineFlags::fgZERO);
    ctx.SetFlag(ktools::emulator::MachineFlags::fgSIGNED);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgALL));
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgALL));
    ctx.SetFlag(ktools::emulator::MachineFlags::fgOVERFLOW);
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgALL));

    _FILL_REGS
    ctx.RegB() = 0x2C;
    _EXEC_COL_BLOCKROW(7, 2, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 0x1F);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.RegC() = 0x2C;
    _EXEC_COL_BLOCKROW(7, 2, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 0x1F);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.RegD() = 0x2C;
    _EXEC_COL_BLOCKROW(7, 2, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 0x1F);

    ctx.RegA() = 0xAC;
    _EXEC_COL_BLOCKROW(7, 2, 0);
    EXPECT_EQ(ctx.RegA(), 0x9F);
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));

    ctx.RegA() = 0x33;
    _EXEC_COL_BLOCKROW(7, 2, 0);
    EXPECT_EQ(ctx.RegA(), 0);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
}

// :: SHL R?, X
TEST(ExecutionStandard7, TestStandardCol_7b3) {
    _EXEC_TEST_BEGIN;
    inst.arg = 1;

    _FILL_REGS
    ctx.RegA() = 0x2C;
    ctx.SetFlag(ktools::emulator::MachineFlags::fgALL);
    _EXEC_COL_BLOCKROW(7, 3, 0);
    EXPECT_EQ(ctx.RegA(), 0x58);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    ctx.SetFlag(ktools::emulator::MachineFlags::fgZERO);
    ctx.SetFlag(ktools::emulator::MachineFlags::fgSIGNED);
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgALL));
    ctx.SetFlag(ktools::emulator::MachineFlags::fgOVERFLOW);
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgALL));

    _FILL_REGS
    ctx.RegB() = 0x2C;
    _EXEC_COL_BLOCKROW(7, 3, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 0x58);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.RegC() = 0x2C;
    _EXEC_COL_BLOCKROW(7, 3, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 0x58);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.RegD() = 0x2C;
    _EXEC_COL_BLOCKROW(7, 3, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 0x58);

    ctx.RegA() = 0x7D;
    _EXEC_COL_BLOCKROW(7, 3, 0);
    EXPECT_EQ(ctx.RegA(), 0xFA);
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));

    ctx.RegA() = 0x80;
    _EXEC_COL_BLOCKROW(7, 3, 0);
    EXPECT_EQ(ctx.RegA(), 0);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
}
