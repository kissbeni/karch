
/**
 * @file test_assembler.cc
 * @author Kiss Benedek
 * @brief Assembler releated automatic tests
 */

#include <assembler/Assembler.h>
#include <assembler/Instruction.hpp>
#include <assembler/AssemblerException.hpp>

#include <shared/fnv1a.hpp>

#include <gtest/gtest.h>

#include <sstream>

using namespace ktools::assembler;

const char* g_testcode_1 =
"LOOP:\n"
"JMP LOOP\n"
"HLT\n";

TEST(Assembler, RemoveCommentFromLine1) {
    std::string test = "hello world";
    Assembler::RemoveCommentFromLine(test);
    EXPECT_STREQ(test.c_str(), "hello world");

    test = "nop;hello world";
    Assembler::RemoveCommentFromLine(test);
    EXPECT_STREQ(test.c_str(), "nop");

    test = "nop ;hello world";
    Assembler::RemoveCommentFromLine(test);
    EXPECT_STREQ(test.c_str(), "nop ");

    test = "nop\t;hello world";
    Assembler::RemoveCommentFromLine(test);
    EXPECT_STREQ(test.c_str(), "nop\t");

    test = "nop; hello world";
    Assembler::RemoveCommentFromLine(test);
    EXPECT_STREQ(test.c_str(), "nop");

    test = "nop ; hello world";
    Assembler::RemoveCommentFromLine(test);
    EXPECT_STREQ(test.c_str(), "nop ");

    test = "nop\t; hello world";
    Assembler::RemoveCommentFromLine(test);
    EXPECT_STREQ(test.c_str(), "nop\t");
}

TEST(Assembler, RemoveCommentFromLine2) {
    std::string test = "hello world";
    Assembler::RemoveCommentFromLine(test);
    EXPECT_STREQ(test.c_str(), "hello world");

    test = "nop#hello world";
    Assembler::RemoveCommentFromLine(test);
    EXPECT_STREQ(test.c_str(), "nop");

    test = "nop #hello world";
    Assembler::RemoveCommentFromLine(test);
    EXPECT_STREQ(test.c_str(), "nop ");

    test = "nop\t#hello world";
    Assembler::RemoveCommentFromLine(test);
    EXPECT_STREQ(test.c_str(), "nop\t");

    test = "nop# hello world";
    Assembler::RemoveCommentFromLine(test);
    EXPECT_STREQ(test.c_str(), "nop");

    test = "nop # hello world";
    Assembler::RemoveCommentFromLine(test);
    EXPECT_STREQ(test.c_str(), "nop ");

    test = "nop\t# hello world";
    Assembler::RemoveCommentFromLine(test);
    EXPECT_STREQ(test.c_str(), "nop\t");
}

TEST(Assembler, ParseAddress) {
    karch_addr_t addr_out;
    EXPECT_TRUE(Assembler::ParseAddress("100", addr_out));
    EXPECT_EQ(addr_out, 100);

    EXPECT_TRUE(Assembler::ParseAddress("0xDAD", addr_out));
    EXPECT_EQ(addr_out, 0xDAD);

    EXPECT_TRUE(Assembler::ParseAddress("DADh", addr_out));
    EXPECT_EQ(addr_out, 0xDAD);

    EXPECT_TRUE(Assembler::ParseAddress("0b101", addr_out));
    EXPECT_EQ(addr_out, 5);

    addr_out = 0;
    EXPECT_FALSE(Assembler::ParseAddress("0b", addr_out));
    EXPECT_EQ(addr_out, 0);

    EXPECT_FALSE(Assembler::ParseAddress("0x", addr_out));
    EXPECT_EQ(addr_out, 0);

    EXPECT_FALSE(Assembler::ParseAddress("bab", addr_out));
    EXPECT_EQ(addr_out, 0);

    EXPECT_FALSE(Assembler::ParseAddress("", addr_out));
    EXPECT_EQ(addr_out, 0);
}

TEST(Assembler, ParseLabel) {
    Assembler a;

    std::stringstream ss(g_testcode_1);
    a.ParseLabels(ss);
    EXPECT_TRUE(a.DoesLabelExist("LOOP"));
    EXPECT_FALSE(a.DoesLabelExist("loop"));

    EXPECT_EQ(a.mLabels.size(), 1);
    Label* l = a.mLabels[0];
    EXPECT_FALSE(l == nullptr);
    EXPECT_EQ(l->GetHash(), fnv1a32_const("LOOP"));
    EXPECT_STREQ(l->GetName().c_str(), "LOOP");
    EXPECT_EQ(l->GetLine(), 1);
}

TEST(Assembler, ParseLabelInvalid) {
    Assembler a;

    std::stringstream ss("L O O P:\n");
    EXPECT_THROW(a.ParseLabels(ss), ktools::assembler::AssemblerException);
    EXPECT_EQ(a.mLabels.size(), 0);

    ss.clear();
    ss.str("JMP [RA], [RB]\n");
    EXPECT_NO_THROW(a.ParseLabels(ss));
    EXPECT_EQ(a.mLabels.size(), 0);

    ss.clear();
    ss.str("CMP\n");
    EXPECT_NO_THROW(a.ParseLabels(ss));
    EXPECT_EQ(a.mLabels.size(), 0);

    ss.clear();
    ss.str(":\n");
    EXPECT_THROW(a.ParseLabels(ss), ktools::assembler::AssemblerException);
    EXPECT_EQ(a.mLabels.size(), 0);

    ss.clear();
    ss.str("LOOP:\n");
    EXPECT_NO_THROW(a.ParseLabels(ss));
    EXPECT_EQ(a.mLabels.size(), 1);
    EXPECT_TRUE(a.DoesLabelExist("LOOP"));

    EXPECT_THROW(a.ParseLabels(ss), ktools::assembler::AssemblerException);
    EXPECT_EQ(a.mLabels.size(), 1);
}

TEST(ParseSingleInstruction, Parse1) {
    Assembler a;
    AbstractInstruction* inst = a.ParseSingleInstruction("INC RC", 0x24, false);

    EXPECT_EQ(inst->GetOpcode(), 0x24);
    EXPECT_EQ(inst->GetMnemonic(), Mnemonic::INC);
    EXPECT_FALSE(inst->IsExtended());
    EXPECT_EQ(inst->Size(), 1);
    EXPECT_EQ(inst->NumOps(), 1);
    EXPECT_STREQ(inst->ToString().c_str(), "INC RC");
}

TEST(ParseSingleInstruction, ParseOperand1) {
    Assembler a;
    AbstractInstruction* inst = a.ParseSingleInstruction("INC RC", 0x24, false);

    EXPECT_EQ(inst->NumOps(), 1);

    const Operand* op = inst->Op(1);
    EXPECT_EQ(op->GetType(), OperandType::Register);
    EXPECT_EQ(op->Size(false), 0);
    EXPECT_EQ(op->GetRegister(), Register::RC);
    EXPECT_FALSE(op->IsReference());
    EXPECT_FALSE(op->IsOffset());
    EXPECT_FALSE(op->IsLabel());
}

TEST(ParseSingleInstruction, Parse2) {
    Assembler a;
    AbstractInstruction* inst = a.ParseSingleInstruction("MOV [RB], 0x15", 0x91, false);

    EXPECT_EQ(inst->GetOpcode(), 0x91);
    EXPECT_EQ(inst->GetMnemonic(), Mnemonic::MOV);
    EXPECT_FALSE(inst->IsExtended());
    EXPECT_EQ(inst->Size(), 2);
    EXPECT_EQ(inst->NumOps(), 2);
    EXPECT_STREQ(inst->ToString().c_str(), "MOV [RB], 21");
}

TEST(ParseSingleInstruction, ParseOperand2) {
    Assembler a;
    AbstractInstruction* inst = a.ParseSingleInstruction("MOV [RB], 0x15", 0x91, false);

    EXPECT_EQ(inst->NumOps(), 2);

    const Operand* op = inst->Op(1);
    EXPECT_EQ(op->GetType(), OperandType::Register | OperandType::Reference);
    EXPECT_EQ(op->Size(false), 0);
    EXPECT_EQ(op->GetRegister(), Register::RB);
    EXPECT_TRUE(op->IsReference());
    EXPECT_FALSE(op->IsOffset());
    EXPECT_FALSE(op->IsLabel());

    op = inst->Op(2);
    EXPECT_EQ(op->GetType(), OperandType::ConstNumber);
    EXPECT_EQ(op->Size(false), 1);
    EXPECT_EQ(op->GetRegister(), Register::Unknown);
    EXPECT_EQ(op->GetNumber(), 0x15);
    EXPECT_FALSE(op->IsReference());
    EXPECT_FALSE(op->IsOffset());
    EXPECT_FALSE(op->IsLabel());
}

TEST(ParseSingleInstruction, FullParse1) {
    Assembler a;
    AbstractInstruction* inst = a.ParseSingleInstruction("PUSH RC");

    EXPECT_EQ(inst->GetOpcode(), 0x26);
    EXPECT_EQ(inst->GetMnemonic(), Mnemonic::PUSH);
    EXPECT_FALSE(inst->IsExtended());
    EXPECT_EQ(inst->Size(), 1);
    EXPECT_EQ(inst->NumOps(), 1);
    EXPECT_STREQ(inst->ToString().c_str(), "PUSH RC");

    const Operand* op = inst->Op(1);
    EXPECT_EQ(op->GetType(), OperandType::Register);
    EXPECT_EQ(op->Size(false), 0);
    EXPECT_EQ(op->GetRegister(), Register::RC);
    EXPECT_FALSE(op->IsReference());
    EXPECT_FALSE(op->IsOffset());
    EXPECT_FALSE(op->IsLabel());
}

TEST(ParseSingleInstruction, FullParse2) {
    Assembler a;
    AbstractInstruction* inst = a.ParseSingleInstruction("MOV [RB], 0x15");

    EXPECT_EQ(inst->GetOpcode(), 0x91);
    EXPECT_EQ(inst->GetMnemonic(), Mnemonic::MOV);
    EXPECT_FALSE(inst->IsExtended());
    EXPECT_EQ(inst->Size(), 2);
    EXPECT_EQ(inst->NumOps(), 2);
    EXPECT_STREQ(inst->ToString().c_str(), "MOV [RB], 21");

    const Operand* op = inst->Op(1);
    EXPECT_EQ(op->GetType(), OperandType::Register | OperandType::Reference);
    EXPECT_EQ(op->Size(false), 0);
    EXPECT_EQ(op->GetRegister(), Register::RB);
    EXPECT_TRUE(op->IsReference());
    EXPECT_FALSE(op->IsOffset());
    EXPECT_FALSE(op->IsLabel());

    op = inst->Op(2);
    EXPECT_EQ(op->GetType(), OperandType::ConstNumber);
    EXPECT_EQ(op->Size(false), 1);
    EXPECT_EQ(op->GetRegister(), Register::Unknown);
    EXPECT_EQ(op->GetNumber(), 0x15);
    EXPECT_FALSE(op->IsReference());
    EXPECT_FALSE(op->IsOffset());
    EXPECT_FALSE(op->IsLabel());
}

int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    Assembler::InitializeInstructionMap();
    int res = RUN_ALL_TESTS();
    Assembler::DestroyInstructionMap();
    return res;
}
