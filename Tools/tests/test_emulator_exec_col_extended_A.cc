
/**
 * @file test_emulator_exec_col_extended_A.cc
 * @author Kiss Benedek
 * @brief Automated tests for emulator (extended column A)
 */

#include "test_emulator.h"

TEST(ExecutionExtendedA, TestExtendedCol_A_BAD) {
    _EXEC_TEST_BEGIN;
    _EXEC_COL_BAD_INST(exA)
}

// :: CFL
TEST(ExecutionExtendedA, TestExtendedCol_Ar0) {
    _EXEC_TEST_BEGIN;
    ctx.SetFlag(ktools::emulator::MachineFlags::fgALL);
    _EXEC_COL_ROW(exA, 0);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);
}

