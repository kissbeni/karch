
/**
 * @file test_emulator_exec_col_standard_9.cc
 * @author Kiss Benedek
 * @brief Automated tests for emulator (standard column 9)
 */

#include "test_emulator.h"

TEST(ExecutionStandard9, TestStandardCol_9_BAD) {
    _EXEC_TEST_BEGIN;
    _EXEC_COL_BAD_INST(9)
}

// :: SHR R?, X
TEST(ExecutionStandard9, TestStandardCol_9b0) {
    _EXEC_TEST_BEGIN;
    inst.arg = 1;

    _FILL_REGS
    ctx.RegA() = 0x2C;
    ctx.SetFlag(ktools::emulator::MachineFlags::fgALL);
    _EXEC_COL_BLOCKROW(9, 0, 0);
    EXPECT_EQ(ctx.RegA(), 0x16);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    ctx.SetFlag(ktools::emulator::MachineFlags::fgZERO);
    ctx.SetFlag(ktools::emulator::MachineFlags::fgSIGNED);
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgALL));
    ctx.SetFlag(ktools::emulator::MachineFlags::fgOVERFLOW);
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgALL));

    _FILL_REGS
    ctx.RegB() = 0x2C;
    _EXEC_COL_BLOCKROW(9, 0, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 0x16);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.RegC() = 0x2C;
    _EXEC_COL_BLOCKROW(9, 0, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 0x16);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.RegD() = 0x2C;
    _EXEC_COL_BLOCKROW(9, 0, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 0x16);

    ctx.RegA() = 1;
    _EXEC_COL_BLOCKROW(9, 0, 0);
    EXPECT_EQ(ctx.RegA(), 0);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgSIGNED));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
}

// :: ROL R?, X
TEST(ExecutionStandard9, TestStandardCol_9b1) {
    _EXEC_TEST_BEGIN;
    inst.arg = 1;

    _FILL_REGS
    ctx.RegA() = 0xB0;
    _EXEC_COL_BLOCKROW(9, 1, 0);
    EXPECT_EQ(ctx.RegA(), 0x61);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);

    _FILL_REGS
    ctx.RegB() = 0xB0;
    _EXEC_COL_BLOCKROW(9, 1, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 0x61);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.RegC() = 0xB0;
    _EXEC_COL_BLOCKROW(9, 1, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 0x61);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.RegD() = 0xB0;
    _EXEC_COL_BLOCKROW(9, 1, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 0x61);
}

// :: ROR R?, X
TEST(ExecutionStandard9, TestStandardCol_9b2) {
    _EXEC_TEST_BEGIN;
    inst.arg = 1;

    _FILL_REGS
    ctx.RegA() = 0xB;
    _EXEC_COL_BLOCKROW(9, 2, 0);
    EXPECT_EQ(ctx.RegA(), 0x85);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);

    _FILL_REGS
    ctx.RegB() = 0xB;
    _EXEC_COL_BLOCKROW(9, 2, 1);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), 0x85);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.RegC() = 0xB;
    _EXEC_COL_BLOCKROW(9, 2, 2);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), 0x85);
    EXPECT_EQ(ctx.RegD(), gTestRegValues[3]);

    _FILL_REGS
    ctx.RegD() = 0xB;
    _EXEC_COL_BLOCKROW(9, 2, 3);
    EXPECT_EQ(ctx.RegA(), gTestRegValues[0]);
    EXPECT_EQ(ctx.RegB(), gTestRegValues[1]);
    EXPECT_EQ(ctx.RegC(), gTestRegValues[2]);
    EXPECT_EQ(ctx.RegD(), 0x85);
}

// :: CMP R?, X
TEST(ExecutionStandard9, TestStandardCol_9b3) {
    _EXEC_TEST_BEGIN;
    inst.arg = 5;

    ctx.SetFlag(ktools::emulator::MachineFlags::fgALL);
    ctx.RegA() = 10;
    _EXEC_COL_BLOCKROW(9, 3, 0);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    ctx.SetFlag(ktools::emulator::MachineFlags::fgZERO);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgALL);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgALL);
    ctx.RegA() = 5;
    _EXEC_COL_BLOCKROW(9, 3, 0);
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgALL);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgALL);
    ctx.RegA() = 4;
    _EXEC_COL_BLOCKROW(9, 3, 0);
    EXPECT_TRUE(ctx.FlagSet(ktools::emulator::MachineFlags::fgCARRY));
    EXPECT_FALSE(ctx.FlagSet(ktools::emulator::MachineFlags::fgZERO));
    ctx.SetFlag(ktools::emulator::MachineFlags::fgZERO);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgALL);


    ctx.Reset();
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegB() = 10;
    _EXEC_COL_BLOCKROW(9, 3, 1);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegB() = 5;
    _EXEC_COL_BLOCKROW(9, 3, 1);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgZERO);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegB() = 4;
    _EXEC_COL_BLOCKROW(9, 3, 1);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgCARRY);


    ctx.Reset();
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegC() = 10;
    _EXEC_COL_BLOCKROW(9, 3, 2);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegC() = 5;
    _EXEC_COL_BLOCKROW(9, 3, 2);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgZERO);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegC() = 4;
    _EXEC_COL_BLOCKROW(9, 3, 2);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgCARRY);


    ctx.Reset();
    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegD() = 10;
    _EXEC_COL_BLOCKROW(9, 3, 3);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgNONE);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegD() = 5;
    _EXEC_COL_BLOCKROW(9, 3, 3);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgZERO);

    ctx.SetFlag(ktools::emulator::MachineFlags::fgCARRY | ktools::emulator::MachineFlags::fgZERO);
    ctx.RegD() = 4;
    _EXEC_COL_BLOCKROW(9, 3, 3);
    EXPECT_EQ(ctx.FlagSet(), ktools::emulator::MachineFlags::fgCARRY);
}
