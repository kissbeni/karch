#!/bin/bash

mkdir -p build
cd build
cmake ../
e=$?
if [[ $e != 0 ]]; then
    exit $e
fi

make -j16
exit $?
