
/**
 * @file exec_col_base.hpp
 * @author Kiss Benedek
 * @brief Column handler base
 */

#ifndef KEMU_EXEC_COL_BASE_H
#define KEMU_EXEC_COL_BASE_H

#include <emulator/VirtualMachine.h>

//! Header for an instruction column handler function
#define _VM_COL_HANDLER_(n) void _kemu_vm_handle_column_##n(    \
                const VirtualMachine::RawInstruction& inst,     \
                MachineContext& context, bool& halt             \
            )

//! Update ZERO and SIGNED flags according to @p r
#define _VM_SET_GENERIC_FLAGS(r) {                              \
        context.SetFlag(MachineFlags::fgZERO, (r) == 0);        \
        context.SetFlag(MachineFlags::fgSIGNED, (r) & 128);     \
    }

#define _VM_CHECK_BAD_INST \
    if (inst.block_row > 3 || inst.block > 3) \
        throw "bad_instruction";

//! Number of handlers in @ref ktools::emulator::execute::g_ColumnHandlers
#define _VM_COL_STANDARD_HANDLER_COUNT_ 16

namespace ktools
{
    namespace emulator
    {
        namespace execute
        {
            //! Column handler function type for @ref g_ColumnHandlers
            typedef void(*ColumnHandlerFunc)(const VirtualMachine::RawInstruction&,MachineContext&,bool&);

            //// STANDARD INSTRUCTIONS ////
            _VM_COL_HANDLER_(0);
            _VM_COL_HANDLER_(1);
            _VM_COL_HANDLER_(2);
            _VM_COL_HANDLER_(3);
            _VM_COL_HANDLER_(4);
            // _VM_COL_HANDLER_(5);
            _VM_COL_HANDLER_(6);
            _VM_COL_HANDLER_(7);
            _VM_COL_HANDLER_(8);
            _VM_COL_HANDLER_(9);
            _VM_COL_HANDLER_(A);
            _VM_COL_HANDLER_(B);
            _VM_COL_HANDLER_(C);
            _VM_COL_HANDLER_(D);
            _VM_COL_HANDLER_(E);
            _VM_COL_HANDLER_(F);

            //// EXTENDED INSTRUCTIONS ////
            _VM_COL_HANDLER_(ex0);
            // _VM_COL_HANDLER_(ex1);
            _VM_COL_HANDLER_(ex2);
            // _VM_COL_HANDLER_(ex3);
            _VM_COL_HANDLER_(ex4);
            // _VM_COL_HANDLER_(ex5);
            _VM_COL_HANDLER_(ex6);
            // _VM_COL_HANDLER_(ex7);
            _VM_COL_HANDLER_(ex8);
            // _VM_COL_HANDLER_(ex9);
            _VM_COL_HANDLER_(exA);
            // _VM_COL_HANDLER_(exB);
            // _VM_COL_HANDLER_(exC);
            // _VM_COL_HANDLER_(exD);
            // _VM_COL_HANDLER_(exE);
            // _VM_COL_HANDLER_(exF);

            //! Handlers for all instruction columns
            const ColumnHandlerFunc g_ColumnHandlers[] = {
                //// STANDARD INSTRUCTIONS ////
                _kemu_vm_handle_column_0,
                _kemu_vm_handle_column_1,
                _kemu_vm_handle_column_2,
                _kemu_vm_handle_column_3,
                _kemu_vm_handle_column_4,
                nullptr,
                _kemu_vm_handle_column_6,
                _kemu_vm_handle_column_7,
                _kemu_vm_handle_column_8,
                _kemu_vm_handle_column_9,
                _kemu_vm_handle_column_A,
                _kemu_vm_handle_column_B,
                _kemu_vm_handle_column_C,
                _kemu_vm_handle_column_D,
                _kemu_vm_handle_column_E,
                _kemu_vm_handle_column_F,

                //// EXTENDED INSTRUCTIONS ////
                _kemu_vm_handle_column_ex0,
                nullptr,
                _kemu_vm_handle_column_ex2,
                nullptr,
                _kemu_vm_handle_column_ex4,
                nullptr,
                _kemu_vm_handle_column_ex6,
                nullptr,
                _kemu_vm_handle_column_ex8,
                nullptr,
                _kemu_vm_handle_column_exA,
                nullptr,
                nullptr,
                nullptr,
                nullptr,
                nullptr
            };
        }
    }
}

#endif
