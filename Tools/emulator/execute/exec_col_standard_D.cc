
/**
 * @file exec_col_standard_D.cc
 * @author Kiss Benedek
 * @brief standard_D column handler
 */

#include <emulator/VirtualMachine.h>
#include <emulator/MachineContext.hpp>
#include <shared/miscfunc.hpp>
#include <emulator/execute/exec_col_base.hpp>

namespace ktools
{
    namespace emulator
    {
        namespace execute
        {
            /**
             * @brief standard_D column handler
             *
             * Instructions:
             *   - @code MUL R?, X @endcode
             *   - @code DIV R?, X @endcode
             *   - @code MUL R?, [X] @endcode
             *   - @code DIV R?, [X] @endcode
             */
            _VM_COL_HANDLER_(D)
            {
                _VM_CHECK_BAD_INST
                karch_word_t& reg = context.Reg(inst.block_row);
                karch_word_t r;

                switch (inst.block)
                {
                    case 0x0:   // MUL R?, X
                        r = reg * inst.arg;
                        reg = r;
                        break;
                    case 0x1:   // DIV R?, X
                        r = reg / inst.arg;
                        reg = r;
                        break;
                    case 0x2:   // MUL [R?], X
                        r = context.ReadMemory<karch_word_t>(context.GetBankOffset(reg)) * inst.arg;
                        context.WriteMemory(context.GetBankOffset(reg), r);
                        break;
                    case 0x3:   // DIV [R?], X
                        r = context.ReadMemory<karch_word_t>(context.GetBankOffset(reg)) / inst.arg;
                        context.WriteMemory(context.GetBankOffset(reg), r);
                        break;
                    default: throw "bad_instruction";
                }

                _VM_SET_GENERIC_FLAGS(r);
            }
        }
    }
}
