
/**
 * @file exec_col_standard_3.cc
 * @author Kiss Benedek
 * @brief standard_3 column handler
 */

#include <emulator/VirtualMachine.h>
#include <emulator/MachineContext.hpp>
#include <shared/miscfunc.hpp>
#include <emulator/execute/exec_col_base.hpp>

namespace ktools
{
    namespace emulator
    {
        namespace execute
        {
            /**
             * @brief standard_3 column handler
             *
             * Instructions:
             *   - @code ADD R?, X @endcode
             *   - @code SUB R?, X @endcode
             *   - @code ADD [R?], X @endcode
             *   - @code SUB [R?], X @endcode
             */
            _VM_COL_HANDLER_(3)
            {
                _VM_CHECK_BAD_INST
                karch_word_t& reg = context.Reg(inst.block_row);
                karch_word_t r, a = reg;

                switch (inst.block)
                {
                    case 0x0:   // ADD R?, X
                        r = reg + inst.arg;
                        context.SetFlag(MachineFlags::fgCARRY, r < reg);
                        reg = r;
                        break;
                    case 0x1:   // SUB R?, X
                        r = reg - inst.arg;
                        context.SetFlag(MachineFlags::fgCARRY, r > reg);
                        reg = r;
                        break;
                    case 0x2:   // ADD [R?], X
                        a = context.ReadMemory<karch_word_t>(context.GetBankOffset(reg));
                        r = a + inst.arg;

                        context.SetFlag(MachineFlags::fgCARRY, r < a);
                        context.WriteMemory(context.GetBankOffset(reg), r);
                        break;
                    case 0x3:   // SUB [R?], X
                        a = context.ReadMemory<karch_word_t>(context.GetBankOffset(reg));
                        r = a - inst.arg;

                        context.SetFlag(MachineFlags::fgCARRY, r > a);
                        context.WriteMemory(context.GetBankOffset(reg), r);
                        break;
                }

                context.SetFlag(MachineFlags::fgOVERFLOW, (r & 128) != (a & 128));
                _VM_SET_GENERIC_FLAGS(r);
            }
        }
    }
}
