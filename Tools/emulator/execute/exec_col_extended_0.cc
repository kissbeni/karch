
/**
 * @file exec_col_extended_0.cc
 * @author Kiss Benedek
 * @brief extended_0 column handler
 */

#include <emulator/VirtualMachine.h>
#include <emulator/MachineContext.hpp>
#include <shared/miscfunc.hpp>
#include <emulator/execute/exec_col_base.hpp>

namespace ktools
{
    namespace emulator
    {
        namespace execute
        {
            /**
             * @brief extended_0 column handler
             *
             * Instructions:
             *   - @code IIC R? @endcode
             *   - @code DIC R? @endcode
             *   - @code IIC [R?] @endcode
             *   - @code DIC [R?] @endcode
             */
            _VM_COL_HANDLER_(ex0)
            {
                _VM_CHECK_BAD_INST
                karch_word_t n = context.FlagSet(MachineFlags::fgCARRY) ? 1 : 0;
                karch_word_t& reg = context.Reg(inst.block_row);
                karch_word_t r, a = reg;

                switch (inst.block)
                {
                    case 0x0:   // IIC R?
                        r = reg + n;
                        context.SetFlag(MachineFlags::fgCARRY, r < reg);
                        reg = r;
                        break;
                    case 0x1:   // DIC R?
                        r = reg - n;
                        context.SetFlag(MachineFlags::fgCARRY, r > reg);
                        reg = r;
                        break;
                    case 0x2:   // IIC [R?]
                        a = context.ReadMemory<karch_word_t>(context.GetBankOffset(reg));
                        r = a + n;

                        context.SetFlag(MachineFlags::fgCARRY, r < a);
                        context.WriteMemory(context.GetBankOffset(reg), r);
                        break;
                    case 0x3:   // DIC [R?]
                        a = context.ReadMemory<karch_word_t>(context.GetBankOffset(reg));
                        r = a - n;

                        context.SetFlag(MachineFlags::fgCARRY, r > a);
                        context.WriteMemory(context.GetBankOffset(reg), r);
                        break;
                }

                context.SetFlag(MachineFlags::fgOVERFLOW, (r & 128) != (a & 128));
                _VM_SET_GENERIC_FLAGS(r);
            }
        }
    }
}
