
/**
 * @file exec_col_standard_C.cc
 * @author Kiss Benedek
 * @brief standard_C column handler
 */

#include <emulator/VirtualMachine.h>
#include <emulator/MachineContext.hpp>
#include <shared/miscfunc.hpp>
#include <emulator/execute/exec_col_base.hpp>

namespace ktools
{
    namespace emulator
    {
        namespace execute
        {
            /**
             * @brief standard_C column handler
             *
             * Instructions:
             *   - @code XOR R?, R? @endcode
             */
            _VM_COL_HANDLER_(C) // XOR R?, R?
            {
                _VM_CHECK_BAD_INST
                karch_word_t& reg = context.Reg(inst.block);
                karch_word_t r = reg ^ context.Reg(inst.block_row);

                context.ClearFlag(MachineFlags::fgCARRY);
                context.ClearFlag(MachineFlags::fgOVERFLOW);

                reg = r;
                _VM_SET_GENERIC_FLAGS(r);
            }
        }
    }
}
