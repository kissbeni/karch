
/**
 * @file exec_col_standard_6.cc
 * @author Kiss Benedek
 * @brief standard_6 column handler
 */

#include <emulator/VirtualMachine.h>
#include <emulator/MachineContext.hpp>
#include <shared/miscfunc.hpp>
#include <emulator/execute/exec_col_base.hpp>

namespace ktools
{
    namespace emulator
    {
        namespace execute
        {
            /**
             * @brief standard_6 column handler
             *
             * Instructions:
             *   - @code PUSH R? @endcode
             *   - @code POP R? @endcode
             *   - @code PUSH [R?] @endcode
             *   - @code NOT R? @endcode
             */
            _VM_COL_HANDLER_(6)
            {
                switch (inst.block)
                {
                    case 0x0:   // PUSH R?
                        context.Push(context.Reg(inst.block_row));
                        break;
                    case 0x1:   // POP R?
                        context.Reg(inst.block_row) = context.Pop<karch_word_t>();
                        break;
                    case 0x2:   // PUSH [R?]
                        context.Push(context.ReadMemory<karch_word_t>(context.GetBankOffset(context.Reg(inst.block_row))));
                        break;
                    case 0x3:   // NOT R?
                        context.Reg(inst.block_row) = ~context.Reg(inst.block_row);
                        break;
                    default: throw "bad_instruction";
                }
            }
        }
    }
}
