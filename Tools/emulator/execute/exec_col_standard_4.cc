
/**
 * @file exec_col_standard_4.cc
 * @author Kiss Benedek
 * @brief standard_4 column handler
 */

#include <emulator/VirtualMachine.h>
#include <emulator/MachineContext.hpp>
#include <shared/miscfunc.hpp>
#include <emulator/execute/exec_col_base.hpp>

namespace ktools
{
    namespace emulator
    {
        namespace execute
        {
            /**
             * @brief standard_4 column handler
             *
             * Instructions:
             *   - @code INC R? @endcode
             *   - @code DEC R? @endcode
             *   - @code INC [R?] @endcode
             *   - @code DEC [R?] @endcode
             */
            _VM_COL_HANDLER_(4)
            {
                _VM_CHECK_BAD_INST
                karch_word_t& reg = context.Reg(inst.block_row);
                karch_word_t r, a = reg;

                switch (inst.block)
                {
                    case 0x0:   // INC R?
                        r = reg + 1;
                        context.SetFlag(MachineFlags::fgCARRY, r < reg);
                        reg = r;
                        break;
                    case 0x1:   // DEC R?
                        r = reg - 1;
                        context.SetFlag(MachineFlags::fgCARRY, r > reg);
                        reg = r;
                        break;
                    case 0x2:   // INC [R?]
                        a = context.ReadMemory<karch_word_t>(context.GetBankOffset(reg));
                        r = a + 1;

                        context.SetFlag(MachineFlags::fgCARRY, r < a);
                        context.WriteMemory(context.GetBankOffset(reg), r);
                        break;
                    case 0x3:   // DEC [R?]
                        a = context.ReadMemory<karch_word_t>(context.GetBankOffset(reg));
                        r = a - 1;

                        context.SetFlag(MachineFlags::fgCARRY, r > a);
                        context.WriteMemory(context.GetBankOffset(reg), r);
                        break;
                }

                context.SetFlag(MachineFlags::fgOVERFLOW, (r & 128) != (a & 128));
                _VM_SET_GENERIC_FLAGS(r);
            }
        }
    }
}
