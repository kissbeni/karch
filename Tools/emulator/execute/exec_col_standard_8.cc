
/**
 * @file exec_col_standard_8.cc
 * @author Kiss Benedek
 * @brief standard_8 column handler
 */

#include <emulator/VirtualMachine.h>
#include <emulator/MachineContext.hpp>
#include <shared/miscfunc.hpp>
#include <emulator/execute/exec_col_base.hpp>

namespace ktools
{
    namespace emulator
    {
        namespace execute
        {
            /**
             * @brief standard_8 column handler
             *
             * Instructions:
             *   - @code AND R?, R? @endcode
             */
            _VM_COL_HANDLER_(8) // AND R?, R?
            {
                _VM_CHECK_BAD_INST
                karch_word_t& reg = context.Reg(inst.block);
                karch_word_t r = reg & context.Reg(inst.block_row);

                context.ClearFlag(MachineFlags::fgCARRY);
                context.ClearFlag(MachineFlags::fgOVERFLOW);

                reg = r;

                _VM_SET_GENERIC_FLAGS(r);
            }
        }
    }
}
