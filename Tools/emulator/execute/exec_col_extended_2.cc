
/**
 * @file exec_col_extended_2.cc
 * @author Kiss Benedek
 * @brief extended_2 column handler
 */

#include <emulator/VirtualMachine.h>
#include <emulator/MachineContext.hpp>
#include <shared/miscfunc.hpp>
#include <emulator/execute/exec_col_base.hpp>

namespace ktools
{
    namespace emulator
    {
        namespace execute
        {
            /**
             * @brief extended_2 column handler
             *
             * Instructions:
             *   - @code MOV R?, [R?] @endcode
             */
            _VM_COL_HANDLER_(ex2) // MOV R?, [R?]
            {
                _VM_CHECK_BAD_INST
                context.Reg(inst.block) = context.ReadOne(context.GetBankOffset(context.Reg(inst.block_row)));
            }
        }
    }
}
