
/**
 * @file exec_col_standard_7.cc
 * @author Kiss Benedek
 * @brief standard_7 column handler
 */

#include <emulator/VirtualMachine.h>
#include <emulator/MachineContext.hpp>
#include <shared/miscfunc.hpp>
#include <emulator/execute/exec_col_base.hpp>

namespace ktools
{
    namespace emulator
    {
        namespace execute
        {
            /**
             * @brief standard_7 column handler
             *
             * Instructions:
             *   - @code AND R?, X @endcode
             *   - @code OR R?, X @endcode
             *   - @code XOR R?, X @endcode
             *   - @code SHL R?, X @endcode
             */
            _VM_COL_HANDLER_(7)
            {
                _VM_CHECK_BAD_INST
                karch_word_t& reg = context.Reg(inst.block_row);
                karch_word_t r;

                context.ClearFlag(MachineFlags::fgCARRY);
                context.ClearFlag(MachineFlags::fgOVERFLOW);

                switch (inst.block)
                {
                    case 0x0:   // AND R?, X
                        reg = r = reg & inst.arg;
                        break;
                    case 0x1:   // OR R?, X
                        reg = r = reg | inst.arg;
                        break;
                    case 0x2:   // XOR R?, X
                        reg = r = reg ^ inst.arg;
                        break;
                    case 0x3:   // SHL R?, X
                        r = reg << inst.arg;

                        context.SetFlag(MachineFlags::fgCARRY, reg & 128);

                        reg = r;
                        break;
                }

                _VM_SET_GENERIC_FLAGS(r);
            }
        }
    }
}
