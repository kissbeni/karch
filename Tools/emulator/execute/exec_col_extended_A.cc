
/**
 * @file exec_col_extended_A.cc
 * @author Kiss Benedek
 * @brief extended_A column handler
 */

#include <emulator/VirtualMachine.h>
#include <emulator/MachineContext.hpp>
#include <shared/miscfunc.hpp>
#include <emulator/execute/exec_col_base.hpp>

namespace ktools
{
    namespace emulator
    {
        namespace execute
        {
            /**
             * @brief extended_A column handler
             *
             * Instructions:
             *   - @code CFL @endcode
             */
            _VM_COL_HANDLER_(exA)
            {
                switch (inst.row)
                {
                    case 0x0:   // CFL
                        context.ClearFlag(MachineFlags::fgALL);
                        break;
                    default: throw "bad_instruction";
                }
            }
        }
    }
}
