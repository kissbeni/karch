
/**
 * @file exec_col_standard_B.cc
 * @author Kiss Benedek
 * @brief standard_B column handler
 */

#include <emulator/VirtualMachine.h>
#include <emulator/MachineContext.hpp>
#include <shared/miscfunc.hpp>
#include <emulator/execute/exec_col_base.hpp>

namespace ktools
{
    namespace emulator
    {
        namespace execute
        {
            /**
             * @brief standard_B column handler
             *
             * Instructions:
             *   - @code PUSH X @endcode
             *   - @code JMP $+X @endcode
             *   - @code JE $+X @endcode
             *   - @code JNE $+X @endcode
             *   - @code JC $+X @endcode
             *   - @code JNC $+X @endcode
             *   - @code JBE $+X @endcode
             *   - @code JA $+X @endcode
             *   - @code JS $+X @endcode
             *   - @code JNS $+X @endcode
             *   - @code BNK X @endcode
             *   - @code SYS X @endcode
             *   - @code JO $+X @endcode
             *   - @code JNO $+X @endcode
             *   - @code CALL $+X @endcode
             *   - @code PUSH [X] @endcode
             */
            _VM_COL_HANDLER_(B)
            {
                switch (inst.row)
                {
                    case 0x0:   // PUSH X
                        context.Push(inst.arg);
                        break;
                    case 0x1:   // JMP $+X
                        context.RelativeJump(inst.arg16, inst.wide_arg ? 4 : 2);
                        break;
                    case 0x2:   // JE $+X
                        if (context.FlagSet(MachineFlags::fgZERO))
                            context.RelativeJump(inst.arg16, inst.wide_arg ? 4 : 2);
                        break;
                    case 0x3:   // JNE $+X
                        if (!context.FlagSet(MachineFlags::fgZERO))
                            context.RelativeJump(inst.arg16, inst.wide_arg ? 4 : 2);
                        break;
                    case 0x4:   // JC $+X
                        if (context.FlagSet(MachineFlags::fgCARRY))
                            context.RelativeJump(inst.arg16, inst.wide_arg ? 4 : 2);
                        break;
                    case 0x5:   // JNC $+X
                        if (!context.FlagSet(MachineFlags::fgCARRY))
                            context.RelativeJump(inst.arg16, inst.wide_arg ? 4 : 2);
                        break;
                    case 0x6:   // JBE $+X
                        if (context.FlagSet(MachineFlags::fgCARRY | MachineFlags::fgZERO))
                            context.RelativeJump(inst.arg16, inst.wide_arg ? 4 : 2);
                        break;
                    case 0x7:   // JA $+X
                        if (!context.FlagSet(MachineFlags::fgCARRY | MachineFlags::fgZERO))
                            context.RelativeJump(inst.arg16, inst.wide_arg ? 4 : 2);
                        break;
                    case 0x8:   // JS $+X
                        if (context.FlagSet(MachineFlags::fgSIGNED))
                            context.RelativeJump(inst.arg16, inst.wide_arg ? 4 : 2);
                        break;
                    case 0x9:   // JNS $+X
                        if (!context.FlagSet(MachineFlags::fgSIGNED))
                            context.RelativeJump(inst.arg16, inst.wide_arg ? 4 : 2);
                        break;
                    case 0xA:   // BNK X
                        context.CurrentMemoryBank() = inst.arg;
                        break;
                    case 0xB:   // SYS X
                        printf("[VirtualMachine] syscall interrupts are not implemented yet (sys 0x%02x)\n", inst.arg16);
                        break;
                    case 0xC:   // JO $+X
                        if (context.FlagSet(MachineFlags::fgOVERFLOW))
                            context.RelativeJump(inst.arg16, inst.wide_arg ? 4 : 2);
                        break;
                    case 0xD:   // JNO $+X
                        if (!context.FlagSet(MachineFlags::fgOVERFLOW))
                            context.RelativeJump(inst.arg16, inst.wide_arg ? 4 : 2);
                        break;
                    case 0xE:   // CALL $+X
                        context.Push(context.IP());
                        context.RelativeJump(inst.arg16, inst.wide_arg ? 4 : 2);
                        break;
                    case 0xF:   // PUSH [X]
                        if (inst.wide_arg)
                            context.Push(context.ReadMemory<karch_word_t>(inst.arg16));
                        else
                            context.Push(context.ReadMemory<karch_word_t>(context.GetBankOffset(inst.arg)));
                        break;
                    default: throw "bad_instruction";
                }
            }
        }
    }
}
