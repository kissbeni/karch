
/**
 * @file exec_col_extended_8.cc
 * @author Kiss Benedek
 * @brief extended_8 column handler
 */

#include <emulator/VirtualMachine.h>
#include <emulator/MachineContext.hpp>
#include <shared/miscfunc.hpp>
#include <emulator/execute/exec_col_base.hpp>

namespace ktools
{
    namespace emulator
    {
        namespace execute
        {
            /**
             * @brief extended_8 column handler
             *
             * Instructions:
             *   - @code CMP R?, R? @endcode
             */
            _VM_COL_HANDLER_(ex8)
            {
                _VM_CHECK_BAD_INST
                karch_word_t& reg1 = context.Reg(inst.block);
                karch_word_t& reg2 = context.Reg(inst.block_row);

                if (reg1 == reg2)
                {
                    context.ClearFlag(MachineFlags::fgCARRY);
                    context.SetFlag(MachineFlags::fgZERO);
                }
                else if (reg1 < reg2)
                {
                    context.SetFlag(MachineFlags::fgCARRY);
                    context.ClearFlag(MachineFlags::fgZERO);
                }
                else if (reg1 > reg2)
                {
                    context.ClearFlag(MachineFlags::fgCARRY);
                    context.ClearFlag(MachineFlags::fgZERO);
                }
            }
        }
    }
}
