
/**
 * @file exec_col_standard_9.cc
 * @author Kiss Benedek
 * @brief standard_9 column handler
 */

#include <emulator/VirtualMachine.h>
#include <emulator/MachineContext.hpp>
#include <shared/miscfunc.hpp>
#include <emulator/execute/exec_col_base.hpp>

namespace ktools
{
    namespace emulator
    {
        namespace execute
        {
            /**
             * @brief standard_9 column handler
             *
             * Instructions:
             *   - @code SHR R?, X @endcode
             *   - @code ROL R?, X @endcode
             *   - @code ROR R?, X @endcode
             *   - @code CMP R?, X @endcode
             */
            _VM_COL_HANDLER_(9)
            {
                _VM_CHECK_BAD_INST
                karch_word_t& reg = context.Reg(inst.block_row);
                karch_word_t r;

                switch (inst.block)
                {
                    case 0x0:   // SHR R?, X
                        r = reg >> inst.arg;

                        context.ClearFlag(MachineFlags::fgOVERFLOW);
                        context.SetFlag(MachineFlags::fgCARRY, reg & 1);

                        reg = r;

                        _VM_SET_GENERIC_FLAGS(r);
                        break;
                    case 0x1:   // ROL R?, X
                        reg = rol8(reg, inst.arg);
                        break;
                    case 0x2:   // ROR R?, X
                        reg = ror8(reg, inst.arg);
                        break;
                    case 0x3:   // CMP R?, X
                        if (reg == inst.arg)
                        {
                            context.ClearFlag(MachineFlags::fgCARRY);
                            context.SetFlag(MachineFlags::fgZERO);
                        }
                        else if (reg < inst.arg)
                        {
                            context.SetFlag(MachineFlags::fgCARRY);
                            context.ClearFlag(MachineFlags::fgZERO);
                        }
                        else if (reg > inst.arg)
                        {
                            context.ClearFlag(MachineFlags::fgCARRY);
                            context.ClearFlag(MachineFlags::fgZERO);
                        }

                        break;
                }
            }
        }
    }
}
