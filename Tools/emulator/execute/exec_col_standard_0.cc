
/**
 * @file exec_col_standard_0.cc
 * @author Kiss Benedek
 * @brief standard_0 column handler
 */

#include <emulator/VirtualMachine.h>
#include <emulator/MachineContext.hpp>
#include <shared/miscfunc.hpp>
#include <emulator/execute/exec_col_base.hpp>

namespace ktools
{
    namespace emulator
    {
        namespace execute
        {
            /**
             * @brief standard_0 column handler
             *
             * Instructions:
             *   - @code NOP @endcode
             *   - @code RET @endcode
             *   - @code PUSHA @endcode
             *   - @code POPA @endcode
             *   - @code SETC @endcode
             *   - @code SETZ @endcode
             *   - @code SETS @endcode
             *   - @code SETO @endcode
             *   - @code CFC @endcode
             *   - @code CFZ @endcode
             *   - @code CFS @endcode
             *   - @code CFO @endcode
             *   - @code RETI @endcode
             *   - @code HLT @endcode
             */
            _VM_COL_HANDLER_(0)
            {
                switch (inst.row)
                {
                    case 0x0:   // NOP
                        break;
                    case 0x1:   // RET
                        context.IP() = context.Pop<karch_addr_t>();
                        break;
                    case 0x2:   // PUSHA
                        context.Push(context.RegA());
                        context.Push(context.RegB());
                        context.Push(context.RegC());
                        context.Push(context.RegD());
                        break;
                    case 0x3:   // POPA
                        context.RegD() = context.Pop<karch_word_t>();
                        context.RegC() = context.Pop<karch_word_t>();
                        context.RegB() = context.Pop<karch_word_t>();
                        context.RegA() = context.Pop<karch_word_t>();
                        break;
                    case 0x4:   // WADDR (not handled here)
                        break;
                    case 0x5:   // SETC
                        context.SetFlag(MachineFlags::fgCARRY);
                        break;
                    case 0x6:   // SETZ
                        context.SetFlag(MachineFlags::fgZERO);
                        break;
                    case 0x7:   // SETS
                        context.SetFlag(MachineFlags::fgSIGNED);
                        break;
                    case 0x8:   // SETO
                        context.SetFlag(MachineFlags::fgOVERFLOW);
                        break;
                    case 0x9:   // CFC
                        context.ClearFlag(MachineFlags::fgCARRY);
                        break;
                    case 0xA:   // CFZ
                        context.ClearFlag(MachineFlags::fgZERO);
                        break;
                    case 0xB:   // CFS
                        context.ClearFlag(MachineFlags::fgSIGNED);
                        break;
                    case 0xC:   // CFO
                        context.ClearFlag(MachineFlags::fgOVERFLOW);
                        break;
                    case 0xD:   // EXTEND (not handled here)
                        break;
                    case 0xE:   // RETI
                        context.ClearFlag(MachineFlags::fgALL);
                        context.SetFlag(context.Pop<MachineFlags>());
                        context.RegD() = context.Pop<karch_word_t>();
                        context.RegC() = context.Pop<karch_word_t>();
                        context.RegB() = context.Pop<karch_word_t>();
                        context.RegA() = context.Pop<karch_word_t>();
                        context.IP() = context.Pop<karch_addr_t>();
                        break;
                    case 0xF:   // HLT
                        halt = true;
                        break;
                    default: throw "bad_instruction";
                }
            }
        }
    }
}
