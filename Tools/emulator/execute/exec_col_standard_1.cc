
/**
 * @file exec_col_standard_1.cc
 * @author Kiss Benedek
 * @brief standard_1 column handler
 */

#include <emulator/VirtualMachine.h>
#include <emulator/MachineContext.hpp>
#include <shared/miscfunc.hpp>
#include <emulator/execute/exec_col_base.hpp>

namespace ktools
{
    namespace emulator
    {
        namespace execute
        {
            /**
             * @brief standard_1 column handler
             *
             * Instructions:
             *   - @code MOV R?, X @endcode
             *   - @code MOV R?, [X] @endcode
             *   - @code MOV [R?], X @endcode
             *   - @code MOV [X], R? @endcod
             */
            _VM_COL_HANDLER_(1)
            {
                switch (inst.block)
                {
                    case 0x0:   // MOV R?, X
                        context.Reg(inst.block_row) = inst.arg;
                        break;
                    case 0x1:   // MOV R?, [X]
                        context.Reg(inst.block_row) = context.ReadMemory<karch_word_t>(
                            context.GetBankOffset(inst.arg));
                        break;
                    case 0x2:   // MOV [R?], X
                        context.WriteMemory(
                            context.GetBankOffset(context.Reg(inst.block_row)), inst.arg);
                        break;
                    case 0x3:   // MOV [X], R?
                        context.WriteMemory(context.GetBankOffset(inst.arg),
                            context.Reg(inst.block_row));
                        break;
                    default: throw "bad_instruction";
                }
            }
        }
    }
}
