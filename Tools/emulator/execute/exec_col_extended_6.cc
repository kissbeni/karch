
/**
 * @file exec_col_extended_6.cc
 * @author Kiss Benedek
 * @brief extended_6 column handler
 */

#include <emulator/VirtualMachine.h>
#include <emulator/MachineContext.hpp>
#include <shared/miscfunc.hpp>
#include <emulator/execute/exec_col_base.hpp>

namespace ktools
{
    namespace emulator
    {
        namespace execute
        {
            /**
             * @brief extended_6 column handler
             *
             * Instructions:
             *   - @code SUB R?, R? @endcode
             */
            _VM_COL_HANDLER_(ex6) // SUB R?, R?
            {
                _VM_CHECK_BAD_INST
                karch_word_t& reg = context.Reg(inst.block);
                karch_word_t r = reg - context.Reg(inst.block_row);

                context.SetFlag(MachineFlags::fgCARRY, r > reg);
                context.SetFlag(MachineFlags::fgOVERFLOW, (r & 128) != (reg & 128));

                reg = r;
                _VM_SET_GENERIC_FLAGS(r);
            }
        }
    }
}
