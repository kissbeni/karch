
/**
 * @file LEDDisplay.cc
 * @author Kiss Benedek
 * @brief LEDDisplay class implementation
 */

#include <emulator/gui/LEDDisplay.h>

namespace ktools
{
    namespace emulator
    {
        namespace gui
        {
            LEDDisplay::LEDDisplay(const int16_t x, const int16_t y)
                : Drawable(x, y)
            {
                mValue = 0;
            }

            void LEDDisplay::DrawTo(DisplayWindow& window) const
            {
                window.DrawBox(mX, mY, 16, 64, 44, 44, 44);
                window.DrawCircle(mX + 8,  mY + 8, mValue & 1, 6);
                window.DrawCircle(mX + 24, mY + 8, mValue & 2, 6);
                window.DrawCircle(mX + 40, mY + 8, mValue & 4, 6);
                window.DrawCircle(mX + 56, mY + 8, mValue & 8, 6);
            }
        }
    }
}
