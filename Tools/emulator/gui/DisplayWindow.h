
/**
 * @file DisplayWindow.h
 * @author Kiss Benedek
 * @brief DisplayWindow class
 */

#ifndef KEMU_DISPLAY_WINDOW_H
#define KEMU_DISPLAY_WINDOW_H

#ifdef USING_SDL
#include <SDL2/SDL.h>
#endif

#include <vector>
#include <string>
#include <functional>

#include <shared/property.h>
#include <shared/logger.h>

#include <emulator/gui/Drawable.hpp>

namespace ktools
{
    namespace emulator
    {
        namespace gui
        {
            class DisplayWindow
            {
                public:
                    /**
                     * @brief      Constructs the object
                     *
                     * @param  w      The width of the window
                     * @param  h      The height of the window
                     * @param  title  The title of the window
                     */
                    DisplayWindow(const int16_t w, const int16_t h, const std::string& title)
                        : mWidth(w), mHeight(h), mTitle(title), mCreated(false) { }

                    ~DisplayWindow();

                    //// ============================================================ ////

                    /**
                     * @brief      Creates the window
                     *
                     * @return     true if successful
                     */
                    bool Create();

                    //! Destroys the window
                    void Destroy();

                    //// ============================================================ ////

                    /**
                     * @brief      Updates the window
                     *
                     * Updates all objects and processes window events
                     *
                     * @return     true if the window is not colsed
                     */
                    bool Update();

                    //! Clears the display
                    void Clear();

                    //! Updates the renderer
                    void Present();

                    //// ============================================================ ////

                    /**
                     * @brief      Draws a pixel
                     *
                     * @param  x     The x position
                     * @param  y     The y position
                     * @param  r     Red value
                     * @param  g     Green value
                     * @param  b     Blue value
                     */
                    void DrawPixel(
                        const int16_t x, const int16_t y,
                        const uint8_t r = 255,
                        const uint8_t g = 255,
                        const uint8_t b = 255);

                    /**
                     * @brief      Draws a segment (for seven segment displays)
                     *
                     * @param  x         The x position
                     * @param  y         The y position
                     * @param  vertical  Is this segment vertical?
                     * @param  set       Is this segment turned on?
                     */
                    void DrawSegment(
                        int16_t x, int16_t y,
                        const bool vertical,
                        const bool set);

                    /**
                     * @brief      Draws a circle
                     *
                     * @param  x       The x position
                     * @param  y       The y position
                     * @param  filled  Filled?
                     * @param  rad     The radius of the circle
                     */
                    void DrawCircle(
                        const int16_t x, const int16_t y,
                        const bool filled,
                        const int16_t rad = 4);

                    /**
                     * @brief      Draws a filled box
                     *
                     * @param  x     The x position
                     * @param  y     The y position
                     * @param  h     The height
                     * @param  w     The width
                     * @param  r     Red value
                     * @param  g     Green value
                     * @param  b     Blue value
                     */
                    void DrawBox(
                        const int16_t x, const int16_t y,
                        const int16_t h, const int16_t w,
                        const uint8_t r, const uint8_t g, const uint8_t b);

                    /**
                     * @brief      Draws an ascii text
                     *
                     * @param  x     The x position
                     * @param  y     The y position
                     * @param  str   The string
                     * @param  r     Red value
                     * @param  g     Green value
                     * @param  b     Blue value
                     */
                    void DrawText(
                        const int16_t x, const int16_t y,
                        const std::string& str,
                        const uint8_t r = 255,
                        const uint8_t g = 255,
                        const uint8_t b = 255);

                    /**
                     * @brief      Draws a line between 2 points
                     *
                     * @param  x1    The x position for the start point
                     * @param  y1    The y position for the start point
                     * @param  x2    The x position for the end point
                     * @param  y2    The y position for the end point
                     * @param  r     Red value
                     * @param  g     Green value
                     * @param  b     Blue value
                     */
                    void DrawLine(
                        const int16_t x1, const int16_t y1,
                        const int16_t x2, const int16_t y2,
                        const uint8_t r = 255,
                        const uint8_t g = 255,
                        const uint8_t b = 255);

                    /**
                     * @brief      Sets the keyboard input handler
                     *
                     * @param  handler  The new handler
                     */
                    void SetKeyboardHandler(const std::function<void(const char)>& handler) {
                        mLogger.verbose("new keyboard handler attached");
                        mKeyboardHandler = handler;
                    }

                    /**
                     * @brief      Adds an object to the window
                     *
                     * @param      drawable  The drawable object
                     *
                     * @note The given pointer will be freed on the window's deletion
                     */
                    DisplayWindow& operator<<(Drawable* drawable);

                    SPROPERTY_RW(bool, GraphicsEnabled, mGraphicsEnabled)
                private:
                    #ifdef USING_SDL
                    SDL_Renderer* mRenderer; //!< SDL renderer
                    SDL_Window* mWindow;     //!< SDL window
                    #endif

                    const int16_t mWidth;  //!< Width of the window
                    const int16_t mHeight; //!< Height of the window

                    std::vector<Drawable*> mElements; //!< Objects to draw

                    //! The current keyboard handler
                    std::function<void(const char)> mKeyboardHandler;

                    const std::string mTitle;     //!< Title of the window
                    bool mCreated;                //!< Is the window created?
                    static bool mGraphicsEnabled; //!< Is graphical display enabled?

                    static Logger& mLogger; //!< Logger for this class
            };
        }
    }
}

#endif
