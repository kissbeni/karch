
/**
 * @file DebugDisplay.cc
 * @author Kiss Benedek
 * @brief DebugDisplay class implementation
 */

#include <functional>

#include <emulator/gui/DebugDisplay.h>

#include <fstream>

#include <shared/profiling.h>

namespace ktools
{
    namespace emulator
    {
        namespace gui
        {
            DebugDisplay::DebugDisplay(const int16_t x, const int16_t y, const MachineContext* ctx)
                : Drawable(x, y),
                    mContext(ctx),
                    mIPS(x + 10, y + 180, 70, 230),
                    mAvgRenderTime(x + 10, y + 180, 70, 230, 180, 40, 80) { }

            void DebugDisplay::Update(const uint32_t ips, const uint32_t avg_render_time)
            {
                PROFILER_BEGIN
                mIPS.Update(ips);
                mAvgRenderTime.Update(avg_render_time);
                PROFILER_END
            }

            void DebugDisplay::DrawTo(DisplayWindow& displ) const
            {
                PROFILER_BEGIN
                char temp[64];

                displ.DrawBox(mX, mY, 205, 250, 44, 44, 44);
                displ.DrawText(mX + 5, mY + 5, "Registers:");

                snprintf(temp, 64, "RA: 0x%02X", mContext->RegA());
                displ.DrawText(mX + 5, mY + 20, temp);

                snprintf(temp, 64, "RB: 0x%02X", mContext->RegB());
                displ.DrawText(mX + 5, mY + 30, temp);

                snprintf(temp, 64, "RC: 0x%02X", mContext->RegC());
                displ.DrawText(mX + 5, mY + 40, temp);

                snprintf(temp, 64, "RD: 0x%02X", mContext->RegD());
                displ.DrawText(mX + 5, mY + 50, temp);


                snprintf(temp, 64, "IP: 0x%04X", mContext->IP());
                displ.DrawText(mX + 85, mY + 20, temp);

                snprintf(temp, 64, "SP: 0x%04X (0x%02X)", mContext->SP(), mContext->ReadOne(mContext->SP()));
                displ.DrawText(mX + 85, mY + 30, temp);

                displ.DrawText(mX + 5, mY + 70, "Flags:");


                if (mContext->FlagSet(MachineFlags::fgCARRY))
                    displ.DrawText(mX + 65, mY + 70, "C", 0, 255, 0);
                else
                    displ.DrawText(mX + 65, mY + 70, "C", 255, 0, 0);

                if (mContext->FlagSet(MachineFlags::fgZERO))
                    displ.DrawText(mX + 75, mY + 70, "Z", 0, 255, 0);
                else
                    displ.DrawText(mX + 75, mY + 70, "Z", 255, 0, 0);

                if (mContext->FlagSet(MachineFlags::fgSIGNED))
                    displ.DrawText(mX + 85, mY + 70, "S", 0, 255, 0);
                else
                    displ.DrawText(mX + 85, mY + 70, "S", 255, 0, 0);

                if (mContext->FlagSet(MachineFlags::fgOVERFLOW))
                    displ.DrawText(mX + 95, mY + 70, "O", 0, 255, 0);
                else
                    displ.DrawText(mX + 95, mY + 70, "O", 255, 0, 0);

                snprintf(temp, 64, "Bank: #%d", mContext->CurrentMemoryBank());
                displ.DrawText(mX + 120, mY + 70, temp);

                snprintf(temp, 64, "%d", mIPS.Max());
                displ.DrawText(mX + 5, mY + 90, temp, mIPS.R(), mIPS.G(), mIPS.B());

                snprintf(temp, 64, "%d", mAvgRenderTime.Max());
                displ.DrawText(mX + 40, mY + 90, temp, mAvgRenderTime.R(), mAvgRenderTime.G(), mAvgRenderTime.B());

                snprintf(temp, 64, "%dIPS", mIPS.Current());
                displ.DrawText(mX + 10, mY + 190, temp, mIPS.R(), mIPS.G(), mIPS.B());

                snprintf(temp, 64, "Render: %dus", mAvgRenderTime.Current());
                displ.DrawText(mX + 70, mY + 190, temp, mAvgRenderTime.R(), mAvgRenderTime.G(), mAvgRenderTime.B());

                mIPS.DrawTo(displ);
                mAvgRenderTime.DrawTo(displ);

                // Axes
                displ.DrawLine(mX + 10, mY + 110, mX + 10, mY + 180);
                displ.DrawLine(mX + 10, mY + 180, mX + 240, mY + 180);
                PROFILER_END
            }
        }
    }
}
