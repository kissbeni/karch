
/**
 * @file LEDDisplay.h
 * @author Kiss Benedek
 * @brief LEDDisplay class
 */

#ifndef KEMU_LED_DISPLAY_H
#define KEMU_LED_DISPLAY_H

#include <emulator/gui/DisplayWindow.h>
#include <emulator/gui/Drawable.hpp>

namespace ktools
{
    namespace emulator
    {
        namespace gui
        {
            //! LED displaying element
            class LEDDisplay : public Drawable
            {
                public:
                    /**
                     * @brief      Constructs the object
                     *
                     * @param  x     The x position
                     * @param  y     The y position
                     */
                    LEDDisplay(const int16_t x, const int16_t y);

                    void DrawTo(DisplayWindow& window) const override;

                    /**
                     * @brief      Sets the value
                     *
                     * @param  val   The new value
                     */
                    void SetValue(const uint8_t val) {
                        mValue = val & 0xF;
                    }

                    /**
                     * @brief      Gets the value
                     *
                     * @return     The value
                     */
                    const uint8_t GetValue() const {
                        return mValue;
                    }
                private:
                    uint8_t mValue; //!< The current value
            };
        }
    }
}

#endif
