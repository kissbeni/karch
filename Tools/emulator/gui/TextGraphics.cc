
/**
 * @file TextGraphics.cc
 * @author Kiss Benedek
 * @brief TextGraphics class implementation
 */

#include "TextGraphics.h"
#include <fstream>
#include <cstring>

#include <shared/profiling.h>

namespace ktools
{
    namespace emulator
    {
        namespace gui
        {
            Logger& TextGraphics::mLogger = Logger::Get("TextGraphics");

            TextGraphics::TextGraphics(const std::string& fontFileName, const int16_t w, const int16_t h)
                : Drawable(0, 0), mWidth(w), mHeight(h), mPixelsSetItems(((w * h) / mPixelsSetBits) + 1),
                  mPixelsSet(nullptr)
            {
                PROFILER_BEGIN
                if (DisplayWindow::GetGraphicsEnabled())
                {
                    std::ifstream infile;
                    infile.open(fontFileName, std::ios::binary | std::ios::in);

                    if (!infile.is_open())
                        throw std::runtime_error("Could not open file");

                    uint8_t buf[8];
                    while (infile.read((char*)buf, 8) && mFont.size() <= 255)
                    {
                        uint8_t* addbuf = new uint8_t[8];
                        memcpy(addbuf, buf, 8);
                        mFont.push_back(addbuf);
                    }

                    mPixelsSet = new uint64_t[mPixelsSetItems];
                    mPixelsToUpdate = new uint64_t[mPixelsSetItems];
                    Clear();

                    mLogger.verbose("loaded %ld symbols from %s", mFont.size(), fontFileName.c_str());

                    infile.close();
                }
                else mLogger.warn("loading skipped because running in CLI mode");
                PROFILER_END
            }

            TextGraphics::~TextGraphics()
            {
                for (std::vector<uint8_t*>::iterator it = mFont.begin(); it != mFont.end(); it++)
                {
                    delete[] *it;
                    *it = nullptr;
                }

                delete[] mPixelsSet;
                delete[] mPixelsToUpdate;

                mFont.clear();
            }

            void TextGraphics::Clear()
            {
                if (mPixelsSet)
                {
                    memset(mPixelsSet, 0, sizeof(uint64_t) * mPixelsSetItems);
                    memset(mPixelsToUpdate, 0xff, sizeof(uint64_t) * mPixelsSetItems);
                }
            }

            void TextGraphics::DrawCharacter(const int16_t x, const int16_t y, const uint8_t charCode)
            {
                PROFILER_BEGIN
                if (mFont.size() <= charCode)
                {
                    mLogger.error("Invalid char code {0x%02x} for the current font", charCode);
                    PROFILER_END
                    return;
                }

                uint8_t* buf = mFont[charCode];
                for (int16_t oy = 0; oy < 8; oy++)
                {
                    uint8_t b = buf[oy];

                    for (int16_t ox = 0; ox < 8; ox++)
                    {
                        SetPixel(x + ox, y + oy, b & 128);
                        b <<= 1;
                    }
                }
                PROFILER_END
            }

            bool TextGraphics::CanFitCharacter(const int16_t x, const int16_t y)
            {
                return (x >= 0 && y >= 0 && x <= (mWidth - 8) && y <= (mHeight - 8));
            }

            void TextGraphics::Scroll()
            {
                PROFILER_BEGIN
                for (int16_t x = 0; x < mWidth; x++)
                    for (int16_t y = 8; y < mHeight; y++)
                    {
                        SetPixel(x, y - 8, GetPixel(x, y));
                        SetPixel(x, y, false);
                    }

                PROFILER_END
            }

            void TextGraphics::DrawTo(DisplayWindow& displ) const
            {
                PROFILER_BEGIN
                if (!mPixelsSet)
                {
                    PROFILER_END
                    return;
                }

                for (int16_t x = 0; x < mWidth; x++)
                    for (int16_t y = 0; y < mHeight; y++)
                        if (PixelNeedsUpdate(x, y))
                        {
                            if (GetPixel(x, y))
                                displ.DrawPixel(x, y);
                            else
                                displ.DrawPixel(x, y, 0, 60, 100);
                        }

                memset(mPixelsToUpdate, 0, sizeof(uint64_t) * mPixelsSetItems);
                PROFILER_END
            }

            void TextGraphics::SetPixel(const int16_t x, const int16_t y, const bool value)
            {
                if (!mPixelsSet)
                    return;

                uint32_t index = (uint32_t)x * mHeight + y;
                uint64_t flg = 1lu << (index % mPixelsSetBits);

                if (!!(mPixelsSet[index / mPixelsSetBits] & flg) != value)
                    mPixelsToUpdate[index / mPixelsSetBits] |= flg;

                if (value)
                    mPixelsSet[index / mPixelsSetBits] |= flg;
                else
                    mPixelsSet[index / mPixelsSetBits] &= ~flg;
            }

            bool TextGraphics::GetPixel(const int16_t x, const int16_t y) const
            {
                if (!mPixelsSet)
                    return false;

                uint32_t index = (uint32_t)x * mHeight + y;
                return mPixelsSet[index / mPixelsSetBits] & (1lu << (index % mPixelsSetBits));
            }

            bool TextGraphics::PixelNeedsUpdate(const int16_t x, const int16_t y) const
            {
                if (!mPixelsSet)
                    return false;

                uint32_t index = (uint32_t)x * mHeight + y;
                return mPixelsToUpdate[index / mPixelsSetBits] & (1lu << (index % mPixelsSetBits));
            }
        }
    }
}
