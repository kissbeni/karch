
/**
 * @file DebugDisplay.h
 * @author Kiss Benedek
 * @brief DebugDisplay class
 */

#ifndef KEMU_DEBUG_DISPLAY_H
#define KEMU_DEBUG_DISPLAY_H

#include <emulator/MachineContext.hpp>
#include <emulator/gui/Drawable.hpp>
#include <emulator/gui/DisplayWindow.h>
#include <emulator/gui/GraphLine.hpp>

namespace ktools
{
    namespace emulator
    {
        namespace gui
        {
            //! Debugging display element
            class DebugDisplay : public Drawable
            {
                public:
                    /**
                     * @brief      Constructs the object
                     *
                     * @param  x     The x position
                     * @param  y     The y position
                     * @param  ctx   The context to dispaly
                     */
                    DebugDisplay(const int16_t x, const int16_t y,
                        const MachineContext* ctx);

                    /**
                     * @brief      Update statistics
                     *
                     * @param  ips              The IPS (instructions per second)
                     * @param  avg_render_time  The average render time (in the last second)
                     */
                    void Update(const uint32_t ips,
                                const uint32_t avg_render_time);

                    void DrawTo(DisplayWindow& displ) const override;
                private:
                    const MachineContext* mContext; //!< The context to be displayed
                    GraphLine<uint32_t> mIPS; //!< Graph for the IPS data
                    GraphLine<uint32_t> mAvgRenderTime; //!< Graph for the render time data
            };
        }
    }
}

#endif
