
/**
 * @file TextGraphics.h
 * @author Kiss Benedek
 * @brief TextGraphics class
 */

#ifndef TEXT_GRAPHICS_H
#define TEXT_GRAPHICS_H

#include <emulator/gui/DisplayWindow.h>
#include <emulator/gui/Drawable.hpp>

#include <shared/testing.h>

#include <vector>
#include <string>

namespace ktools
{
    namespace emulator
    {
        namespace gui
        {
            //! Text graphics display element
            class TextGraphics : public Drawable
            {
                public:
                    /**
                     * @brief      Constructs the object
                     *
                     * @param[in]  fontFileName  The font file name
                     * @param[in]  w             The height of the used area
                     * @param[in]  h             The width of the used area
                     */
                    TextGraphics(const std::string& fontFileName, const int16_t w, const int16_t h);
                    ~TextGraphics();

                    //! Clears all text
                    VIRTUAL_TESTING void Clear();

                    /**
                     * @brief      Draws a character
                     *
                     * @param  x         The x position
                     * @param  y         The y position
                     * @param  charCode  The character code
                     */
                    VIRTUAL_TESTING void DrawCharacter(const int16_t x, const int16_t y, const uint8_t charCode);

                    /**
                     * @brief      Determines ability to fit a character
                     *
                     * @param  x     The x position
                     * @param  y     The y position
                     *
                     * @return     True if able to fit a character, false otherwise
                     */
                    VIRTUAL_TESTING bool CanFitCharacter(const int16_t x, const int16_t y);

                    VIRTUAL_TESTING void Scroll();

                    VIRTUAL_TESTING void DrawTo(DisplayWindow& displ) const override;
                private:
                    /**
                     * @brief      Sets a pixel
                     *
                     * @param  x     The x position
                     * @param  y     The y position
                     * @param  value The value to set
                     */
                    void SetPixel(const int16_t x, const int16_t y, const bool value);

                    /**
                     * @brief      Get a pixel's value
                     *
                     * @param  x     The x position
                     * @param  y     The y position
                     *
                     * @return     The pixel state
                     */
                    bool GetPixel(const int16_t x, const int16_t y) const;

                    /**
                     * @brief      Checks if a pixel needs to be rendered
                     *
                     * @param[in]  x     The x position
                     * @param[in]  y     The y position
                     *
                     * @return     True if the pixel should be rendered, false otherwise
                     */
                    bool PixelNeedsUpdate(const int16_t x, const int16_t y) const;

                    std::vector<uint8_t*> mFont; //!< The font data

                    int16_t mWidth,              //!< The width of the area
                            mHeight;             //!< The height of the area

                    //! Number of bits per entry in @ref mPixelsSet
                    static const uint8_t mPixelsSetBits = sizeof(uint64_t) * 8;
                    const size_t mPixelsSetItems; //!< Number of entries in @ref mPixelsSet

                    uint64_t* mPixelsSet;       //!< Bitfield for which pixels are set
                    uint64_t* mPixelsToUpdate;  //!< Bitfield for which pixels should be updated

                    static Logger& mLogger; //!< Logger for this class
            };
        }
    }
}

#endif
