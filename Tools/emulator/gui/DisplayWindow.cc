
/**
 * @file DisplayWindow.cc
 * @author Kiss Benedek
 * @brief DisplayWindow class implementation
 */

#include "DisplayWindow.h"

#ifdef USING_SDL
#   include <SDL2/SDL.h>
#   include <SDL2/SDL2_gfxPrimitives.h>
#endif
#include <emulator/gui/Drawable.hpp>

#include <shared/profiling.h>

namespace ktools
{
    namespace emulator
    {
        namespace gui
        {
            bool DisplayWindow::mGraphicsEnabled =
            #ifdef USING_SDL
            true;
            #else
            false;
            #endif

            Logger& DisplayWindow::mLogger = Logger::Get("DisplayWindow");

            DisplayWindow::~DisplayWindow()
            {
                for (Drawable*& d : mElements)
                {
                    delete d;
                    d = nullptr;
                }
            }


            bool DisplayWindow::Create()
            {
                PROFILER_BEGIN

                if (mCreated)
                {
                    PROFILER_END
                    return false;
                }

                mLogger.verbose("creating window");

#ifdef USING_SDL
                if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
                {
                    mLogger.error("Could not initialize SDL!");
                    PROFILER_END
                    return false;
                }

                mWindow = SDL_CreateWindow(
                    mTitle.c_str(),
                    SDL_WINDOWPOS_CENTERED,
                    SDL_WINDOWPOS_CENTERED,
                    mWidth,
                    mHeight,
                    0
                );

                if (!mWindow)
                {
                    mLogger.error("Could not create window!");
                    PROFILER_END
                    return false;
                }

                mRenderer = SDL_CreateRenderer(mWindow, -1, SDL_RENDERER_SOFTWARE);
                if (!mRenderer)
                {
                    mLogger.error("Could not create renderer!");
                    PROFILER_END
                    return false;
                }

                SDL_RenderClear(mRenderer);
                boxRGBA(mRenderer, 0, 0, mWidth, mHeight, 0x00, 0x00, 0x00, 0xFF);
#endif
                Update();
                mCreated = true;

                mLogger.verbose("window created");

                PROFILER_END
                return true;
            }

            void DisplayWindow::Destroy()
            {
                if (!mCreated)
                    return;

                #ifdef USING_SDL
                    mLogger.verbose("destroying window");
                    SDL_DestroyRenderer(mRenderer);
                    SDL_DestroyWindow(mWindow);
                    SDL_Quit();
                    mLogger.verbose("window destroyed");
                #endif
                mCreated = false;
            }


            bool DisplayWindow::Update()
            {
                PROFILER_BEGIN
                if (!mCreated)
                {
                    PROFILER_END
                    return false;
                }

                mLogger.debug("window update");

                #ifdef USING_SDL
                    SDL_Event ev;
                    while (SDL_PollEvent(&ev))
                    {
                        switch (ev.type)
                        {
                            case SDL_QUIT:
                                mLogger.verbose("event: SDL_QUIT");
                                PROFILER_END
                                return false;
                            case SDL_WINDOWEVENT:
                                mLogger.verbose("event: SDL_WINDOWEVENT");
                                mLogger.debug("We should redraw everything!");
                                break;
                            case SDL_TEXTINPUT:
                                mLogger.verbose("event: SDL_TEXTINPUT");
                                if (mKeyboardHandler)
                                    for (size_t i = 0; ev.text.text[i]; i++)
                                        mKeyboardHandler(ev.text.text[i]);
                                break;
                            default:
                                // printf("[DisplayWindow] unhandled SDL event!\n");
                                break;
                        }
                    }

                    // SDL_RenderClear(mRenderer);
                    // boxRGBA(mRenderer, 0, 0, mWidth, mHeight, 0x00, 0x00, 0x00, 0xFF);
                #endif

                for (Drawable*& d : mElements)
                    d->DrawTo(*this);

                Present();

                PROFILER_END
                return true;
            }

            void DisplayWindow::Clear()
            {
                PROFILER_BEGIN
                if (!mCreated)
                {
                    PROFILER_END
                    return;
                }

                mLogger.verbose("clearing window");

                #ifdef USING_SDL
                    boxRGBA(mRenderer, 0, 0, mWidth, mHeight, 0x00, 0x00, 0x00, 0xFF);
                #endif

                Present();
                PROFILER_END
            }

            void DisplayWindow::Present()
            {
                PROFILER_BEGIN
                #ifdef USING_SDL
                    if (mCreated)
                        SDL_RenderPresent(mRenderer);
                #endif
                PROFILER_END
            }


            void DisplayWindow::DrawPixel(const int16_t x, const int16_t y,
                const uint8_t r, const uint8_t g, const uint8_t b)
            {
                if (!mCreated)
                    return;

                #ifdef USING_SDL
                    SDL_SetRenderDrawColor(mRenderer, r, g, b, 255);
                    SDL_RenderDrawPoint(mRenderer, x, y);
                #endif
            }

            void DisplayWindow::DrawSegment(int16_t x, int16_t y,
                const bool vertical, const bool set)
            {
                PROFILER_BEGIN
                #ifdef USING_SDL
                    if (vertical)
                    {
                        int16_t tmp = y;
                        y = x;
                        x = tmp;
                    }

                    int16_t px[6] = {
                        static_cast<int16_t>(x),
                        static_cast<int16_t>(x+4),
                        static_cast<int16_t>(x+23),
                        static_cast<int16_t>(x+27),
                        static_cast<int16_t>(x+23),
                        static_cast<int16_t>(x+4)
                    };

                    int16_t py[6] = {
                        static_cast<int16_t>(y+4),
                        static_cast<int16_t>(y),
                        static_cast<int16_t>(y),
                        static_cast<int16_t>(y+4),
                        static_cast<int16_t>(y+8),
                        static_cast<int16_t>(y+8)
                    };

                    if (set)
                    {
                        if (vertical)
                            filledPolygonRGBA(mRenderer, py, px, 6, 255, 0, 0, 255);
                        else
                            filledPolygonRGBA(mRenderer, px, py, 6, 255, 0, 0, 255);
                    }
                    else
                    {
                        if (vertical)
                            filledPolygonRGBA(mRenderer, py, px, 6, 0, 0, 0, 255);
                        else
                            filledPolygonRGBA(mRenderer, px, py, 6, 0, 0, 0, 255);
                    }

                    if (vertical)
                        polygonRGBA(mRenderer, py, px, 6, 60, 60, 60, 255);
                    else
                        polygonRGBA(mRenderer, px, py, 6, 60, 60, 60, 255);
                #endif
                PROFILER_END
            }

            void DisplayWindow::DrawCircle(const int16_t x, const int16_t y,
                const bool filled, const int16_t rad)
            {
                #ifdef USING_SDL
                    if (filled)
                        filledCircleRGBA(mRenderer, x, y, rad, 255, 0, 0, 255);
                    else
                        circleRGBA(mRenderer, x, y, rad, 60, 60, 60, 255);
                #endif
            }

            void DisplayWindow::DrawBox(const int16_t x, const int16_t y, const int16_t h,
                const int16_t w, const uint8_t r, const uint8_t g, const uint8_t b)
            {
                #ifdef USING_SDL
                    boxRGBA(mRenderer, x, y, x + w, y + h, r, g, b, 255);
                #endif
            }

            void DisplayWindow::DrawText(const int16_t x, const int16_t y,
                const std::string& str, const uint8_t r, const uint8_t g, const uint8_t b)
            {
                #ifdef USING_SDL
                    stringRGBA(mRenderer, x, y, str.c_str(), r, g, b, 255);
                #endif
            }

            void DisplayWindow::DrawLine(const int16_t x1, const int16_t y1, const int16_t x2,
                const int16_t y2, const uint8_t r, const uint8_t g, const uint8_t b)
            {
                #ifdef USING_SDL
                    lineRGBA(mRenderer, x1, y1, x2, y2, r, g, b, 255);
                #endif
            }

            DisplayWindow& DisplayWindow::operator<<(Drawable* drawable)
            {
                mElements.push_back(drawable);
                return *this;
            }
        }
    }
}
