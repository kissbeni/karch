
/**
 * @file SevenSegmentDisplay.h
 * @author Kiss Benedek
 * @brief SevenSegmentDisplay class
 */

#ifndef KEMU_SEVEN_SEGMENT_DISPLAY_H
#define KEMU_SEVEN_SEGMENT_DISPLAY_H

#include <emulator/gui/DisplayWindow.h>
#include <emulator/gui/Drawable.hpp>

#include <shared/property.h>

namespace ktools
{
    namespace emulator
    {
        namespace gui
        {
            //! A single seven segment digit
            class SevenSegmentDisplay : public Drawable
            {
                public:
                    /**
                     * @brief      Constructs the object
                     *
                     * @param  x     The x position
                     * @param  y     The y position
                     */
                    SevenSegmentDisplay(const int16_t x, const int16_t y);

                    void DrawTo(DisplayWindow& window) const override;

                    /**
                     * @brief      Sets the value
                     *
                     * @param  val   The value
                     */
                    void SetValue(const uint8_t val) {
                        mValue = val & 0xF;
                    }

                    /**
                     * @brief      Gets the value
                     *
                     * @return     The current value
                     */
                    const uint8_t GetValue() const {
                        return mValue;
                    }

                    PROPERTY_RW(bool, DecimalPoint, mDP)
                    PROPERTY_RW(bool, Enabled, mEnabled)
                private:
                    static const uint8_t mHex2SevenSeg[];   //!< Hex segment bits
                    uint8_t mValue;                         //!< The current value
                    bool mDP,                               //!< Decimal point enabled?
                         mEnabled;                          //!< This display is enabled?
            };
        }
    }
}

#endif
