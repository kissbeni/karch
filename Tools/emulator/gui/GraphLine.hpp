
/**
 * @file GraphLine.hpp
 * @author Kiss Benedek
 * @brief GraphLine class
 */

#ifndef KEMU_GRAPH_LINE_H
#define KEMU_GRAPH_LINE_H

#include <shared/property.h>
#include <emulator/gui/Drawable.hpp>
#include <emulator/gui/DisplayWindow.h>
#include <shared/profiling.h>

namespace ktools
{
    namespace emulator
    {
        namespace gui
        {
            /**
             * @brief      Class for a data graph line
             *
             * @tparam     T     The stored data type
             */
            template<typename T>
            class GraphLine : public Drawable
            {
                public:
                    /**
                     * @brief      Constructs the object
                     *
                     * @param  x     The x position of the drawing area
                     * @param  y     The y position of the drawing area
                     * @param  h     The height of the drawing area
                     * @param  w     The width of the drawing area
                     * @param  r     The red value of the line
                     * @param  g     The green value of the line
                     * @param  b     The blue value of the line
                     */
                    GraphLine(const int16_t x, const int16_t y, const int16_t h, const int16_t w,
                              const uint8_t r = 255, const uint8_t g = 255, const uint8_t b = 255)
                        : Drawable(x, y), mHeight(h), mWidth(w), mR(r), mG(g), mB(b), mMax(0)
                    {
                        mGraphVals = new T[w];
                        memset(mGraphVals, 0, sizeof(T) * w);
                    }

                    ~GraphLine()
                    {
                        delete[] mGraphVals;
                    }

                    /**
                     * @brief      Adds a new value to the graph, shifting everything left
                     *
                     * @param  val   The value to add
                     */
                    void Update(const T val)
                    {
                        PROFILER_BEGIN
                        for (int i = 1; i < mWidth; i++)
                            mGraphVals[i - 1] = mGraphVals[i];

                        mMax = mGraphVals[mWidth - 1] = val;

                        for (int i = 0; i < mWidth; i++)
                            if (mMax < mGraphVals[i])
                                mMax = mGraphVals[i];

                        PROFILER_END
                    }

                    void DrawTo(DisplayWindow& displ) const override
                    {
                        PROFILER_BEGIN

                        if (mMax == 0)
                        {
                            PROFILER_END
                            return;
                        }

                        for (int i = 1; i < mWidth; i++)
                            displ.DrawLine(
                                mX + i - 1,
                                mY - (((float)mGraphVals[i - 1] / mMax) * mHeight),
                                mX + i,
                                mY - (((float)mGraphVals[i] / mMax) * mHeight),
                                mR, mG, mB
                            );

                        PROFILER_END
                    }

                    /**
                     * @brief      Gives the latest entry
                     *
                     * @return     The last entry added to the graph
                     */
                    const T& Current() const {
                        return mGraphVals[mWidth - 1];
                    }

                    PROPERTY_R(T, Max, mMax);
                    PROPERTY_R(uint8_t, R, mR);
                    PROPERTY_R(uint8_t, G, mG);
                    PROPERTY_R(uint8_t, B, mB);
                private:
                    const int16_t mHeight, //!< The height of the drawing area
                                  mWidth;  //!< The width of the drawing area

                    const uint8_t mR, //!< The graph line's red value
                                  mG, //!< The graph line's green value
                                  mB; //!< The graph line's blue value

                    T mMax;         //!< Maximum value of the graph
                    T* mGraphVals;  //!< Graph data
            };
        }
    }
}

#endif
