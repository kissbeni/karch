
/**
 * @file SevenSegmentDisplay.cc
 * @author Kiss Benedek
 * @brief SevenSegmentDisplay class implementation
 */

#include <emulator/gui/SevenSegmentDisplay.h>
#include <shared/profiling.h>

namespace ktools
{
    namespace emulator
    {
        namespace gui
        {
            const uint8_t SevenSegmentDisplay::mHex2SevenSeg[] =
            {
                //GFEDCBA
                0b0111111,  // 0
                0b0000110,  // 1
                0b1011011,  // 2
                0b1001111,  // 3
                0b1100110,  // 4
                0b1101101,  // 5
                0b1111101,  // 6
                0b0000111,  // 7
                0b1111111,  // 8
                0b1101111,  // 9
                0b1110111,  // A
                0b1111100,  // B
                0b0111001,  // C
                0b1011110,  // D
                0b1111001,  // E
                0b1110001   // F
            };

            SevenSegmentDisplay::SevenSegmentDisplay(const int16_t x, const int16_t y)
                : Drawable(x, y), mEnabled(false)
            {
                mValue = 0xF;
                mDP = false;
            }

            void SevenSegmentDisplay::DrawTo(DisplayWindow& window) const
            {
                PROFILER_BEGIN
                uint8_t var = mEnabled ? mHex2SevenSeg[mValue] : 0;

                const int16_t offsetsX[] = { 4, 27, 27, 4, 0, 0, 4 };
                const int16_t offsetsY[] = { 0, 3, 29, 26 * 2, 29, 3, 26 };
                const bool directions[] = { false, true, true, false, true, true, false };

                for (int i = 0; i < 7; i++)
                    if (!((var >> i) & 1))
                        window.DrawSegment(mX + offsetsX[i], mY + offsetsY[i], directions[i], false);

                for (int i = 0; i < 7; i++)
                    if ((var >> i) & 1)
                        window.DrawSegment(mX + offsetsX[i], mY + offsetsY[i], directions[i], true);

                window.DrawCircle(mX + 44,    mY + 26 * 2 + 4, mDP, 4);
                PROFILER_END
            }
        }
    }
}
