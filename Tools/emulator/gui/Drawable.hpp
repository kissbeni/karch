
/**
 * @file Drawable.hpp
 * @author Kiss Benedek
 * @brief Drawable interface
 */

#ifndef KEMU_DRAWABLE_H
#define KEMU_DRAWABLE_H

#include <cstdint>

namespace ktools
{
    namespace emulator
    {
        namespace gui
        {
            class DisplayWindow;

            //! Abstract drawable object
            class Drawable
            {
                public:
                    /**
                     * @brief      Constructs the object
                     *
                     * @param  x     The x position
                     * @param  y     The y position
                     */
                    Drawable(const int16_t x, const int16_t y) : mX(x), mY(y) {}
                    virtual ~Drawable() {}

                    /**
                     * @brief      Draws this object on the given window
                     *
                     * @param      window  The window
                     */
                    virtual void DrawTo(DisplayWindow& window) const = 0;
                protected:
                    const int16_t mX, //!< The x position of this object
                                  mY; //!< The y position of this object
            };
        }
    }
}

#endif
