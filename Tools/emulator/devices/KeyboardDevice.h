
/**
 * @file KeyboardDevice.h
 * @author Kiss Benedek
 * @brief KeyboardDevice class
 */

#ifndef KEMU_KEYBOARD_DEVICE_H
#define KEMU_KEYBOARD_DEVICE_H

#include <emulator/devices/PortDefs.h>
#include <emulator/devices/DeviceInterface.h>

#include <deque>
#include <iostream>

namespace ktools
{
    namespace emulator
    {
        namespace devices
        {
            //! Implements a simplified PS/2 keyboard
            class KeyboardDevice : public DeviceInterface<IOPORT_PS2_DATA>
            {
                public:
                    /**
                     * @brief      Constructs the object
                     *
                     * @param  maxkeys  Maximum keycodes to queue up
                     */
                    KeyboardDevice(const size_t maxkeys)
                        : mMaxInQueue(maxkeys) { }

                    const karch_word_t OnRead() override;

                    //// ============================================================ ////

                    /**
                     * @brief      Sends an ascii character
                     *
                     * Converts the given ascii character to it's corresponding
                     * PS/2 key code(s).
                     *
                     * @param  chr   The character
                     *
                     * @note Only a limited number of characters supported
                     */
                    void SendASCII(const char chr);

                    /**
                     * @brief      Send a raw multi-byte keycode
                     *
                     * @param  codes  Bytes of the keycode(s)
                     * @param  len    The length of @p codes
                     *
                     * @note @p len _can_ be longer than the defined maxmum length, in
                     * this case, the oldest keycode in the queue is removed until the
                     * data fits into the buffer.
                     */
                    void SendRawKeycode(const uint8_t* codes, const size_t len);

                    /**
                     * @brief      Send a raw keycode
                     *
                     * @param  code  The raw keycode
                     */
                    void SendRawKeycode(const uint8_t code);

                    //// ============================================================ ////

                    /**
                     * @brief      Determines if the key queue is empty
                     *
                     * @return     True if key if the queue is empty, False otherwise
                     */
                    bool IsKeyQueueEmpty() const {
                        return mKeyQueue.empty();
                    }
                private:
                    std::deque<karch_word_t> mKeyQueue; //!< FIFO queue for key codes
                    size_t mMaxInQueue; //!< Maximum entries in the queue
            };
        }
    }
}

#endif
