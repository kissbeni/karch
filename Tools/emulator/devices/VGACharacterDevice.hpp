
/**
 * @file VGACharacterDevice.hpp
 * @author Kiss Benedek
 * @brief VGACharacterDevice class
 */

#ifndef VGA_CHARACTER_DEVICE_H
#define VGA_CHARACTER_DEVICE_H

#include <emulator/devices/PortDefs.h>
#include <emulator/devices/DeviceInterface.h>
#include <emulator/gui/TextGraphics.h>

#include <iostream>

namespace ktools
{
    namespace emulator
    {
        namespace devices
        {
            /**
             * @brief Implements a device for character output
             *
             * @see @ref gui::TextGraphics
             */
            class VGACharacterDevice : public DeviceInterface<IOPORT_VGA_CHARACTER_DEVICE>
            {
                public:
                    /**
                     * @brief      Constructs the object
                     *
                     * @param      graphics  Text graphics to use
                     */
                    VGACharacterDevice(gui::TextGraphics* graphics)
                    {
                        mWindow = graphics;
                        mCurX = 0;
                        mCurY = 0;

                        mWindow->Clear();
                    }

                    void OnWrite(const karch_word_t data) override
                    {
                        if (gui::DisplayWindow::GetGraphicsEnabled())
                        {
                            mWindow->DrawCharacter(mCurX, mCurY, data);

                            mCurX += 8;

                            if (!mWindow->CanFitCharacter(mCurX, mCurY))
                            {
                                mCurX = 0;

                                if (!mWindow->CanFitCharacter(mCurX, mCurY + 8))
                                {
                                    mCurX = 0;
                                    mWindow->Scroll();
                                }
                                else mCurY += 8;
                            }

                            mWindow->DrawCharacter(mCurX, mCurY, 219);
                        }
                        else std::cout << (char)data;
                    }

                private:
                    gui::TextGraphics* mWindow; //!< The display to output to
                    uint16_t mCurX, //!< The current X position of the cursor
                             mCurY; //!< The current Y position of the cursor
            };
        }
    }
}

#endif
