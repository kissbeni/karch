
/**
 * @file PortDefs.h
 * @author Kiss Benedek
 * @brief Device port number constants
 */

#ifndef PORT_DEFS_H
#define PORT_DEFS_H

/**
 * @brief I/O port for LEDs
 * @see @ref ktools::emulator::devices::LEDDevice
 */
#define IOPORT_LEDS                 0x01


/**
 * @brief I/O port for sevent segment display flags
 * @see @ref ktools::emulator::devices::SevenSegmentDisplay
 */
#define IOPORT_SSDIS_FLAGS          0x02

/**
 * @brief I/O port for sevent segment display left side
 * @see @ref ktools::emulator::devices::SevenSegmentDisplay
 */
#define IOPORT_SSDIS_SEG_L          0x03

/**
 * @brief I/O port for sevent segment display right side
 * @see @ref ktools::emulator::devices::SevenSegmentDisplay
 */
#define IOPORT_SSDIS_SEG_R          0x04


/**
 * @brief I/O port for buttons
 */
#define IOPORT_BUTTONS              0x11


/**
 * @brief I/O port for PS/2
 */
#define IOPORT_PS2_DATA             0x12


/**
 * @brief I/O port for SPI flash address (upper 8 bits)
 */
#define IOPORT_SPI_FLASH_ADDR_HI8   0x20

/**
 * @brief I/O port for SPI flash address (lower 8 bits)
 */
#define IOPORT_SPI_FLASH_ADDR_LO8   0x21

/**
 * @brief I/O port for SPI flash data
 */
#define IOPORT_SPI_FLASH_DATA       0x22


/**
 * @brief I/O port for VGA 80 column character device
 * @see @ref ktools::emulator::devices::VGACharacterDevice
 */
#define IOPORT_VGA_CHARACTER_DEVICE 0xD0

#endif
