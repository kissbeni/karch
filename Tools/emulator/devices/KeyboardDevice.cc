
/**
 * @file KeyboardDevice.cc
 * @author Kiss Benedek
 * @brief KeyboardDevice class
 */

#include <emulator/devices/KeyboardDevice.h>
#include <shared/profiling.h>

namespace ktools
{
    namespace emulator
    {
        namespace devices
        {
            //! Information for an ASCII -> PS/2 mappings
            struct keydef {
                const char ascii;
                const uint8_t key[8];
            };

            /**
             * @brief List of ASCII -> PS/2 mappings
             *
             * @see https://gitlab.com/kissbeni/ct02-keyboard-stuff/blob/master/oldfw_328p/PS2KeyCode.h
             */
            const keydef gKeycodeMap[] = {
                // numbers:
                { '0',  { 0x45 } },
                { '1',  { 0x16 } },
                { '2',  { 0x1E } },
                { '3',  { 0x26 } },
                { '4',  { 0x25 } },
                { '5',  { 0x2E } },
                { '6',  { 0x36 } },
                { '7',  { 0x3D } },
                { '8',  { 0x3E } },
                { '9',  { 0x46 } },
                // misc:
                { ' ',  { 0x29 } },
                { '`',  { 0x0E } },
                { '-',  { 0x4E } },
                { '=',  { 0x55 } },
                { '\t', { 0x0D } },
                { '\n', { 0x5A } },
                { '*',  { 0x7C } },
                { '[',  { 0x54 } },
                { ']',  { 0x5B } },
                { '+',  { 0x79 } },
                { '\\', { 0x5D } },
                { '.',  { 0x71 } },
                { ';',  { 0x4C } },
                { '\'', { 0x52 } },
                // letters:
                { 'Q',  { 0x15 } },
                { 'W',  { 0x1D } },
                { 'E',  { 0x24 } },
                { 'R',  { 0x2D } },
                { 'T',  { 0x2C } },
                { 'Y',  { 0x35 } },
                { 'U',  { 0x3C } },
                { 'I',  { 0x43 } },
                { 'O',  { 0x44 } },
                { 'P',  { 0x4D } },
                { 'A',  { 0x1C } },
                { 'S',  { 0x1B } },
                { 'D',  { 0x23 } },
                { 'F',  { 0x2B } },
                { 'G',  { 0x34 } },
                { 'H',  { 0x33 } },
                { 'J',  { 0x3B } },
                { 'K',  { 0x42 } },
                { 'L',  { 0x4B } },
                { 'Z',  { 0x1A } },
                { 'X',  { 0x22 } },
                { 'C',  { 0x21 } },
                { 'V',  { 0x2A } },
                { 'B',  { 0x32 } },
                { 'N',  { 0x31 } },
                { 'M',  { 0x3A } }
            };

            //! Number of items in @ref gKeyCodeMap
            const size_t gKeycodeMapSize = sizeof(gKeycodeMap) / sizeof(keydef);

            const karch_word_t KeyboardDevice::OnRead()
            {
                if (mKeyQueue.empty())
                    return 0;

                karch_word_t res = mKeyQueue.front();
                mKeyQueue.pop_front();
                return res;
            }

            void KeyboardDevice::SendASCII(const char chr)
            {
                uint8_t k;
                for (size_t i = 0; i < gKeycodeMapSize; i++)
                    if (gKeycodeMap[i].ascii == chr)
                    {
                        k = 0;
                        while (gKeycodeMap[i].key[k++]);
                        SendRawKeycode(gKeycodeMap[i].key, k - 1);
                        return;
                    }

                printf("Missing ascii to keycode mapping for 0x%02x!\n", chr);
            }

            void KeyboardDevice::SendRawKeycode(const uint8_t* codes, const size_t len)
            {
                for (size_t i = 0; i < len; i++)
                    SendRawKeycode(codes[i]);
            }

            void KeyboardDevice::SendRawKeycode(const uint8_t code)
            {
                if (mKeyQueue.size() >= mMaxInQueue)
                    mKeyQueue.pop_front();

                mKeyQueue.push_back(code);
            }

            #if defined(__GNUC__)
            //! Checks for errors in the keymap definitions on startup
            __attribute__((constructor))
            static void __init_check_keymap_errors()
            {
                for (size_t i = 0; i < gKeycodeMapSize; i++)
                    for (size_t j = 0; j < gKeycodeMapSize; j++)
                        if (i != j && gKeycodeMap[i].ascii == gKeycodeMap[j].ascii)
                        {
                            printf("Keycode redefinition for character: '%c'\n", gKeycodeMap[j].ascii);
                            abort();
                        }
            }
            #endif
        }
    }
}
