
/**
 * @file DeviceInterface.h
 * @author Kiss Benedek
 * @brief The base interface for device implementation
 */

#ifndef DEVICE_INTERFACE_H
#define DEVICE_INTERFACE_H

#include <shared/types.h>
#include <stdexcept>
#include <cstdio>

namespace ktools
{
    namespace emulator
    {
        namespace devices
        {
            //! An interface for describing an abstract port device
            class DeviceBaseInterface
            {
                public:
                    /**
                     * @brief Handler for I/O write
                     *
                     * @param data - the data sent to the device
                     */
                    virtual void OnWrite(const karch_word_t data) = 0;

                    /**
                     * @brief Handler for I/O read
                     *
                     * @return the resulted data from the device
                     */
                    virtual const karch_word_t OnRead() = 0;

                    virtual ~DeviceBaseInterface() {}
            };

            //! Base class for a port device
            template<karch_word_t portID>
            class DeviceInterface : public DeviceBaseInterface
            {
                public:
                    //! The I/O port number used by this device
                    static const karch_word_t IOPort = portID;

                    //! Default write handler
                    virtual void OnWrite(const karch_word_t data) override {
                        #ifdef __TEST__
                            throw std::runtime_error("[DeviceInterface<" + std::to_string(portID) + ">] writing to this device is pointless, check your code!\n");
                        #else
                            printf("[DeviceInterface<%u>] writing to this device is pointless, check your code!\n", portID);
                        #endif
                    }

                    //! Default read handler
                    virtual const karch_word_t OnRead() override {
                        #ifdef __TEST__
                            throw std::runtime_error("[DeviceInterface<" + std::to_string(portID) + ">] reading from this device is pointless, check your code!\n");
                        #else
                            printf("[DeviceInterface<%u>] reading from this device is pointless, check your code!\n", portID);
                            return 0;
                        #endif
                    }

                    virtual ~DeviceInterface() {}
            };
        }
    }
}

#endif
