
/**
 * @file LEDDevice.hpp
 * @author Kiss Benedek
 * @brief LEDDevice class
 */

#ifndef KEMU_LED_DEVICE_H
#define KEMU_LED_DEVICE_H

#include <emulator/devices/PortDefs.h>
#include <emulator/devices/DeviceInterface.h>

#include <emulator/gui/LEDDisplay.h>

#include <bitset>
#include <iostream>

namespace ktools
{
    namespace emulator
    {
        namespace devices
        {
            /**
             * @brief Implements a device for 1-8 LEDs
             *
             * @see @ref gui::LEDDisplay
             */
            class LEDDevice : public DeviceInterface<IOPORT_LEDS>
            {
                public:
                    /**
                     * @brief      Constructs the object
                     *
                     * @param      displ  The LEDDsiplay to use
                     */
                    LEDDevice(gui::LEDDisplay* displ) {
                        mDisplay = displ;
                    }

                    void OnWrite(const karch_word_t data) override {
                        mDisplay->SetValue(data);

                        if (!gui::DisplayWindow::GetGraphicsEnabled())
                            std::cout << std::endl << "Leds: " << std::bitset<8>(data) << std::endl;
                    }

                private:
                    gui::LEDDisplay* mDisplay; //!< The display to output to
            };
        }
    }
}

#endif
