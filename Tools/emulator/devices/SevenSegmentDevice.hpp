
/**
 * @file SevenSegmentDevice.hpp
 * @author Kiss Benedek
 * @brief SevenSegmentDevice class
 */

#ifndef KEMU_SEVEN_SEGMENT_DEVICE_H
#define KEMU_SEVEN_SEGMENT_DEVICE_H

#include <emulator/devices/PortDefs.h>
#include <emulator/devices/DeviceInterface.h>
#include <emulator/gui/SevenSegmentDisplay.h>

#include <iostream>
#include <cstring>

namespace ktools
{
    namespace emulator
    {
        namespace devices
        {
            namespace __SevenSegmentDevice
            {
                //! SSDIS_FLAGS abstract subdevice
                class FLAGS : public DeviceInterface<IOPORT_SSDIS_FLAGS>
                {
                    public:
                        void OnWrite(const karch_word_t data) override {
                            OnWriteFlags(data);
                        }

                        const karch_word_t OnRead() override {
                            return OnReadFlags();
                        }
                    protected:
                        virtual void OnWriteFlags(const karch_word_t data) = 0;
                        virtual const karch_word_t OnReadFlags() = 0;
                };

                //! SSDIS_SEG_L abstract subdevice
                class SEG_L : public DeviceInterface<IOPORT_SSDIS_SEG_L>
                {
                    public:
                        void OnWrite(const karch_word_t data) override {
                            OnWriteSegL(data);
                        }

                    protected:
                        virtual void OnWriteSegL(const karch_word_t data) = 0;
                };

                //! SSDIS_SEG_R abstract subdevice
                class SEG_R : public DeviceInterface<IOPORT_SSDIS_SEG_R>
                {
                    public:
                        void OnWrite(const karch_word_t data) override {
                            OnWriteSegR(data);
                        }

                    protected:
                        virtual void OnWriteSegR(const karch_word_t data) = 0;
                };
            }

            /**
             * @brief Implements a device for seven segment displays
             *
             * This device is made out of 3 subdevices described in the
             * __SevenSegmentDevice namesapce. Use casting to register all
             * subdevices one-by-one.
             *
             * @see @ref gui::SevenSegmentDisplay
             */
            class SevenSegmentDevice : public __SevenSegmentDevice::FLAGS,
                                       public __SevenSegmentDevice::SEG_L,
                                       public __SevenSegmentDevice::SEG_R
            {
                public:
                    /**
                     * @brief      Constructs the object
                     *
                     * @param      displays  The displays
                     */
                    SevenSegmentDevice(gui::SevenSegmentDisplay* displays[4])
                        : mFlags(0)
                    {
                        memcpy(mDisplays, displays, sizeof(mDisplays));
                    }

                protected:
                    virtual void OnWriteFlags(const karch_word_t data) override
                    {
                        mFlags = data;

                        mDisplays[0]->DecimalPoint() = data & 8;
                        mDisplays[1]->DecimalPoint() = data & 4;
                        mDisplays[2]->DecimalPoint() = data & 2;
                        mDisplays[3]->DecimalPoint() = data & 1;

                        mDisplays[0]->Enabled() = (data >> 4) & 8;
                        mDisplays[1]->Enabled() = (data >> 4) & 4;
                        mDisplays[2]->Enabled() = (data >> 4) & 2;
                        mDisplays[3]->Enabled() = (data >> 4) & 1;

                        if (!gui::DisplayWindow::GetGraphicsEnabled())
                            PrintToStdout();
                    }

                    virtual const karch_word_t OnReadFlags() override {
                        return mFlags;
                    }

                    virtual void OnWriteSegL(const karch_word_t data) override
                    {
                        mDisplays[0]->SetValue(data >> 4);
                        mDisplays[1]->SetValue(data & 0xF);

                        if (!gui::DisplayWindow::GetGraphicsEnabled())
                            PrintToStdout();
                    }

                    virtual void OnWriteSegR(const karch_word_t data) override
                    {
                        mDisplays[2]->SetValue(data >> 4);
                        mDisplays[3]->SetValue(data & 0xF);

                        if (!gui::DisplayWindow::GetGraphicsEnabled())
                            PrintToStdout();
                    }

                    //! Prints the current state to stdout
                    void PrintToStdout() const
                    {
                        std::cout << std::endl << "7-segment display: ";
                        for (int i = 0; i < 4; i++)
                        {
                            gui::SevenSegmentDisplay* d = mDisplays[i];
                            if (d->Enabled())
                            {
                                char buf[4];
                                sprintf(buf, "%01x", d->GetValue());
                                std::cout << buf;

                                if (d->DecimalPoint())
                                    std::cout << ".";
                                else
                                    std::cout << " ";
                            }
                            else std::cout << "  ";
                        }
                        std::cout << std::endl;
                    }
                private:
                    gui::SevenSegmentDisplay* mDisplays[4]; //!< The displays to output to
                    karch_word_t mFlags;                    //!< Options for the displays
            };
        }
    }
}

#endif
