
/**
 * @file emulator/main.cc
 * @author Kiss Benedek
 * @brief Program entry point
 */

#include <emulator/gui/DisplayWindow.h>
#include <emulator/gui/TextGraphics.h>
#include <emulator/gui/SevenSegmentDisplay.h>
#include <emulator/gui/DebugDisplay.h>
#include <emulator/gui/LEDDisplay.h>
#include <emulator/devices/VGACharacterDevice.hpp>
#include <emulator/devices/SevenSegmentDevice.hpp>
#include <emulator/devices/DeviceInterface.h>
#include <emulator/devices/LEDDevice.hpp>
#include <emulator/devices/KeyboardDevice.h>
#include <emulator/VirtualMachine.h>

#include <shared/profiling.h>
#include <shared/logger.h>
#include <shared/stringutil.hpp>

#include <argp.h>
#include <time.h>
#include <version.h>
#include <unistd.h>

const char* argp_program_version = "kemu " KTOOLS_VERSION;
// const char* argp_program_bug_address = "<your@email.address>";

namespace ktools
{
    namespace emulator
    {
        Logger& logger = Logger::Get("KEMU-Main");

        namespace argp
        {
            struct load_def {
                std::string name;
                karch_addr_t addr;
            };

            static error_t parse_opt(int key, char *arg, struct argp_state *state)
            {
                std::vector<load_def>* defs = (std::vector<load_def>*)state->input;

                switch (key)
                {
                    case 'l':
                        {
                            std::string as = arg;
                            size_t pos = as.find(':');
                            load_def ld;
                            ld.addr = 0;
                            ld.name = as.substr(0, pos);
                            ksu::trim(ld.name);

                            if (pos != std::string::npos)
                            {
                                std::string addr = as.substr(pos + 1);
                                ksu::tolower(ksu::trim(addr));

                                char* p;
                                long n;
                                if (addr.find('x') != std::string::npos)
                                    n = strtol(addr.c_str(), &p, 16);
                                else
                                    n = strtol(addr.c_str(), &p, 10);

                                if (*p != 0)
                                {
                                    logger.error("Invalid address in load argument: '%s'", addr.c_str());
                                    return ARGP_KEY_ERROR;
                                }
                                else
                                {
                                    if (static_cast<long>(KARCH_ADDR_MAX) < n || n < 0)
                                    {
                                        logger.error("The given address is out of range (0-%d): '%lu'", KARCH_ADDR_MAX, n);
                                        return ARGP_KEY_ERROR;
                                    }

                                    ld.addr = static_cast<karch_addr_t>(n);
                                }
                            }

                            defs->push_back(ld);
                        }
                        break;
                    case 'u':
                        gui::DisplayWindow::SetGraphicsEnabled(false);
                        break;
                    case 'v':
                        Logger::SetMinLogLevel(Logger::GetMinLogLevel() - 1);
                        break;
                    case ARGP_KEY_ARG:
                        break;
                    case ARGP_KEY_END:
                        break;
                    default:
                        return ARGP_ERR_UNKNOWN;
                }
                return 0;
            }

            struct argp_option options[] = {
                { "load",    'l', "FILE:WHERE", 0,  "Loads a binary file into the virtual machine's memory"},
                { "nogui",   'u', 0, 0,             "Force launch without a user interface"},
                { "verbose", 'v', 0, 0,             "Be verbose"},
                { 0 }
            };

            static struct argp argp = {
                options,
                parse_opt,
                0,
                "Your program description.",
            };

            error_t ProcessCommandLine(int argc, char *argv[], std::vector<load_def>& loads) {
                return argp_parse(&argp, argc, argv, 0, 0, &loads);
            }
        }

        void KemuMain(std::vector<ktools::emulator::argp::load_def>& loads)
        {
            logger.info("Running toolchain version %s", KTOOLS_VERSION);

            if (loads.size() == 0) {
                logger.fatal("Nothing to load");
                exit(1);
                return;
            }

            VirtualMachine* machine = new VirtualMachine();

            for (argp::load_def& ld : loads)
            {
                logger.info("Load: %s to 0x%04x", ld.name.c_str(), ld.addr);
                bool fail;
                try {
                    fail = !machine->Load(ld.name, ld.addr);
                } catch (std::exception& ex) {
                    logger.fatal("Load failed: %s", ex.what());
                    fail = true;
                }

                if (fail) {
                    delete machine;
                    exit(1);
                    return;
                }
            }

            #ifdef USING_SDL
            char* displ1 = getenv("DISPLAY");
            char* displ2 = getenv("WAYLAND_DISPLAY");
            if (!(displ1 && strlen(displ1) > 1) && !(displ2 && strlen(displ2) > 1))
            {
                logger.warn("Looks like no Xorg or wayland display is specified, falling back to CLI");
                gui::DisplayWindow::SetGraphicsEnabled(false);
            }
            #endif

            gui::SevenSegmentDisplay* displays[4] = {
                new gui::SevenSegmentDisplay(20, 500),
                new gui::SevenSegmentDisplay(80, 500),
                new gui::SevenSegmentDisplay(140, 500),
                new gui::SevenSegmentDisplay(200, 500)
            };

            gui::DisplayWindow win(640, 768, "KEMU");

            gui::TextGraphics*        text   = new gui::TextGraphics("Font.pf", 640, 480);
            gui::LEDDisplay*          leds   = new gui::LEDDisplay(300, 490);

            if (gui::DisplayWindow::GetGraphicsEnabled())
            {
                for (int i = 0; i < 4; i++)
                    win << displays[i];

                win << text << leds;
            }

            devices::SevenSegmentDevice* ssdis   = new devices::SevenSegmentDevice(displays);
            devices::VGACharacterDevice* chardev = new devices::VGACharacterDevice(text);
            devices::LEDDevice*          leddev  = new devices::LEDDevice(leds);

            #ifdef USING_SDL
            gui::DebugDisplay* debug = new gui::DebugDisplay(380, 490, machine->GetCurrentContext());
            if (gui::DisplayWindow::GetGraphicsEnabled())
                win << debug;
            #endif

            devices::KeyboardDevice* keyboard = new devices::KeyboardDevice(1);

            win.SetKeyboardHandler([keyboard](const char kc) {
                keyboard->SendASCII(kc);
            });

            machine->RegisterDevice(chardev);
            machine->RegisterDevice(leddev);
            machine->RegisterDevice(keyboard);
            machine->RegisterDevice<IOPORT_SSDIS_FLAGS>(ssdis);
            machine->RegisterDevice<IOPORT_SSDIS_SEG_L>(ssdis);
            machine->RegisterDevice<IOPORT_SSDIS_SEG_R>(ssdis);

            if (gui::DisplayWindow::GetGraphicsEnabled())
            {
                win.Create();
                win.Present();
            }

            #ifdef USING_SDL
                clock_t start = clock(), now;
                uint32_t steps = 0;

                clock_t total_render_time = 0;
                clock_t avg_start = clock();
                while (win.Update())
                {
                    total_render_time += clock() - avg_start;
                    if (!machine->IsHalted())
                    {
                        steps++;
                        machine->Step();

                        if (((now = clock()) - start) > (CLOCKS_PER_SEC / 2))
                        {
                            debug->Update(
                                steps * 2,
                                (double)total_render_time / steps / ((double)CLOCKS_PER_SEC / 1000000));
                            total_render_time = 0;
                            steps = 0;
                            start = now;
                        }

                        if (!keyboard->IsKeyQueueEmpty())
                            machine->ExecInterrupt(0x102);
                    }

                    avg_start = clock();
                }
                win.Destroy();
            #else
                while (!machine->IsHalted())
                    machine->Step();
            #endif

            delete machine;
            delete ssdis;
            delete leddev;
            delete chardev;
            delete keyboard;
        }
    }
}

#ifndef __TEST__
int main(int argc, char *argv[])
#else
int _emulator_main(int argc, char *argv[])
#endif
{
    #ifdef PROFILING
    ktools::shared::Profiler::StartProfiling();
    atexit(ktools::shared::Profiler::FinishProfiling);
    #endif

    std::vector<ktools::emulator::argp::load_def> v;
    error_t err = ktools::emulator::argp::ProcessCommandLine(argc, argv, v);

    if (err != 0)
    {
        ktools::emulator::logger.fatal("Failed to parse arguments");
        return err;
    }

    ktools::emulator::KemuMain(v);

    return 0;
}
