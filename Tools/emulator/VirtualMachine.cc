
/**
 * @file VirtualMachine.cc
 * @author Kiss Benedek
 * @brief VirtualMachine class implementation
 */

#include <emulator/VirtualMachine.h>
#include <emulator/execute/exec_col_base.hpp>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <fstream>

#include <shared/profiling.h>

namespace ktools
{
    namespace emulator
    {
        const uint64_t VirtualMachine::maxram_size = ((((uint64_t)1) << (sizeof(karch_addr_t)*8)) - 1);

        Logger& VirtualMachine::mLogger = Logger::Get("VirtualMachine");

        VirtualMachine::VirtualMachine()
            : mContext(maxram_size)
        {
            Reset();
        }

        void VirtualMachine::Reset()
        {
            mContext.Reset();

            mContext.IP() = VM_RESET_ADDRESS;
            mContext.SP() = maxram_size - (maxram_size/8);

            mLogger.verbose("initial ip: 0h%04x; initial sp: 0h%04x", mContext.IP(), mContext.SP());

            mHalted = false;
            mInInterrupt = false;
        }

        bool VirtualMachine::Load(std::istream& stream, const karch_addr_t where)
        {
            PROFILER_BEGIN
            mLogger.debug("loading stream to 0x%04x", where);

            std::streampos fsize = 0;
            fsize = stream.tellg();
            stream.seekg(0, std::ios::end);
            fsize = stream.tellg() - fsize;
            stream.seekg(0, std::ios::beg);

            mLogger.debug(" - size is: %lu", fsize);

            if ((((uint64_t)where) + fsize) > maxram_size)
                throw std::out_of_range("Stream is longer than the available RAM");

            karch_word_t* buf = new karch_word_t[fsize];

            if (stream.read((char*)buf, fsize))
            {
                mContext.WriteMemoryBulk(where, buf, 0, fsize);
                delete[] buf;
                mLogger.debug(" - load ok.");
                PROFILER_END
                return true;
            }

            delete[] buf;
            mLogger.error("failed to read data!");
            PROFILER_END
            return false;
        }

        bool VirtualMachine::Load(const std::string& fileName, const karch_addr_t where)
        {
            PROFILER_BEGIN
            std::ifstream infile;
            infile.open(fileName, std::ios::binary | std::ios::in);

            if (!infile.is_open())
            {
                mLogger.error("could not open %s, binary not loaded :(", fileName.c_str());
                PROFILER_END
                return false;
            }

            mLogger.verbose("loading data from %s to address 0h%04x", fileName.c_str(), where);
            PROFILER_END
            return Load(infile, where);
        }

        void VirtualMachine::ExecInterrupt(const karch_addr_t& new_ip)
        {
            if (mInInterrupt)
                return;

            mInInterrupt = true;

            mLogger.verbose("executing interrupt 0h%04x", new_ip);

            mContext.Push(mContext.IP());
            mContext.Push(mContext.RegA());
            mContext.Push(mContext.RegB());
            mContext.Push(mContext.RegC());
            mContext.Push(mContext.RegD());
            mContext.Push(mContext.FlagSet());

            mContext.IP() = new_ip;
        }

        MachineContext* VirtualMachine::GetCurrentContext() {
            return &mContext;
        }

        const bool VirtualMachine::IsHalted() const {
            return mHalted;
        }

        void VirtualMachine::Step()
        {
            PROFILER_BEGIN
            if (mHalted)
            {
                PROFILER_END
                return;
            }

            mLogger.debug("stepping VM... ip: 0x%04x", mContext.IP());

            VirtualMachine::RawInstruction inst = FetchSingle();

            if (inst.opc == 0x40) // WADDR marker
            {
                mLogger.debug(" - got WADDR marker");
                inst = FetchSingle(true);
            }

            if (inst.opc == 0xE0) // RETI
            {
                mLogger.debug(" - the instruction is RETI, marking end of interrupt");
                mInInterrupt = false;
            }

            // Coulmn 5 is a special case, because of the device IO instructions
            if (inst.col == 0x05)
            {
                HandleDevices(inst);
                PROFILER_END
                return;
            }

            execute::ColumnHandlerFunc func;

            if (inst.opc == 0xD0) // EXTEND marker
            {
                mLogger.debug(" - got EXTEND marker");
                inst = FetchSingle();
                func = execute::g_ColumnHandlers[inst.col + _VM_COL_STANDARD_HANDLER_COUNT_];
            }
            else
                func = execute::g_ColumnHandlers[inst.col];

            if (func)
            {
                func(inst, mContext, mHalted);
                PROFILER_END
            }
            else
            {
                PROFILER_END
                mLogger.fatal("invalid instruction @ 0x%04x (OPC:0x%02x)", mContext.IP(), inst.opc);
                throw "bad_instruction";
            }
        }

        void VirtualMachine::HandleDevices(const VirtualMachine::RawInstruction& inst)
        {
            std::map<uint8_t, devices::DeviceBaseInterface*>::iterator it = mPortHandlers.find(inst.arg);
            devices::DeviceBaseInterface* dev = nullptr;

            if (it != mPortHandlers.end())
            {
                dev = it->second;

                switch (inst.block)
                {
                    case 0x0:   // OUT X, R?
                        dev->OnWrite(mContext.Reg(inst.block_row));
                        break;
                    case 0x1:   // IN R?, X
                        mContext.Reg(inst.block_row) = dev->OnRead();
                        break;
                    case 0x2:   // OUT X, [R?]
                        dev->OnWrite(mContext.ReadMemory<karch_word_t>(mContext.GetBankOffset(mContext.Reg(inst.block_row))));
                        break;
                    case 0x3:   // IN [R?], X
                        mContext.WriteMemory(mContext.GetBankOffset(mContext.Reg(inst.block_row)), dev->OnRead());
                        break;
                    default: throw "bad_instruction";
                }
            }
            else
            {
                mLogger.error("there is no device handler for CPU port 0h%02x, system halted", inst.arg);
                mHalted = true;
            }
        }

        const VirtualMachine::RawInstruction VirtualMachine::FetchSingle(bool wide)
        {
            PROFILER_BEGIN
            if (mContext.IP() == maxram_size)
                mLogger.warn("instruction pointer overflowing memory!");

            if (mContext.IP() >= mContext.SP())
            {
                mLogger.error("tried to execute instruction near the stack pointer, system halted");
                mHalted = true;
            }

            uint8_t opcode = mContext.ReadOne(mContext.IP()++);

            VirtualMachine::RawInstruction inst;
            inst.opc = opcode;
            inst.col = opcode & 0xF;
            inst.row = (opcode & 0xF0) >> 4;
            inst.block_row = (opcode & 0x30) >> 4;
            inst.block = (opcode & 0xC0) >> 6;
            inst.wide_arg = wide;

            if ((inst.opc & 1) && inst.col != 0xF)
            {
                if (mContext.IP() == maxram_size || (wide && mContext.IP() + 1 == maxram_size))
                    mLogger.warn("instruction pointer overflowing memory!");

                if (mContext.IP() >= mContext.SP() || (wide && mContext.IP() + 1 >= mContext.SP()))
                {
                    mLogger.error("tried to execute instruction near the stack pointer, system halted");
                    mHalted = true;
                }

                if (!wide)
                {
                    inst.arg16 = 0;
                    inst.arg = mContext.ReadOne(mContext.IP()++);

                    if (inst.arg & 0x80)
                        inst.arg16 |= 0xFF00;
                }
                else
                {
                    inst.arg16 = mContext.ReadMemory<karch_addr_t>(mContext.IP());
                    mContext.IP() += 2;
                }
            }

            PROFILER_END
            return inst;
        }
    }
}
