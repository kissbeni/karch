
/**
 * @file MachineFlags.h
 * @author Kiss Benedek
 * @brief MachineFlags enum and operators
 */

#ifndef KEMU_MACHINE_FLAGS_H
#define KEMU_MACHINE_FLAGS_H

namespace ktools
{
    namespace emulator
    {
        //! Flags for the machine context
        enum MachineFlags
        {
            fgNONE = 0,         //!< none of the flags are set
            fgCARRY = 1,        //!< the previous arithmetical operation generated a carry bit
            fgZERO = 2,         //!< the previous arithmetical operation resulted zero
            fgSIGNED = 4,       //!< the previous arithmetical resulted a number with the sign bit set
            fgOVERFLOW = 8,     //!< the previous arithmetical operation overflowed
            fgALL = 0xF         //!< all flags are set
        };

        inline MachineFlags operator|(const MachineFlags& lhs, const MachineFlags& rhs) {
            return static_cast<MachineFlags>(static_cast<int>(lhs) | static_cast<int>(rhs));
        }

        inline MachineFlags operator|=(MachineFlags& lhs, const MachineFlags& rhs) {
            return static_cast<MachineFlags>(*((int*)&lhs) |= static_cast<int>(rhs));
        }

        inline MachineFlags operator&(const MachineFlags& lhs, const int& rhs) {
            return static_cast<MachineFlags>(static_cast<int>(lhs) & rhs);
        }

        inline MachineFlags operator&=(MachineFlags& lhs, const MachineFlags& rhs) {
            return static_cast<MachineFlags>(*((int*)&lhs) &= static_cast<int>(rhs));
        }
    }
}

#endif
