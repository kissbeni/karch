
/**
 * @file VirtualMachine.h
 * @author Kiss Benedek
 * @brief VirtualMachine class
 */

#ifndef VM_H
#define VM_H

#include <emulator/devices/DeviceInterface.h>
#include <emulator/MachineContext.hpp>

#include <shared/logger.h>

#include <stdint.h>
#include <string>
#include <map>

#define VM_RESET_ADDRESS 0x100

namespace ktools
{
    namespace emulator
    {
        //! Implements a karch virtual machine
        class VirtualMachine
        {
            public:
                //! Maximum size of the emulated system's memory
                static const uint64_t maxram_size;

                /**
                 * @brief Descibes a raw, fetched instruction
                 *
                 * @see FetchSingle()
                 * @see InstructionMap.md
                */
                struct RawInstruction
                {
                    uint8_t opc;        //!< the opcode of the instruction
                    uint8_t row;        //!< the row of this instruction (0-15)
                    uint8_t col;        //!< the column of this instruction (0-15)
                    uint8_t block;      //!< the block number in a column (0-3)
                    uint8_t block_row;  //!< the row in the block (0-3)

                    //! the automatically fetched argument of the instruction
                    union {
                        karch_word_t arg;
                        karch_addr_t arg16;
                    };

                    bool wide_arg;      //!< is the argument a wide number
                };

                //// ================================================================ ////

                //! Initialize the virtual machine instance
                VirtualMachine();

                //// ================================================================ ////

                /**
                 * @brief Reset the virtual machine
                 *
                 * Resets all registers. The instruction pointer is
                 * set to the reset address @ref VM_RESET_ADDRESS. The
                 * machine's memory is kept intact.
                 */
                void Reset();

                /**
                 * @brief Loads a stream into the emulated system's memory
                 *
                 * @param stream - An open stream to read from
                 * @param where  - Where the file should be loaded
                 *
                 * @return true if the file is loaded successfully
                 */
                bool Load(std::istream& stream, const karch_addr_t where);

                /**
                 * @brief Loads a file into the emulated system's memory
                 *
                 * @param fileName - The name of the binary file to be loaded
                 * @param where    - Where the file should be loaded
                 *
                 * @return true if the file is loaded successfully
                 */
                bool Load(const std::string& fileName, const karch_addr_t where);

                /**
                 * @brief Executes an interrupt
                 *
                 * @param new_ip - the interrupt handler address
                 *
                 * Every register (incl. flags) are pushed onto the
                 * stack, then the instruction pointer is set to @p
                 * new_ip
                 */
                void ExecInterrupt(const karch_addr_t& new_ip);

                /**
                 * @brief Executes the next instruction
                 *
                 * If the system isn't halted, the instruction
                 * at the *IP* is fetched and executed accordingly.
                 *
                 * @see FetchSingle()
                 */
                void Step();

                //! Returns a pointer to the machine's context
                MachineContext* GetCurrentContext();

                //! Returns whether the machine is halted
                const bool IsHalted() const;

                //// ================================================================ ////

                /**
                 * @brief Registers a device
                 *
                 * @param dev - a pointer to a device which implements
                 * a @ref devices::DeviceInterface whith the specified
                 * port ID given in the template
                 */
                template<karch_word_t portID>
                void RegisterDevice(devices::DeviceInterface<portID>* dev)
                {
                    if (mPortHandlers.find(portID) != mPortHandlers.end())
                        throw std::runtime_error("Tried to register a device to an already in-use port");

                    mLogger.verbose("registered new device with id %02xh", portID);
                    mPortHandlers.insert(std::pair<karch_word_t, devices::DeviceBaseInterface*>(portID, dev));
                }
            private:
                /**
                 * @brief Special handler for *basic_5* column
                 *
                 * @param inst - the fetched instruction to process
                 *
                 * Executes reads and writes to devices (in/out instructions)
                 */
                void HandleDevices(const VirtualMachine::RawInstruction& inst);

                /**
                 * @brief Fetch the instruction at the current *IP*
                 *
                 * @param wide - Fetch an 16 bit number instead of an 8-bit one
                 *
                 * @return the raw instruction read from memory
                 */
                const RawInstruction FetchSingle(bool wide = false);

                MachineContext mContext; //!< The execution context of the machine
                bool mHalted;            //!< The *halted* state of the machine
                bool mInInterrupt;       //!< Currently executing an interupt handler

                //! Map for the registered devices (keyed with the device IO port)
                std::map<karch_word_t, devices::DeviceBaseInterface*> mPortHandlers;

                static Logger& mLogger; //!< Logger for this class
        };
    }
}

#endif
