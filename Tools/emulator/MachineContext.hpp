
/**
 * @file MachineContext.hpp
 * @author Kiss Benedek
 * @brief MachineContext class
 */

#ifndef KEMU_MACHINE_CONTEXT_H
#define KEMU_MACHINE_CONTEXT_H

#include <shared/types.h>
#include <shared/property.h>

#include <emulator/MachineFlags.h>

#include <cstring>
#include <stdexcept>
#include <cstdint>

#include <shared/profiling.h>

namespace ktools
{
    namespace emulator
    {
        /**
         * @brief This class contains the current execution context for the virtual machine
         *
         * Contains:
         *  - Memory (RAM)
         *  - Current memory bank
         *  - Registers
         *  - Stack operators (push/pop)
         *  - Flags (for conditional jumps)
         */
        class MachineContext
        {
            public:
                //! The largest number for register indexing
                const uint8_t MAX_REGISTER_INDEX = 3;

                //// ================================================================ ////

                /**
                 * @brief Creates the context with the given memory size
                 *
                 * @param memsize - The size of the emulated system's memory
                 * @note the memory size can **NOT** be modified in the class' lifetime
                 */
                MachineContext(const size_t& memsize);

                ~MachineContext() {
                    delete[] mMem;
                }

                /**
                 * @brief Resets the context
                 *
                 * Every register (including flags), and the current memory bank index
                 * are set to zero
                 *
                 * @note The emulated memory is **NOT** modified
                 */
                void Reset();

                /**
                 * @brief Merge in an other context's registers and flags
                 *
                 * @param other - The left hand side of the operator (the context to copy)
                 *
                 * @note The emulated memory is **NOT** copied nor modified
                 */
                MachineContext& operator=(const MachineContext& other);


                //// ================================================================ ////
                ////        == MEMORY ==                                              ////
                //// ================================================================ ////

                /**
                 * @brief Writes an array to the emulated system's memory
                 *
                 * @param where     - the virtual address to write to
                 * @param what      - a *non-null* pointer to an array
                 * @param offset    - offset in the given array
                 * @param count     - number of bytes to write
                 *
                 * @throws std::out_of_range when the given buffer cannot be fitted into
                 * the memory
                 */
                void WriteMemoryBulk(const karch_addr_t& where,
                    const karch_word_t* what, const size_t& offset, const size_t& count);

                /**
                 * @brief Reads an array from the emulated system's memory
                 *
                 * @param where     - the virtual address to read from
                 * @param buffer    - a *non-null* pointer to an array
                 * @param offset    - offset in the given array
                 * @param count     - number of bytes to read
                 *
                 * @throws std::out_of_range when the given read range `where + count` is
                 * larger than @ref mMemorySize
                 */
                void ReadMemoryBulk(const karch_addr_t& where, karch_word_t* buffer,
                    const size_t& offset, const size_t& count) const;

                /**
                 * @brief Read a type from the emulated system's memory
                 *
                 * @param where     - the virtual address to read from
                 *
                 * @return The read value
                 *
                 * @throws std::out_of_range when the given object's size is outside of
                 * @ref mMemorySize
                 */
                template<typename T>
                inline const volatile T ReadMemory(const karch_addr_t& where) const
                {
                    if ((((size_t)where) + sizeof(T)) > mMemorySize)
                        throw std::out_of_range("Read request does not fit into the memory!");

                    return *reinterpret_cast<const volatile T*>(&mMem[where]);
                }

                /**
                 * @brief Write a value to the emulated system's memory
                 *
                 * @param where     - the virtual address to write to
                 * @param what      - the value to write
                 *
                 * @throws std::out_of_range when the given object's size is outside of
                 * @ref mMemorySize
                 */
                template<typename T>
                inline void WriteMemory(const karch_addr_t& where, const T& what)
                {
                    if ((((size_t)where) + sizeof(T)) > mMemorySize)
                        throw std::out_of_range("Write request does not fit into the memory!");

                    memcpy((char*)&mMem[where], (char*)&what, sizeof(T));
                }

                /**
                 * @brief Read a @ref karch_word_t from the emulated system's memory
                 *
                 * @param addr      - the virtual address to read from
                 *
                 * @return The read value
                 *
                 * @see @ref ReadMemory()
                 */
                inline const karch_word_t ReadOne(const karch_addr_t& addr) const {
                    return ReadMemory<karch_word_t>(addr);
                }

                PROPERTY_RW(karch_word_t, CurrentMemoryBank, mMemoryBank)

                /**
                 * @brief Calculates the real addres from the given bank address
                 *
                 * @param addr      - the address to translate
                 *
                 * @return the absolute memory address
                 */
                const karch_addr_t GetBankOffset(const karch_addr_t& addr) {
                    return addr + (mMemoryBank * 256);
                }

                /**
                 * @brief Dumps the given memory region to the standard output
                 *
                 * @param begin     - the start address (inclusive)
                 * @param end       - the end address (exclusive)
                 */
                void DumpMem(const karch_addr_t& begin = 0, const karch_addr_t& end = -1) const;

                //// ================================================================ ////
                ////        == STACK ==                                               ////
                //// ================================================================ ////

                /**
                 * @brief Puts the given value onto the stack
                 *
                 * @param data      - the data to be put onto the stack
                 *
                 * @see @ref CPU::CPU_Exec (Softcore implementation)
                 * @see @ref Pop()
                 *
                 * @note As on the "real" hardvare stack pointer overflow is an intended
                 * behaviour
                 */
                template<typename T>
                void Push(const T& data)
                {
                    for (uint32_t i = 0; i < (sizeof(T) / sizeof(karch_word_t)); i++)
                    {
                        mSP += sizeof(karch_word_t);
                        WriteMemory(mSP, reinterpret_cast<const volatile karch_word_t*>(&data)[i]);
                    }
                }

                /**
                 * @brief Removes a value from the stack
                 *
                 * @return the popped value
                 *
                 * @see @ref CPU::CPU_Exec (Softcore implementation)
                 * @see @ref Push()
                 *
                 * @note As on the "real" hardvare stack pointer overflow is an intended
                 * behaviour
                 */
                template<typename T>
                const T Pop()
                {
                    T res = T();

                    uint32_t numblocks = sizeof(T) / sizeof(karch_word_t);

                    for (int32_t i = numblocks - 1; i >= 0; i--)
                    {
                        reinterpret_cast<karch_word_t*>(&res)[i] = ReadOne(mSP);
                        mSP -= sizeof(karch_word_t);
                    }

                    return res;
                }

                //// ================================================================ ////
                ////        == REGISTERS ==                                           ////
                //// ================================================================ ////
                PROPERTY_RW(karch_word_t, RegA, _mRegsU._mRegs.mRA)
                PROPERTY_RW(karch_word_t, RegB, _mRegsU._mRegs.mRB)
                PROPERTY_RW(karch_word_t, RegC, _mRegsU._mRegs.mRC)
                PROPERTY_RW(karch_word_t, RegD, _mRegsU._mRegs.mRD)

                /**
                 * @brief Gives a reference to the indexed register
                 *
                 * @throws std::out_of_range if the given index is larger than @ref
                 * MAX_REGISTER_INDEX
                 *
                 * @param id The index
                 *
                 * @return A register reference
                 */
                inline karch_word_t& Reg(const uint8_t& id)
                {
                    if (id > MAX_REGISTER_INDEX)
                        throw std::out_of_range("Invalid register index");

                    return _mRegsU.mGeneralRegisters[id];
                }

                PROPERTY_RW(karch_addr_t, IP, mIP)
                PROPERTY_RW(karch_addr_t, SP, mSP)

                /**
                 * @brief Calculates a realative jump
                 *
                 * @param  offset  Signed 2-byte offset to jump to
                 * @param  pad     Size of the jump instruction
                 *
                 * @see @ref RelativeJump(const karch_saddr_t&,const karch_addr_t)
                 * @see @ref RelativeJump(const karch_word_t&,const karch_addr_t)
                 */
                void RelativeJump(const karch_addr_t& offset, const karch_addr_t pad = 2) {
                    RelativeJump(static_cast<const karch_saddr_t>(offset), pad);
                }

                /**
                 * @brief Calculates a realative jump
                 *
                 * @param  offset  Signed 1-byte offset to jump to
                 * @param  pad     Size of the jump instruction
                 *
                 * @see @ref RelativeJump(const karch_saddr_t&,const karch_addr_t)
                 * @see @ref RelativeJump(const karch_addr_t&,const karch_addr_t)
                 */
                void RelativeJump(const karch_word_t& offset, const karch_addr_t pad = 2) {
                    RelativeJump((karch_saddr_t)static_cast<const karch_sword_t>(offset), pad);
                }

                /**
                 * @brief Calculates a relatitve jump
                 *
                 * @param  offset  Signed 2-byte offset to jump to
                 * @param  pad     Size of the jump instruction
                 *
                 * @see @ref RelativeJump(const karch_word_t&, const karch_addr_t)
                 * @see @ref RelativeJump(const karch_addr_t&,const karch_addr_t)
                 */
                void RelativeJump(const karch_saddr_t& offset, const karch_addr_t pad = 2) {
                    int new_ip = mIP;
                    new_ip += offset - pad;
                    mIP = static_cast<karch_addr_t>(new_ip);
                }

                //// ================================================================ ////
                ////        == FLAGS ==                                               ////
                //// ================================================================ ////

                /**
                 * @brief Sets a flag
                 *
                 * @param  flag  The flag
                 *
                 * @see @ref MachineFlags
                 * @see @ref ClearFlag
                 * @see @ref FlagSet
                 */
                void SetFlag(const MachineFlags flag) {
                    mFlags |= flag;
                }

                /**
                 * @brief Clears a flag
                 *
                 * @param  flag  The flag
                 */
                void ClearFlag(const MachineFlags flag) {
                    mFlags &= static_cast<MachineFlags>(~flag);
                }

                /**
                 * @brief Sets a flag to a value
                 *
                 * @param  flag  The flag
                 * @param  val   If false, the flag is cleared; otherwise the flag is set.
                 */
                void SetFlag(const MachineFlags flag, const bool val) {
                    if (val) SetFlag(flag);
                    else     ClearFlag(flag);
                }

                PROPERTY_R(MachineFlags, FlagSet, mFlags)


                /**
                 * @brief Check if a flag is set
                 *
                 * @param  flag  The flag (or flags) to check
                 *
                 * @return true if the given flag(s) are *ALL* set
                 */
                const bool FlagSet(const MachineFlags flag) const {
                    return (mFlags & flag) == flag;
                }
            private:
                volatile karch_word_t* mMem;    //!< The emulated system memory
                const size_t mMemorySize;       //!< The size of the emulated memory

                union {
                    //! Array of all the generic registers
                    karch_word_t mGeneralRegisters[4];
                    struct {
                        karch_word_t mRA, //!< Register A
                                     mRB, //!< Register B
                                     mRC, //!< Register C
                                     mRD; //!< Register D
                    } _mRegs;
                } _mRegsU;

                karch_addr_t mIP,           //!< Instruction pointer register
                             mSP;           //!< Stack pointer register
                karch_word_t mMemoryBank;   //!< Current memory bank register
                MachineFlags mFlags;        //!< Flags register
        };
    }
}

#endif
