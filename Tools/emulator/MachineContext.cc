
/**
 * @file MachineContext.cc
 * @author Kiss Benedek
 * @brief MachineContext class implementation
 */

#include <emulator/MachineContext.hpp>
#include <shared/profiling.h>

#include <fstream>
#include <cstring>

namespace ktools
{
    namespace emulator
    {
        MachineContext::MachineContext(const size_t& memsize)
            : mMem(new karch_word_t[memsize]), mMemorySize(memsize), _mRegsU({}),
              mIP(0), mSP(0)
        {
            std::fstream fs("/dev/urandom");
            fs.read((char*)mMem, memsize);
            fs.close();
            Reset();
        }

        void MachineContext::Reset()
        {
            for (uint8_t i = 0; i <= MAX_REGISTER_INDEX; i++)
                _mRegsU.mGeneralRegisters[i] = 0;

            mIP         = 0;
            mSP         = 0;
            mMemoryBank = 0;
            mFlags      = fgNONE;
        }

        MachineContext& MachineContext::operator=(const MachineContext& other)
        {
            if (mMem == other.mMem)
                return *this;

            for (uint8_t i = 0; i <= MAX_REGISTER_INDEX; i++)
                _mRegsU.mGeneralRegisters[i] = other._mRegsU.mGeneralRegisters[i];

            mIP         = other.mIP;
            mSP         = other.mSP;
            mMemoryBank = other.mMemoryBank;
            mFlags      = other.mFlags;

            return *this;
        }

        //// MEMORY ////
        void MachineContext::WriteMemoryBulk(const karch_addr_t& where,
            const karch_word_t* what, const size_t& offset, const size_t& count)
        {
            if ((where + count) >= mMemorySize)
                throw std::out_of_range("Write request does not fit into the memory!");

            memcpy((char*)&mMem[where], what + offset, count);
        }

        void MachineContext::ReadMemoryBulk(const karch_addr_t& where, karch_word_t* buffer,
            const size_t& offset, const size_t& count) const
        {
            if ((where + count) >= mMemorySize)
                throw std::out_of_range("Read request does not fit into the memory!");

            memcpy(buffer + offset, (char*)&mMem[where], count);
        }

        void MachineContext::DumpMem(const karch_addr_t& begin, const karch_addr_t& end) const
        {
            PROFILER_BEGIN
            uint32_t n = 0;
            printf("\n%04x:", begin);
            for (uint32_t a = begin; a < ((end < mMemorySize) ? end : mMemorySize); a++)
            {
                if (((n++)%32) == 0 && a != begin)
                    printf("\n%04x:", a);

                if (a == mIP)
                    printf(" \x1b[42m%02x\x1b[49m", mMem[a]);
                else if (a == mSP)
                    printf(" \x1b[41m%02x\x1b[49m", mMem[a]);
                else
                    printf(" %02x", mMem[a]);
            }

            puts("");
            PROFILER_END
        }
    }
}
