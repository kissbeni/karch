
/**
 * @file types.h
 * @author Kiss Benedek
 * @brief KArch types
 */

#ifndef KARCH_TYPES_H
#define KARCH_TYPES_H

typedef unsigned char karch_word_t;     //!< Type for a karch unsigned word
typedef signed char karch_sword_t;      //!< Type for a karch signed word
typedef unsigned short karch_addr_t;    //!< Type for a karch unsigned address
typedef signed short karch_saddr_t;     //!< Type for a karch signed address

static_assert(sizeof(karch_word_t) == 1, "karch_word_t is not 8 bits");
static_assert(sizeof(karch_word_t) == sizeof(karch_sword_t), "karch_word_t is not the same size as karch_sword_t");

static_assert(sizeof(karch_addr_t) == 2, "karch_addr_t is not 16 bits");
static_assert(sizeof(karch_addr_t) == sizeof(karch_saddr_t), "karch_addr_t is not the same size as karch_saddr_t");

#define KARCH_ADDR_MAX ((1llu << (sizeof(karch_addr_t) * 8)) - 1)
#define KARCH_WORD_MAX ((1llu << (sizeof(karch_word_t) * 8)) - 1)

#endif
