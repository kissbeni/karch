
/**
 * @file profiling.h
 * @author Kiss Benedek
 * @brief A simple profiling system
 */

#ifndef KTOOLS_PROFILING_H
#define KTOOLS_PROFILING_H

#ifdef PROFILING

#define PROFILER_BEGIN \
    ktools::shared::Profiler::TimingNode* __profiler_node = \
        ktools::shared::Profiler::GetTimingNode(__PRETTY_FUNCTION__, __FILE__, __LINE__); \
    if (__profiler_node) {__profiler_node->begin();}

#define PROFILER_END \
    if (__profiler_node) {__profiler_node->end();}

#include <stdint.h>
#include <cstring>
#include <thread>
#include <mutex>
#include <sstream>
#include <map>
#include <shared/fnv1a.hpp>
#include <shared/property.h>

namespace ktools
{
    namespace shared
    {
        class Profiler
        {
            public:
                struct Node {
                    virtual void begin() = 0;
                    virtual void end() = 0;

                    const char* func;
                    const char* file;
                    const uint32_t line;
                    const clock_t created;

                    Node(const char* _func, const char* _file, const uint32_t _line)
                        : func(_func), file(_file), line(_line), created(clock()) {}
                };

                struct TimingNode : public Node {
                    void begin();
                    void end();

                    TimingNode(const char* _func, const char* _file, const uint32_t _line, const std::thread::id _id)
                        : Node(_func, _file, _line), thread(_id), times_hit(0), total_time(0), can_begin(true) {}

                    const std::thread::id thread;

                    LC_GETTER(size_t, times_hit);
                    LC_GETTER(size_t, total_time);

                    private:
                        uint64_t times_hit;
                        uint64_t total_time;
                        clock_t begin_time;
                        bool can_begin;
                };

                struct MemoryNode : public Node {
                    void begin();
                    void end();

                    MemoryNode(const char* _func, const char* _file, const uint32_t _line, const size_t _bs)
                        : Node(_func, _file, _line), blocksize(_bs), total_alloc(0), total_free(0) {}

                    const size_t blocksize;

                    LC_GETTER(size_t, total_alloc);
                    LC_GETTER(size_t, total_free);

                    private:
                        size_t total_alloc;
                        size_t total_free;
                };

                static inline TimingNode* GetTimingNode(const char* _func, const char* _file, const uint32_t _line) {
                    if (enabled) {
                        std::stringstream ss;
                        ss << _func << '$' << _file << ':' << _line << '@' << std::this_thread::get_id();
                        fnv64_t hash = hash_64_fnv1a(ss.str());

                        mx.lock();
                        map_type::iterator n = nodes.find(hash);
                        mx.unlock();

                        if (n != nodes.end())
                            return dynamic_cast<TimingNode*>(n->second);

                        TimingNode* node = new TimingNode(_func, _file, _line, std::this_thread::get_id());
                        mx.lock();
                        nodes.insert(map_pair_type(hash, node));
                        mx.unlock();
                        return node;
                    }
                    else return nullptr;
                }

                static inline MemoryNode* GetMemoryNode(const char* _func, const char* _file, const uint32_t _line, const size_t blocksize) {
                    if (enabled) {
                        char str[1024];
                        snprintf(str, 1024, "%d-%lu@%s$%s", _line, blocksize, _func, _file);
                        fnv64_t hash = hash_64_fnv1a(str);

                        mx.lock();
                        map_type::iterator n = nodes.find(hash);
                        mx.unlock();

                        if (n != nodes.end())
                            return dynamic_cast<MemoryNode*>(n->second);

                        MemoryNode* node = new MemoryNode(_func, _file, _line, blocksize);
                        mx.lock();
                        nodes.insert(map_pair_type(hash, node));
                        mx.unlock();
                        return node;
                    }
                    else return nullptr;
                }

                static void StartProfiling();
                static void FinishProfiling();
            private:
                typedef std::pair<fnv64_t, Node*> map_pair_type;
                typedef std::map<fnv64_t, Node*> map_type;
                static map_type nodes;
                static std::mutex mx;
                static bool enabled;
        };
    }
}

void* operator new(size_t size, int line, const char* file, const char* func) noexcept(false);
void* operator new[](size_t size, int line, const char* file, const char* func) noexcept(false);
void* operator new(size_t size) noexcept(false);
void* operator new[](size_t size) noexcept(false);
void operator delete(void* p) noexcept;
void operator delete[](void* p) noexcept;

#define new new(__LINE__, __FILE__, __PRETTY_FUNCTION__)

#else

#define PROFILER_BEGIN {}
#define PROFILER_END {}

#endif

#endif
