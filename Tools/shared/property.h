
/**
 * @file property.h
 * @author Kiss Benedek
 * @brief Property defining macros for classes
 */

#ifndef KTOOLS_PROPERTY_H
#define KTOOLS_PROPERTY_H

//! Const getter without using reference
#define PROPERTY_R_NR(type, name, var)  \
    const type name() const {           \
        return var;                     \
    }

//! Const getter using reference
#define PROPERTY_R(type, name, var)     \
    PROPERTY_R_NR(type&, name, var)

//! Read-write property without using reference
#define PROPERTY_RW_NR(type, name, var)    \
    PROPERTY_R_NR(type, name, var)         \
    type name() {                      \
        return var;                     \
    }

//! Read-write property using references
#define PROPERTY_RW(type, name, var)    \
    PROPERTY_R(type, name, var)         \
    type& name() {                      \
        return var;                     \
    }

//! Static read-only property without using reference
#define SPROPERTY_R_NR(type, name, var) \
    static const type Get##name() {     \
        return var;                     \
    }

//! Static read-only property using reference
#define SPROPERTY_R(type, name, var)    \
    SPROPERTY_R_NR(type&, name, var)

//! Static read-write property using references
#define SPROPERTY_RW(type, name, var)         \
    SPROPERTY_R(type, name, var)              \
    static void Set##name(const type& _var) { \
        var = _var;                           \
    }


//! Const getter
#define LC_GETTER(type, var)            \
    const type get_##var() const {      \
        return var;                     \
    }

#endif
