
#ifndef KTOOLS_LOGGER
#define KTOOLS_LOGGER

#include <shared/property.h>
#include <map>
#include <cstdarg>
#include <mutex>

#define LOGGER_BUFFER_SIZE 4096

class Logger
{
    public:
        static Logger& Get(const char* const category);
        inline static void SetMinLogLevel(const int level) {
            min_level = level;
        }

	    inline static const int GetMinLogLevel() {
	        return min_level;
	    }

        void debug(const char* str, ...);
        void verbose(const char* str, ...);
        void info(const char* str, ...);
        void warn(const char* str, ...);
        void error(const char* str, ...);
        void fatal(const char* str, ...);

        Logger(const Logger& other) : Logger(other.name) { }

        ~Logger() {
            {
                std::lock_guard<std::mutex> guard(mx);
                delete[] buffer;
            }
        }

        LC_GETTER(char* const, name)
    private:
        Logger(const char* const name)
            : name(name) {

            std::lock_guard<std::mutex> guard(mx);
            buffer = new char[LOGGER_BUFFER_SIZE];
        }

        const char* const name;
        char* buffer;
        std::mutex mx;

        static int min_level;
};

#endif

