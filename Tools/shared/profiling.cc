
/**
 * @file profiling.cc
 * @author Kiss Benedek
 * @brief Profiling implementation
 */

#include <shared/profiling.h>
#include <cstring>

#ifdef PROFILING

namespace ktools
{
    namespace shared
    {
        Profiler::map_type Profiler::nodes;
        std::mutex Profiler::mx;

        bool Profiler::enabled = false;

        void Profiler::TimingNode::begin() {
            if (!can_begin) {
                printf("Called multiple begins on a profiler node of %s!\n", func);
                abort();
            }

            can_begin = false;
            begin_time = clock();
            times_hit++;
        }

        void Profiler::TimingNode::end() {
            if (can_begin) {
                printf("Called multiple ends on a profiler node of %s!\n", func);
                abort();
            }

            can_begin = true;
            total_time += clock() - begin_time;
        }

        void Profiler::MemoryNode::begin() {
            total_alloc += blocksize;
        }

        void Profiler::MemoryNode::end() {
            total_free += blocksize;

            if (total_free > total_alloc) {
                printf("Double freeing memory in %s (%s:%d)\n", func, file, line);
                abort();
            }
        }

        void Profiler::StartProfiling() {
            nodes.clear();
            enabled = true;
        }

        inline double _time_ms(clock_t c) {
            return (double)c / (CLOCKS_PER_SEC / 1000);
        }

        void Profiler::FinishProfiling() {
            enabled = false;

            puts("\nExecution timing summary:");
            printf("                                                        Function                                                         | Line |     Thread ID     |   Times hit   |   Total time   |  Avg time  | Node created time\n");
            for (const map_pair_type& p : nodes) {
                TimingNode* n = dynamic_cast<TimingNode*>(p.second);
                if (!n) continue;
                std::stringstream ss;
                ss << n->thread;
                printf("%-120.120s | %-4d | %-17s | %-13lu | %12.3lfms | %8.3lfms | %lu\n",
                    n->func,
                    n->line,
                    ss.str().c_str(),
                    n->get_times_hit(),
                    _time_ms(n->get_total_time()),
                    _time_ms(n->get_total_time() / n->get_times_hit()),
                    n->created);
            }

            puts("\nMemory summary:");
            size_t total_alloc = 0, total_free = 0;
            printf("                                                          File                                                           | Line |  Size  | Total allocated | Total freed | Leaking\n");
            for (const map_pair_type& p : nodes) {
                MemoryNode* n = dynamic_cast<MemoryNode*>(p.second);
                if (!n) continue;
                printf("%-120.120s | %-4d | %-6ld | %-15ld | %-11ld | %ld\n",
                    n->file,
                    n->line,
                    n->blocksize,
                    n->get_total_alloc(),
                    n->get_total_free(),
                    n->get_total_alloc() - n->get_total_free());
                total_alloc += n->get_total_alloc();
                total_free += n->get_total_free();
            }
            printf(":::::::: Total allocated: %lu bytes, Total freed: %lu bytes (Leaking %lu bytes)\n", total_alloc, total_free, total_alloc - total_free);

            nodes.clear();
        }
    }
}

#ifdef new
#   undef new
#endif

#ifdef delete
#   undef delete
#endif

struct mem_header {
    uint32_t canary;
    const void* obj;
};

const uint32_t CANARY1 = 0xDEADBEEF;
const uint32_t CANARY2 = 0xCAFEBABE;
const size_t MEMOFFSET = sizeof(mem_header);

inline void* __new_impl(size_t size, int line, const char* file, const char* func) {
    uint8_t* mem = (uint8_t*)malloc(size + MEMOFFSET);
    if (file && func) {
        ktools::shared::Profiler::MemoryNode* n = ktools::shared::Profiler::GetMemoryNode(func, file, line, size);
        if (n) {
            reinterpret_cast<mem_header*>(mem)->canary = CANARY1;
            reinterpret_cast<mem_header*>(mem)->obj = n;
            n->begin();
            return mem + MEMOFFSET;
        }
    }

    reinterpret_cast<mem_header*>(mem)->canary = CANARY2;
    reinterpret_cast<mem_header*>(mem)->obj = nullptr;
    return mem + MEMOFFSET;
}

inline void __delete_impl(void* p) {
    if (!p) return;

    uint8_t* _p = (uint8_t*)p;
    mem_header* header = reinterpret_cast<mem_header*>(_p - MEMOFFSET);
    if (header->canary == CANARY1)
    {
        ktools::shared::Profiler::MemoryNode* n = (ktools::shared::Profiler::MemoryNode*)header->obj;
        n->end();
    }
    else if (header->canary != CANARY2)
    {
        printf("Invalid memory canary when freeing up %p\n", p);
        free(p);
        return;
    }

    free(header);
}

void* operator new(size_t size, int line, const char* file, const char* func) noexcept(false) {
    return __new_impl(size, line, file, func);
}

void* operator new[](size_t size, int line, const char* file, const char* func) noexcept(false) {
    return __new_impl(size, line, file, func);
}

void* operator new(size_t size) noexcept(false) {
    return __new_impl(size, 0, nullptr, nullptr);
}

void* operator new[](size_t size) noexcept(false) {
    return __new_impl(size, 0, nullptr, nullptr);
}

void operator delete(void* p) noexcept {
    __delete_impl(p);
}

void operator delete[](void* p) noexcept {
    __delete_impl(p);
}

#endif
