
/**
 * @file fnv1a.hpp
 * @author Kiss Benedek
 * @brief FNV1a hash functions (and c++11 constexpr implementations)
 */

#ifndef FNV1A_H
#define FNV1A_H

#include <string>

// based on https://gist.github.com/underscorediscovery/81308642d0325fd386237cfa3b44785c

typedef unsigned int fnv32_t;           //!< Type for 32-bit FNV1a hashes
typedef unsigned long long int fnv64_t; //!< Type for 64-bit FNV1a hashes

static_assert(sizeof(fnv32_t) == 4, "fnv32_t must be 32 bits");
static_assert(sizeof(fnv64_t) == 8, "fnv64_t must be 64 bits");

// FNV1a c++11 constexpr compile time hash functions, 32 and 64 bit
// str should be a null terminated string literal, value should be left out
// e.g hash_32_fnv1a_const("example")
// code license: public domain or equivalent
// post: https://notes.underscorediscovery.com/constexpr-fnv1a/

constexpr fnv32_t val_32_const = 0x811c9dc5;            //!< Initial fnv1a 32-bit hash
constexpr fnv32_t prime_32_const = 0x1000193;           //!< Prime for 32-bit fnv1a hashing
constexpr fnv64_t val_64_const = 0xcbf29ce484222325;    //!< Initial fnv1a 64-bit hash
constexpr fnv64_t prime_64_const = 0x100000001b3;       //!< Prime for 64-bit fnv1a hashing

/**
 * @brief      Calculates a constant Fowler–Noll–Vo hash
 *
 * @param  str    The string
 * @param  value  The previous hash value used in recursion
 *
 * @return     The created 32-bit FNV1a hash
 *
 * @see https://notes.underscorediscovery.com/constexpr-fnv1a/
 */
inline constexpr fnv32_t fnv1a32_const(const char* const str, const fnv32_t value = val_32_const) noexcept {
    return (str[0] == '\0') ? value : fnv1a32_const(&str[1], (value ^ fnv32_t(str[0])) * prime_32_const);
}

/**
 * @brief      Calculates a constant Fowler–Noll–Vo hash
 *
 * @param  str    The string
 * @param  value  The previous hash value used in recursion
 *
 * @return     The created 64-bit FNV1a hash
 *
 * @see https://notes.underscorediscovery.com/constexpr-fnv1a/
 */
inline constexpr fnv64_t fnv1a64_const(const char* const str, const fnv64_t value = val_64_const) noexcept {
    return (str[0] == '\0') ? value : fnv1a64_const(&str[1], (value ^ fnv64_t(str[0])) * prime_64_const);
}

/**
 * @brief      Calculates a Fowler-Noll-Vo hash
 *
 * @param  str   The string
 * @param  len   The length of the string
 *
 * @return     The created 32-bit FNV1a hash
 */
inline const fnv32_t hash_32_fnv1a(const void* str, const unsigned int len) {
    const char* data = (char*)str;
    fnv32_t hash = val_32_const;

    for(unsigned int i = 0; i < len; ++i) {
        unsigned char value = data[i];
        hash = hash ^ value;
        hash *= prime_32_const;
    }

    return hash;
}

/**
 * @brief      Calculates a Fowler-Noll-Vo hash
 *
 * @param  str   The string
 * @param  len   The length of the string
 *
 * @return     The created 64-bit hash
 */
inline const fnv64_t hash_64_fnv1a(const void* str, const unsigned int len) {
    const char* data = (char*)str;
    fnv64_t hash = val_64_const;

    for(unsigned int i = 0; i < len; ++i) {
        unsigned char value = data[i];
        hash = hash ^ value;
        hash *= prime_64_const;
    }

    return hash;
}

/**
 * @brief      Calculates a Fowler-Noll-Vo hash
 *
 * @param  str   The string
 *
 * @return     The created 32-bit hash
 *
 * @see @ref hash_32_fnv1a(const char*,const unsigned int)
 */
inline const fnv32_t hash_32_fnv1a(const std::string& str) {
    return hash_32_fnv1a(str.c_str(), str.length());
}

/**
 * @brief      Calculates a Fowler-Noll-Vo hash
 *
 * @param  str   The string
 *
 * @return     The created 64-bit hash
 *
 * @see @ref hash_64_fnv1a(const char*,const unsigned int)
 */
inline const fnv64_t hash_64_fnv1a(const std::string& str) {
    return hash_64_fnv1a(str.c_str(), str.length());
}

#endif
