
/**
 * @file stringutil.hpp
 * @author Kiss Benedek
 * @brief String utility functions
 */

#ifndef KTOOLS_STRINGUTIL_H
#define KTOOLS_STRINGUTIL_H

#include <algorithm>
#include <functional>
#include <cctype>
#include <locale>
#include <string>
#include <cstring>

namespace ksu
{
    /**
     * @brief      Trim whitespace from the start of the string
     * @param      s     The string to trim
     * @return     The trimmed string
     *
     * @note This function modifies the given string
     */
    inline std::string &ltrim(std::string &s) {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(),
                std::not1(std::ptr_fun<int, int>(std::isspace))));
        return s;
    }

    /**
     * @brief      Trim whitespace from the end of the string
     * @param      s     The string to trim
     * @return     The trimmed string
     *
     * @note This function modifies the given string
     */
    inline std::string &rtrim(std::string &s) {
        s.erase(std::find_if(s.rbegin(), s.rend(),
                std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
        return s;
    }

    /**
     * @brief      Trim whitespace from both ends of the string
     * @param      s     The string to trim
     * @return     The trimmed string
     *
     * @note This function modifies the given string
     */
    inline std::string &trim(std::string &s) {
        return ltrim(rtrim(s));
    }

    /**
     * @brief      Checks if a container begins with an other container
     *
     * @param  input       The input container
     * @param  match       The container to search for
     *
     * @tparam     TContainer  Type of the container
     *
     * @return     True if @p input starts with @p match
     */
    template<class TContainer>
    bool begins_with(const TContainer& input, const TContainer& match) {
        return input.size() >= match.size()
            && equal(match.begin(), match.end(), input.begin());
    }

    /**
     * @brief      Transforms a string to lowercase
     *
     * @param  s     The string to transform
     *
     * @return     The transformed stirng
     */
    inline std::string tolower(const std::string &s) {
        std::string ws = s;
        std::transform(ws.begin(), ws.end(), ws.begin(), ::tolower);
        return ws;
    }

    /**
     * @brief      Transforms a string to uppercase
     *
     * @param  s     The string to transform
     *
     * @return     The transformed stirng
     */
    inline std::string toupper(const std::string &s) {
        std::string ws = s;
        std::transform(ws.begin(), ws.end(), ws.begin(), ::toupper);
        return ws;
    }
}

#endif
