
#include <shared/logger.h>

int Logger::min_level = 2;

Logger& Logger::Get(const char* const category) {
    static std::map<std::string, Logger> loggers;
    static std::mutex map_mx;

    std::lock_guard<std::mutex> guard(map_mx);

    std::map<std::string, Logger>::iterator it = loggers.find(category);

    if (it == loggers.end())
    {
        loggers.insert(std::pair<std::string, Logger>(category, category));
        return loggers.find(category)->second;
    }

    return it->second;
}

void Logger::debug(const char* str, ...) {
    if (min_level > 0) return;

    va_list args;
    va_start(args, str);
    {
        std::lock_guard<std::mutex> guard(mx);
        vsnprintf(buffer, LOGGER_BUFFER_SIZE, str, args);
        printf("[%-16s][\x1b[36mDEBUG\x1b[39m] %s\n", name, buffer);
    }
    va_end(args);
}

void Logger::verbose(const char* str, ...) {
    if (min_level > 1) return;

    va_list args;
    va_start(args, str);
    {
        std::lock_guard<std::mutex> guard(mx);
        vsnprintf(buffer, LOGGER_BUFFER_SIZE, str, args);
        printf("[%-16s][\x1b[96mLOG\x1b[39m  ] %s\n", name, buffer);
    }
    va_end(args);
}

void Logger::info(const char* str, ...) {
    if (min_level > 2) return;

    va_list args;
    va_start(args, str);
    {
        std::lock_guard<std::mutex> guard(mx);
        vsnprintf(buffer, LOGGER_BUFFER_SIZE, str, args);
        printf("[%-16s][INFO ] %s\n", name, buffer);
    }
    va_end(args);
}

void Logger::warn(const char* str, ...) {
    if (min_level > 3) return;

    va_list args;
    va_start(args, str);
    {
        std::lock_guard<std::mutex> guard(mx);
        vsnprintf(buffer, LOGGER_BUFFER_SIZE, str, args);
        printf("[%-16s][\x1b[93mWARN\x1b[39m ] %s\n", name, buffer);
    }
    va_end(args);
}

void Logger::error(const char* str, ...) {
    if (min_level > 4) return;

    va_list args;
    va_start(args, str);
    {
        std::lock_guard<std::mutex> guard(mx);
        vsnprintf(buffer, LOGGER_BUFFER_SIZE, str, args);
        fprintf(stderr, "[%-16s][\x1b[95mERROR\x1b[39m] %s\n", name, buffer);
    }
    va_end(args);
}

void Logger::fatal(const char* str, ...) {
    va_list args;
    va_start(args, str);
    {
        std::lock_guard<std::mutex> guard(mx);
        vsnprintf(buffer, LOGGER_BUFFER_SIZE, str, args);
        fprintf(stderr, "[%-16s][\x1b[91mFATAL\x1b[39m] %s\n", name, buffer);
    }
    va_end(args);
}
