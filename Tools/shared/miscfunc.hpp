
/**
 * @file miscfunc.hpp
 * @author Kiss Benedek
 * @brief Miscellaneous functions
 */

#ifndef KTOOLS_MISCUTIL_H
#define KTOOLS_MISCUTIL_H

#include <cstdint>

/**
 * @brief      Rotate byte bits left
 *
 * This function will be optimalized to a single ROL instruction on x86
 *
 * @param  n     The byte to rotate
 * @param  c     Times to rotate
 *
 * @return     The rotated byte
 */
inline uint8_t rol8(uint8_t n, uint8_t c)
{
    c %= sizeof(n) * 8;
    const unsigned int mask = (8*sizeof(n) - 1);
    c &= mask;
    return (n << c) | (n >> ((-c) & mask));
}

/**
 * @brief      Rotate byte bits right
 *
 * This function will be optimalized to a single ROR instruction on x86
 *
 * @param  n     The byte to rotate
 * @param  c     Times to rotate
 *
 * @return     The rotated byte
 */
inline uint8_t ror8(uint8_t n, uint8_t c)
{
    c %= sizeof(n) * 8;
    const unsigned int mask = (8*sizeof(n) - 1);
    c &= mask;
    return (n >> c) | (n << ((-c) & mask));
}

#endif
