
/**
 * @file testing.h
 * @author Kiss Benedek
 * @brief Stuff for testing
 */

#ifndef KTOOLS_TESTING_H
#define KTOOLS_TESTING_H

#ifdef __TEST__
#define PRIVATE public
#define VIRTUAL_TESTING virtual
#else
#define PRIVATE private
#define VIRTUAL_TESTING
#endif

#endif
