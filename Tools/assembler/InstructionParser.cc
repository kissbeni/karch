
/**
 * @file InstructionParser.cc
 * @author Kiss Benedek
 * @brief Assembler class implementation (instruction parser)
 */

#include <assembler/Assembler.h>
#include <assembler/AssemblerException.hpp>

#include <regex>

namespace ktools
{
    namespace assembler
    {
        static const std::regex g_reg_inst_0_arg("^([A-Za-z]+)\\s*$");
        static const std::regex g_reg_inst_1_arg("^([A-Za-z]+)\\s+(\\[.+?\\]|.+?)\\s*$");
        static const std::regex g_reg_inst_2_arg("^([A-Za-z]+)\\s+(\\[.+?\\]|.+?)\\s*,\\s*(\\[.+?\\]|.+?)\\s*$");

        AbstractInstruction* Assembler::ParseSingleInstruction(const std::string& text)
        {
            if (text.empty())
                return nullptr;

            Operand* op1 = nullptr;
            Operand* op2 = nullptr;
            unsigned int numops = 0;
            std::string mnem;
            uint32_t pad = 0;

            std::smatch m;

            try
            {
                if (std::regex_match(text, m, g_reg_inst_2_arg))
                {
                    mnem = m[1].str();

                    pad = text.find(m[2].str(), m.position() + mnem.length());
                    op1 = ParseOperand(m[2].str());

                    pad = text.find(m[3].str(), pad);
                    op2 = ParseOperand(m[3].str());

                    numops = 2;
                }
                else if (std::regex_match(text, m, g_reg_inst_1_arg))
                {
                    mnem = m[1].str();

                    pad = text.find(m[2].str(), m.position() + mnem.length());
                    op1 = ParseOperand(m[2].str());

                    numops = 1;
                }
                else if (std::regex_match(text, m, g_reg_inst_0_arg))
                    mnem = m[1].str();
                else
                    return nullptr;
            } catch (AssemblerException& e) {
                throw AssemblerException(e.what(), e.GetColumn() + pad);
            }

            std::transform(mnem.begin(), mnem.end(), mnem.begin(), [](unsigned char c){ return std::toupper(c); });
            fnv32_t hash = hash_32_fnv1a(mnem);
            Mnemonic mnemonic = Mnemonic::UNKNOWN;

            for (unsigned int i = 0; gMnemonicAliasMap[i].name != Mnemonic::UNKNOWN; i++)
                if (hash == gMnemonicAliasMap[i].alt_name)
                {
                    mnemonic = gMnemonicAliasMap[i].name;
                    break;
                }

            if (mnemonic == Mnemonic::UNKNOWN)
                mnemonic = static_cast<const Mnemonic>(hash_32_fnv1a(mnem));

            AbstractInstruction* r = IterateAllInstructions<AbstractInstruction*, nullptr>(
            [op1, op2, mnemonic, numops](const inst_iterator it) -> AbstractInstruction*
            {
                AbstractInstruction* inst = *it;

                if (inst && inst->GetMnemonic() == mnemonic && inst->NumOps() == numops)
                {
                    if (numops == 0)
                    {
                        if (inst->IsExtended())
                            return new Instruction<0, true>(inst->GetOpcode(), inst->GetMnemonic());

                        return new Instruction<0>(inst->GetOpcode(), inst->GetMnemonic());
                    }

                    op1->SetAllowWideNumber(inst->Op(1)->GetNumber() ? true : false);

                    if (inst->Op(1)->GetType() != (op1->GetType() & OperandType::ConsistentFlags))
                        return nullptr;

                    if (inst->Op(1)->IsRegister() && inst->Op(1)->GetRegister() != op1->GetRegister())
                        return nullptr;

                    if (numops == 1)
                    {
                        if (inst->IsExtended())
                            return new Instruction<1, true>(inst->GetOpcode(), inst->GetMnemonic(), op1);

                        return new Instruction<1>(inst->GetOpcode(), inst->GetMnemonic(), op1);
                    }
                    else
                    {
                        op2->SetAllowWideNumber(inst->Op(2)->GetNumber() ? true : false);

                        if (inst->Op(2)->GetType() != (op2->GetType() & OperandType::ConsistentFlags))
                            return nullptr;

                        if (inst->Op(2)->IsRegister() && inst->Op(2)->GetRegister() != op2->GetRegister())
                            return nullptr;

                        if (inst->IsExtended())
                            return new Instruction<2, true>(inst->GetOpcode(), inst->GetMnemonic(), op1, op2);

                        return new Instruction<2>(inst->GetOpcode(), inst->GetMnemonic(), op1, op2);
                    }
                }

                return nullptr;
            });

            if (r) return r;

            delete op1;
            delete op2;
            return nullptr;
        }

        AbstractInstruction* Assembler::ParseSingleInstruction(const std::string& text, const karch_word_t& opcode, const bool& isExtended)
        {
            if (text.empty())
                return nullptr;

            std::smatch m;

            if (std::regex_match(text, m, g_reg_inst_2_arg))
            {
                std::string mnem = m[1].str();
                std::transform(mnem.begin(), mnem.end(), mnem.begin(), [](unsigned char c){ return std::toupper(c); });

                if (isExtended) return new Instruction<2, true>(
                    opcode,
                    static_cast<const Mnemonic>(hash_32_fnv1a(mnem.c_str(), mnem.length())),
                    ParseOperand(m[2].str()),
                    ParseOperand(m[3].str())
                );

                return new Instruction<2>(
                    opcode,
                    static_cast<const Mnemonic>(hash_32_fnv1a(mnem.c_str(), mnem.length())),
                    ParseOperand(m[2].str()),
                    ParseOperand(m[3].str())
                );
            }

            if (std::regex_match(text, m, g_reg_inst_1_arg))
            {
                std::string mnem = m[1].str();
                std::transform(mnem.begin(), mnem.end(), mnem.begin(), [](unsigned char c){ return std::toupper(c); });

                if (isExtended) return new Instruction<1, true>(
                    opcode,
                    static_cast<const Mnemonic>(hash_32_fnv1a(mnem.c_str(), mnem.length())),
                    ParseOperand(m[2].str())
                );

                return new Instruction<1>(
                    opcode,
                    static_cast<const Mnemonic>(hash_32_fnv1a(mnem.c_str(), mnem.length())),
                    ParseOperand(m[2].str())
                );
            }

            if (std::regex_match(text, m, g_reg_inst_0_arg))
            {
                std::string mnem = m[1].str();
                std::transform(mnem.begin(), mnem.end(), mnem.begin(), [](unsigned char c){ return std::toupper(c); });

                if (isExtended) return new Instruction<0, true>(
                    opcode,
                    static_cast<const Mnemonic>(hash_32_fnv1a(mnem.c_str(), mnem.length()))
                );

                return new Instruction<0>(
                    opcode,
                    static_cast<const Mnemonic>(hash_32_fnv1a(mnem.c_str(), mnem.length()))
                );
            }

            return nullptr;
        }
    }
}
