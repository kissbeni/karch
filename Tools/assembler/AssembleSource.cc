
/**
 * @file AssembleSource.cc
 * @author Kiss Benedek
 * @brief Assembler class implementation (source parsing)
 */

#include "Assembler.h"
#include "AssemblerException.hpp"

#include <shared/stringutil.hpp>

#include <regex>
#include <iostream>

namespace ktools
{
    namespace assembler
    {
        static const std::regex g_reg_var("^[^0-9][\\w_^]*$");
        static const std::regex g_reg_def("^[dD][eE][fF]\\s+(.+)\\s+(.+)$");
        static const std::regex g_reg_label("^(.+):$");

        void Assembler::AssembleSource(std::istream& stream)
        {
            std::smatch m;

            karch_addr_t address = 0;

            ParseLabels(stream);

            uint32_t ln = 1, trimlen = 0;
            for (std::string line; std::getline(stream, line); ln++)
            {
                trimlen = line.length();
                RemoveCommentFromLine(line);
                ksu::trim(line);
                trimlen -= line.length();

                if (line.empty())
                    continue;

                bool isLabel = false;
                for (Label* l : mLabels)
                    if (l->GetLine() == ln)
                    {
                        isLabel = true;
                        l->Relocate(address);
                        break;
                    }

                if (isLabel || std::regex_match(line, m, g_reg_def))
                    continue;

                std::string line_upper = ksu::toupper(line);

                if (line_upper == "DATA") {
                    address = 0;
                    continue;
                }

                if (line_upper == "CODE") {
                    address = 0x100;
                    continue;
                }

                if (ksu::begins_with(line_upper, std::string("ORG")) && isspace(line[3])) {
                    std::string wk = line.substr(3);
                    uint32_t tr = wk.length();
                    ksu::trim(wk);
                    tr -= wk.length();

                    if (!ParseAddress(wk, address))
                        throw AssemblerException("Invalid origin address", trimlen + 3 + tr, ln);

                    continue;
                }

                AbstractInstruction* inst = nullptr;

                if (ksu::begins_with(line_upper, std::string("DB")) && isspace(line[2])) {
                    std::string wk = line.substr(3);
                    uint32_t tr = wk.length();
                    ksu::trim(wk);
                    tr -= wk.length();

                    try {
                        inst = ParseData(wk);
                    } catch (AssemblerException& e) {
                        throw AssemblerException(e.what(), trimlen + 3 + tr + e.GetColumn(), ln);
                    }
                }

                if (!inst)
                {
                    try {
                        inst = ParseSingleInstruction(line);
                    } catch (AssemblerException& e) {
                        throw AssemblerException(e.what(), trimlen + e.GetColumn(), ln);
                    }
                }

                if (inst)
                {
                    if (mCode.find(address) != mCode.end())
                        mCode[address] = inst;
                    else
                        mCode.insert(std::pair<karch_addr_t, AbstractInstruction*>(address, inst));

                    inst->Relocate(address);
                    address += inst->Size();
                }
                else throw AssemblerException("Invalid instruction", trimlen, ln);
            }

            for (std::pair<karch_addr_t, AbstractInstruction*> p : mCode)
                p.second->Relocate(p.first);
        }

        void Assembler::ParseLabels(std::istream& stream)
        {
            std::istream::pos_type initial_pos = stream.tellg();
            std::smatch m;

            mLogger.info("Reading labels...");

            uint32_t ln = 1, trimlen = 0;
            for (std::string line; std::getline(stream, line); ln++)
            {
                trimlen = line.length();
                RemoveCommentFromLine(line);
                ksu::trim(line);
                trimlen -= line.length();

                if (line == ":")
                    throw AssemblerException("Unexpected `:` (empty label name?)", ln);

                if (std::regex_match(line, m, g_reg_label))
                {
                    std::string label = m[1].str();

                    if (!std::regex_match(label, m, g_reg_var))
                        throw AssemblerException("Invalid character in label name", trimlen, ln);

                    if (DoesLabelExist(label))
                        throw AssemblerException("Redefinition of label '" + label + "'", trimlen, ln);

                    Label* l = new Label(label, ln);

                    mLogger.verbose("Label '%s' (0x%x) on line %d", label.c_str(), l->GetHash(), ln);

                    mLabels.push_back(l);
                }
                else if (std::regex_match(line, m, g_reg_def))
                {
                    std::string def_name = m[1].str();
                    ksu::trim(def_name);

                    std::string op_name = m[2].str();
                    ksu::trim(op_name);

                    if (!std::regex_match(def_name, m, g_reg_var))
                        throw AssemblerException("Invalid character in variable name", trimlen, ln);

                    Operand* v = ParseOperand(op_name);
                    if (!v) throw AssemblerException("Invalid value", m.position() + m[1].length() + trimlen, ln);

                    fnv32_t hash = hash_32_fnv1a(def_name);

                    if (mDefines.find(hash) != mDefines.end())
                        throw AssemblerException("Redefinition of '" + def_name + "'", m.position() + trimlen, ln);

                    mLogger.verbose("Defined '%s' (0x%x) with value (%s) on line %d", def_name.c_str(), hash, v->ToString().c_str(), ln);

                    mDefines.insert(std::pair<fnv32_t, Operand*>(hash, v));
                }
            }

            mLogger.info("Finished reading labels");

            stream.clear();
            stream.seekg(initial_pos, std::ios::beg);
        }

        DataInstruction* Assembler::ParseData(const std::string& line)
        {
            DataInstruction* res        = new DataInstruction();
            char current_string_char    = 0;
            bool expect_sep             = false;
            std::string buf             = "";
            uint32_t chrindex           = -1;
            karch_addr_t data;

            for (const char& ch : line)
            {
                chrindex++;

                if (current_string_char)
                {
                    if (current_string_char == ch)
                    {
                        current_string_char = 0;
                        expect_sep = true;
                    }
                    else res->Append(static_cast<const karch_word_t>(ch));

                    continue;
                }

                if (isspace(ch))
                {
                    if (!buf.empty())
                        throw AssemblerException("Missing separator", chrindex);

                    continue;
                }

                if (expect_sep && ch != ',')
                    throw AssemblerException("Missing ','", chrindex);

                if (ch == ',')
                {
                    if (!buf.empty())
                    {
                        if (DoesLabelExist(buf))
                            throw AssemblerException("Label constants are not supported yet", chrindex);
                        else if (!ParseAddress(buf, data))
                        {
                            fnv32_t hash = hash_32_fnv1a(buf);
                            std::map<fnv32_t, Operand*>::iterator it = mDefines.find(hash);

                            if (it == mDefines.end())
                                throw AssemblerException("Could not parse constant", chrindex);

                            if (it->second->IsRegister())
                                throw AssemblerException("Invalid constant", chrindex);

                            data = it->second->GetNumber();
                        }

                        if (data > 0xFF)
                            throw AssemblerException("Only 8-bit constants and string are supported for now", chrindex);

                        res->Append(static_cast<karch_word_t>(data & 0xFF));
                        buf.clear();
                    }

                    expect_sep = false;
                    continue;
                }

                if (ch == '\'' || ch == '\"')
                {
                    current_string_char = ch;
                    continue;
                }

                buf += ch;
            }

            if (current_string_char == '\'')
                throw AssemblerException("Missing string closing `'`", chrindex);

            if (current_string_char == '"')
                throw AssemblerException("Missing string closing character `\"`", chrindex);

            if (!buf.empty())
            {
                if (DoesLabelExist(buf))
                    throw AssemblerException("Label constants are not supported yet", chrindex);
                else if (!ParseAddress(buf, data))
                {
                    fnv32_t hash = hash_32_fnv1a(buf);
                    std::map<fnv32_t, Operand*>::iterator it = mDefines.find(hash);

                    if (it == mDefines.end())
                        throw AssemblerException("Could not parse constant", chrindex);

                    if (it->second->IsRegister())
                        throw AssemblerException("Invalid constant", chrindex);

                    data = it->second->GetNumber();
                }

                if (data > 0xFF)
                    throw AssemblerException("Only 8-bit constants and string are supported for now", chrindex);

                res->Append(static_cast<karch_word_t>(data & 0xFF));
                buf.clear();
            }

            return res;
        }
    }
}
