
/**
 * @file Assembler.h
 * @author Kiss Benedek
 * @brief Assembler class
 */

#ifndef KASM_ASSEMBLER_H
#define KASM_ASSEMBLER_H

#include <assembler/Instruction.hpp>
#include <assembler/DataInstruction.hpp>
#include <assembler/Operand.hpp>
#include <assembler/Label.hpp>

#include <shared/fnv1a.hpp>
#include <shared/testing.h>
#include <shared/logger.h>

#include <vector>
#include <functional>
#include <fstream>
#include <map>

namespace ktools
{
    namespace assembler
    {
        class Assembler
        {
            public:
                static void InitializeInstructionMap();
                static void DestroyInstructionMap();

                void AssembleSourceFile(const std::string& filename);
                void AssembleSource(std::istream& stream);

                // This function assumes a clean input
                AbstractInstruction* ParseSingleInstruction(const std::string& text);
                AbstractInstruction* ParseSingleInstruction(const std::string& text,
                    const karch_word_t& opcode, const bool& isExtended);
                Operand* ParseOperand(const std::string& text);

                static bool ParseAddress(const std::string& text, karch_addr_t& addr_out);

                void ListCode(std::ostream& stream) const;
                void WriteBinary(std::ostream& stream) const;

                const bool DoesLabelExist(const std::string& name) const;

                const std::string FindFirstLabelWithSimilarName(const std::string& name, size_t* distance = nullptr) const;

            PRIVATE:
                static const char* mPrimaryInstructionMap[256];
                static const char* mSecondaryInstructionMap[256];

                typedef std::vector<AbstractInstruction*>::iterator inst_iterator;

                static std::vector<AbstractInstruction*> mPrimaryInstructions;
                static std::vector<AbstractInstruction*> mSecondaryInstructions;

                static bool mInitialized;

                static void RemoveCommentFromLine(std::string& line);

                void ParseLabels(std::istream& stream);
                DataInstruction* ParseData(const std::string& line);

                template<typename Tres = bool, const Tres nContinue = false>
                static Tres IterateAllInstructions(std::function<Tres(const inst_iterator)> f) {
                    Tres r;

                    inst_iterator it = mPrimaryInstructions.begin();
                    for (; it != mPrimaryInstructions.end(); ++it)
                        if ((r = f(it)) != nContinue) return r;

                    it = mSecondaryInstructions.begin();
                    for (; it != mSecondaryInstructions.end(); ++it)
                        if ((r = f(it)) != nContinue) return r;

                    return nContinue;
                }

                static void IterateAllInstructions(std::function<void(const inst_iterator)> f) {
                    inst_iterator it = mPrimaryInstructions.begin();
                    for (; it != mPrimaryInstructions.end(); ++it)
                        f(it);

                    it = mSecondaryInstructions.begin();
                    for (; it != mSecondaryInstructions.end(); ++it)
                        f(it);
                }

                std::vector<Label*> mLabels;
                std::map<fnv32_t, Operand*> mDefines;
                std::map<karch_addr_t, AbstractInstruction*> mCode;

                static Logger& mLogger;
        };
    }
}

#endif
