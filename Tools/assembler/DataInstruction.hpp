
/**
 * @file DataInstruction.hpp
 * @author Kiss Benedek
 * @brief DataInstruction class
 */

#ifndef KASM_DATA_INST_H
#define KASM_DATA_INST_H

#include <assembler/Instruction.hpp>

namespace ktools
{
    namespace assembler
    {
        class DataInstruction : public AbstractInstruction
        {
            public:
                DataInstruction() {
                    mAddress = 0;
                }

                ~DataInstruction()
                {
                    for (Operand* op : mData)
                        delete op;
                }

                const Operand* Op(const unsigned int n) const
                {
                    if ((n - 1) >= mData.size())
                        throw std::out_of_range("Operand index out of range");

                    return mData[n - 1];
                }

                const unsigned int NumOps() const {
                    return mData.size();
                }

                const Mnemonic GetMnemonic() const {
                    return Mnemonic::UNKNOWN;
                }

                const karch_word_t GetOpcode() const {
                    return 0;
                }

                const bool IsExtended() const {
                    return false;
                }

                const unsigned int Size() const {
                    return mData.size();
                }

                const unsigned int ToArray(unsigned char* p, unsigned int offset,
                    const unsigned int len) const
                {
                    if ((len - offset) < Size())
                        throw std::length_error("Output buffer is too small");

                    unsigned int off_before = offset;

                    for (Operand* op : mData)
                        offset += op->ToArray(p, offset, len, false);

                    // We should newer get here
                    if ((offset - off_before) != Size())
                        throw std::runtime_error("Offset change does not equal to the size of the instruction");

                    return offset - off_before;
                }

                const std::string ToString() const
                {
                    std::string res = "DB ";
                    char tmp[8];

                    for (Operand* op : mData)
                    {
                        if (res.length() > 3)
                            sprintf(tmp, ", %02x", op->GetNumber());
                        else
                            sprintf(tmp, "%02x", op->GetNumber());

                        res += tmp;
                    }

                    return res;
                }

                const karch_addr_t GetAddress() const {
                    return mAddress;
                }

                void Relocate(const karch_addr_t& addr) {
                    mAddress = addr;
                }

                void Append(const karch_word_t& data) {
                    mData.push_back(new Operand(OperandType::ConstNumber, data));
                }
            private:
                std::vector<Operand*> mData;
                karch_addr_t mAddress;
        };
    }
}

#endif
