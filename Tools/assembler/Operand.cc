
/**
 * @file Operand.cc
 * @author Kiss Benedek
 * @brief OperandType enum operator implementations
 */

#include "Operand.hpp"

namespace ktools
{
    namespace assembler
    {
        OperandType operator|(const OperandType& a, const OperandType& b)
        {
            return static_cast<OperandType>(static_cast<const __OperandType_underlying_type>(a) |
                static_cast<const __OperandType_underlying_type>(b));
        }

        OperandType operator|=(OperandType& a, const OperandType& b)
        {
            a = a | b;
            return a;
        }

        OperandType operator&(const OperandType& a, const OperandType& b)
        {
            return static_cast<OperandType>(static_cast<const __OperandType_underlying_type>(a) &
                static_cast<const __OperandType_underlying_type>(b));
        }

        bool operator!(const OperandType& a)
        {
            return !static_cast<__OperandType_underlying_type>(a);
        }
    }
}
