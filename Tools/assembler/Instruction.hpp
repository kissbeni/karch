
/**
 * @file Instruction.hpp
 * @author Kiss Benedek
 * @brief Instruction and AbstractInstruction classes
 */

#ifndef KASM_INSTRUCTION_H
#define KASM_INSTRUCTION_H

#include <assembler/Mnemonic.h>
#include <assembler/Operand.hpp>

#include <shared/property.h>

#include <cstdarg>
#include <cstdio>
#include <vector>
#include <stdexcept>

namespace ktools
{
    namespace assembler
    {
        class AbstractInstruction
        {
            public:
                virtual const Operand* Op(const unsigned int n) const = 0;
                virtual const unsigned int NumOps() const = 0;
                virtual const Mnemonic GetMnemonic() const = 0;
                virtual const karch_word_t GetOpcode() const = 0;
                virtual const bool IsExtended() const = 0;
                virtual const unsigned int Size() const = 0;
                virtual const unsigned int ToArray(unsigned char* p, unsigned int offset,
                    const unsigned int len) const = 0;
                virtual const std::string ToString() const = 0;
                virtual const karch_addr_t GetAddress() const = 0;
                virtual void Relocate(const karch_addr_t& addr) = 0;

                virtual ~AbstractInstruction() {}
        };

        template<const unsigned int nOperands = 0, const bool nExtended = false>
        class Instruction : public AbstractInstruction
        {
            public:
                Instruction(const karch_word_t& opcode, const Mnemonic mnem, ...)
                    : mMnemonic(mnem), mOpcode(opcode), mUseWideAddress(false)
                {
                    va_list vl;
                    va_start(vl, mnem);

                    for (unsigned int i = 0; i < nOperands; i++)
                    {
                        Operand* op = va_arg(vl, Operand*);

                        if (op == nullptr)
                        {
                            char buf[64];
                            sprintf(buf, "Argument %u was null", i+1);
                            throw std::invalid_argument(buf);
                        }

                        mOperands[i] = op;
                    }

                    va_end(vl);

                    Relocate(0);
                }

                ~Instruction()
                {
                    for (unsigned int i = 0; i < nOperands; i++)
                        delete mOperands[i];
                }

                const Operand* Op(const unsigned int n) const
                {
                    if ((n - 1) >= nOperands)
                        throw std::out_of_range("Operand index out of range");

                    return mOperands[n - 1];
                }

                const unsigned int NumOps() const {
                    return nOperands;
                }

                const Mnemonic GetMnemonic() const {
                    return mMnemonic;
                }

                const karch_word_t GetOpcode() const {
                    return mOpcode;
                }

                const bool IsExtended() const {
                    return nExtended;
                }

                const karch_addr_t GetAddress() const {
                    return mAddress;
                }

                void Relocate(const karch_addr_t& addr) {
                    mAddress = addr;
                    mUseWideAddress = false;

                    for (unsigned int i = 0; i < nOperands; i++)
                    {
                        mOperands[i]->Relocate(addr);

                        if (mOperands[i]->HasWideNumber())
                            mUseWideAddress = true;
                    }
                }

                const unsigned int Size() const
                {
                    unsigned int s = sizeof(karch_word_t);

                    for (unsigned int i = 0; i < nOperands; i++)
                        s += mOperands[i]->Size(mUseWideAddress);

                    if (nExtended)
                        s++;

                    if (mUseWideAddress)
                        s++;

                    return s;
                }

                const unsigned int ToArray(unsigned char* p, unsigned int offset,
                    const unsigned int len) const
                {
                    if ((len - offset) < Size())
                        throw std::length_error("Output buffer is too small");

                    unsigned int off_before = offset;

                    if (mUseWideAddress)
                        p[offset++] = 0x40; // WADDR marker

                    if (nExtended)
                        p[offset++] = 0xD0; // EXTEND marker

                    p[offset++] = mOpcode;

                    for (unsigned int i = 0; i < nOperands; i++)
                        offset += mOperands[i]->ToArray(p, offset, len, mUseWideAddress);

                    // We should newer get here
                    if ((offset - off_before) != Size())
                        throw std::runtime_error("Offset change does not equal to the size of the instruction");

                    return offset - off_before;
                }

                const std::string ToString() const
                {
                    std::string res = mnemstr(mMnemonic);

                    if (nOperands > 0)
                        res += " " + mOperands[0]->ToString();

                    for (unsigned int i = 1; i < nOperands; i++)
                        res += ", " + mOperands[i]->ToString();

                    return res;
                }
            private:
                Operand* mOperands[nOperands];
                const Mnemonic mMnemonic;
                const karch_word_t mOpcode;
                karch_addr_t mAddress;
                bool mUseWideAddress;
        };
    }
}

#endif
