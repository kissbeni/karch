
/**
 * @file Register.hpp
 * @author Kiss Benedek
 * @brief Register enum and ParseRegister function
 */

#ifndef KASM_REGISTER_H
#define KASM_REGISTER_H

#include <shared/fnv1a.hpp>
#include <cstring>

namespace ktools
{
    namespace assembler
    {
        enum class Register : fnv32_t
        {
            Unknown,
            RA = fnv1a32_const("RA"),
            RB = fnv1a32_const("RB"),
            RC = fnv1a32_const("RC"),
            RD = fnv1a32_const("RD")
        };

        static inline const Register ParseRegister(const char* str)
        {
            Register hash = static_cast<Register>(hash_32_fnv1a(str, strlen(str)));

            if (hash != Register::RA && hash != Register::RB && hash != Register::RC && hash != Register::RD)
                return Register::Unknown;

            return hash;
        }
    }
}

#endif
