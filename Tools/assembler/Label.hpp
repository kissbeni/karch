
/**
 * @file Label.hpp
 * @author Kiss Benedek
 * @brief Label class
 */

#ifndef KASM_LABLE_H
#define KASM_LABLE_H

#include <shared/fnv1a.hpp>
#include <shared/types.h>

#include <string>

namespace ktools
{
    namespace assembler
    {
        class Label
        {
            public:
                Label(const std::string& name, const uint32_t& line)
                    : mName(name), mLine(line), mNameHash(hash_32_fnv1a(name)), mAddress(0) { }

                void Relocate(const fnv32_t& newaddr) {
                    mAddress = newaddr;
                }

                const std::string& GetName() const {
                    return mName;
                }

                const uint32_t GetLine() const {
                    return mLine;
                }

                const fnv32_t GetHash() const {
                    return mNameHash;
                }

                const karch_addr_t GetAddress() const {
                    return mAddress;
                }
            private:
                const std::string mName;
                const uint32_t mLine;
                const fnv32_t mNameHash;
                karch_addr_t mAddress;
        };
    }
}

#endif
