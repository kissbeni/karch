
/**
 * @file InstructionMaps.cc
 * @author Kiss Benedek
 * @brief Assembler class implementation (instruction maps)
 */

#include "Assembler.h"

#include <vector>

namespace ktools
{
    namespace assembler
    {
        const char* Assembler::mPrimaryInstructionMap[256] = {
            "NOP",    "MOV RA, 0",     "MOV RA, RA", "ADD RA, 0",   "INC RA",   "OUT 0, RA",   "PUSH RA",   "AND RA, 0", "AND RA, RA", "SHR RA, 0", "OR RA, RA", "PUSH 0",   "XOR RA, RA", "MUL RA, 0",   "MUL RA, RA", "DIV RA, RA",
            "RET",    "MOV RB, 0",     "MOV RA, RB", "ADD RB, 0",   "INC RB",   "OUT 0, RB",   "PUSH RB",   "AND RB, 0", "AND RA, RB", "SHR RB, 0", "OR RA, RB", "JMP $+1",  "XOR RA, RB", "MUL RB, 0",   "MUL RA, RB", "DIV RA, RB",
            "PUSHA",  "MOV RC, 0",     "MOV RA, RC", "ADD RC, 0",   "INC RC",   "OUT 0, RC",   "PUSH RC",   "AND RC, 0", "AND RA, RC", "SHR RC, 0", "OR RA, RC", "JE $+1",   "XOR RA, RC", "MUL RC, 0",   "MUL RA, RC", "DIV RA, RC",
            "POPA",   "MOV RD, 0",     "MOV RA, RD", "ADD RD, 0",   "INC RD",   "OUT 0, RD",   "PUSH RD",   "AND RD, 0", "AND RA, RD", "SHR RD, 0", "OR RA, RD", "JNE $+1",  "XOR RA, RD", "MUL RD, 0",   "MUL RA, RD", "DIV RA, RD",
            "",       "MOV RA, [1]",   "MOV RB, RA", "SUB RA, 0",   "DEC RA",   "IN RA, 0",    "POP RA",    "OR RA, 0",  "AND RB, RA", "ROL RA, 0", "OR RB, RA", "JC $+1",   "XOR RB, RA", "DIV RA, 0",   "MUL RB, RA", "DIV RB, RA",
            "SETC",   "MOV RB, [1]",   "MOV RB, RB", "SUB RB, 0",   "DEC RB",   "IN RB, 0",    "POP RB",    "OR RB, 0",  "AND RB, RB", "ROL RB, 0", "OR RB, RB", "JNC $+1",  "XOR RB, RB", "DIV RB, 0",   "MUL RB, RB", "DIV RB, RB",
            "SETZ",   "MOV RC, [1]",   "MOV RB, RC", "SUB RC, 0",   "DEC RC",   "IN RC, 0",    "POP RC",    "OR RC, 0",  "AND RB, RC", "ROL RC, 0", "OR RB, RC", "JBE $+1",  "XOR RB, RC", "DIV RC, 0",   "MUL RB, RC", "DIV RB, RC",
            "SETS",   "MOV RD, [1]",   "MOV RB, RD", "SUB RD, 0",   "DEC RD",   "IN RD, 0",    "POP RD",    "OR RD, 0",  "AND RB, RD", "ROL RD, 0", "OR RB, RD", "JA $+1",   "XOR RB, RD", "DIV RD, 0",   "MUL RB, RD", "DIV RB, RD",
            "SETO",   "MOV [RA], 0",   "MOV RC, RA", "ADD [RA], 0", "INC [RA]", "OUT 0, [RA]", "PUSH [RA]", "XOR RA, 0", "AND RC, RA", "ROR RA, 0", "OR RC, RA", "JS $+1",   "XOR RC, RA", "MUL [RA], 0", "MUL RC, RA", "DIV RC, RA",
            "CFC",    "MOV [RB], 0",   "MOV RC, RB", "ADD [RB], 0", "INC [RB]", "OUT 0, [RB]", "PUSH [RB]", "XOR RB, 0", "AND RC, RB", "ROR RB, 0", "OR RC, RB", "JNS $+1",  "XOR RC, RB", "MUL [RB], 0", "MUL RC, RB", "DIV RC, RB",
            "CFZ",    "MOV [RC], 0",   "MOV RC, RC", "ADD [RC], 0", "INC [RC]", "OUT 0, [RC]", "PUSH [RC]", "XOR RC, 0", "AND RC, RC", "ROR RC, 0", "OR RC, RC", "BNK 0",    "XOR RC, RC", "MUL [RC], 0", "MUL RC, RC", "DIV RC, RC",
            "CFS",    "MOV [RD], 0",   "MOV RC, RD", "ADD [RD], 0", "INC [RD]", "OUT 0, [RD]", "PUSH [RD]", "XOR RD, 0", "AND RC, RD", "ROR RD, 0", "OR RC, RD", "SYS 0",    "XOR RC, RD", "MUL [RD], 0", "MUL RC, RD", "DIV RC, RD",
            "CFO",    "MOV [1], RA",   "MOV RD, RA", "SUB [RA], 0", "DEC [RA]", "IN [RA], 0",  "NOT RA",    "SHL RA, 0", "AND RD, RA", "CMP RA, 0", "OR RD, RA", "JO $+1",   "XOR RD, RA", "DIV [RA], 0", "MUL RD, RA", "DIV RD, RA",
            "",       "MOV [1], RB",   "MOV RD, RB", "SUB [RB], 0", "DEC [RB]", "IN [RB], 0",  "NOT RB",    "SHL RB, 0", "AND RD, RB", "CMP RB, 0", "OR RD, RB", "JNO $+1",  "XOR RD, RB", "DIV [RB], 0", "MUL RD, RB", "DIV RD, RB",
            "RETI",   "MOV [1], RC",   "MOV RD, RC", "SUB [RC], 0", "DEC [RC]", "IN [RC], 0",  "NOT RC",    "SHL RC, 0", "AND RD, RC", "CMP RC, 0", "OR RD, RC", "CALL $+1", "XOR RD, RC", "DIV [RC], 0", "MUL RD, RC", "DIV RD, RC",
            "HLT",    "MOV [1], RD",   "MOV RD, RD", "SUB [RD], 0", "DEC [RD]", "IN [RD], 0",  "NOT RD",    "SHL RD, 0", "AND RD, RD", "CMP RD, 0", "OR RD, RD", "PUSH [1]", "XOR RD, RD", "DIV [RD], 0", "MUL RD, RD", "DIV RD, RD"
        };

        const char* Assembler::mSecondaryInstructionMap[256] = {
            "IIC RA",   "WAITS 0", "MOV RA, [RA]", "", "ADD RA, RA", "", "SUB RA, RA", "", "CMP RA, RA", "", "CFLAGS", "", "", "", "", "",
            "IIC RB",   "WAITM 0", "MOV RA, [RB]", "", "ADD RA, RB", "", "SUB RA, RB", "", "CMP RA, RB", "", "",       "", "", "", "", "",
            "IIC RC",   "WAITC 0", "MOV RA, [RC]", "", "ADD RA, RC", "", "SUB RA, RC", "", "CMP RA, RC", "", "",       "", "", "", "", "",
            "IIC RD",   "BEEP 0",  "MOV RA, [RD]", "", "ADD RA, RD", "", "SUB RA, RB", "", "CMP RA, RD", "", "",       "", "", "", "", "",
            "DIC RA",   "",        "MOV RB, [RA]", "", "ADD RB, RA", "", "SUB RB, RA", "", "CMP RB, RA", "", "",       "", "", "", "", "",
            "DIC RB",   "",        "MOV RB, [RB]", "", "ADD RB, RB", "", "SUB RB, RB", "", "CMP RB, RB", "", "",       "", "", "", "", "",
            "DIC RC",   "",        "MOV RB, [RC]", "", "ADD RB, RC", "", "SUB RB, RC", "", "CMP RB, RC", "", "",       "", "", "", "", "",
            "DIC RD",   "",        "MOV RB, [RD]", "", "ADD RB, RD", "", "SUB RB, RD", "", "CMP RB, RD", "", "",       "", "", "", "", "",
            "IIC [RA]", "",        "MOV RC, [RA]", "", "ADD RC, RA", "", "SUB RC, RA", "", "CMP RC, RA", "", "",       "", "", "", "", "",
            "IIC [RB]", "",        "MOV RC, [RB]", "", "ADD RC, RB", "", "SUB RC, RB", "", "CMP RC, RB", "", "",       "", "", "", "", "",
            "IIC [RC]", "",        "MOV RC, [RC]", "", "ADD RC, RC", "", "SUB RC, RC", "", "CMP RC, RC", "", "",       "", "", "", "", "",
            "IIC [RD]", "",        "MOV RC, [RD]", "", "ADD RC, RD", "", "SUB RC, RD", "", "CMP RC, RD", "", "",       "", "", "", "", "",
            "DIC [RA]", "",        "MOV RD, [RA]", "", "ADD RD, RA", "", "SUB RD, RA", "", "CMP RD, RA", "", "",       "", "", "", "", "",
            "DIC [RB]", "",        "MOV RD, [RB]", "", "ADD RD, RB", "", "SUB RD, RB", "", "CMP RD, RB", "", "",       "", "", "", "", "",
            "DIC [RC]", "",        "MOV RD, [RC]", "", "ADD RD, RC", "", "SUB RD, RC", "", "CMP RD, RC", "", "",       "", "", "", "", "",
            "DIC [RD]", "",        "MOV RD, [RD]", "", "ADD RD, RD", "", "SUB RD, RD", "", "CMP RD, RD", "", "",       "", "", "", "", ""
        };

        std::vector<AbstractInstruction*> Assembler::mPrimaryInstructions;
        std::vector<AbstractInstruction*> Assembler::mSecondaryInstructions;
    }
}
