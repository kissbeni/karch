
/**
 * @file Mnemonic.h
 * @author Kiss Benedek
 * @brief Mnemonic enum and mnemonic alias list
 */

#ifndef KASM_MNEMONIC_H
#define KASM_MNEMONIC_H

#include <shared/fnv1a.hpp>

namespace ktools
{
    namespace assembler
    {
        enum class Mnemonic : fnv32_t
        {
            UNKNOWN = 0,
            NOP     = fnv1a32_const("NOP"),
            RET     = fnv1a32_const("RET"),
            PUSHA   = fnv1a32_const("PUSHA"),
            POPA    = fnv1a32_const("POPA"),
            CFL     = fnv1a32_const("CFL"),
            SETC    = fnv1a32_const("SETC"),
            SETZ    = fnv1a32_const("SETZ"),
            SETS    = fnv1a32_const("SETS"),
            SETO    = fnv1a32_const("SETO"),
            CFC     = fnv1a32_const("CFC"),
            CFZ     = fnv1a32_const("CFZ"),
            CFS     = fnv1a32_const("CFS"),
            CFO     = fnv1a32_const("CFO"),
            RETI    = fnv1a32_const("RETI"),
            HLT     = fnv1a32_const("HLT"),
            MOV     = fnv1a32_const("MOV"),
            ADD     = fnv1a32_const("ADD"),
            SUB     = fnv1a32_const("SUB"),
            INC     = fnv1a32_const("INC"),
            DEC     = fnv1a32_const("DEC"),
            OUT     = fnv1a32_const("OUT"),
            IN      = fnv1a32_const("IN"),
            PUSH    = fnv1a32_const("PUSH"),
            POP     = fnv1a32_const("POP"),
            NOT     = fnv1a32_const("NOT"),
            AND     = fnv1a32_const("AND"),
            OR      = fnv1a32_const("OR"),
            XOR     = fnv1a32_const("XOR"),
            SHL     = fnv1a32_const("SHL"),
            SHR     = fnv1a32_const("SHR"),
            ROL     = fnv1a32_const("ROL"),
            ROR     = fnv1a32_const("ROR"),
            CMP     = fnv1a32_const("CMP"),
            JMP     = fnv1a32_const("JMP"),
            JE      = fnv1a32_const("JE"),
            JNE     = fnv1a32_const("JNE"),
            JC      = fnv1a32_const("JC"),
            JNC     = fnv1a32_const("JNC"),
            JBE     = fnv1a32_const("JBE"),
            JA      = fnv1a32_const("JA"),
            JS      = fnv1a32_const("JS"),
            JNS     = fnv1a32_const("JNS"),
            BNK     = fnv1a32_const("BNK"),
            SYS     = fnv1a32_const("SYS"),
            JO      = fnv1a32_const("JO"),
            JNO     = fnv1a32_const("JNO"),
            CALL    = fnv1a32_const("CALL"),
            MUL     = fnv1a32_const("MUL"),
            DIV     = fnv1a32_const("DIV"),
            IIC     = fnv1a32_const("IIC"),
            DIC     = fnv1a32_const("DIC"),
            WAITS   = fnv1a32_const("WAITS"),
            WAITM   = fnv1a32_const("WAITM"),
            WAITC   = fnv1a32_const("WAITC"),
            BEEP    = fnv1a32_const("BEEP")
        };

        typedef struct _mnemonic_alias {
            const fnv32_t alt_name;
            const Mnemonic name;
        } mnemonic_alias_t;

        const char* mnemstr(const Mnemonic& mn);
        static const mnemonic_alias_t gMnemonicAliasMap[] = {
            { fnv1a32_const("JZ"),   Mnemonic::JE      },
            { fnv1a32_const("JNZ"),  Mnemonic::JNE     },
            { fnv1a32_const("JB"),   Mnemonic::JC      },
            { fnv1a32_const("JNAE"), Mnemonic::JC      },
            { fnv1a32_const("JAE"),  Mnemonic::JNC     },
            { fnv1a32_const("JNB"),  Mnemonic::JNC     },
            { fnv1a32_const("JNA"),  Mnemonic::JBE     },
            { fnv1a32_const("JNBE"), Mnemonic::JA      },
            { 0,                     Mnemonic::UNKNOWN }
        };
    }
}

#endif
