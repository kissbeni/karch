
/**
 * @file Operand.hpp
 * @author Kiss Benedek
 * @brief OperandType enum and Operand class
 */

#ifndef KASM_OPERAND_H
#define KASM_OPERAND_H

#include <shared/types.h>
#include <assembler/Register.hpp>
#include <assembler/Label.hpp>

#include <stdexcept>

namespace ktools
{
    namespace assembler
    {
        typedef unsigned char __OperandType_underlying_type;

        enum class OperandType : __OperandType_underlying_type {
            Unknown         = 0x00,
            ConstNumber     = 0x01,
            OffsetNumber    = 0x02,
            Register        = 0x04,
            Label           = 0x20,
            Reference       = 0x40,
            Defined         = 0x80,

            ConsistentFlags = ConstNumber | OffsetNumber | Register | Reference
        };

        OperandType operator|(const OperandType& a, const OperandType& b);
        OperandType operator|=(OperandType& a, const OperandType& b);
        OperandType operator&(const OperandType& a, const OperandType& b);
        bool operator!(const OperandType& a);

        class Operand {
            public:
                Operand(const OperandType& op, const karch_word_t& val)
                    : mType(op),
                      mIntValue(val),
                      mReg(Register::Unknown),
                      mAllowWideNumber(false) { mLabel = nullptr; }

                Operand(const OperandType& op, const Register& reg)
                    : mType(op),
                      mIntValue(0),
                      mReg(reg),
                      mAllowWideNumber(false) { mLabel = nullptr; }

                Operand(const OperandType& op, Label* label)
                    : mType(op),
                      mIntValue(0),
                      mReg(Register::Unknown),
                      mAllowWideNumber(true) { mLabel = label; }

                const unsigned int Size(const bool wideNumbers) const
                {
                    if ((!!(mType & OperandType::ConstNumber) ||
                         !!(mType & OperandType::OffsetNumber)) &&
                          !(mType & OperandType::Register))
                        return wideNumbers ? sizeof(karch_addr_t) : sizeof(karch_word_t);

                    return 0;
                }

                const OperandType GetType() const {
                    return mType;
                }

                const karch_addr_t GetNumber() const {
                    karch_addr_t res = mIntValue;

                    if (IsLabel())
                    {
                        if (IsOffset())
                            res = mLabel->GetAddress() - mInstAddress;
                        else
                            res = mLabel->GetAddress();
                    }

                    if (mAllowWideNumber)
                        return res;
                    else
                        return res & 0xFF;
                }

                const Register GetRegister() const {
                    return mReg;
                }

                const Label& GetLabel() const {
                    return *mLabel;
                }

                const bool HasWideNumber() const {
                    if (!mAllowWideNumber)
                        return false;

                    uint16_t n = GetNumber();

                    if (n <= 0x7F)
                        return false;

                    if ((n & 0xFF80) == 0xFF80)
                        return false;

                    return true;
                }

                const bool IsReference() const {
                    return !!(mType & OperandType::Reference);
                }

                const bool IsOffset() const {
                    return !!(mType & OperandType::OffsetNumber);
                }

                const bool IsRegister() const {
                    return !!(mType & OperandType::Register);
                }

                const bool IsLabel() const {
                    return !!(mType & OperandType::Label);
                }

                void Relocate(const karch_addr_t& addr) {
                    mInstAddress = addr;
                }

                const unsigned int ToArray(unsigned char* p, unsigned int offset, const unsigned int len, const bool wideNumbers) const
                {
                    if ((len - offset) < Size(wideNumbers))
                        throw std::length_error("Output buffer is too small");

                    unsigned int off_before = offset;

                    if (Size(wideNumbers) > 0)
                    {
                        karch_addr_t n = GetNumber();

                        p[offset++] = n & 0xFF;

                        if (wideNumbers)
                            p[offset++] = (n >> 8);
                    }

                    // We should newer get here
                    if ((offset - off_before) != Size(wideNumbers))
                        throw std::runtime_error("Offset change does not equal to the size of the operand");

                    return offset - off_before;
                }

                const std::string ToString() const
                {
                    if (mType == OperandType::Unknown)
                        return "??";

                    std::string res;

                    int16_t snval = static_cast<int16_t>(GetNumber());

                    if (!!(mType & OperandType::ConstNumber)) {
                        if (IsOffset())
                            res = std::to_string(abs(snval));
                        else
                            res = std::to_string(GetNumber());
                    }
                    else if (!!(mType & OperandType::Register))
                        switch (mReg)
                        {
                            case Register::RA: res = "RA"; break;
                            case Register::RB: res = "RB"; break;
                            case Register::RC: res = "RC"; break;
                            case Register::RD: res = "RD"; break;
                            default: res = "R?"; break;
                        }

                    if (IsLabel())
                        res += " <" + mLabel->GetName() + ">";

                    if (IsOffset()) {
                        char buf[8];
                        sprintf(buf, "0x%04x", mInstAddress + snval);
                        res = std::string("$") + (snval < 0 ? '-' : '+') + res + " [EA:" + buf + "]";
                    }

                    if (!!(mType & OperandType::Reference))
                        res = "[" + res + "]";

                    return res;
                }

                void SetAllowWideNumber(bool val) {
                    mAllowWideNumber = val;
                }
            private:
                const OperandType mType;
                const karch_addr_t mIntValue;
                const Register mReg;
                karch_addr_t mInstAddress;
                bool mAllowWideNumber;
                Label* mLabel;
        };
    }
}

#endif
