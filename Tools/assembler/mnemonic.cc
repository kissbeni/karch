
/**
 * @file mnemonic.cc
 * @author Kiss Benedek
 * @brief Mnemonic to string conversion
 */

#include "Mnemonic.h"

namespace ktools
{
    namespace assembler
    {
        const char* mnemstr(const Mnemonic& mn) {
            switch (mn) {
                case Mnemonic::NOP:
                    return "NOP";
                case Mnemonic::RET:
                    return "RET";
                case Mnemonic::PUSHA:
                    return "PUSHA";
                case Mnemonic::POPA:
                    return "POPA";
                case Mnemonic::CFL:
                    return "CFL";
                case Mnemonic::SETC:
                    return "SETC";
                case Mnemonic::SETZ:
                    return "SETZ";
                case Mnemonic::SETS:
                    return "SETS";
                case Mnemonic::SETO:
                    return "SETO";
                case Mnemonic::CFC:
                    return "CFC";
                case Mnemonic::CFZ:
                    return "CFZ";
                case Mnemonic::CFS:
                    return "CFS";
                case Mnemonic::CFO:
                    return "CFO";
                case Mnemonic::RETI:
                    return "RETI";
                case Mnemonic::HLT:
                    return "HLT";
                case Mnemonic::MOV:
                    return "MOV";
                case Mnemonic::ADD:
                    return "ADD";
                case Mnemonic::SUB:
                    return "SUB";
                case Mnemonic::INC:
                    return "INC";
                case Mnemonic::DEC:
                    return "DEC";
                case Mnemonic::OUT:
                    return "OUT";
                case Mnemonic::IN:
                    return "IN";
                case Mnemonic::PUSH:
                    return "PUSH";
                case Mnemonic::POP:
                    return "POP";
                case Mnemonic::NOT:
                    return "NOT";
                case Mnemonic::AND:
                    return "AND";
                case Mnemonic::OR:
                    return "OR";
                case Mnemonic::XOR:
                    return "XOR";
                case Mnemonic::SHL:
                    return "SHL";
                case Mnemonic::SHR:
                    return "SHR";
                case Mnemonic::ROL:
                    return "ROL";
                case Mnemonic::ROR:
                    return "ROR";
                case Mnemonic::CMP:
                    return "CMP";
                case Mnemonic::JMP:
                    return "JMP";
                case Mnemonic::JE:
                    return "JE";
                case Mnemonic::JNE:
                    return "JNE";
                case Mnemonic::JC:
                    return "JC";
                case Mnemonic::JNC:
                    return "JNC";
                case Mnemonic::JBE:
                    return "JBE";
                case Mnemonic::JA:
                    return "JA";
                case Mnemonic::JS:
                    return "JS";
                case Mnemonic::JNS:
                    return "JNS";
                case Mnemonic::BNK:
                    return "BNK";
                case Mnemonic::SYS:
                    return "SYS";
                case Mnemonic::JO:
                    return "JO";
                case Mnemonic::JNO:
                    return "JNO";
                case Mnemonic::CALL:
                    return "CALL";
                case Mnemonic::MUL:
                    return "MUL";
                case Mnemonic::DIV:
                    return "DIV";
                case Mnemonic::IIC:
                    return "IIC";
                case Mnemonic::DIC:
                    return "DIC";
                case Mnemonic::WAITS:
                    return "WAITS";
                case Mnemonic::WAITM:
                    return "WAITM";
                case Mnemonic::WAITC:
                    return "WAITC";
                case Mnemonic::BEEP:
                    return "BEEP";
                default:
                    return "UNKNOWN";
            }
        }
    }
}
