
/**
 * @file assembler/main.cc
 * @author Kiss Benedek
 * @brief Program entry point
 */

#include <assembler/Assembler.h>
#include <assembler/AssemblerException.hpp>

#include <iostream>

#ifndef __TEST__
int main(int argc, char const *argv[])
#else
int _assembler_main(int argc, const char *argv[])
#endif
{
    Logger::SetMinLogLevel(3);

    if (argc < 2)
    {
        printf("Usage: %s filename.kasm\n", argv[0]);
        return 1;
    }

    std::string infile = argv[1];
    std::string basename = infile.substr(0, infile.find(".", infile.length() - 6));

    ktools::assembler::Assembler::InitializeInstructionMap();

    ktools::assembler::Assembler a;
    try
    {
        a.AssembleSourceFile(infile);
    }
    catch (ktools::assembler::AssemblerException& ex)
    {
        printf("Assembler failed: %s (on line %d:%d)\n", ex.what(), ex.GetLine(), ex.GetColumn());
        if (ex.GetLine() > 0)
        {
            std::ifstream is(infile);
            uint32_t ln = 1;

            for (std::string line; std::getline(is, line); ln++)
            {
                if (ln == ex.GetLine() - 1 || ln == ex.GetLine() + 1)
                    std::cout << line << std::endl;
                else if (ln == ex.GetLine())
                {
                    std::cout << "\x1b[91m\x1b[1m" << line << "\x1b[0m\x1b[93m" << std::endl;

                    for (uint32_t i = 0; i < ex.GetColumn(); i++)
                        std::cout << ' ';

                    std::cout << "^ Here" << "\x1b[39m" << std::endl;
                }
            }

            is.close();
        }
        return 2;
    }
    
    std::ofstream fos1(basename + ".lst");
    a.ListCode(fos1);
    fos1.close();

    std::ofstream fos2(basename + ".bin");
    a.WriteBinary(fos2);
    fos2.close();

    ktools::assembler::Assembler::DestroyInstructionMap();
    return 0;
}
