
/**
 * @file AssemblerException.hpp
 * @author Kiss Benedek
 * @brief AssemblerException class
 */

#ifndef KASM_ASSEMBLER_EXCEPTION_H
#define KASM_ASSEMBLER_EXCEPTION_H

#include <stdexcept>

namespace ktools
{
    namespace assembler
    {
        class AssemblerException : public std::runtime_error
        {
            public:
                AssemblerException(const std::string& s, unsigned int col)
                    : std::runtime_error(s), mColumn(col), mLine(0) {}

                AssemblerException(const std::string& s, unsigned int col, unsigned int line)
                    : std::runtime_error(s), mColumn(col), mLine(line) {}

                const unsigned int GetColumn() const {
                    return mColumn;
                }

                const unsigned int GetLine() const {
                    return mLine;
                }
            private:
                const unsigned int mColumn;
                const unsigned int mLine;
        };
    }
}

#endif
