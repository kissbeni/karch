
/**
 * @file Assembler.cc
 * @author Kiss Benedek
 * @brief Assembler class implementation (generic)
 */

#include "Assembler.h"
#include "AssemblerException.hpp"

#include <shared/stringutil.hpp>

#include <regex>
#include <iostream>
#include <fstream>
#include <algorithm>

#include <version.h>

namespace ktools
{
    namespace assembler
    {
        bool Assembler::mInitialized = false;
        Logger& Assembler::mLogger = Logger::Get("Assembler");

        void Assembler::InitializeInstructionMap()
        {
            if (mInitialized)
                return;

            Assembler dummy;

            mLogger.info("Initializing instruction map...");

            for (int i = 0; i < 256; i++)
                mPrimaryInstructions.push_back(dummy.ParseSingleInstruction(
                    mPrimaryInstructionMap[i],
                    static_cast<karch_word_t>(i),
                    false
                ));

            if (mPrimaryInstructions.size() != 256)
            {
                mLogger.fatal("Primary instruction map is invalid");
                throw std::runtime_error("Primary instruction map is invalid");
            }

            for (int i = 0; i < 256; i++)
                mSecondaryInstructions.push_back(dummy.ParseSingleInstruction(
                    mSecondaryInstructionMap[i],
                    static_cast<karch_word_t>(i),
                    true
                ));

            if (mPrimaryInstructions.size() != 256)
            {
                mLogger.fatal("Secondary instruction map is invalid");
                throw std::runtime_error("Secondary instruction map is invalid");
            }

            mInitialized = true;
        }

        void Assembler::DestroyInstructionMap()
        {
            if (!mInitialized)
                return;

            IterateAllInstructions([](const inst_iterator it) -> void {
                delete *it;
                *it = nullptr;
            });

            mInitialized = false;
        }

        void Assembler::RemoveCommentFromLine(std::string& line)
        {
            char strc = '\0';

            for (size_t i = 0; i < line.length(); i++)
            {
                char c = line[i];
                if (c == strc)
                    strc = '\0';
                else if (strc == '\0')
                {
                    if (c == '\'' || c == '"')
                        strc = c;
                    else if (c == ';' || c == '#')
                    {
                        line = line.substr(0, i);
                        return;
                    }
                }
            }
        }

        void Assembler::AssembleSourceFile(const std::string& file)
        {
            mLogger.verbose("Got source file: %s", file.c_str());

            std::ifstream f(file);
            AssembleSource(f);
            f.close();
        }

        void Assembler::ListCode(std::ostream& stream) const
        {
            char addrbuf[16];

            stream << "kasm version " KTOOLS_VERSION " (Kiss Benedek) 2019" << std::endl;
            stream << std::endl;

            for (const std::pair<karch_addr_t, AbstractInstruction*>& p : mCode)
            {
                for (Label* l : mLabels)
                    if (l->GetAddress() == p.first) {
                        sprintf(addrbuf, "%x", l->GetAddress());
                        stream << std::endl << l->GetName() << ": <0x" << addrbuf << ">" << std::endl;
                    }

                karch_word_t* buf = new karch_word_t[p.second->Size()];
                uint32_t n = p.second->ToArray(buf, 0, p.second->Size());
                char buf2[64];
                if (n == 1) sprintf(buf2, "%02x          ", buf[0]);
                if (n == 2) sprintf(buf2, "%02x %02x       ", buf[0], buf[1]);
                if (n == 3) sprintf(buf2, "%02x %02x %02x    ", buf[0], buf[1], buf[2]);
                if (n == 4) sprintf(buf2, "%02x %02x %02x %02x ", buf[0], buf[1], buf[2], buf[3]);

                sprintf(addrbuf, "%04x", p.first);
                stream << addrbuf << ": " << buf2 << " : " << p.second->ToString() << std::endl;

                delete[] buf;
            }
        }

        void Assembler::WriteBinary(std::ostream& stream) const
        {
            std::vector<karch_word_t> data;
            karch_addr_t address = 0;

            for (const std::pair<karch_addr_t, AbstractInstruction*>& p : mCode)
            {
                while (p.first > address)
                {
                    data.push_back(0x00);
                    address++;
                }

                karch_word_t* buf = new karch_word_t[p.second->Size()];
                uint32_t n = p.second->ToArray(buf, 0, p.second->Size());

                for (uint32_t i = 0; i < n; i++)
                    data.push_back(buf[i]);

                address += n;

                delete[] buf;
            }

            stream.write(reinterpret_cast<const char*>(data.data()), data.size());
        }

        const bool Assembler::DoesLabelExist(const std::string& name) const
        {
            fnv32_t hash = hash_32_fnv1a(name);

            for (const Label* l : mLabels)
                if (l->GetHash() == hash)
                    return true;

            return false;
        }

        size_t str_distance(const std::string& a, const std::string& b)
        {
            if (a == b)
                return 0;

            size_t lendiff = (a.length() > b.length() ? a.length() - b.length() : b.length() - a.length());

            size_t minlen = std::max(a.length(), b.length());

            if (a.find(b) != std::string::npos)
                return a.find(b) + lendiff;

            if (b.find(a) != std::string::npos)
                return b.find(a) + lendiff;

            uint32_t d1 = lendiff;
            for (size_t i = 0; i < minlen; i++)
            {
                char c1 = tolower(a[i]);
                char c2 = tolower(b[i]);

                if (isspace(c1) || isspace(c2))
                    continue;

                if (c1 != c2) d1++;
            }

            uint32_t d2 = lendiff;
            for (ssize_t i = minlen - 1; i >= 0; i--)
            {
                char c1 = tolower(a[i]);
                char c2 = tolower(b[i]);

                if (isspace(c1) || isspace(c2))
                    continue;

                if (c1 != c2) d2++;
            }

            return std::min(d1, d2);
        }

        const std::string Assembler::FindFirstLabelWithSimilarName(const std::string& name, size_t* distance) const
        {
            std::string res = name;
            size_t dist = -1;

            for (const Label* l : mLabels)
            {
                size_t d = str_distance(l->GetName(), name);
                if (d < dist)
                {
                    dist = d;
                    res = l->GetName();
                }
            }

            if (distance)
                *distance = dist;

            return res;
        }

        static const std::regex g_reg_op_ref("^\\[(.+?)\\]$");
        static const std::regex g_reg_op_off("^\\s*\\$\\s*\\+\\s*(.+?)$");
        static const std::regex g_reg_op_dec("^([0-9]+)$");
        static const std::regex g_reg_op_hex("^(0[xX]([0-9a-fA-F]+)|([0-9a-fA-F]+)[hH])$");
        static const std::regex g_reg_op_bin("^(0b([01]+))$");

        Operand* Assembler::ParseOperand(const std::string& text)
        {
            OperandType t = OperandType::Unknown;
            std::string work_str = text;

            std::smatch m;
            if (std::regex_match(work_str, m, g_reg_op_ref))
            {
                work_str = m[1].str();
                t |= OperandType::Reference;
            }

            if (std::regex_match(work_str, m, g_reg_op_off))
            {
                work_str = m[1].str();
                t |= OperandType::OffsetNumber;
            }

            if (std::regex_match(work_str, m, g_reg_op_dec))
            {
                t |= OperandType::ConstNumber;
                return new Operand(t, static_cast<const karch_word_t>(std::stoi(m[1].str(), nullptr, 10)));
            }

            if (std::regex_match(work_str, m, g_reg_op_hex))
            {
                t |= OperandType::ConstNumber;
                std::string istr = m[2].str().empty() ? m[3].str() : m[2].str();
                return new Operand(t, static_cast<const karch_word_t>(std::stoi(istr, nullptr, 16)));
            }

            if (std::regex_match(work_str, m, g_reg_op_bin))
            {
                t |= OperandType::ConstNumber;
                return new Operand(t, static_cast<const karch_word_t>(std::stoi(m[2].str(), nullptr, 2)));
            }

            std::string reg_s =  work_str;
            std::transform(reg_s.begin(), reg_s.end(), reg_s.begin(), [](unsigned char c){ return std::toupper(c); });
            Register reg = ParseRegister(reg_s.c_str());
            if (reg != Register::Unknown)
            {
                t |= OperandType::Register;
                return new Operand(t, reg);
            }

            fnv32_t hash = hash_32_fnv1a(work_str);
            std::map<fnv32_t, Operand*>::iterator it = mDefines.find(hash);
            if (it != mDefines.end())
                return it->second;

            for (Label* l : mLabels)
                if (l->GetHash() == hash)
                {
                    t |= OperandType::Label | OperandType::ConstNumber;
                    return new Operand(t, l);
                }

            size_t distance;
            std::string similar = FindFirstLabelWithSimilarName(work_str, &distance);

            if (distance <= 5)
                throw AssemblerException("Could not parse operand '" + text + "'\nDid you mean '" + similar + "'?", 0);

            throw AssemblerException("Could not parse operand '" + text + "' (" + work_str + ")", 0);
        }

        bool Assembler::ParseAddress(const std::string& text, karch_addr_t& addr_out) {
            std::smatch m;

            if (std::regex_match(text, m, g_reg_op_dec))
            {
                addr_out = static_cast<const karch_addr_t>(std::stoi(m[1].str(), nullptr, 10));
                return true;
            }

            if (std::regex_match(text, m, g_reg_op_hex))
            {
                std::string istr = m[2].str().empty() ? m[3].str() : m[2].str();
                addr_out = static_cast<const karch_addr_t>(std::stoi(istr, nullptr, 16));
                return true;
            }

            if (std::regex_match(text, m, g_reg_op_bin))
            {
                addr_out = static_cast<const karch_addr_t>(std::stoi(m[2].str(), nullptr, 2));
                return true;
            }

            return false;
        }
    }
}
