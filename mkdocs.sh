#!/bin/bash

config="doxygen/doxygen.cfg"

if [[ $1 != "" ]]; then
    if [[ ! -f $1 ]]; then
        echo "Config file does not exist: $1"
        exit 1
    fi

    config=$1
fi

# Checks if a binary is available
function check_binary {
    _binary=$(which $1 2> /dev/null)

    if [[ ! -f $_binary ]]; then
        echo "$1 not found!"
        exit 1
    fi

    echo "Found $1: $_binary"
    return 0
}

# Go to the repository's root directory, so we can use
# relative paths

rootdir=$(git rev-parse --show-toplevel 2> /dev/null)

if [[ $rootdir != "" ]] && [[ -d $rootdir ]]; then
    cd $rootdir
else
    # Use the current working directory when the git command fails
    rootdir=$PWD

    echo "Could not get git root directory, maybe this isn't a git cloned version"
fi

# We need a custom doxygen version for verilog support
# this section only run when doxygen/doxygen-verilog
# folder does not exists.

if [[ ! -d doxygen/doxygen-verilog ]]; then
    # Check if we have git, cmake, gcc and make installed
    check_binary "git"
    check_binary "cmake"
    check_binary "make"
    check_binary "gcc"

    cd doxygen

    # Get doxygen-verilog
    git clone https://github.com/kissbeni/doxygen-verilog.git

    if [[ $? != 0 ]]; then
        echo "Git clone failed"
        exit 1
    fi

    # Build doxygen-verilog
    cd doxygen-verilog
    cmake .

    if [[ $? != 0 ]]; then
        echo "CMake failed"
        exit 1
    fi

    make -j$(nproc --all)

    if [[ $? != 0 ]]; then
        echo "Make clone failed"
        exit 1
    fi
fi

doxygen_verilog="$rootdir/doxygen/doxygen-verilog/bin/doxygen"

cd $rootdir

if [[ $1 == "clean" ]]; then
    rm -rfv doxygen/html 2> /dev/null
    rm -rfv doxygen/rtf  2> /dev/null
    rm -rfv doxygen/latex  2> /dev/null
fi

# Run doxygen
$doxygen_verilog $config

if [[ $? != 0 ]]; then
    echo "Doxygen failed"
    exit 1
fi
