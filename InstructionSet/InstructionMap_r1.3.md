# Instruction map (rev 1.3)
Addition of wide addresses

### Standard instructions

|    | x0                                   |  x1         | x2         | x3          | x4       | x5                                        | x6        | x7        | x8         | x9        | xA        | xB                                   | xC         | xD          | xE         | xF         |
|:--:|:------------------------------------:|:-----------:|:----------:|:-----------:|:--------:|:-----------------------------------------:|:---------:|:---------:|:----------:|:---------:|:---------:|:------------------------------------:|:----------:|:-----------:|:----------:|:----------:|
| 0x | NOP                                  | MOV RA, X   | MOV RA, RA | ADD RA, X   | INC RA   | OUT X, RA                                 | PUSH RA   | AND RA, X | AND RA, RA | SHR RA, X | OR RA, RA | PUSH X                               | XOR RA, RA | MUL RA, X   | MUL RA, RA | DIV RA, RA |
| 1x | RET                                  | MOV RB, X   | MOV RA, RB | ADD RB, X   | INC RB   | OUT X, RB                                 | PUSH RB   | AND RB, X | AND RA, RB | SHR RB, X | OR RA, RB | JMP $+X                              | XOR RA, RB | MUL RB, X   | MUL RA, RB | DIV RA, RB |
| 2x | <span style="color:red">PUSHA</span> | MOV RC, X   | MOV RA, RC | ADD RC, X   | INC RC   | OUT X, RC                                 | PUSH RC   | AND RC, X | AND RA, RC | SHR RC, X | OR RA, RC | JE $+X                               | XOR RA, RC | MUL RC, X   | MUL RA, RC | DIV RA, RC |
| 3x | <span style="color:red">POPA</span>  | MOV RD, X   | MOV RA, RD | ADD RD, X   | INC RD   | OUT X, RD                                 | PUSH RD   | AND RD, X | AND RA, RD | SHR RD, X | OR RA, RD | JNE $+X                              | XOR RA, RD | MUL RD, X   | MUL RA, RD | DIV RA, RD |
| 4x | *WADDR*                              | MOV RA, [X] | MOV RB, RA | SUB RA, X   | DEC RA   | IN RA, X                                  | POP RA    | OR RA, X  | AND RB, RA | ROL RA, X | OR RB, RA | JC $+X                               | XOR RB, RA | DIV RA, X   | MUL RB, RA | DIV RB, RA |
| 5x | SETC                                 | MOV RB, [X] | MOV RB, RB | SUB RB, X   | DEC RB   | IN RB, X                                  | POP RB    | OR RB, X  | AND RB, RB | ROL RB, X | OR RB, RB | JNC $+X                              | XOR RB, RB | DIV RB, X   | MUL RB, RB | DIV RB, RB |
| 6x | SETZ                                 | MOV RC, [X] | MOV RB, RC | SUB RC, X   | DEC RC   | IN RC, X                                  | POP RC    | OR RC, X  | AND RB, RC | ROL RC, X | OR RB, RC | JBE $+X                              | XOR RB, RC | DIV RC, X   | MUL RB, RC | DIV RB, RC |
| 7x | SETS                                 | MOV RD, [X] | MOV RB, RD | SUB RD, X   | DEC RD   | IN RD, X                                  | POP RD    | OR RD, X  | AND RB, RD | ROL RD, X | OR RB, RD | JA $+X                               | XOR RB, RD | DIV RD, X   | MUL RB, RD | DIV RB, RD |
| 8x | SETO                                 | MOV [RA], X | MOV RC, RA | ADD [RA], X | INC [RA] | OUT X, [RA]                               | PUSH [RA] | XOR RA, X | AND RC, RA | ROR RA, X | OR RC, RA | JS $+X                               | XOR RC, RA | MUL [RA], X | MUL RC, RA | DIV RC, RA |
| 9x | CFC                                  | MOV [RB], X | MOV RC, RB | ADD [RB], X | INC [RB] | OUT X, [RB]                               | PUSH [RB] | XOR RB, X | AND RC, RB | ROR RB, X | OR RC, RB | JNS $+X                              | XOR RC, RB | MUL [RB], X | MUL RC, RB | DIV RC, RB |
| ax | CFZ                                  | MOV [RC], X | MOV RC, RC | ADD [RC], X | INC [RC] | OUT X, [RC]                               | PUSH [RC] | XOR RC, X | AND RC, RC | ROR RC, X | OR RC, RC | <span style="color:red">BNK X</span> | XOR RC, RC | MUL [RC], X | MUL RC, RC | DIV RC, RC |
| bx | CFS                                  | MOV [RD], X | MOV RC, RD | ADD [RD], X | INC [RD] | OUT X, [RD]                               | PUSH [RD] | XOR RD, X | AND RC, RD | ROR RD, X | OR RC, RD | <span style="color:red">SYS X</span> | XOR RC, RD | MUL [RD], X | MUL RC, RD | DIV RC, RD |
| cx | CFO                                  | MOV [X], RA | MOV RD, RA | SUB [RA], X | DEC [RA] | <span style="color:red">IN [RA], X</span> | NOT RA    | SHL RA, X | AND RD, RA | CMP RA, X | OR RD, RA | JO $+X                               | XOR RD, RA | DIV [RA], X | MUL RD, RA | DIV RD, RA |
| dx | *EXTEND*                             | MOV [X], RB | MOV RD, RB | SUB [RB], X | DEC [RB] | <span style="color:red">IN [RB], X</span> | NOT RB    | SHL RB, X | AND RD, RB | CMP RB, X | OR RD, RB | JNO $+X                              | XOR RD, RB | DIV [RB], X | MUL RD, RB | DIV RD, RB |
| ex | <span style="color:red">RETI</span>  | MOV [X], RC | MOV RD, RC | SUB [RC], X | DEC [RC] | <span style="color:red">IN [RC], X</span> | NOT RC    | SHL RC, X | AND RD, RC | CMP RC, X | OR RD, RC | CALL $+X                             | XOR RD, RC | DIV [RC], X | MUL RD, RC | DIV RD, RC |
| fx | HLT                                  | MOV [X], RD | MOV RD, RD | SUB [RD], X | DEC [RD] | <span style="color:red">IN [RD], X</span> | NOT RD    | SHL RD, X | AND RD, RD | CMP RD, X | OR RD, RD | PUSH [X]                             | XOR RD, RD | DIV [RD], X | MUL RD, RD | DIV RD, RD |

### Extended instructions

|    | x0       |  x1                                  | x2           | x3 | x4         | x5 | x6         | x7 | x8         | x9 | xA     | xB | xC | xD | xE | xF |
|:--:|:---------|:------------------------------------:|:------------:|:--:|:----------:|:--:|:----------:|:--:|:----------:|:--:|:------:|:--:|:--:|:--:|:--:|:--:|
| 0x | IIC RA   | <span style=color:red>WAITS X</span> | MOV RA, [RA] |    | ADD RA, RA |    | SUB RA, RA |    | CMP RA, RA |    | CFLAGS |    |    |    |    |    |
| 1x | IIC RB   | <span style=color:red>WAITM X</span> | MOV RA, [RB] |    | ADD RA, RB |    | SUB RA, RB |    | CMP RA, RB |    |        |    |    |    |    |    |
| 2x | IIC RC   | <span style=color:red>WAITC X</span> | MOV RA, [RC] |    | ADD RA, RC |    | SUB RA, RC |    | CMP RA, RC |    |        |    |    |    |    |    |
| 3x | IIC RD   | <span style=color:red>BEEP X</span>  | MOV RA, [RD] |    | ADD RA, RD |    | SUB RA, RD |    | CMP RA, RD |    |        |    |    |    |    |    |
| 4x | DIC RA   |                                      | MOV RB, [RA] |    | ADD RB, RA |    | SUB RB, RA |    | CMP RB, RA |    |        |    |    |    |    |    |
| 5x | DIC RB   |                                      | MOV RB, [RB] |    | ADD RB, RB |    | SUB RB, RB |    | CMP RB, RB |    |        |    |    |    |    |    |
| 6x | DIC RC   |                                      | MOV RB, [RC] |    | ADD RB, RC |    | SUB RB, RC |    | CMP RB, RC |    |        |    |    |    |    |    |
| 7x | DIC RD   |                                      | MOV RB, [RD] |    | ADD RB, RD |    | SUB RB, RD |    | CMP RB, RD |    |        |    |    |    |    |    |
| 8x | IIC [RA] |                                      | MOV RC, [RA] |    | ADD RC, RA |    | SUB RC, RA |    | CMP RC, RA |    |        |    |    |    |    |    |
| 9x | IIC [RB] |                                      | MOV RC, [RB] |    | ADD RC, RB |    | SUB RC, RB |    | CMP RC, RB |    |        |    |    |    |    |    |
| ax | IIC [RC] |                                      | MOV RC, [RC] |    | ADD RC, RC |    | SUB RC, RC |    | CMP RC, RC |    |        |    |    |    |    |    |
| bx | IIC [RD] |                                      | MOV RC, [RD] |    | ADD RC, RD |    | SUB RC, RD |    | CMP RC, RD |    |        |    |    |    |    |    |
| cx | DIC [RA] |                                      | MOV RD, [RA] |    | ADD RD, RA |    | SUB RD, RA |    | CMP RD, RA |    |        |    |    |    |    |    |
| dx | DIC [RB] |                                      | MOV RD, [RB] |    | ADD RD, RB |    | SUB RD, RB |    | CMP RD, RB |    |        |    |    |    |    |    |
| ex | DIC [RC] |                                      | MOV RD, [RC] |    | ADD RD, RC |    | SUB RD, RC |    | CMP RD, RC |    |        |    |    |    |    |    |
| fx | DIC [RD] |                                      | MOV RD, [RD] |    | ADD RD, RD |    | SUB RD, RD |    | CMP RD, RD |    |        |    |    |    |    |    |
