# IO port map

| Port number | Port type | Name                                  | VM C++ class name    | Verilog sources                |
|-------------|-----------|---------------------------------------|----------------------|--------------------------------|
| 0x01        | W         | LEDs                                  | *Not implemented*    | `main.v`                       |
| 0x02        | R/W       | 7 segment display control flags       | `SevenSegmentDevice` | `main.v`, `ssdis.v`, `ssdec.v` |
| 0x03        | W         | Numeric data for the right 2 displays | `SevenSegmentDevice` | `main.v`, `ssdis.v`, `ssdec.v` |
| 0x04        | W         | Numeric data for the left 2 displays  | `SevenSegmentDevice` | `main.v`, `ssdis.v`, `ssdec.v` |
| 0x10        | R         | DIP switches                          | *Not implemented*    | `main.v`                       |
| 0x11        | R         | Push buttons                          | *Not implemented*    | `main.v`                       |
| 0x12        | R/W       | PS2 mouse/keyboard data               | *Not implemented*    | *Not implemented*              |
| 0x20        | W         | SPI flash address upper byte          | *Not implemented*    | *Not implemented*              |
| 0x21        | W         | SPI flash address lower byte          | *Not implemented*    | *Not implemented*              |
| 0x22        | R/W       | SPI flash data                        | *Not implemented*    | *Not implemented*              |
| 0xD0        | W         | VGA character display device          | `VGACharacterDevice` | *Not implemented*              |
