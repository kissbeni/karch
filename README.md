# karch
[![pipeline status](https://gitlab.com/kissbeni/karch/badges/master/pipeline.svg)](https://gitlab.com/kissbeni/karch/commits/master)

My architecture (and tools for it) I created for fun and to learn from it. I don't have much time working on this, so the codebase *is* a big mess at some places; and - mostly in verilog - I learn while I'm implementing stuff, so this adds to the mix a bit.

## Tools

### KASM

A simple, mostly regex based assembler for turning `.kasm` files into binary blobs.

Usage: `kasm path/to/your/file.kasm`

### KEMU

A powerful emulator for my architecture, with emulation of leds, seven segment displays, PS/2 keyboard and an 80 column text display.

Command line switches:

| Switch | Argument    | Description                                    |
|--------|-------------|------------------------------------------------|
| -v     | -           | Loggs more information about what's happening  |
| -l     | file[:addr] | Loads file to addr or to 0 by default          |
| -u     | -           | Disables GUI                                   |

## Documentation

A doxygen-generated HTML documentation is hosted at https://csumge.cc/docs-public/karch

## Plans

Softcore:

  - CLEANUP!!
  - improve memory controller
  - dram support
  - caching
  - pipeline
  - some kind of branch prediction

KEMU:

  - find something better than SDL
  - better PS/2 emulation
  - debugger

Others:

  - find a way to run the same test scenarios both as verilog simulation and gtest tests in KEMU
