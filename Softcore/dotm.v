`timescale 1ns / 1ps

module dotm(
    input  wire        clk,
    input  wire        en,
    output wire [ 3:0] dig_n,
    output reg  [ 6:0] seg_n,
    output wire [ 4:0] col_n,
    input  wire [34:0] data
);

reg [20:0] count;
wire cle = en & ~|count;
always @(posedge clk) begin
    if (en) begin
        count <= count + 1;
    end
end

reg [2:0] displ;

always @(*) begin
    case (displ)
        2'd0: seg_n <= data[ 6: 0];
        2'd1: seg_n <= data[13: 7];
        2'd2: seg_n <= data[20:14];
        2'd3: seg_n <= data[27:21];
        2'd4: seg_n <= data[34:28];
        default: seg_n <= 7'd0;
    endcase
end

always @(posedge clk) begin
    if (cle) begin
        if (displ == 4)
            displ <= 0;
        else
            displ <= displ + 1;
    end
end

assign col_n = ~(5'b1 << displ);
assign dig_n = 4'b1111;

endmodule
