`timescale 1ns / 1ps

module ssdec(
    input  wire [3:0] num,
    output reg  [7:0] seg,
    input  wire       dp
);

always @(*) begin
    case (num)
        4'd0:    seg <= {~dp,7'b1000000};
        4'd1:    seg <= {~dp,7'b1111001};
        4'd2:    seg <= {~dp,7'b0100100};
        4'd3:    seg <= {~dp,7'b0110000};
        4'd4:    seg <= {~dp,7'b0011001};
        4'd5:    seg <= {~dp,7'b0010010};
        4'd6:    seg <= {~dp,7'b0000010};
        4'd7:    seg <= {~dp,7'b1111000};
        4'd8:    seg <= {~dp,7'b0000000};
        4'd9:    seg <= {~dp,7'b0010000};
        4'd10:   seg <= {~dp,7'b0001000};
        4'd11:   seg <= {~dp,7'b0000011};
        4'd12:   seg <= {~dp,7'b1000110};
        4'd13:   seg <= {~dp,7'b0100001};
        4'd14:   seg <= {~dp,7'b0000110};
        4'd15:   seg <= {~dp,7'b0001110};
        default: seg <= {~dp,7'b1111111};
    endcase
end

endmodule
