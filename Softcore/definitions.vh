// Base addresses
parameter STACK_START = 16'hf000 - 1;
parameter START_ADDR  = 16'h0100;

// CPU States
parameter STATE_PREFETCH                =     0;
parameter STATE_FETCH                   =     1;
parameter STATE_DECODE                  =     2;
parameter STATE_PREFETCHARG             =     3;
parameter STATE_FETCHARG                =     4;
parameter STATE_FINISHWR                =     5;
parameter STATE_FINISHRD                =     6;
parameter STATE_INSTMEMREF_PREFETCH     =     7;
parameter STATE_INSTMEMREF_FETCH        =     8;
parameter STATE_AALU_OPFINISH           =     9;
parameter STATE_WAIT_ONE                =    10;
parameter STATE_HALTED                  =    11;

// Special instructions
parameter INST_NOP                      = 8'h00;
parameter INST_RET                      = 8'h10;
parameter INST_EXTEND                   = 8'hd0;
parameter INST_HLT                      = 8'hf0;
parameter INST_PUSHX                    = 8'h0b;
parameter INST_CALLX                    = 8'heb;
parameter INST_PUSHRX                   = 8'hfb;

// Register indexes
parameter RA                            =     0;
parameter RB                            =     1;
parameter RC                            =     2;
parameter RD                            =     3;
parameter RIP                           =     4;
parameter RSP                           =     5;

// Flag indexes
parameter FLG_C                         =     0;
parameter FLG_Z                         =     1;
parameter FLG_S                         =     2;
parameter FLG_O                         =     3;

// ALU operations
parameter ALUOP_NOP                     =     0;
parameter ALUOP_ADD                     =     1;
parameter ALUOP_SUB                     =     2;
parameter ALUOP_AND                     =     3;
parameter ALUOP_OR                      =     4;
parameter ALUOP_XOR                     =     5;
parameter ALUOP_SHL                     =     6;
parameter ALUOP_SHR                     =     7;
