`timescale 1ns / 1ps

module ALU(
    input  wire [7:0] paramA,
    input  wire [7:0] paramB,
    output reg  [7:0] paramO,
    input  wire [2:0] operation,
    output wire [3:0] flags
);

reg cf;

wire clear_of =
    (operation == ALUOP_AND) |
    (operation == ALUOP_OR)  |
    (operation == ALUOP_XOR) |
    (operation == ALUOP_SHL) |
    (operation == ALUOP_SHR);

wire clear_cf =
    (operation == ALUOP_AND) |
    (operation == ALUOP_OR)  |
    (operation == ALUOP_XOR);

assign flags[FLG_C] = clear_cf ? 0 : cf;
assign flags[FLG_Z] = ~|paramO;
assign flags[FLG_S] = paramO[7];
assign flags[FLG_O] = clear_of ? 0 : paramO[7] != paramA[7];

always @(*) begin
    case (operation)
        ALUOP_ADD: begin
            paramO <= paramA + paramB;
            cf <= (paramA + paramB) < paramA;
        end
        ALUOP_SUB: begin
            paramO <= paramA - paramB;
            cf <= (paramA - paramB) > paramA;
        end
        ALUOP_AND: begin
            paramO <= paramA & paramB;
            cf <= 0;
        end
        ALUOP_OR: begin
            paramO <= paramA | paramB;
            cf <= 0;
        end
        ALUOP_XOR: begin
            paramO <= paramA ^ paramB;
            cf <= 0;
        end
        ALUOP_SHL: begin
            paramO <= paramA << paramB;
            cf <= paramA[7];
        end
        ALUOP_SHR: begin
            paramO <= paramA >> paramB;
            cf <= paramA[0];
        end
        default: begin
            paramO <= 0;
            cf <= 0;
        end
    endcase
end

endmodule
