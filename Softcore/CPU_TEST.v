`timescale 1ns / 1ps

module CPU_TEST;

    // Inputs
    reg clk;
    reg rst;

    // Outputs
    wire hlt;
    wire [15:0] addr;
    wire iorq;
    wire we;
    wire re;
    wire portrq;

    // Bidirs
    wire [7:0] data;

    wire [7:0] ram_data;
    reg  [7:0] port_data;

    TEST_RAM ram(
        .cs(iorq),
        .oe(re),
        .we(we),
        .pt_en(portrq),
        .pt_data(port_data),
        .data(data),
        .addr(addr)
    );

    // Instantiate the Unit Under Test (UUT)
    CPU uut (
        .clk(clk),
        .rst(rst),
        .hlt(hlt),
        .data(data),
        .addr(addr),
        .iorq(iorq),
        .we(we),
        .re(re),
        .portrq(portrq)
    );

    always #31 clk = ~clk;

    initial begin
        // Initialize Inputs
        clk = 0;
        rst = 1;

        // Wait 100 ns for global reset to finish
        #100;

        // Add stimulus here
        rst = 0;
    end

    always @(negedge portrq) begin
        $display("Port %2x %c data=%2x", addr[7:0], we ? "O" : re ? "R" : "?", data);
    end
    always @(negedge clk) begin
        if (portrq & re) begin
            case (addr)
                8'h10:   port_data <= 'h40;
                8'h11:   port_data <= 'h77;
                default: port_data <= 'h69;
            endcase
        end
        else             port_data <= 'h00;
    end
    always @(posedge hlt) #30 $stop;

endmodule

