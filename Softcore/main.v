`timescale 1ns / 1ps

module main(
    input  wire        clk,
    input  wire        rst,

    input  wire        devrst,
    input  wire        devclk,
    input  wire        devmosi,

    output wire [15:0] sram_addr,
    inout  wire [ 7:0] sram_data,
    output wire        sram_oe_n,
    output wire        sram_we_n,
    output wire        sram_cs_n,

    input  wire [7:0]  sw,
    output wire [7:0]  ld,
    output wire [7:0]  seg_n,
    output wire [3:0]  dig_n,
    output wire [4:0]  col_n,
    input  wire [3:0]  bt
);

assign sram_cs_n = 0;

wire        cpurst;
reg  [ 7:0] indata;
reg  [15:0] prog_addr;
reg  [ 7:0] ldport;

reg  [ 2:0] usrt_clk_samples;
wire        usrt_clk_falling = (usrt_clk_samples[2:1] == 2'b10);

reg  [ 1:0] rxd_samples;
reg  [ 9:0] rx_shr;
wire        rx_shr_rst = ~cpurst | ~rx_shr[0];
wire        rx_valid   = ~rx_shr[0] & rx_shr[9];
reg  [ 2:0] rx_valid_samples;
wire        rx_valid_rise = (rx_valid_samples[2:1] == 2'b01);

always @(posedge clk) begin
    usrt_clk_samples <= {usrt_clk_samples[1:0], devclk};
end

always @(posedge clk) begin
    rxd_samples <= {rxd_samples[0], devmosi};
end

always @(posedge clk) begin
    if (rx_shr_rst)
        rx_shr <= 10'b11_1111_1111;
    else if (usrt_clk_falling)
        rx_shr <= {rxd_samples[1], rx_shr[9:1]};
end

always @(posedge clk) begin
    if (rx_valid)
        indata <= rx_shr[8:1];
end

always @(posedge clk) begin
    rx_valid_samples <= {rx_valid_samples[1:0], rx_valid};
end

always @(posedge clk) begin
    if (~cpurst)
        prog_addr <= -1;

    if (rx_valid_rise)
        prog_addr <= prog_addr + 1;
end

reg  [ 7:0] ssdis_flags;
reg  [15:0] ssdis_data;
reg  [ 3:0] btdata;

reg  [ 7:0] data_out;

wire [ 7:0] data;
wire [15:0] addr;
wire        iorq;
wire        portrq;
wire        we;
wire        re;
wire        hlt;

// assign ld = hlt ? 8'hf0 : cpurst ? {devclk,~sram_oe_n,~sram_we_n,~sram_cs_n,devmosi} : ldport;
assign ld           = cpurst ? {devclk,~sram_oe_n,~sram_we_n,~sram_cs_n,devmosi} : ldport;

assign sram_oe_n    = ~(~rst & iorq & re);
assign cpurst       = rst | devrst;
assign data         = re ? portrq ? data_out : iorq ? sram_data : 'bz : 'bz;
assign sram_data    = rst ? indata : (we & iorq) ? data : 'bz;
assign sram_addr    = rst ? prog_addr : (iorq) ? addr : 'bz;
assign sram_we_n    = ~((rst & rx_valid) | (~rst & we & iorq));

CPU cpu(clk, rst, hlt, data, addr, iorq, we, re, portrq);

initial begin
    ldport <= 0;
    prog_addr <= -1;
end

always @(negedge clk) begin
    if (cpurst) begin
        ssdis_flags <= 8'hf0;
        ssdis_data <= prog_addr;
    end else if (portrq) begin
        case (addr[7:0])
            8'h00:;
            8'h01: if (we) ldport <= data;
            8'h02: begin
                if (we)
                    ssdis_flags <= data;
                else if (re)
                    data_out <= ssdis_flags;
            end
            8'h03: if (we) ssdis_data[15:8] <= data;
            8'h04: if (we) ssdis_data[ 7:0] <= data;
            8'h10: if (re) data_out <= sw;
            8'h11: if (re) data_out <= {4'b0,btdata};
        endcase
    end
end

reg         cle;
reg         displsel;
reg  [14:0] count;
always @(posedge clk) begin
    count <= count + 1;
    cle <= ~|count;
    displsel <= ~displsel;
end

always @(posedge clk) begin
    if (cle) btdata <= bt;
end

wire [ 7:0] seg_7, seg_mx;
wire [ 3:0] dig_7, dig_mx;
wire [ 4:0] col_7, col_mx;

ssdis sd(
    clk,
    displsel,
    dig_7,
    seg_7,
    col_7,
    ssdis_flags[3:0],
    ssdis_flags[7:4],
    ssdis_data
);

dotm md(
    clk,
    ~displsel,
    dig_mx,
    seg_mx,
    col_mx,
    35'h272910234
);

assign seg_n = displsel ? seg_7 : seg_mx;
assign dig_n = displsel ? dig_7 : dig_mx;
assign col_n = displsel ? col_7 : col_mx;


endmodule
