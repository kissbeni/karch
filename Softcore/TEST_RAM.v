`timescale 1ns / 1ps

module TEST_RAM(
    input wire        cs,
    input wire        oe,
    input wire        we,
    input wire        pt_en,
    input wire [ 7:0] pt_data,
    inout wire [ 7:0] data,
    input wire [15:0] addr
);

reg [7:0] mem [65534:0];
reg [7:0] outreg;

assign data = oe ? pt_en ? pt_data : cs ? outreg : 'bz : 'bz;

integer i;
initial begin
    // for (i = 23; i < 256; i = i + 1) begin
    //    mem[i] = 8'h55;
    // end
    $readmemh("prg.txt", mem, 0, 511);
end

always @(*) begin
    outreg <= mem[addr];
    if (cs & we) mem[addr] <= data;
end

endmodule
