`timescale 1ns / 1ps

module REG #(parameter BUS_WIDTH = 8, INCREMENT_SIZE = 1)(
    input  wire                 clk,
    input  wire                 rst,
    input  wire [BUS_WIDTH-1:0] p,
    output reg  [BUS_WIDTH-1:0] q,
    input  wire                 load,
    input  wire                 inc,
    input  wire                 dec
);

always @(posedge clk) begin
    if (rst) q <= 0;
    else if (load) q <= p;
    else if (inc)  q <= q + INCREMENT_SIZE;
    else if (dec)  q <= q - INCREMENT_SIZE;
end

endmodule
