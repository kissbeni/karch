`timescale 1ns / 1ps

module CPU(
    input  wire        clk,
    input  wire        rst,
    output wire        hlt,
    inout  wire [ 7:0] data,
    output reg  [15:0] addr,
    output reg         iorq,
    output reg         we,
    output reg         re,
    output reg         portrq
);

`include "definitions.vh"

reg  [ 4:0] state;
reg  [ 4:0] prvstate;
reg  [ 4:0] inst_nextstate;
reg         init;

assign hlt = state == STATE_HALTED;

reg        [7:0] fetched_inst;
reg signed [7:0] fetched_arg;
reg        [7:0] fetched_mem;

reg         inc_ip;
reg         inc_sp;
reg         dec_sp;
reg         arg_ok;
reg  [ 3:0] reg_load;
reg         ip_load;
reg         sp_load;

reg  [ 3:0] flags;

wire [31:0] creg_data;
reg  [ 7:0] reg_load_data;
reg  [15:0] ip_load_data;
reg  [15:0] sp_load_data;
reg  [ 7:0] regval;
reg  [ 7:0] data_out;
wire [ 7:0] data_in;
reg  [ 7:0] reg_tmp;
reg  [ 7:0] ror_n;
reg  [ 7:0] rol_n;
reg  [ 7:0] aaluarg;
reg  [ 7:0] aaluw;
reg  [ 7:0] aaluo;
reg         aaludiv;
reg         aalumul;
reg         extend;
wire [15:0] r_ip_val;
wire [15:0] r_sp_val;
reg         stackwrw;

wire signed [15:0] s_r_ip_val = r_ip_val;
wire        [15:0] reljump_newip = s_r_ip_val + fetched_arg - 2;

wire outen = (we & (iorq | portrq));
assign data = outen ? data_out : 8'bz;

REG#(16,1) r_ip(.clk(clk), .rst(rst), .p(ip_load_data),  .q(r_ip_val), .load(ip_load), .inc(inc_ip), .dec(1'b0));
REG#(16,1) r_sp(.clk(clk), .rst(rst), .p(sp_load_data), .q(r_sp_val), .load(sp_load), .inc(inc_sp), .dec(dec_sp));

REG#(8) regs[3:0](.clk(clk), .rst(rst), .p(reg_load_data), .load(reg_load), .q(creg_data), .dec(4'b0), .inc(4'b0));

wire [1:0] inst_top2 = fetched_inst[7:6];
wire [1:0] inst_mid2 = fetched_inst[5:4];
wire [3:0] inst_base = fetched_inst[3:0];
wire [3:0] inst_parm = fetched_inst[7:4];

// should we read at the stack pointer?
wire rdspref = (inst_base == 6 && inst_top2 == 1) || fetched_inst == INST_RET;

// should we read from register-referenced address?
wire rdregref = ((inst_base == 3 || inst_base == 4 || inst_base == 13) && (inst_top2 > 1)) ||
                    ((inst_base == 5 || inst_base == 6) && inst_top2 == 2) || rdspref;

// same for extended instructions
wire rdregrefex = ((inst_base == 0 && inst_top2 > 1) || (inst_base == 2));

// should we read from argument-referenced address?
wire rdargref = ((inst_base == 1) && (inst_top2 & 1)) || fetched_inst == INST_PUSHRX;

// should we write to register-referenced address?
wire wrregref = ((inst_base == 1 || inst_base == 3 || inst_base == 4 || inst_base == 13)
                    && (inst_top2 > 1)) || (inst_base == 5 && inst_top2 == 3);

// same for extended instructions
wire wrregrefex = ((inst_base == 0 && inst_top2 > 1));

// should we increment the sp register?
wire incsp = (inst_base == 6 && (inst_top2 == 0 || inst_top2 == 2)) || fetched_inst == INST_PUSHX || fetched_inst == INST_CALLX;

// should we write to the stack?
wire wrstack = (inst_base == 6 && inst_top2 != 3) || fetched_inst == INST_PUSHX || fetched_inst == INST_PUSHRX || fetched_inst == INST_CALLX;

// the cpu waits for an operation to finish
wire _wait = (inc_sp | dec_sp);

reg  [7:0] alu_param_a;
reg  [7:0] alu_param_b;
reg  [2:0] alu_operation;
wire [3:0] alu_flags;
wire [7:0] alu_output;

ALU alu(alu_param_a, alu_param_b, alu_output, alu_operation, alu_flags);

always @(negedge clk) begin
    if (extend) begin
        case (inst_base)
            4'h0: begin
                alu_param_b <= flags[FLG_C];

                if (inst_top2 == 0) begin           // IIC R?
                    alu_param_a <= regval;
                    alu_operation <= ALUOP_ADD;
                end else if (inst_top2 == 1) begin  // DIC R?
                    alu_param_a <= regval;
                    alu_operation <= ALUOP_SUB;
                end else if (inst_top2 == 2) begin  // IIC [R?]
                    alu_param_a <= fetched_mem;
                    alu_operation <= ALUOP_ADD;
                end else if (inst_top2 == 3) begin  // DIC [R?]
                    alu_param_a <= fetched_mem;
                    alu_operation <= ALUOP_SUB;
                end
            end
            4'h4: begin
                alu_param_b <= regval;
                alu_param_a <= reg_tmp;
                alu_operation <= ALUOP_ADD;
            end
            4'h6: begin
                alu_param_b <= regval;
                alu_param_a <= reg_tmp;
                alu_operation <= ALUOP_SUB;
            end
        endcase
    end else begin
        case (inst_base)
            4'h3: begin
                alu_param_b <= fetched_arg;

                if (inst_top2 == 0) begin          // ADD R?, X
                    alu_param_a <= regval;
                    alu_operation <= ALUOP_ADD;
                end else if (inst_top2 == 1) begin  // SUB R?, X
                    alu_param_a <= regval;
                    alu_operation <= ALUOP_SUB;
                end else if (inst_top2 == 2) begin  // ADD [R?], X
                    alu_param_a <= fetched_mem;
                    alu_operation <= ALUOP_ADD;
                end else if (inst_top2 == 3) begin  // SUB [R?], X
                    alu_param_a <= fetched_mem;
                    alu_operation <= ALUOP_SUB;
                end
            end
            4'h4: begin
                alu_param_b <= 1;

                if (inst_top2 == 0) begin           // INC R?
                    alu_param_a <= regval;
                    alu_operation <= ALUOP_ADD;
                end else if (inst_top2 == 1) begin  // DEC R?
                    alu_param_a <= regval;
                    alu_operation <= ALUOP_SUB;
                end else if (inst_top2 == 2) begin  // INC [R?]
                    alu_param_a <= fetched_mem;
                    alu_operation <= ALUOP_ADD;
                end else if (inst_top2 == 3) begin  // DEC [R?]
                    alu_param_a <= fetched_mem;
                    alu_operation <= ALUOP_SUB;
                end
            end
            4'h7: begin
                alu_param_a <= regval;
                alu_param_b <= fetched_arg;

                if (inst_top2 == 0) begin           // AND R?, X
                    alu_operation <= ALUOP_AND;
                end else if (inst_top2 == 1) begin  // OR R?, X
                    alu_operation <= ALUOP_OR;
                end else if (inst_top2 == 2) begin  // XOR R?, X
                    alu_operation <= ALUOP_XOR;
                end else if (inst_top2 == 3) begin  // SHL R?, X
                    alu_operation <= ALUOP_SHL;
                end
            end
            4'h8: begin                             // ------ COLUMN 8 ------
                alu_param_a <= regval;
                alu_param_b <= reg_tmp;
                alu_operation <= ALUOP_AND;
            end
            4'h9: begin
                alu_param_a <= regval;
                alu_param_b <= fetched_arg;

                if (inst_top2 == 0) begin           // SHR R?, X
                    alu_operation <= ALUOP_SHR;
                end else if (inst_top2 == 1) begin
                end else if (inst_top2 == 2) begin
                end else if (inst_top2 == 3) begin
                end
            end
            4'ha: begin                             // ------ COLUMN A ------
                alu_param_a <= regval;
                alu_param_b <= reg_tmp;
                alu_operation <= ALUOP_OR;
            end
            4'hc: begin                             // ------ COLUMN C ------
                alu_param_a <= regval;
                alu_param_b <= reg_tmp;
                alu_operation <= ALUOP_XOR;
            end
        endcase
    end
end

always @(*) begin
    case (inst_top2)
         0: reg_tmp <= creg_data[ 7: 0];
         1: reg_tmp <= creg_data[15: 8];
         2: reg_tmp <= creg_data[23:16];
         3: reg_tmp <= creg_data[31:24];
    endcase

    case (inst_mid2)
         0: regval <= creg_data[ 7: 0];
         1: regval <= creg_data[15: 8];
         2: regval <= creg_data[23:16];
         3: regval <= creg_data[31:24];
    endcase
end

always @(*) begin
    if (fetched_inst == INST_HLT)
        inst_nextstate <= STATE_HALTED;
    else if (extend) begin
        if (wrregrefex)
            inst_nextstate <= STATE_FINISHWR;
        else
            inst_nextstate <= STATE_PREFETCH;
    end else begin
        if ((inst_base == 5 && (inst_top2 == 0 || inst_top2 == 2)) || wrregref || wrstack)
            inst_nextstate <= STATE_FINISHWR;
        else if (inst_base == 5 && (inst_top2 == 1 || inst_top2 == 3))
            inst_nextstate <= STATE_FINISHRD;
        else
            inst_nextstate <= STATE_PREFETCH;
    end
end

always @(posedge clk) begin : CPU_Exec
    if (inc_sp || dec_sp) begin
        inc_sp <= 0;
        dec_sp <= 0;
    end

    if (rst) begin
        init <= 0;
        state <= STATE_PREFETCH;
        inc_ip <= 0;
        flags <= 0;
        reg_load_data <= 0;
        reg_load <= 0;
        ror_n <= 0;
        addr <= 0;
        rol_n <= 0;
        inc_sp <= 0;
        dec_sp <= 0;
        portrq <= 0;
        iorq <= 0;
        extend <= 0;
        aaluarg <= 0;
        data_out <= 0;
        stackwrw <= 0;
        aalumul <= 0;
    end else if (!init) begin
        reg_load_data <= 0;
        reg_load <= 6'h3F;
        ip_load <= 1;
        sp_load <= 1;
        ip_load_data <= START_ADDR;
        sp_load_data <= STACK_START;
        init <= 1;
    end else if (|reg_load | ip_load | sp_load) begin
        reg_load <= 0;
        ip_load <= 0;
        sp_load <= 0;
    end else if (ror_n) begin
        reg_load_data <= {regval[0], regval[7:1]};
        reg_load[inst_mid2] <= 1;
        ror_n <= ror_n - 1;
    end else if (rol_n) begin
        reg_load_data <= {regval[6:0], regval[7]};
        reg_load[inst_mid2] <= 1;
        rol_n <= rol_n - 1;
    end else if (aaludiv) begin
        if (aaluarg > aaluw) begin
            aaluarg <= 0;
            aaludiv <= 0;
            state <= STATE_AALU_OPFINISH;
        end else begin
            aaluo <= aaluo + 1;
            aaluw <= aaluw - aaluarg;
        end
    end else if (aalumul) begin
        if (aaluw == 0) begin
            aalumul <= 0;
            state <= STATE_AALU_OPFINISH;
        end else begin
            aaluo <= aaluo + aaluarg;
            aaluw <= aaluw - 1;
        end
    end else if (!_wait) begin
        case (state)
            STATE_PREFETCH: begin
                $display("Addr: 0x%4x", r_ip_val);
                addr <= r_ip_val;
                inc_sp <= 0;
                dec_sp <= 0;

                if (prvstate != STATE_FETCH)
                    extend <= 0;

                inc_ip <= 1;
                we <= 0;
                re <= 1;
                iorq <= 1;
                state <= STATE_FETCH;
            end
            STATE_FETCH: begin
                fetched_inst <= data;
                iorq <= 0;
                inc_ip <= 0;
                arg_ok <= 0;

                if (data == INST_EXTEND & ~extend) begin
                    extend <= 1;
                    state <= STATE_PREFETCH;
                end else begin
                    if (data == INST_RET)
                        stackwrw <= 1;

                    state <= STATE_INSTMEMREF_PREFETCH;
                end
            end
            STATE_DECODE: begin
                $display("Decoding instruction: %2x", fetched_inst);
                if (!arg_ok && (fetched_inst & 1) && inst_base != 4'hf) begin
                    $display("Fetching arg");
                    state <= STATE_PREFETCHARG;
                end else begin
                    if (extend) begin
                        $display("Executing extended instruction: %2x", fetched_inst);
                        case (inst_base)
                            4'h0: begin
                                if (inst_top2 == 0) begin           // IIC R?
                                    reg_load_data <= alu_output;
                                    flags <= alu_flags;
                                    reg_load[inst_mid2] <= 1;
                                end else if (inst_top2 == 1) begin  // DIC R?
                                    reg_load_data <= alu_output;
                                    flags <= alu_flags;
                                    reg_load[inst_mid2] <= 1;
                                end else if (inst_top2 == 2) begin  // IIC [R?]
                                    iorq <= 1;
                                    we <= 1;
                                    re <= 0;
                                    addr <= regval;
                                    data_out <= alu_output;
                                    flags <= alu_flags;
                                end else if (inst_top2 == 3) begin  // DIC [R?]
                                    iorq <= 1;
                                    we <= 1;
                                    re <= 0;
                                    addr <= regval;
                                    data_out <= alu_output;
                                    flags <= alu_flags;
                                end
                            end
                            4'h2: begin
                                reg_load_data <= fetched_mem;
                                reg_load[inst_top2] <= 1;
                            end
                            4'h4: begin
                                reg_load_data <= alu_output;
                                flags <= alu_flags;
                                reg_load[inst_top2] <= 1;
                            end
                            4'h6: begin
                                reg_load_data <= alu_output;
                                flags <= alu_flags;
                                reg_load[inst_top2] <= 1;
                            end
                        endcase
                    end else begin
                        case (inst_base)
                            4'h0: begin                             // ------ COLUMN 0 ------
                                case (inst_parm)
                                    4'h0:;                          // NOP
                                    4'h1: begin                     // RET
                                        ip_load_data <= {8'h01, fetched_mem};
                                        $display(" < Returning to %4x", {8'h01, fetched_mem});
                                        ip_load <= 1;
                                        dec_sp <= 1;
                                    end
                                    4'h4: flags        <= 0;        // CFLAGS
                                    4'h5: flags[FLG_C] <= 1;        // SETC
                                    4'h6: flags[FLG_Z] <= 1;        // SETZ
                                    4'h7: flags[FLG_S] <= 1;        // SETS
                                    4'h8: flags[FLG_O] <= 1;        // SETO
                                    4'h9: flags[FLG_C] <= 0;        // CFC
                                    4'ha: flags[FLG_Z] <= 0;        // CFZ
                                    4'hb: flags[FLG_S] <= 0;        // CFS
                                    4'hc: flags[FLG_O] <= 0;        // CFO
                                    4'hf: state <= STATE_HALTED;    // HLT
                                endcase
                            end
                            4'h1: begin                             // ------ COLUMN 1 ------
                                if (inst_top2 == 0) begin           // MOV R?, X
                                    reg_load_data <= fetched_arg;
                                    reg_load[inst_mid2] <= 1;
                                end else if (inst_top2 == 1) begin  // MOV R?, [X]
                                    reg_load_data <= fetched_mem;
                                    reg_load[inst_mid2] <= 1;
                                end else if (inst_top2 == 2) begin  // MOV [R?], X
                                    iorq <= 1;
                                    we <= 1;
                                    re <= 0;
                                    data_out <= fetched_arg;
                                    addr <= regval;
                                end else if (inst_top2 == 3) begin  // MOV [X], R?
                                    iorq <= 1;
                                    we <= 1;
                                    re <= 0;
                                    data_out <= regval;
                                    addr <= fetched_arg;
                                end
                            end
                            4'h2: begin                             // ------ COLUMN 2 ------
                                reg_load_data <= regval;            // MOV R?, R?
                                reg_load[inst_top2] <= 1;
                            end
                            4'h3: begin                             // ------ COLUMN 3 ------
                                if (inst_top2 == 0) begin          // ADD R?, X
                                    reg_load_data <= alu_output;
                                    flags <= alu_flags;
                                    reg_load[inst_mid2] <= 1;
                                end else if (inst_top2 == 1) begin  // SUB R?, X
                                    reg_load_data <= alu_output;
                                    flags <= alu_flags;
                                    reg_load[inst_mid2] <= 1;
                                end else if (inst_top2 == 2) begin  // ADD [R?], X
                                    iorq <= 1;
                                    we <= 1;
                                    re <= 0;
                                    addr <= regval;
                                    data_out <= alu_output;
                                    flags <= alu_flags;
                                end else if (inst_top2 == 3) begin  // SUB [R?], X
                                    iorq <= 1;
                                    we <= 1;
                                    re <= 0;
                                    addr <= regval;
                                    data_out <= alu_output;
                                    flags <= alu_flags;
                                end
                            end
                            4'h4: begin                             // ------ COLUMN 4 ------
                                if (inst_top2 == 0) begin           // INC R?
                                    reg_load_data <= alu_output;
                                    flags <= alu_flags;
                                    reg_load[inst_mid2] <= 1;
                                end else if (inst_top2 == 1) begin  // DEC R?
                                    reg_load_data <= alu_output;
                                    flags <= alu_flags;
                                    reg_load[inst_mid2] <= 1;
                                end else if (inst_top2 == 2) begin  // INC [R?]
                                    iorq <= 1;
                                    we <= 1;
                                    re <= 0;
                                    addr <= regval;
                                    data_out <= alu_output;
                                    flags <= alu_flags;
                                end else if (inst_top2 == 3) begin  // DEC [R?]
                                    iorq <= 1;
                                    we <= 1;
                                    re <= 0;
                                    addr <= regval;
                                    data_out <= alu_output;
                                    flags <= alu_flags;
                                end
                            end
                            4'h5: begin                             // ------ COLUMN 5 ------
                                if (inst_top2 == 0) begin           // OUT X, R?
                                    portrq <= 1;
                                    we <= 1;
                                    re <= 0;
                                    data_out <= regval;
                                    addr <= fetched_arg;
                                end else if (inst_top2 == 1) begin  // IN R?, X
                                    portrq <= 1;
                                    we <= 0;
                                    re <= 1;
                                    addr <= fetched_arg;
                                end else if (inst_top2 == 2) begin  // OUT X, [R?]
                                    portrq <= 1;
                                    we <= 1;
                                    re <= 0;
                                    data_out <= fetched_mem;
                                    addr <= fetched_arg;
                                end else if (inst_top2 == 3) begin  // IN [R?], X

                                end
                            end
                            4'h6: begin                             // ------ COLUMN 6 ------
                                if (inst_top2 == 0) begin           // PUSH R?
                                    data_out <= regval;
                                    iorq <= 1;
                                    we <= 1;
                                    re <= 0;
                                    addr <= r_sp_val;
                                end else if (inst_top2 == 1) begin  // POP R?
                                    dec_sp <= 1;
                                    reg_load_data <= fetched_mem;
                                    reg_load[inst_mid2] <= 1;
                                end else if (inst_top2 == 2) begin  // PUSH [R?]
                                    data_out <= fetched_mem;
                                    iorq <= 1;
                                    we <= 1;
                                    re <= 0;
                                    addr <= r_sp_val;
                                end else if (inst_top2 == 3) begin  // NOT R?
                                    reg_load_data <= ~regval;
                                    reg_load[inst_mid2] <= 1;
                                end
                            end
                            4'h7: begin                             // ------ COLUMN 7 ------
                                reg_load_data <= alu_output;
                                flags <= alu_flags;
                                reg_load[inst_mid2] <= 1;
                            end
                            4'h8: begin                             // ------ COLUMN 8 ------
                                reg_load_data <= alu_output;        // AND R?, R?
                                flags <= alu_flags;
                                reg_load[inst_top2] <= 1;
                            end
                            4'h9: begin                             // ------ COLUMN 9 ------
                                if (inst_top2 == 0) begin           // SHR R?, X
                                    reg_load_data <= alu_output;
                                    flags <= alu_flags;
                                    reg_load[inst_mid2] <= 1;
                                end else if (inst_top2 == 1) begin  // ROL R?, X
                                    rol_n <= fetched_arg;
                                end else if (inst_top2 == 2) begin  // ROR R?, X
                                    ror_n <= fetched_arg;
                                end else if (inst_top2 == 3) begin  // CMP R?, R?
                                    if (regval == fetched_arg) begin
                                        flags[FLG_Z] <= 1;
                                        flags[FLG_C] <= 0;
                                    end else if (regval < fetched_arg) begin
                                        flags[FLG_Z] <= 0;
                                        flags[FLG_C] <= 1;
                                    end else if (regval > fetched_arg) begin
                                        flags[FLG_Z] <= 0;
                                        flags[FLG_C] <= 0;
                                    end
                                end
                            end
                            4'ha: begin                             // ------ COLUMN A ------
                                reg_load_data <= alu_output;        // OR R?, R?
                                flags <= alu_flags;
                                reg_load[inst_top2] <= 1;
                            end
                            4'hb: begin                             // ------ COLUMN B ------
                                case (inst_parm)
                                    4'h0: begin                     // PUSH X
                                        data_out <= fetched_arg;
                                        iorq <= 1;
                                        we <= 1;
                                        re <= 0;
                                        addr <= r_sp_val;
                                    end
                                    4'h1: begin                     // JMP $+X
                                        ip_load_data <= reljump_newip;
                                        ip_load <= 1;
                                    end
                                    4'h2: begin                     // JE $+X (~JZ)
                                        if (flags[FLG_Z]) begin
                                            ip_load_data <= reljump_newip;
                                            ip_load <= 1;
                                        end
                                    end
                                    4'h3: begin                     // JNE $+X (~JNZ)
                                        if (!flags[FLG_Z]) begin
                                            ip_load_data <= reljump_newip;
                                            ip_load <= 1;
                                        end
                                    end
                                    4'h4: begin                     // JC $+X (~JB,~JNAE)
                                        if (flags[FLG_C]) begin
                                            ip_load_data <= reljump_newip;
                                            ip_load <= 1;
                                        end
                                    end
                                    4'h5: begin                     // JNC $+X (~JAE,~JNB)
                                        if (!flags[FLG_C]) begin
                                            ip_load_data <= reljump_newip;
                                            ip_load <= 1;
                                        end
                                    end
                                    4'h6: begin                     // JBE $+X (~JNA)
                                        if (flags[FLG_C] | flags[FLG_Z]) begin
                                            ip_load_data <= reljump_newip;
                                            ip_load <= 1;
                                        end
                                    end
                                    4'h7: begin                     // JA $+X (~JNBE)
                                        if (!flags[FLG_C] & !flags[FLG_Z]) begin
                                            ip_load_data <= reljump_newip;
                                            ip_load <= 1;
                                        end
                                    end
                                    4'h8: begin                     // JS $+X
                                        if (flags[FLG_S]) begin
                                            ip_load_data <= reljump_newip;
                                            ip_load <= 1;
                                        end
                                    end
                                    4'h9: begin                     // JNS $+X
                                        if (!flags[FLG_S]) begin
                                            ip_load_data <= reljump_newip;
                                            ip_load <= 1;
                                        end
                                    end
                                    4'hc: begin                     // JO $+X
                                        if (flags[FLG_O]) begin
                                            ip_load_data <= reljump_newip;
                                            ip_load <= 1;
                                        end
                                    end
                                    4'hd: begin                     // JNO $+X
                                        if (!flags[FLG_O]) begin
                                            ip_load_data <= reljump_newip;
                                            ip_load <= 1;
                                        end
                                    end
                                    4'he: begin                     // CALL $+X
                                        data_out <= r_ip_val[7:0];
                                        iorq <= 1;
                                        we <= 1;
                                        re <= 0;
                                        inc_sp <= 1;
                                        addr <= r_sp_val;
                                        stackwrw <= 1;
                                        $display(" > Called %4x", reljump_newip);
                                        ip_load_data <= reljump_newip;
                                        ip_load <= 1;
                                    end
                                    4'hf: begin                     // PUSH [X]
                                        data_out <= fetched_mem;
                                        iorq <= 1;
                                        we <= 1;
                                        re <= 0;
                                        addr <= r_sp_val;
                                    end
                                endcase
                            end
                            4'hc: begin                             // ------ COLUMN C ------
                                reg_load_data <= alu_output;        // XOR R?, R?
                                flags <= alu_flags;
                                reg_load[inst_top2] <= 1;
                            end
                            4'hd: begin                             // ------ COLUMN D ------
                                if (inst_top2 == 0) begin           // MUL R?, X
                                    aalumul <= 1;
                                    aaluo <= 0;
                                    aaluw <= fetched_arg;
                                    aaluarg <= regval;
                                end else if (inst_top2 == 1) begin  // DIV R?, X
                                    if (fetched_arg == 0)
                                        flags[FLG_O] <= 1;
                                    else begin
                                        aaluarg <= fetched_arg;
                                        aaluw <= regval;
                                        aaluo <= 0;
                                        aaludiv <= 1;
                                    end
                                end else if (inst_top2 == 2) begin  // MUL [R?], X
                                    iorq <= 1;
                                    we <= 1;
                                    re <= 0;
                                    addr <= regval;
                                    data_out <= fetched_mem * fetched_arg;
                                    if ((fetched_mem * fetched_arg) < fetched_mem)
                                        flags[FLG_O] <= 1;
                                end else if (inst_top2 == 3) begin  // DIV [R?], X
                                    if (fetched_arg == 0)
                                        flags[FLG_O] <= 1;
                                    else begin
                                        aaluarg <= fetched_arg;
                                        aaluw <= fetched_mem;
                                        aaluo <= 0;
                                        aaludiv <= 1;
                                    end
                                end
                            end
                            4'he: begin                             // ------ COLUMN E ------
                                reg_load_data <= regval * reg_tmp;  // MUL R?, R?
                                if ((regval * reg_tmp) < regval)
                                    flags[FLG_O] <= 1;
                                reg_load[inst_top2] <= 1;
                            end
                            4'hf: begin                             // ------ COLUMN F ------
                                if (regval == 0)                    // DIV R?, R?
                                    flags[FLG_O] <= 1;
                                else begin
                                    aaluarg <= regval;
                                    aaluw <= reg_tmp;
                                    aaluo <= 0;
                                    aaludiv <= 1;
                                end
                            end
                        endcase
                    end

                    state <= inst_nextstate;
                end
            end
            STATE_PREFETCHARG: begin
                addr <= r_ip_val;
                inc_ip <= 1;
                we <= 0;
                re <= 1;
                iorq <= 1;
                state <= STATE_FETCHARG;
            end
            STATE_FETCHARG: begin
                fetched_arg <= data;
                inc_ip <= 0;
                iorq <= 0;
                arg_ok <= 1;

                if (rdargref)
                    state <= STATE_INSTMEMREF_PREFETCH;
                else
                    state <= STATE_DECODE;
            end
            STATE_FINISHWR: begin
                portrq <= 0;
                iorq <= 0;
                we <= 0;
                re <= 0;

                if (fetched_inst == INST_CALLX && stackwrw) begin
                    data_out <= r_ip_val[15:8];
                    iorq <= 1;
                    we <= 1;
                    re <= 0;
                    stackwrw <= 0;
                    addr <= r_sp_val;
                end

                state <= STATE_WAIT_ONE;
            end
            STATE_FINISHRD: begin
                reg_load_data <= data;
                reg_load[inst_mid2] <= 1;
                portrq <= 0;
                iorq <= 0;
                state <= STATE_PREFETCH;
            end
            STATE_INSTMEMREF_PREFETCH: begin
                if (incsp) inc_sp <= 1;

                if (prvstate == STATE_FETCH && (~extend & rdregref | extend & rdregrefex)) begin
                    if (rdspref)
                        addr <= r_sp_val;
                    else
                        addr <= regval;

                    we <= 0;
                    re <= 1;
                    iorq <= 1;
                    if (~extend && fetched_inst == INST_RET && stackwrw) begin
                        dec_sp <= 1;
                    end

                    state <= STATE_INSTMEMREF_FETCH;
                end else if (prvstate == STATE_FETCHARG) begin
                    addr <= fetched_arg;
                    we <= 0;
                    re <= 1;
                    iorq <= 1;
                    state <= STATE_INSTMEMREF_FETCH;
                end else
                    state <= STATE_DECODE;
            end
            STATE_INSTMEMREF_FETCH: begin
                iorq <= 0;

                if (~extend && fetched_inst == INST_RET && stackwrw) begin
                    fetched_arg <= data;
                    we <= 0;
                    re <= 1;
                    iorq <= 1;
                    stackwrw <= 0;
                    addr <= r_sp_val;
                    state <= STATE_INSTMEMREF_FETCH;
                end else begin
                    fetched_mem <= data;
                    state <= STATE_DECODE;
                end
            end
            STATE_AALU_OPFINISH: begin
                if (inst_base == 4'hd) begin
                    if (inst_top2 == 2'h0) begin
                        reg_load_data <= aaluo;
                        reg_load[inst_mid2] <= 1;
                    end else if (inst_top2 == 2'h1) begin
                        reg_load_data <= aaluo;
                        reg_load[inst_mid2] <= 1;
                    end else if (inst_top2 == 2'h3) begin
                        iorq <= 1;
                        we <= 1;
                        re <= 0;
                        addr <= regval;
                        data_out <= aaluo;
                    end
                end else if (inst_base == 4'hf) begin
                    reg_load_data <= aaluo;
                    reg_load[inst_top2] <= 1;
                end

                state <= inst_nextstate;
            end
            STATE_WAIT_ONE: begin
                state <= STATE_PREFETCH;
            end
        endcase
    end
    prvstate <= state;
end

endmodule
