`timescale 1ns / 1ps

module ssdis(
    input  wire        clk,
    input  wire        en,
    output wire [ 3:0] dig_n,
    output wire [ 7:0] seg_n,
    output wire [ 4:0] col_n,
    input  wire [ 3:0] dp,
    input  wire [ 3:0] seg,
    input  wire [15:0] data
);

reg [11:0] count;
wire cle = en & ~|count;
always @(posedge clk) begin
    if (en) count <= count + 1;
end

reg [1:0] displ;
reg [3:0] num;

always @(*) begin
    case (displ)
        2'b00: num <= data[ 3: 0];
        2'b01: num <= data[ 7: 4];
        2'b10: num <= data[11: 8];
        2'b11: num <= data[15:12];
    endcase
end

always @(posedge clk) begin
    if (cle) displ <= displ + 1;
end

assign dig_n = ~((4'b1 << displ) & seg);
assign col_n = 5'b11111;

ssdec dec(
    .num(num),
    .seg(seg_n),
    .dp(dp[displ])
);

endmodule
